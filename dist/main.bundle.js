webpackJsonp([0,4],{

/***/ 1072:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 492,
	"./af.js": 492,
	"./ar": 499,
	"./ar-dz": 493,
	"./ar-dz.js": 493,
	"./ar-kw": 494,
	"./ar-kw.js": 494,
	"./ar-ly": 495,
	"./ar-ly.js": 495,
	"./ar-ma": 496,
	"./ar-ma.js": 496,
	"./ar-sa": 497,
	"./ar-sa.js": 497,
	"./ar-tn": 498,
	"./ar-tn.js": 498,
	"./ar.js": 499,
	"./az": 500,
	"./az.js": 500,
	"./be": 501,
	"./be.js": 501,
	"./bg": 502,
	"./bg.js": 502,
	"./bn": 503,
	"./bn.js": 503,
	"./bo": 504,
	"./bo.js": 504,
	"./br": 505,
	"./br.js": 505,
	"./bs": 506,
	"./bs.js": 506,
	"./ca": 507,
	"./ca.js": 507,
	"./cs": 508,
	"./cs.js": 508,
	"./cv": 509,
	"./cv.js": 509,
	"./cy": 510,
	"./cy.js": 510,
	"./da": 511,
	"./da.js": 511,
	"./de": 514,
	"./de-at": 512,
	"./de-at.js": 512,
	"./de-ch": 513,
	"./de-ch.js": 513,
	"./de.js": 514,
	"./dv": 515,
	"./dv.js": 515,
	"./el": 516,
	"./el.js": 516,
	"./en-au": 517,
	"./en-au.js": 517,
	"./en-ca": 518,
	"./en-ca.js": 518,
	"./en-gb": 519,
	"./en-gb.js": 519,
	"./en-ie": 520,
	"./en-ie.js": 520,
	"./en-nz": 521,
	"./en-nz.js": 521,
	"./eo": 522,
	"./eo.js": 522,
	"./es": 524,
	"./es-do": 523,
	"./es-do.js": 523,
	"./es.js": 524,
	"./et": 525,
	"./et.js": 525,
	"./eu": 526,
	"./eu.js": 526,
	"./fa": 527,
	"./fa.js": 527,
	"./fi": 528,
	"./fi.js": 528,
	"./fo": 529,
	"./fo.js": 529,
	"./fr": 532,
	"./fr-ca": 530,
	"./fr-ca.js": 530,
	"./fr-ch": 531,
	"./fr-ch.js": 531,
	"./fr.js": 532,
	"./fy": 533,
	"./fy.js": 533,
	"./gd": 534,
	"./gd.js": 534,
	"./gl": 535,
	"./gl.js": 535,
	"./gom-latn": 536,
	"./gom-latn.js": 536,
	"./he": 537,
	"./he.js": 537,
	"./hi": 538,
	"./hi.js": 538,
	"./hr": 539,
	"./hr.js": 539,
	"./hu": 540,
	"./hu.js": 540,
	"./hy-am": 541,
	"./hy-am.js": 541,
	"./id": 542,
	"./id.js": 542,
	"./is": 543,
	"./is.js": 543,
	"./it": 544,
	"./it.js": 544,
	"./ja": 545,
	"./ja.js": 545,
	"./jv": 546,
	"./jv.js": 546,
	"./ka": 547,
	"./ka.js": 547,
	"./kk": 548,
	"./kk.js": 548,
	"./km": 549,
	"./km.js": 549,
	"./kn": 550,
	"./kn.js": 550,
	"./ko": 551,
	"./ko.js": 551,
	"./ky": 552,
	"./ky.js": 552,
	"./lb": 553,
	"./lb.js": 553,
	"./lo": 554,
	"./lo.js": 554,
	"./lt": 555,
	"./lt.js": 555,
	"./lv": 556,
	"./lv.js": 556,
	"./me": 557,
	"./me.js": 557,
	"./mi": 558,
	"./mi.js": 558,
	"./mk": 559,
	"./mk.js": 559,
	"./ml": 560,
	"./ml.js": 560,
	"./mr": 561,
	"./mr.js": 561,
	"./ms": 563,
	"./ms-my": 562,
	"./ms-my.js": 562,
	"./ms.js": 563,
	"./my": 564,
	"./my.js": 564,
	"./nb": 565,
	"./nb.js": 565,
	"./ne": 566,
	"./ne.js": 566,
	"./nl": 568,
	"./nl-be": 567,
	"./nl-be.js": 567,
	"./nl.js": 568,
	"./nn": 569,
	"./nn.js": 569,
	"./pa-in": 570,
	"./pa-in.js": 570,
	"./pl": 571,
	"./pl.js": 571,
	"./pt": 573,
	"./pt-br": 572,
	"./pt-br.js": 572,
	"./pt.js": 573,
	"./ro": 574,
	"./ro.js": 574,
	"./ru": 575,
	"./ru.js": 575,
	"./sd": 576,
	"./sd.js": 576,
	"./se": 577,
	"./se.js": 577,
	"./si": 578,
	"./si.js": 578,
	"./sk": 579,
	"./sk.js": 579,
	"./sl": 580,
	"./sl.js": 580,
	"./sq": 581,
	"./sq.js": 581,
	"./sr": 583,
	"./sr-cyrl": 582,
	"./sr-cyrl.js": 582,
	"./sr.js": 583,
	"./ss": 584,
	"./ss.js": 584,
	"./sv": 585,
	"./sv.js": 585,
	"./sw": 586,
	"./sw.js": 586,
	"./ta": 587,
	"./ta.js": 587,
	"./te": 588,
	"./te.js": 588,
	"./tet": 589,
	"./tet.js": 589,
	"./th": 590,
	"./th.js": 590,
	"./tl-ph": 591,
	"./tl-ph.js": 591,
	"./tlh": 592,
	"./tlh.js": 592,
	"./tr": 593,
	"./tr.js": 593,
	"./tzl": 594,
	"./tzl.js": 594,
	"./tzm": 596,
	"./tzm-latn": 595,
	"./tzm-latn.js": 595,
	"./tzm.js": 596,
	"./uk": 597,
	"./uk.js": 597,
	"./ur": 598,
	"./ur.js": 598,
	"./uz": 600,
	"./uz-latn": 599,
	"./uz-latn.js": 599,
	"./uz.js": 600,
	"./vi": 601,
	"./vi.js": 601,
	"./x-pseudo": 602,
	"./x-pseudo.js": 602,
	"./yo": 603,
	"./yo.js": 603,
	"./zh-cn": 604,
	"./zh-cn.js": 604,
	"./zh-hk": 605,
	"./zh-hk.js": 605,
	"./zh-tw": 606,
	"./zh-tw.js": 606
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 1072;


/***/ }),

/***/ 1171:
/***/ (function(module, exports) {

module.exports = ".loginPage {\n  background: red; }\n  .loginPage .page-header-topbar {\n    display: none; }\n  .loginPage #wrapper #page-sidebar {\n    display: none; }\n    .loginPage #wrapper #page-sidebar #sidebar {\n      display: none;\n      width: 0px; }\n  .loginPage #wrapper #page-wrapper {\n    margin: 0px; }\n"

/***/ }),

/***/ 1172:
/***/ (function(module, exports) {

module.exports = "#addClaim #addChangeClaimStore #rootwizard-custom-circle > .navbar > .navbar-inner > ul > li {\n  margin: 0px;\n  width: 25%; }\n"

/***/ }),

/***/ 1173:
/***/ (function(module, exports) {

module.exports = "#change-info #changeInfoContent .form-horizontal .form-group {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  margin-bottom: 15px; }\n  #change-info #changeInfoContent .form-horizontal .form-group .control-label {\n    padding: 0px;\n    color: black; }\n    #change-info #changeInfoContent .form-horizontal .form-group .control-label span {\n      margin-right: -8px; }\n  #change-info #changeInfoContent .form-horizontal .form-group .form-control[readonly] {\n    background: none;\n    border: none; }\n  #change-info #changeInfoContent .form-horizontal .form-group p {\n    margin: 0px;\n    color: #4e4e4e; }\n\n#change-info #changeInfoContent .form-horizontal .btnAction {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center; }\n  #change-info #changeInfoContent .form-horizontal .btnAction .btn-danger {\n    margin-right: 10px; }\n  #change-info #changeInfoContent .form-horizontal .btnAction .btn-success {\n    background-image: none; }\n\n#change-info #changeInfoContent .action .portlet-body {\n  text-align: center; }\n  #change-info #changeInfoContent .action .portlet-body .btn {\n    min-width: 120px; }\n    #change-info #changeInfoContent .action .portlet-body .btn:first-child {\n      margin: 3px; }\n\n#change-info #changeInfoContent .productOriginal,\n#change-info #changeInfoContent .productChange {\n  text-align: center; }\n  #change-info #changeInfoContent .productOriginal .row,\n  #change-info #changeInfoContent .productChange .row {\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n    #change-info #changeInfoContent .productOriginal .row .iconArrow .fa,\n    #change-info #changeInfoContent .productChange .row .iconArrow .fa {\n      font-size: -webkit-xxx-large; }\n    #change-info #changeInfoContent .productOriginal .row p,\n    #change-info #changeInfoContent .productChange .row p {\n      margin: 0px; }\n\n#change-info #changeInfoContent .inputTypeahead .divInputAhead {\n  padding: 0px; }\n\n#change-info #changeInfoContent .inputTypeahead .divButtonAhead {\n  padding-right: 0px; }\n  #change-info #changeInfoContent .inputTypeahead .divButtonAhead .btn {\n    width: 100%;\n    height: 34px;\n    padding: 0px; }\n\n#change-info #changeInfoContent .no-data {\n  margin-top: -15px;\n  margin-bottom: 10px; }\n\n#change-info #changeInfoContent .status .statusText .statusShow {\n  text-align: center;\n  font-weight: 600; }\n\n#change-info #changeInfoContent .status .statusEdit .form-horizontal .form-group {\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center; }\n  #change-info #changeInfoContent .status .statusEdit .form-horizontal .form-group select.selectCenter {\n    width: 100%; }\n\n#change-info #changeInfoContent .broken .brokenInput .input-group .form-control {\n  background: none; }\n\n#change-info #changeInfoContent .remark .form-horizontal .form-group {\n  -webkit-box-align: start;\n      -ms-flex-align: start;\n          align-items: flex-start; }\n  #change-info #changeInfoContent .remark .form-horizontal .form-group .form-control {\n    padding: 0px 12px; }\n"

/***/ }),

/***/ 1174:
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ 1175:
/***/ (function(module, exports) {

module.exports = "#claimPrint-page .panel {\n  border-radius: 0; }\n  #claimPrint-page .panel .panel-body {\n    padding: 30px; }\n    #claimPrint-page .panel .panel-body .claimPrint-title {\n      text-align: center; }\n      #claimPrint-page .panel .panel-body .claimPrint-title h2 {\n        margin-top: 0;\n        margin-bottom: 0; }\n      #claimPrint-page .panel .panel-body .claimPrint-title p {\n        margin: 0px; }\n    #claimPrint-page .panel .panel-body .logo {\n      font-family: 'Oswald';\n      font-weight: bold;\n      margin-top: 0; }\n    #claimPrint-page .panel .panel-body hr {\n      margin-bottom: 10px;\n      margin-top: 0px;\n      margin-left: -20px;\n      margin-right: -20px; }\n    #claimPrint-page .panel .panel-body .table {\n      table-layout: fixed;\n      width: 100%;\n      max-width: 100%;\n      margin: 0px; }\n      #claimPrint-page .panel .panel-body .table td {\n        border-top: none;\n        padding: 8px;\n        line-height: 1.42857143;\n        vertical-align: top; }\n        #claimPrint-page .panel .panel-body .table td .header {\n          margin: 0px;\n          font-weight: 600; }\n    #claimPrint-page .panel .panel-body .table.table-claim-info {\n      margin: 0px;\n      margin-top: 18px; }\n      #claimPrint-page .panel .panel-body .table.table-claim-info tr td address {\n        margin-bottom: 0px;\n        font-style: normal;\n        line-height: 1.42857143; }\n    #claimPrint-page .panel .panel-body .table.table-product tr td {\n      padding: 8px 0px; }\n      #claimPrint-page .panel .panel-body .table.table-product tr td .imei {\n        padding: 3px 6px;\n        border: 1px solid #ddd;\n        margin-right: 5px; }\n      #claimPrint-page .panel .panel-body .table.table-product tr td .nopadding {\n        padding: 0px; }\n    #claimPrint-page .panel .panel-body .table.table-product tr .table-list p {\n      margin: 10px 0px; }\n    #claimPrint-page .panel .panel-body .table.table-product tr .cost {\n      padding-top: 30px;\n      width: 20%; }\n      #claimPrint-page .panel .panel-body .table.table-product tr .cost p {\n        margin: 10px 0px; }\n      #claimPrint-page .panel .panel-body .table.table-product tr .cost .textextra {\n        word-wrap: break-word;\n        overflow: hidden;\n        text-overflow: ellipsis; }\n    #claimPrint-page .panel .panel-body .table.table-product tr .table-option thead tr td {\n      text-align: center; }\n    #claimPrint-page .panel .panel-body .table.table-product tr .table-option tbody tr td {\n      width: 33%;\n      line-height: 25px; }\n    #claimPrint-page .panel .panel-body .table.table-product tr .table-option tbody tr .hasIcon {\n      text-align: center; }\n      #claimPrint-page .panel .panel-body .table.table-product tr .table-option tbody tr .hasIcon .icon {\n        font-size: 25px; }\n    #claimPrint-page .panel .panel-body .table.table-signature .signatureHeader {\n      text-align: center;\n      font-weight: bolder; }\n    #claimPrint-page .panel .panel-body .table.table-signature .signature {\n      text-align: center;\n      height: 20px; }\n      #claimPrint-page .panel .panel-body .table.table-signature .signature td {\n        padding: 0px; }\n      #claimPrint-page .panel .panel-body .table.table-signature .signature .borderShow {\n        border-bottom: 1px solid #ddd; }\n    #claimPrint-page .panel .panel-body .table.table-signature .signatureDate {\n      text-align: center;\n      height: 25px; }\n      #claimPrint-page .panel .panel-body .table.table-signature .signatureDate td {\n        padding: 0px;\n        padding-top: 10px;\n        position: relative; }\n        #claimPrint-page .panel .panel-body .table.table-signature .signatureDate td p {\n          margin: 0px;\n          display: -webkit-box;\n          display: -ms-flexbox;\n          display: flex;\n          -webkit-box-align: center;\n              -ms-flex-align: center;\n                  align-items: center;\n          -webkit-box-pack: end;\n              -ms-flex-pack: end;\n                  justify-content: flex-end;\n          vertical-align: text-bottom;\n          position: absolute;\n          right: 5px;\n          bottom: -5px; }\n      #claimPrint-page .panel .panel-body .table.table-signature .signatureDate .borderShow {\n        border-bottom: 1px solid #ddd; }\n    #claimPrint-page .panel .panel-body .table.table-signature .signatureSpace {\n      height: 25px; }\n"

/***/ }),

/***/ 1176:
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ 1177:
/***/ (function(module, exports) {

module.exports = "#change-info #changeInfoContent .form-horizontal .form-group {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  margin-bottom: 15px; }\n  #change-info #changeInfoContent .form-horizontal .form-group .control-label {\n    padding: 0px;\n    color: black; }\n    #change-info #changeInfoContent .form-horizontal .form-group .control-label span {\n      margin-right: -8px; }\n  #change-info #changeInfoContent .form-horizontal .form-group .form-control[readonly] {\n    background: none;\n    border: none; }\n  #change-info #changeInfoContent .form-horizontal .form-group p {\n    margin: 0px;\n    color: #4e4e4e; }\n\n#change-info #changeInfoContent .form-horizontal .btnAction {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center; }\n  #change-info #changeInfoContent .form-horizontal .btnAction .btn-danger {\n    margin-right: 10px; }\n  #change-info #changeInfoContent .form-horizontal .btnAction .btn-success {\n    background-image: none; }\n\n#change-info #changeInfoContent .action .portlet-body {\n  text-align: center; }\n  #change-info #changeInfoContent .action .portlet-body .btn {\n    min-width: 120px; }\n    #change-info #changeInfoContent .action .portlet-body .btn:first-child {\n      margin-bottom: 5px; }\n\n#change-info #changeInfoContent .productOriginal .row,\n#change-info #changeInfoContent .productChange .row {\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center; }\n  #change-info #changeInfoContent .productOriginal .row .iconArrow .fa,\n  #change-info #changeInfoContent .productChange .row .iconArrow .fa {\n    font-size: -webkit-xxx-large; }\n  #change-info #changeInfoContent .productOriginal .row p,\n  #change-info #changeInfoContent .productChange .row p {\n    margin: 0px;\n    padding: 6px 12px; }\n\n#change-info #changeInfoContent .inputTypeahead .divInputAhead {\n  padding: 0px; }\n\n#change-info #changeInfoContent .inputTypeahead .divButtonAhead {\n  padding-right: 0px; }\n  #change-info #changeInfoContent .inputTypeahead .divButtonAhead .btn {\n    width: 100%;\n    height: 34px;\n    padding: 0px; }\n\n#change-info #changeInfoContent .no-data {\n  margin-top: -15px;\n  margin-bottom: 10px; }\n\n#change-info #changeInfoContent .status .statusText .statusShow {\n  text-align: center;\n  font-weight: 600; }\n\n#change-info #changeInfoContent .status .statusEdit .form-horizontal .form-group {\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center; }\n  #change-info #changeInfoContent .status .statusEdit .form-horizontal .form-group select.selectCenter {\n    width: 100%; }\n\n#change-info #changeInfoContent .broken .brokenInput .input-group .form-control,\n#change-info #changeInfoContent .fix .brokenInput .input-group .form-control,\n#change-info #changeInfoContent .agreement .brokenInput .input-group .form-control {\n  background: none; }\n\n#change-info #changeInfoContent .broken .extra,\n#change-info #changeInfoContent .fix .extra,\n#change-info #changeInfoContent .agreement .extra {\n  margin-top: 15px; }\n\n#change-info #changeInfoContent .extra .form-horizontal .form-group {\n  -webkit-box-align: start;\n      -ms-flex-align: start;\n          align-items: flex-start; }\n"

/***/ }),

/***/ 1178:
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ 1179:
/***/ (function(module, exports) {

module.exports = "#claimPrint-page .panel {\n  border-radius: 0; }\n  #claimPrint-page .panel .panel-body {\n    padding: 30px; }\n    #claimPrint-page .panel .panel-body .claimPrint-title {\n      text-align: center; }\n      #claimPrint-page .panel .panel-body .claimPrint-title h2 {\n        margin-top: 0;\n        margin-bottom: 0; }\n      #claimPrint-page .panel .panel-body .claimPrint-title p {\n        margin: 0px; }\n    #claimPrint-page .panel .panel-body .logo {\n      font-family: 'Oswald';\n      font-weight: bold;\n      margin-top: 0; }\n    #claimPrint-page .panel .panel-body hr {\n      margin-bottom: 10px;\n      margin-top: 0px;\n      margin-left: -20px;\n      margin-right: -20px; }\n    #claimPrint-page .panel .panel-body .table {\n      table-layout: fixed;\n      width: 100%;\n      max-width: 100%;\n      margin: 0px; }\n      #claimPrint-page .panel .panel-body .table td {\n        border-top: none;\n        padding: 8px;\n        line-height: 1.42857143;\n        vertical-align: top; }\n        #claimPrint-page .panel .panel-body .table td .header {\n          margin: 0px;\n          font-weight: 600; }\n    #claimPrint-page .panel .panel-body .table.table-claim-info {\n      margin: 0px;\n      margin-top: 18px; }\n      #claimPrint-page .panel .panel-body .table.table-claim-info tr td address {\n        margin-bottom: 0px;\n        font-style: normal;\n        line-height: 1.42857143; }\n    #claimPrint-page .panel .panel-body .table.table-product tr td {\n      padding: 8px 0px; }\n      #claimPrint-page .panel .panel-body .table.table-product tr td .imei {\n        padding: 3px 6px;\n        border: 1px solid #ddd;\n        margin-right: 5px; }\n      #claimPrint-page .panel .panel-body .table.table-product tr td .nopadding {\n        padding: 0px; }\n    #claimPrint-page .panel .panel-body .table.table-product tr .table-list p {\n      margin: 10px 0px; }\n    #claimPrint-page .panel .panel-body .table.table-product tr .cost {\n      padding-top: 30px;\n      width: 20%; }\n      #claimPrint-page .panel .panel-body .table.table-product tr .cost p {\n        margin: 10px 0px; }\n      #claimPrint-page .panel .panel-body .table.table-product tr .cost .textextra {\n        word-wrap: break-word;\n        overflow: hidden;\n        text-overflow: ellipsis; }\n    #claimPrint-page .panel .panel-body .table.table-product tr .table-option thead tr td {\n      text-align: center; }\n    #claimPrint-page .panel .panel-body .table.table-product tr .table-option tbody tr td {\n      width: 33%;\n      line-height: 25px; }\n    #claimPrint-page .panel .panel-body .table.table-product tr .table-option tbody tr .hasIcon {\n      text-align: center; }\n      #claimPrint-page .panel .panel-body .table.table-product tr .table-option tbody tr .hasIcon .icon {\n        font-size: 25px; }\n    #claimPrint-page .panel .panel-body .table.table-product .borderDiv {\n      border-bottom: 1px solid #eee; }\n    #claimPrint-page .panel .panel-body .table.table-signature .signatureHeader {\n      text-align: center;\n      font-weight: bolder; }\n    #claimPrint-page .panel .panel-body .table.table-signature .signature {\n      text-align: center;\n      height: 20px; }\n      #claimPrint-page .panel .panel-body .table.table-signature .signature td {\n        padding: 0px; }\n      #claimPrint-page .panel .panel-body .table.table-signature .signature .borderShow {\n        border-bottom: 1px solid #ddd; }\n    #claimPrint-page .panel .panel-body .table.table-signature .signatureDate {\n      text-align: center;\n      height: 25px; }\n      #claimPrint-page .panel .panel-body .table.table-signature .signatureDate td {\n        padding: 0px;\n        padding-top: 10px;\n        position: relative; }\n        #claimPrint-page .panel .panel-body .table.table-signature .signatureDate td p {\n          margin: 0px;\n          display: -webkit-box;\n          display: -ms-flexbox;\n          display: flex;\n          -webkit-box-align: center;\n              -ms-flex-align: center;\n                  align-items: center;\n          -webkit-box-pack: end;\n              -ms-flex-pack: end;\n                  justify-content: flex-end;\n          vertical-align: text-bottom;\n          position: absolute;\n          right: 5px;\n          bottom: -5px; }\n      #claimPrint-page .panel .panel-body .table.table-signature .signatureDate .borderShow {\n        border-bottom: 1px solid #ddd; }\n    #claimPrint-page .panel .panel-body .table.table-signature .signatureSpace {\n      height: 25px; }\n"

/***/ }),

/***/ 1180:
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ 1181:
/***/ (function(module, exports) {

module.exports = "#customer .page {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center; }\n\n#customer .table thead tr th {\n  text-align: center;\n  font-weight: 600; }\n\n#customer .table tbody tr {\n  vertical-align: middle; }\n\n#customer .table tbody td {\n  vertical-align: middle; }\n  #customer .table tbody td div {\n    text-align: center; }\n    #customer .table tbody td div a {\n      text-shadow: none; }\n  #customer .table tbody td:first-child, #customer .table tbody td:last-child {\n    text-align: center; }\n\n#customer #addCustomer .btn {\n  box-shadow: none;\n  background-image: none;\n  text-shadow: none; }\n\n@media screen and (min-width: 769px) {\n  #modalConfirmDelete {\n    width: 598px;\n    margin: 0 auto; }\n    #modalConfirmDelete .btn-primary {\n      background-image: none; }\n    #modalConfirmDelete .btn {\n      box-shadow: none;\n      background-image: none;\n      text-shadow: none; } }\n"

/***/ }),

/***/ 1182:
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ 1183:
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ 1184:
/***/ (function(module, exports) {

module.exports = "@media only screen and (max-width: 768px) {\n  #topbar {\n    height: auto; }\n    #topbar .navbar-header {\n      transition: width 200ms linear;\n      width: 100%; }\n      #topbar .navbar-header #logo {\n        width: auto;\n        padding-left: 20px; }\n        #topbar .navbar-header #logo span.logo-text,\n        #topbar .navbar-header #logo .logo-text-icon {\n          font-family: 'Oswald', sans-serif; }\n        #topbar .navbar-header #logo .logo-text-icon {\n          display: none; }\n    #topbar .topbar-main {\n      line-height: 19px; }\n      #topbar .topbar-main .nav {\n        margin: 0px; }\n        #topbar .topbar-main .nav .topbar-user {\n          height: 50px;\n          margin-top: -1px;\n          margin: 0px 10px;\n          padding-left: 10px;\n          width: 44%; }\n          #topbar .topbar-main .nav .topbar-user .dropdown-toggle {\n            display: -webkit-box;\n            display: -ms-flexbox;\n            display: flex;\n            -webkit-box-align: center;\n                -ms-flex-align: center;\n                    align-items: center; }\n        #topbar .topbar-main .nav .topbar-logout {\n          text-align: right; } }\n\n@media only screen and (min-width: 768px) {\n  #topbar .navbar-header {\n    transition: width 200ms linear; }\n    #topbar .navbar-header #logo span.logo-text,\n    #topbar .navbar-header #logo .logo-text-icon {\n      font-family: 'Oswald', sans-serif; }\n    #topbar .navbar-header #logo .logo-text-icon {\n      display: none; }\n  #topbar .topbar-main {\n    line-height: 19px; }\n    #topbar .topbar-main .nav {\n      margin: 0px; }\n      #topbar .topbar-main .nav .topbar-user {\n        height: 50px;\n        margin-top: -1px;\n        margin: 0px 10px; }\n        #topbar .topbar-main .nav .topbar-user .dropdown-toggle {\n          display: -webkit-box;\n          display: -ms-flexbox;\n          display: flex;\n          -webkit-box-align: center;\n              -ms-flex-align: center;\n                  align-items: center; } }\n\n.headerP {\n  line-height: 50px;\n  margin: 0px;\n  color: #fff; }\n"

/***/ }),

/***/ 1185:
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ 1186:
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ 1187:
/***/ (function(module, exports) {

module.exports = ".loginBody {\n  padding: 5%;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  background: url(\"assets/images/bg/1.jpg\");\n  height: 100vh;\n  margin-left: -50px !important; }\n  .loginBody .page-form {\n    margin: 0px;\n    background: none; }\n    .loginBody .page-form .form {\n      background: #eeeeee; }\n  .loginBody .load {\n    width: 100%;\n    height: 100%;\n    position: absolute;\n    top: 20px;\n    left: 15px; }\n"

/***/ }),

/***/ 1188:
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ 1189:
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ 1190:
/***/ (function(module, exports) {

module.exports = "#productType .page {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center; }\n\n#productType .table thead tr th {\n  text-align: center;\n  font-weight: 600; }\n\n#productType .table tbody td .btn-group {\n  width: 100%; }\n  #productType .table tbody td .btn-group a {\n    text-shadow: none;\n    width: 49%; }\n    #productType .table tbody td .btn-group a:first-child {\n      margin-right: 2%; }\n\n#productType .table tbody td:first-child {\n  text-align: center; }\n\n#productType #addProductType .btn {\n  box-shadow: none;\n  background-image: none;\n  text-shadow: none; }\n\n@media screen and (min-width: 769px) {\n  #modalConfirmDelete {\n    width: 598px;\n    margin: 0 auto; }\n    #modalConfirmDelete .btn-primary {\n      background-image: none; }\n    #modalConfirmDelete .btn {\n      box-shadow: none;\n      background-image: none;\n      text-shadow: none; } }\n"

/***/ }),

/***/ 1191:
/***/ (function(module, exports) {

module.exports = "#acceptProduct #acceptProductTabContent .remark {\n  max-width: 200px; }\n  #acceptProduct #acceptProductTabContent .remark p {\n    word-wrap: break-word; }\n"

/***/ }),

/***/ 1192:
/***/ (function(module, exports) {

module.exports = "#addAcceptProduct #addAcceptProductTabContent .form-body .form-group .input-group {\n  padding-left: 15px;\n  padding-right: 15px; }\n  #addAcceptProduct #addAcceptProductTabContent .form-body .form-group .input-group .d-inline-block table tbody tr td .btn {\n    background-color: transparent;\n    font-size: 15px;\n    border: 1px solid red;\n    margin: 5px 10px;\n    border-radius: 5px !important;\n    padding: 5px 10px; }\n  #addAcceptProduct #addAcceptProductTabContent .form-body .form-group .input-group .d-inline-block table tbody tr td .custom-select {\n    font-size: 15px;\n    width: auto; }\n    #addAcceptProduct #addAcceptProductTabContent .form-body .form-group .input-group .d-inline-block table tbody tr td .custom-select::first-child {\n      margin-right: 5px; }\n"

/***/ }),

/***/ 1193:
/***/ (function(module, exports) {

module.exports = "#productInfo #sum_box {\n  margin-bottom: 15px; }\n  #productInfo #sum_box .panel .panel-body .value {\n    margin-top: 0px;\n    font-size: 20px;\n    margin-bottom: 2px;\n    padding-bottom: 0px;\n    text-align: left;\n    line-height: 30px; }\n  #productInfo #sum_box .panel .panel-body p {\n    font-size: 16px;\n    line-height: 1;\n    margin: 0; }\n  #productInfo #sum_box .green .icon,\n  #productInfo #sum_box .green .value {\n    color: #5CB860; }\n  #productInfo #sum_box .green:hover {\n    background: #5CB860;\n    color: #fff; }\n    #productInfo #sum_box .green:hover .icon,\n    #productInfo #sum_box .green:hover .value {\n      color: #fff; }\n  #productInfo #sum_box .blue .icon,\n  #productInfo #sum_box .blue .value {\n    color: #5BC0E7; }\n  #productInfo #sum_box .blue:hover {\n    background: #5BC0E7;\n    color: #fff; }\n    #productInfo #sum_box .blue:hover .icon,\n    #productInfo #sum_box .blue:hover .value {\n      color: #fff; }\n  #productInfo #sum_box .red .icon,\n  #productInfo #sum_box .red .value {\n    color: #d9534f; }\n  #productInfo #sum_box .red:hover {\n    background: #d9534f;\n    color: #fff; }\n    #productInfo #sum_box .red:hover .icon,\n    #productInfo #sum_box .red:hover .value {\n      color: #fff; }\n  #productInfo #sum_box .yellow .icon,\n  #productInfo #sum_box .yellow .value {\n    color: #f08c33; }\n  #productInfo #sum_box .yellow:hover {\n    background: #f08c33;\n    color: #fff; }\n    #productInfo #sum_box .yellow:hover .icon,\n    #productInfo #sum_box .yellow:hover .value {\n      color: #fff; }\n\n#productInfo #productInfoContent .tab-content .title {\n  margin: 0px; }\n\n#productInfo #productInfoContent .tab-content .detail .row {\n  -webkit-box-align: start;\n      -ms-flex-align: start;\n          align-items: flex-start;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex; }\n  #productInfo #productInfoContent .tab-content .detail .row:last-child {\n    padding-top: 15px;\n    text-align: center;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center; }\n    #productInfo #productInfoContent .tab-content .detail .row:last-child .btn {\n      text-shadow: none; }\n      #productInfo #productInfoContent .tab-content .detail .row:last-child .btn:first-child {\n        margin-right: 10px; }\n\n#productInfo #productInfoContent .tab-content .detail .nameDatail {\n  text-align: right;\n  color: #d94e37; }\n\n#productInfo #productInfoContent .tab-content .detail .contentDetail {\n  text-align: left;\n  margin: 0px;\n  margin-top: 10px;\n  font-size: 15px; }\n\n#productInfo #productInfoContent .tab-content .detail .table thead tr th {\n  text-align: center; }\n\n#productInfo #productInfoContent .tab-content .detail .table tbody tr td {\n  text-align: center; }\n"

/***/ }),

/***/ 1194:
/***/ (function(module, exports) {

module.exports = "#addProduct .form-body .form-group .pbm {\n  padding-bottom: 0px !important;\n  padding-top: 5px; }\n  #addProduct .form-body .form-group .pbm .icheckbox_flat-red {\n    position: relative;\n    display: inline-block;\n    vertical-align: middle;\n    margin: 0;\n    padding: 0;\n    width: 20px;\n    height: 20px;\n    background: url(\"assets/images/red.png\") no-repeat;\n    border: none;\n    cursor: pointer; }\n    #addProduct .form-body .form-group .pbm .icheckbox_flat-red input[type=\"checkbox\"] {\n      position: absolute;\n      opacity: 0;\n      width: 100%;\n      height: 100%;\n      padding: 0;\n      margin: 0; }\n  #addProduct .form-body .form-group .pbm .icheckbox_flat-red.checked {\n    background-position: -22px 0; }\n\n#addProduct .form-body .form-group .selectProductType {\n  width: 56%; }\n\n#addProduct .form-body .form-group .addProductTypeBtn {\n  width: 10%;\n  padding: 0;\n  padding-right: 8px; }\n  #addProduct .form-body .form-group .addProductTypeBtn .btn {\n    width: 100%; }\n\n#addProduct .form-body .form-group .addCode {\n  margin-top: 10px; }\n\n#addProduct .form-body .form-group .codesRow {\n  margin: 0px; }\n  #addProduct .form-body .form-group .codesRow .codesDiv {\n    padding: 0px;\n    padding-top: 5px;\n    padding-right: 5px; }\n\n#addProduct .form-body .form-group .radioButtonCustom.clearMargin label {\n  margin-top: 0px;\n  padding-top: 0px; }\n\n#addProduct .form-body .form-group .radioButtonCustom label {\n  height: 34px;\n  padding-top: 5px;\n  margin-top: 10px; }\n\n#addProduct .form-body .form-group .selectCenter {\n  padding-right: 0px;\n  height: 34px; }\n\n#addProduct .btn {\n  box-shadow: none;\n  background-image: none;\n  text-shadow: none; }\n"

/***/ }),

/***/ 1195:
/***/ (function(module, exports) {

module.exports = "#manageProduct .page {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center; }\n\n#manageProduct #manageProductTabContent .actionWhenSelect {\n  position: absolute;\n  z-index: 3; }\n  #manageProduct #manageProductTabContent .actionWhenSelect .btn {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    text-align: center; }\n    #manageProduct #manageProductTabContent .actionWhenSelect .btn i {\n      margin-right: 10px; }\n\n#manageProduct #manageProductTabContent .radioButtonCustom label.btn {\n  width: 20px;\n  padding: 0px;\n  text-align: center; }\n\n#manageProduct .contentLeft {\n  text-align: left;\n  padding-left: 15px; }\n\n.modalEditProduct .addProductTypeBtn {\n  width: 16%;\n  padding: 0;\n  padding-right: 8px; }\n  .modalEditProduct .addProductTypeBtn .btn {\n    width: 100%; }\n"

/***/ }),

/***/ 1196:
/***/ (function(module, exports) {

module.exports = "#barcodePrint #print-section .panel-body .row {\n  margin: 0px; }\n  #barcodePrint #print-section .panel-body .row .eachBarcode {\n    border: 1px solid;\n    text-align: center;\n    margin-left: -1px;\n    margin-bottom: -1px;\n    width: 20%;\n    padding: 10px 15px 5px 15px; }\n    #barcodePrint #print-section .panel-body .row .eachBarcode img {\n      padding: 0px;\n      height: 25px;\n      margin-bottom: 5px; }\n    #barcodePrint #print-section .panel-body .row .eachBarcode p {\n      font-size: x-small;\n      color: black;\n      margin: 0px;\n      line-height: 10px; }\n\n#barcodePrint .barcodeForm .panel-body {\n  padding: 0px; }\n  #barcodePrint .barcodeForm .panel-body .list-group {\n    box-shadow: none; }\n    #barcodePrint .barcodeForm .panel-body .list-group .list-group-item {\n      border: none;\n      border-bottom: 1px solid #e5e5e5;\n      margin-bottom: 0px; }\n      #barcodePrint .barcodeForm .panel-body .list-group .list-group-item .fa-close {\n        position: absolute;\n        right: 5px;\n        top: 5px; }\n  #barcodePrint .barcodeForm .panel-body .actionBtn {\n    margin: 10px 0px;\n    text-align: center; }\n"

/***/ }),

/***/ 1197:
/***/ (function(module, exports) {

module.exports = "#addSellProduct #addSellProductTabContent .form-group .pbm {\n  padding-bottom: 0px !important;\n  padding-top: 5px; }\n  #addSellProduct #addSellProductTabContent .form-group .pbm .icheckbox_flat-red {\n    position: relative;\n    display: inline-block;\n    vertical-align: middle;\n    margin: 0;\n    padding: 0;\n    width: 20px;\n    height: 20px;\n    background: url(\"assets/images/red.png\") no-repeat;\n    border: none;\n    cursor: pointer; }\n    #addSellProduct #addSellProductTabContent .form-group .pbm .icheckbox_flat-red input[type=\"checkbox\"] {\n      position: absolute;\n      opacity: 0;\n      width: 100%;\n      height: 100%;\n      padding: 0;\n      margin: 0; }\n  #addSellProduct #addSellProductTabContent .form-group .pbm .icheckbox_flat-red.checked {\n    background-position: -22px 0; }\n\n#addSellProduct #addSellProductTabContent .form-group .addCustomerInput {\n  margin-right: 0px; }\n\n#addSellProduct #addSellProductTabContent .form-group .addCustomerBtn {\n  padding: 0px;\n  padding-right: 5px;\n  margin-right: 0px;\n  margin-left: -10px; }\n  #addSellProduct #addSellProductTabContent .form-group .addCustomerBtn .btn {\n    width: 100%; }\n\n#addSellProduct #addSellProductTabContent .form-group .no-data {\n  margin-top: 10px; }\n\n@media screen and (min-width: 769px) {\n  #modalConfirmDelete {\n    width: 598px;\n    margin: 0 auto; }\n    #modalConfirmDelete .btn-primary {\n      background-image: none; }\n    #modalConfirmDelete .btn {\n      box-shadow: none;\n      background-image: none;\n      text-shadow: none; } }\n"

/***/ }),

/***/ 1198:
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ 1199:
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ 1200:
/***/ (function(module, exports) {

module.exports = "#transferProduct #transferProductTabContent .table thead tr th {\n  text-align: center;\n  font-weight: 600; }\n\n#transferProduct #transferProductTabContent .table tbody td .btn-group {\n  width: 100%; }\n  #transferProduct #transferProductTabContent .table tbody td .btn-group a {\n    text-shadow: none; }\n\n#transferProduct #transferProductTabContent .table tbody td:first-child {\n  text-align: center; }\n"

/***/ }),

/***/ 1201:
/***/ (function(module, exports) {

module.exports = "#reportTabContent .btnGroupSelect {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  margin-bottom: 15px; }\n"

/***/ }),

/***/ 1202:
/***/ (function(module, exports) {

module.exports = ".portlet {\n  border-color: #777777; }\n  .portlet .portlet-header {\n    background: #777777;\n    color: #ffffff;\n    vertical-align: middle;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: start;\n        -ms-flex-pack: start;\n            justify-content: flex-start;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n    .portlet .portlet-header .caption {\n      line-height: inherit;\n      width: 80%; }\n    .portlet .portlet-header .tools {\n      width: 20%; }\n      .portlet .portlet-header .tools select {\n        float: right; }\n  .portlet .portlet-body {\n    border: 1px solid #777777;\n    border-top: 0px; }\n    .portlet .portlet-body .datatable-simple {\n      float: inherit; }\n      .portlet .portlet-body .datatable-simple .contentCenter {\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-align: center;\n            -ms-flex-align: center;\n                align-items: center; }\n    .portlet .portlet-body .form-horizontal .form-body .form-actions {\n      padding: 0px; }\n    .portlet .portlet-body .form-horizontal .btnAction {\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-pack: center;\n          -ms-flex-pack: center;\n              justify-content: center; }\n      .portlet .portlet-body .form-horizontal .btnAction .btn:first-child {\n        margin-right: 10px; }\n    .portlet .portlet-body .btnGroupSelect {\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-pack: center;\n          -ms-flex-pack: center;\n              justify-content: center;\n      margin-bottom: 15px; }\n\n.totalCost {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  vertical-align: middle;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center; }\n  .totalCost table {\n    width: 100%; }\n    .totalCost table tr td {\n      text-align: end;\n      padding: 10px; }\n      .totalCost table tr td .title {\n        width: 40%; }\n      .totalCost table tr td:last-child {\n        text-align: left;\n        width: 35%;\n        padding-left: 10px; }\n      .totalCost table tr td h3,\n      .totalCost table tr td h4 {\n        margin: 0px;\n        padding: 0px; }\n    .totalCost table tr:last-child {\n      color: #d94e37; }\n      .totalCost table tr:last-child td h3 {\n        font-weight: 500; }\n    .totalCost table .title {\n      width: auto;\n      text-align: end;\n      padding-right: 10px; }\n\n.general_expenses .title {\n  margin: 0px; }\n\n.general_expenses .table th {\n  text-align: center; }\n\n.general_expenses .table tbody tr td {\n  text-align: center; }\n"

/***/ }),

/***/ 1203:
/***/ (function(module, exports) {

module.exports = ".portlet {\n  border-color: #777777; }\n  .portlet .portlet-header {\n    background: #777777;\n    color: #ffffff;\n    vertical-align: middle;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: start;\n        -ms-flex-pack: start;\n            justify-content: flex-start;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n    .portlet .portlet-header .caption {\n      line-height: inherit;\n      width: 80%; }\n    .portlet .portlet-header .tools {\n      width: 20%; }\n      .portlet .portlet-header .tools select {\n        float: right; }\n  .portlet .portlet-body {\n    border: 1px solid #777777;\n    border-top: 0px; }\n    .portlet .portlet-body .datatable-simple {\n      float: inherit; }\n      .portlet .portlet-body .datatable-simple .contentCenter {\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-align: center;\n            -ms-flex-align: center;\n                align-items: center; }\n    .portlet .portlet-body .form-horizontal .form-body .form-actions {\n      padding: 0px; }\n    .portlet .portlet-body .form-horizontal .btnAction {\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-pack: center;\n          -ms-flex-pack: center;\n              justify-content: center; }\n      .portlet .portlet-body .form-horizontal .btnAction .btn:first-child {\n        margin-right: 10px; }\n    .portlet .portlet-body .btnGroupSelect {\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-pack: center;\n          -ms-flex-pack: center;\n              justify-content: center;\n      margin-bottom: 15px; }\n\n.totalCost {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  vertical-align: middle;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center; }\n  .totalCost table {\n    width: 100%; }\n    .totalCost table tr td {\n      text-align: end;\n      padding: 10px; }\n      .totalCost table tr td .title {\n        width: 40%; }\n      .totalCost table tr td:last-child {\n        text-align: left;\n        width: 35%;\n        padding-left: 10px; }\n      .totalCost table tr td h3,\n      .totalCost table tr td h4 {\n        margin: 0px;\n        padding: 0px; }\n    .totalCost table tr:last-child {\n      color: #d94e37; }\n      .totalCost table tr:last-child td h3 {\n        font-weight: 500; }\n    .totalCost table .title {\n      width: auto;\n      text-align: end;\n      padding-right: 10px; }\n\n.general_expenses .title {\n  margin: 0px; }\n\n.general_expenses .table th {\n  text-align: center; }\n\n.general_expenses .table tbody tr td {\n  text-align: center; }\n"

/***/ }),

/***/ 1204:
/***/ (function(module, exports) {

module.exports = "#report #reportTabContent .portlet {\n  border-color: #777777; }\n  #report #reportTabContent .portlet .portlet-header {\n    background: #777777;\n    color: #ffffff;\n    vertical-align: middle;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-pack: start;\n        -ms-flex-pack: start;\n            justify-content: flex-start;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center; }\n    #report #reportTabContent .portlet .portlet-header .caption {\n      line-height: inherit;\n      width: 80%; }\n    #report #reportTabContent .portlet .portlet-header .tools {\n      width: 20%; }\n      #report #reportTabContent .portlet .portlet-header .tools select {\n        float: right; }\n  #report #reportTabContent .portlet .portlet-body {\n    border: 1px solid #777777;\n    border-top: 0px; }\n    #report #reportTabContent .portlet .portlet-body .datatable-simple {\n      float: inherit; }\n      #report #reportTabContent .portlet .portlet-body .datatable-simple .contentCenter {\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-align: center;\n            -ms-flex-align: center;\n                align-items: center; }\n    #report #reportTabContent .portlet .portlet-body .form-horizontal .form-body .form-actions {\n      padding: 0px; }\n    #report #reportTabContent .portlet .portlet-body .form-horizontal .btnAction {\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-pack: center;\n          -ms-flex-pack: center;\n              justify-content: center; }\n      #report #reportTabContent .portlet .portlet-body .form-horizontal .btnAction .btn:first-child {\n        margin-right: 10px; }\n\n#report #reportTabContent .totalCost {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  vertical-align: middle;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center; }\n  #report #reportTabContent .totalCost .detail {\n    width: 100%; }\n  #report #reportTabContent .totalCost table {\n    width: 100%; }\n    #report #reportTabContent .totalCost table tr .title {\n      min-width: 30%;\n      padding-left: 0px; }\n    #report #reportTabContent .totalCost table tr td {\n      text-align: end;\n      padding: 10px; }\n      #report #reportTabContent .totalCost table tr td:last-child {\n        text-align: left;\n        padding-left: 10px; }\n      #report #reportTabContent .totalCost table tr td h3,\n      #report #reportTabContent .totalCost table tr td h4 {\n        margin: 0px;\n        padding: 0px; }\n    #report #reportTabContent .totalCost table tr:last-child {\n      color: #d94e37; }\n      #report #reportTabContent .totalCost table tr:last-child td h3 {\n        font-weight: 500; }\n    #report #reportTabContent .totalCost table .title {\n      width: auto;\n      text-align: end;\n      padding-right: 10px; }\n\n#report #reportTabContent .general_expenses .title {\n  margin: 0px; }\n\n#report #reportTabContent .general_expenses .table {\n  margin-bottom: 10px; }\n  #report #reportTabContent .general_expenses .table thead {\n    border-bottom: 2px solid #ccc; }\n    #report #reportTabContent .general_expenses .table thead th {\n      text-align: center; }\n  #report #reportTabContent .general_expenses .table tbody tr td {\n    text-align: center; }\n  #report #reportTabContent .general_expenses .table tfoot td {\n    border-top: 2px solid #ccc; }\n  #report #reportTabContent .general_expenses .table tfoot .sum_title {\n    text-align: right;\n    font-weight: 600;\n    color: #d94e37;\n    font-size: 20px; }\n  #report #reportTabContent .general_expenses .table tfoot .sum_value {\n    text-align: center;\n    font-weight: 600;\n    color: #d94e37;\n    font-size: 20px; }\n\n#report #reportTabContent p.noContent {\n  text-align: center; }\n\n#report #reportTabContent .btnGroupSelect {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  margin: 0px 5px 10px 5px; }\n\n#report #reportTabContent .selectMonth .row {\n  margin-bottom: 10px; }\n\n#report #reportTabContent .selectMonth .btnAction {\n  text-align: center; }\n"

/***/ }),

/***/ 1205:
/***/ (function(module, exports) {

module.exports = "#sidebar {\n  height: 100%;\n  border-radius: 0px; }\n  #sidebar .sidebar-collapse {\n    background-color: #322f2b; }\n    #sidebar .sidebar-collapse #side-menu li a .menu-title {\n      font-size: 16px;\n      font-style: normal;\n      font-weight: 200; }\n    #sidebar .sidebar-collapse #side-menu li a .icon-collapse {\n      float: right;\n      margin-top: 3px; }\n    #sidebar .sidebar-collapse #side-menu > li::first-child {\n      border: none;\n      height: 0px; }\n    #sidebar .sidebar-collapse #side-menu .rotateIcon {\n      -webkit-transform: rotate(270deg);\n              transform: rotate(270deg); }\n    #sidebar .sidebar-collapse .nav-second-level li a {\n      font-size: 15px;\n      padding-left: 30px; }\n"

/***/ }),

/***/ 1206:
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ 1207:
/***/ (function(module, exports) {

module.exports = "#store .page {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-pack: center;\n      -ms-flex-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center; }\n\n#store .table thead tr th {\n  text-align: center;\n  font-weight: 600; }\n\n#store .table tbody td .btn-group {\n  width: 100%; }\n  #store .table tbody td .btn-group a {\n    text-shadow: none;\n    width: 49%; }\n    #store .table tbody td .btn-group a:first-child {\n      margin-right: 2%; }\n\n#store .table tbody td:first-child {\n  text-align: center; }\n\n#store #addStore .btn {\n  box-shadow: none;\n  background-image: none;\n  text-shadow: none; }\n\n@media screen and (min-width: 769px) {\n  #modalConfirmDelete {\n    width: 598px;\n    margin: 0 auto; }\n    #modalConfirmDelete .btn-primary {\n      background-image: none; }\n    #modalConfirmDelete .btn {\n      box-shadow: none;\n      background-image: none;\n      text-shadow: none; } }\n"

/***/ }),

/***/ 1208:
/***/ (function(module, exports) {

module.exports = "#userPermission .selectStore {\n  width: 56%; }\n\n#userPermission .addStoreBtn {\n  width: 10%;\n  padding: 0;\n  padding-right: 8px; }\n  #userPermission .addStoreBtn .btn {\n    width: 100%; }\n"

/***/ }),

/***/ 1209:
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ 1212:
/***/ (function(module, exports) {

module.exports = "<div [ngClass]=\"{'loginPage': !isHideSidebar}\">\n  <div id=\"header-topbar-option-demo\" class=\"page-header-topbar\" *ngIf=\"isHideSidebar\">\n    <app-header></app-header>\n  </div>\n  <div id=\"wrapper\">\n    <app-sidebar id=\"page-sidebar\" *ngIf=\"isHideSidebar\"></app-sidebar>\n    <div id=\"page-wrapper\" [ngClass]=\"{'hideMenuContent': isHideMenu}\">\n      <router-outlet></router-outlet>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ 1213:
/***/ (function(module, exports) {

module.exports = "<!-- title -->\n<div id=\"title-breadcrumb-option-demo\" class=\"page-title-breadcrumb\">\n  <div class=\"page-header pull-left\">\n    <div class=\"page-title\">{{Heading}}</div>\n  </div>\n  <ol class=\"breadcrumb page-breadcrumb\">\n    <li>\n      <i class=\"fa fa-home\"></i>&nbsp;\n      <a [routerLink]=\"['/dashboard']\">Home</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n      <a [routerLink]=\"['/claim/list']\">การเคลมสินค้า</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n    </li>\n    <li class=\"active\">{{Heading}}</li>\n  </ol>\n  <div class=\"btn btn-blue reportrange hide\">\n    <i class=\"fa fa-calendar\"></i>&nbsp;\n    <span></span>&nbsp;report&nbsp;\n    <i class=\"fa fa-angle-down\"></i>\n    <input type=\"hidden\" name=\"datestart\" />\n    <input type=\"hidden\" name=\"endstart\" />\n  </div>\n  <div class=\"clearfix\"></div>\n</div>\n\n<!-- content -->\n<div class=\"page-content\" id=\"addClaim\">\n  <ul id=\"addClaimTab\" class=\"nav nav-tabs ul-edit responsive\">\n    <li class=\"active\"><a href=\"#addChangeClaimStore\" data-toggle=\"tab\">เพิ่มรายการเปลี่ยนสินค้า</a></li>\n  </ul>\n  <div id=\"addClaimTabContent\" class=\"tab-content\">\n    <div id=\"addChangeClaimStore\" class=\"tab-pane fade in active\">\n      <div id=\"rootwizard-custom-circle\">\n        <div class=\"navbar\">\n          <div class=\"navbar-inner\">\n            <ul>\n              <li [class.active]=\"i == 0\" *ngFor=\"let formList of formName; let i = index;\" [ngClass]=\"{'active': selectedTab === (i+1)}\">\n                <a>\n                  <i class=\"fa\" [ngClass]=\"formList.icon\"></i>\n                  <p class=\"anchor\">{{formList.name}}</p>\n                </a>\n              </li>\n            </ul>\n          </div>\n        </div>\n        <div id=\"bar\" class=\"progress active\">\n          <progressbar value=\"{{progress}}\" class=\"bar progress-bar-primary\"></progressbar>\n        </div>\n        <div class=\"tab-content form-horizontal\">\n          <app-loading *ngIf=\"isLoading\"></app-loading>\n          <div id=\"tab-1-info\" class=\"tab-pane fadeIn in\" [ngClass]=\"{'active': selectedTab === 1}\">\n            <form [formGroup]=\"infoForm\" class=\"form-horizontal\">\n              <div class=\"form-body pal\">\n                <h3>ข้อมูลทั่วไป</h3>\n                <div class=\"row\">\n                  <div class=\"col-md-6\">\n                    <div class=\"form-group\">\n                      <label for=\"\" class=\"col-md-4 control-label\">วันที่รับเปลี่ยน<span class='require'>*</span></label>\n                      <div class=\"col-md-8\">\n                        <div>\n                          <my-date-picker [options]=\"myDatePickerOptions\" [selDate]=\"selDate\" (dateChanged)=\"onDateChanged($event)\" formControlName=\"exchange_date\"\n                            [(ngModel)]=\"date\"></my-date-picker>\n                        </div>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n              </div>\n              <div class=\"form-body pal\">\n                <h3>ข้อมูลลูกค้า</h3>\n                <div class=\"row\">\n                  <div class=\"col-md-6\">\n                    <div class=\"form-group\"><label class=\"col-md-4 control-label\">ชื่อลูกค้า</label>\n                      <div class=\"col-md-6\" id=\"addCustomerInput\">\n                        <input name=\"nameCustomer\" placeholder=\"กรอกชื่อลูกค้า\" [(ngModel)]=\"customerSelected.name\" [typeahead]=\"customers\" [typeaheadItemTemplate]=\"customItemCustomerTemplate\"\n                          [typeaheadOptionField]=\"'name'\" (typeaheadNoResults)=\"changeTypeaheadNoResults(0,$event)\" (typeaheadOnSelect)=\"typeaheadOnSelect(0,$event)\"\n                          class=\"form-control\" formControlName=\"customer_id\" />\n                        <div *ngIf=\"typeaheadLoading\">\n                          <i class=\"fa fa-refresh ng-hide\"></i>\n                        </div>\n                        <div class=\"no-data\" *ngIf=\"typeaheadNoResults.customer\">\n                          <i class=\"fa fa-remove\"></i> ไม่พบข้อมูลลูกค้า\n                        </div>\n                      </div>\n                      <div class=\"col-md-2 addCustomerBtn\">\n                        <button type=\"button\" class=\"btn btn-success btn-outlined btn-sm\" (click)=\"openAddCustomerModal()\">เพิ่ม</button>\n                      </div>\n                    </div>\n                  </div>\n                  <div class=\"col-md-6\">\n                    <div class=\"form-group\">\n                      <label for=\"tel\" class=\"col-md-4 control-label\">เบอร์โทร<span class='require'>*</span></label>\n                      <div class=\"col-md-8\">\n                        <input type=\"text\" name=\"tel\" placeholder=\"เบอร์โทร\" class=\"inputUsername form-control\" formControlName=\"tel\" [(ngModel)]=\"customerSelected.tel\"\n                        />\n                        <p class=\"text-danger\" *ngIf=\"tel.hasError('required') && tel.dirty\">กรุณากรอกเบอร์โทรศัพท์</p>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n              </div>\n              <div class=\"action text-right\">\n                <button type=\"button\" name=\"previous\" value=\"Previous\" class=\"btn btn-primary btn-outlined button-previous disabled\">\n                  <i class=\"fa fa-arrow-circle-o-left mrx\"></i>ย้อนกลับ\n                </button>\n                <button type=\"button\" name=\"next\" value=\"Next\" class=\"btn btn-primary btn-outlined button-next\" *ngIf=\"!isLastTab\" (click)=\"setTab(0,'next')\"\n                  [ngClass]=\"{'disabled': !infoForm.valid}\" [disabled]=\"!infoForm.valid\">\n                  ถัดไป<i class=\"fa fa-arrow-circle-o-right mlx\"></i>\n                </button>\n              </div>\n            </form>\n          </div>\n          <div id=\"tab-2-product-broken\" class=\"tab-pane fadeIn\" [ngClass]=\"{'active': selectedTab === 2}\">\n            <form [formGroup]=\"productForm\" class=\"form-horizontal\">\n              <div class=\"form-body pal\">\n                <h3>สินค้าที่ต้องการเปลี่ยน</h3>\n                <div class=\"row\">\n                  <div class=\"col-md-6\">\n                    <div class=\"form-group\">\n                      <label for=\"source_product\" class=\"col-md-4 control-label\">ค้นหาสินค้า</label>\n                      <div class=\"col-md-8\" id=\"addCustomerInput\">\n                        <input name=\"source_product\" placeholder=\"กรุณากรอก barcode\" [(ngModel)]=\"product_source.name\" [typeahead]=\"products\" [typeaheadItemTemplate]=\"customItemProductTemplate\"\n                          [typeaheadOptionField]=\"'code'\" (typeaheadNoResults)=\"changeTypeaheadNoResults(1,$event)\" (typeaheadOnSelect)=\"typeaheadOnSelect(1,$event)\"\n                          class=\"form-control\" formControlName=\"source_product_id\" />\n                        <div *ngIf=\"typeaheadLoading\">\n                          <i class=\"fa fa-refresh ng-hide\"></i>\n                        </div>\n                        <div class=\"no-data\" *ngIf=\"typeaheadNoResults.product_source\">\n                          <i class=\"fa fa-remove\"></i> ไม่พบสินค้า\n                        </div>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n              </div>\n              <div class=\"form-body pal\">\n                <h3>สินค้าที่เปลี่ยน</h3>\n                <div class=\"row\">\n                  <div class=\"col-md-6\">\n                    <div class=\"form-group\">\n                      <label for=\"target_product\" class=\"col-md-4 control-label\">ค้นหาสินค้า</label>\n                      <!--<div class=\"col-md-8\">\n                        <input type=\"text\" placeholder=\"รหัสสินค้า\" class=\"inputUsername form-control\" formControlName=\"target_product_id\" />\n                        <p class=\"text-danger\" *ngIf=\"target_product_id.hasError('required') && target_product_id.dirty\">กรุณากรอกสินค้าที่เปลี่ยน</p>\n                      </div>-->\n                      <div class=\"col-md-8\" id=\"addCustomerInput\">\n                        <input name=\"target_product\" placeholder=\"กรุณากรอก barcode\" [(ngModel)]=\"product_target.name\" [typeahead]=\"products\" [typeaheadItemTemplate]=\"customItemProductTemplate\"\n                          [typeaheadOptionField]=\"'code'\" (typeaheadNoResults)=\"changeTypeaheadNoResults(2,$event)\" (typeaheadOnSelect)=\"typeaheadOnSelect(2,$event)\"\n                          class=\"form-control\" formControlName=\"target_product_id\" />\n                        <div *ngIf=\"typeaheadLoading\">\n                          <i class=\"fa fa-refresh ng-hide\"></i>\n                        </div>\n                        <div class=\"no-data\" *ngIf=\"typeaheadNoResults.product_target\">\n                          <i class=\"fa fa-remove\"></i> ไม่พบสินค้า\n                        </div>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n              </div>\n              <div class=\"action text-right\">\n                <button type=\"button\" name=\"previous\" value=\"Previous\" class=\"btn btn-primary btn-outlined button-previous\" (click)=\"setTab(0,'previous')\">\n                  <i class=\"fa fa-arrow-circle-o-left mrx\"></i>ย้อนกลับ\n                </button>\n                <button type=\"button\" name=\"next\" value=\"Next\" class=\"btn btn-primary btn-outlined button-next\" (click)=\"setTab(0,'next')\"\n                  [ngClass]=\"{'disabled': !productForm.valid}\" [disabled]=\"!productForm.valid\">\n                  ถัดไป<i class=\"fa fa-arrow-circle-o-right mlx\"></i>\n                </button>\n              </div>\n            </form>\n          </div>\n          <div id=\"tab-3-detail\" class=\"tab-pane fadeIn\" [ngClass]=\"{'active': selectedTab === 3}\">\n            <form [formGroup]=\"causeForm\" class=\"form-horizontal\">\n              <div class=\"form-body pal\">\n                <h3>อาการ</h3>\n                <div class=\"row\">\n                  <div class=\"col-md-6\">\n                    <div class=\"form-group broken\">\n                      <div class=\"col-md-11 col-md-offset-1 brokenInput\" *ngFor=\"let item of brokens; let i = index\">\n                        <div class=\"input-group\">\n                          <span class=\"input-group-addon\">{{i+1}}</span>\n                          <div class=\"input-icon right\">\n                            <input type=\"text\" placeholder=\"อาการ\" class=\"form-control\" [(ngModel)]=\"brokens[i].message\" [ngModelOptions]=\"{standalone: true}\"\n                            />\n                            <i class=\"fa fa-times\" (click)=deleteBrokens(i)></i>\n                          </div>\n                        </div>\n                      </div>\n                      <div class=\"col-md-11 col-md-offset-1\">\n                        <button type=\"button\" class=\"btn btn-green btn-outlined btn-block brokenAddBtn\" (click)=\"addBrokens()\"><i class=\"fa fa-plus\"></i>&nbsp;&nbsp;เพิ่มอาการ</button>\n                      </div>\n                    </div>\n                  </div>\n                  <div class=\"col-md-6\">\n                    <div class=\"form-group\"><label for=\"remark\" class=\"col-md-4 control-label\">หมายเหตุ</label>\n                      <div class=\"col-md-8\">\n                        <textarea id=\"remark\" rows=\"3\" name=\"remark\" class=\"form-control\" formControlName=\"remark\"></textarea>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n              </div>\n              <div class=\"action text-right\">\n                <button type=\"button\" name=\"previous\" value=\"Previous\" class=\"btn btn-primary btn-outlined button-previous\" (click)=\"setTab(0,'previous')\">\n                  <i class=\"fa fa-arrow-circle-o-left mrx\"></i>ย้อนกลับ\n                </button>\n                <button type=\"button\" name=\"next\" value=\"Next\" class=\"btn btn-primary btn-outlined button-next\" (click)=\"setTab(0,'next')\"\n                  [ngClass]=\"{'disabled': !causeForm.valid}\" [disabled]=\"!causeForm.valid\">\n                  ถัดไป<i class=\"fa fa-arrow-circle-o-right mlx\"></i>\n                </button>\n              </div>\n            </form>\n          </div>\n          <div id=\"tab-4-employee\" class=\"tab-pane fadeIn\" [ngClass]=\"{'active': selectedTab === 4}\">\n            <form [formGroup]=\"employeeForm\" class=\"form-horizontal\">\n              <div class=\"form-body pal\">\n                <h3>ข้อมูลพนักงาน</h3>\n                <div class=\"row\">\n                  <div class=\"col-md-6\">\n                    <div class=\"form-group\">\n                      <label for=\"inputUsername\" class=\"col-md-4 control-label\">พนักงาน</label>\n                      <div class=\"col-md-8\">\n                        <input type=\"text\" placeholder=\"ชื่อ\" class=\"inputUsername form-control\" formControlName=\"staff_name\" />\n                      </div>\n                    </div>\n                  </div>\n                </div>\n              </div>\n              <div class=\"action text-right\">\n                <button type=\"button\" name=\"previous\" value=\"Previous\" class=\"btn btn-primary btn-outlined button-previous\" (click)=\"setTab(0,'previous')\">\n                  <i class=\"fa fa-arrow-circle-o-left mrx\"></i>ย้อนกลับ\n                </button>\n                <button type=\"button\" name=\"next\" value=\"Next\" class=\"btn btn-success btn-outlined button-next\" (click)=\"saveData()\" [ngClass]=\"{'disabled': !employeeForm.valid}\"\n                  [disabled]=\"!employeeForm.valid\">\n                  บันทึก<i class=\"fa fa-floppy-o mlx\"></i>\n                </button>\n              </div>\n            </form>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n\n<template #customItemCustomerTemplate let-model=\"item\" let-index=\"index\">\n  <div class=\"nameAutocomplete\">\n    <h5><strong>ชื่อ :: </strong> {{model.name}}</h5>\n    <h5><strong>เบอร์ :: </strong> {{model.tel}}</h5>\n  </div>\n</template>\n\n<template #customItemProductTemplate let-model=\"item\" let-index=\"index\">\n  <div class=\"nameAutocomplete\">\n    <h5><strong>barcode :: </strong> {{model.code}}</h5>\n    <h5><strong>ชื่อสินค้า :: </strong> {{model.name}}</h5>\n    <h5><strong>ประเภท :: </strong> {{model.product_type.name}}</h5>\n  </div>\n</template>\n\n<div class=\"modal fade\" bsModal #editModal=\"bs-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"mySmallModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-lg modalEditProduct\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title pull-left\">แก้ไข{{Heading}}</h4>\n        <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"editModal.hide()\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <form [formGroup]=\"customerForm\" class=\"form-horizontal\" (ngSubmit)=\"saveCustomer()\">\n        <div class=\"modal-body\">\n          <div class=\"form-body pal\">\n            <div class=\"form-group\">\n              <label for=\"name_customer\" class=\"col-md-3 control-label\">ชื่อ<span class='require'>*</span></label>\n              <div class=\"col-md-5\">\n                <div class=\"input-icon right\">\n                  <input type=\"text\" id=\"name_customer\" name=\"name_customer\" class=\"form-control\" formControlName=\"name_customer\" />\n                  <p class=\"text-danger\" *ngIf=\"name_customer.hasError('required') && name_customer.dirty\">กรุณากรอกชื่อ</p>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <label for=\"tel_customer\" class=\"col-md-3 control-label\">เบอร์ติดต่อ<span class='require'>*</span></label>\n              <div class=\"col-md-5\">\n                <div class=\"input-icon right\">\n                  <input type=\"tel\" id=\"tel_customer\" name=\"tel_customer\" class=\"form-control\" formControlName=\"tel_customer\" />\n                  <p class=\"text-danger\" *ngIf=\"tel_customer.hasError('required') && tel_customer.dirty\">กรุณากรอกเบอร์ติดต่อ</p>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"editModal.hide()\">ยกเลิก</button>\n          <button type=\"submit\" class=\"btn btn-success btn-square\" [disabled]=\"!customerForm.valid\">บันทึก</button>\n        </div>\n      </form>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ 1214:
/***/ (function(module, exports) {

module.exports = "<!-- title -->\n<div id=\"title-breadcrumb-option-demo\" class=\"page-title-breadcrumb\">\n  <div class=\"page-header pull-left\">\n    <div class=\"page-title\">{{Heading}}</div>\n  </div>\n  <ol class=\"breadcrumb page-breadcrumb\">\n    <li>\n      <i class=\"fa fa-home\"></i>&nbsp;\n      <a [routerLink]=\"['/dashboard']\">Home</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n      <a [routerLink]=\"['/claim/change/list']\">รายการเปลี่ยนสินค้า</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n    </li>\n    <li class=\"active\">{{Heading}}</li>\n  </ol>\n</div>\n\n<!-- content -->\n<div class=\"page-content\" id=\"change-info\">\n  <div id=\"changeInfoContent\">\n    <app-loading *ngIf=\"isLoading\"></app-loading>\n    <div class=\"row\" *ngIf=\"!isLoading\">\n      <div class=\"col-md-6\">\n        <div class=\"portlet box portlet-primary information\">\n          <div class=\"portlet-header\">\n            <div class=\"caption\">ข้อมูลทั่วไป</div>\n            <div class=\"tools\">\n              <i class=\"fa fa-pencil\" (click)=\"edit.info = !edit.info\"></i>\n            </div>\n          </div>\n          <div class=\"portlet-body\">\n            <div class=\"row form-horizontal\">\n              <div class=\"col-md-12\">\n                <div class=\"form-group\">\n                  <label for=\"code\" class=\"col-md-3 control-label\">\n                  รหัสเปลี่ยน <span *ngIf=\"!edit.info\">:</span><span class='require' *ngIf=\"edit.info\">*</span></label>\n                  <div class=\"col-md-8\">\n                    <input type=\"text\" placeholder=\"รหัสเปลี่ยน\" class=\"inputUsername form-control\" name=\"code\" id=\"code\" [(ngModel)]=\"data.code\"\n                      [readonly]=\"!edit.info\" />\n                  </div>\n                </div>\n              </div>\n              <div class=\"col-md-12\">\n                <div class=\"form-group\">\n                  <label for=\"date\" class=\"col-md-3 control-label\">\n                  วันที่รับเปลี่ยน <span *ngIf=\"!edit.info\">:</span><span class='require' *ngIf=\"edit.info\">*</span></label>\n                  <div class=\"col-md-8\">\n                    <div class=\"input-group\" *ngIf=\"edit.info\">\n                      <input type=\"text\" name=\"date\" class=\"datepicker-default form-control\" ngbDatepicker #date=\"ngbDatepicker\" (click)=\"date.toggle()\"\n                        [(ngModel)]=\"data.dateFormat\" />\n                      <div class=\"input-group-addon\" (click)=\"date.toggle()\"><i class=\"fa fa-calendar\"></i></div>\n                    </div>\n                    <input type=\"text\" class=\"inputUsername form-control\" name=\"date\" id=\"date\" [(ngModel)]=\"data.exchange_date\" [readonly]=\"!edit.info\"\n                      *ngIf=\"!edit.info\" />\n                  </div>\n                </div>\n              </div>\n              <div class=\"col-md-12\">\n                <div class=\"form-group\">\n                  <label for=\"customerName\" class=\"col-md-3 control-label\">\n                ชื่อลูกค้า <span *ngIf=\"!edit.info\">:</span><span class='require' *ngIf=\"edit.info\">*</span></label>\n                  <div *ngIf=\"edit.info\" class=\"col-md-8 inputTypeahead\">\n                    <div class=\"col-md-9 divInputAhead\" id=\"addCustomerInput\">\n                      <input name=\"nameCustomer\" placeholder=\"ค้นหาชื่อลูกค้า\" [(ngModel)]=\"customerSelected.name\" [typeahead]=\"customers\" [typeaheadItemTemplate]=\"customItemCustomerTemplate\"\n                        [typeaheadOptionField]=\"'first_name'\" (typeaheadNoResults)=\"changeTypeaheadNoResults($event)\" (typeaheadOnSelect)=\"typeaheadOnSelect(0,$event)\"\n                        class=\"form-control\" />\n                      <div *ngIf=\"typeaheadLoading===true\">\n                        <i class=\"fa fa-refresh ng-hide\"></i>\n                      </div>\n                    </div>\n                    <div class=\"col-md-3 addCustomerBtn divButtonAhead\">\n                      <button type=\"button\" class=\"btn btn-success btn-outlined btn-sm\" (click)=\"openModal(addCustomer)\">เพิ่ม</button>\n                    </div>\n                  </div>\n                  <div class=\"col-md-8\" *ngIf=\"!edit.info\">\n                    <input type=\"text\" class=\"inputUsername form-control\" name=\"customerName\" id=\"customerName\" [(ngModel)]=\"data.customer_id\"\n                      [readonly]=\"!edit.info\" />\n                  </div>\n                </div>\n\n                <div class=\"no-data col-md-8 col-md-offset-3\" *ngIf=\"typeaheadNoResults===true\">\n                  <i class=\"fa fa-remove\"></i> ไม่พบข้อมูลลูกค้า\n                </div>\n              </div>\n              <div class=\"col-md-12\">\n                <div class=\"form-group\">\n                  <label for=\"tel\" class=\"col-md-3 control-label\">\n                  เบอร์โทร <span *ngIf=\"!edit.info\">:</span><span class='require' *ngIf=\"edit.info\">*</span></label>\n                  <div class=\"col-md-8\">\n                    <input type=\"text\" placeholder=\"เบอร์โทร\" class=\"inputUsername form-control\" name=\"tel\" id=\"tel\" [(ngModel)]=\"data.tel\" [readonly]=\"!edit.info\"\n                    />\n                  </div>\n                </div>\n              </div>\n              <div class=\"col-md-12 btnAction\" *ngIf=\"edit.info\">\n                <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"cancleEdit()\">ยกเลิก</button>\n                <button type=\"button\" class=\"btn btn-success btn-square\" (click)=\"editData()\">บันทึก</button>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"portlet box portlet-primary broken\">\n          <div class=\"portlet-header\">\n            <div class=\"caption\">อาการ</div>\n            <div class=\"tools\">\n              <i class=\"fa fa-pencil\" (click)=\"edit.broken = !edit.broken\"></i>\n            </div>\n          </div>\n          <div class=\"portlet-body\">\n            <div class=\"row\">\n              <div class=\"col-md-12\">\n                <div class=\"form-group broken\">\n                  <!--<label for=\"inputEmail\" class=\"col-md-3 control-label\">Email<span class='require'>*</span></label>-->\n                  <div class=\"col-md-10 col-md-offset-1 brokenInput\" *ngFor=\"let item of data.broken; let i = index\">\n                    <div class=\"input-group\">\n                      <span class=\"input-group-addon\">{{i+1}}</span>\n                      <input type=\"text\" placeholder=\"อาการ\" class=\"form-control\" [(ngModel)]=\"data.broken[i].message\" [readonly]=\"!edit.broken\"\n                      />\n                    </div>\n                  </div>\n                  <div class=\"col-md-10 col-md-offset-1\" *ngIf=\"edit.broken\">\n                    <button type=\"button\" class=\"btn btn-green btn-outlined btn-block brokenAddBtn\" (click)=\"addBrokens()\"><i class=\"fa fa-plus\"></i>&nbsp;&nbsp;เพิ่มอาการ</button>\n                  </div>\n                </div>\n              </div>\n              <div class=\"col-md-12 form-horizontal\">\n                <div class=\"col-md-12 btnAction\" *ngIf=\"edit.broken\">\n                  <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"cancleEdit()\">ยกเลิก</button>\n                  <button type=\"button\" class=\"btn btn-success btn-square\" (click)=\"editData()\">บันทึก</button>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"portlet box portlet-primary remark\">\n          <div class=\"portlet-header\">\n            <div class=\"caption\">หมายเหตุ</div>\n            <div class=\"tools\">\n              <i class=\"fa fa-pencil\" (click)=\"edit.remark = !edit.remark\"></i>\n            </div>\n          </div>\n          <div class=\"portlet-body\">\n            <div class=\"row form-horizontal\">\n              <div class=\"form-group\">\n                <label for=\"remark\" class=\"col-md-3 control-label\">หมายเหตุ <span *ngIf=\"!edit.remark\">:</span><span class='require' *ngIf=\"edit.remark\">*</span></label>\n                <div class=\"col-md-7\">\n                  <textarea id=\"remark\" class=\"form-control\" [(ngModel)]=\"data.remark\" name=\"remark\" [readonly]=\"!edit.remark\"></textarea>\n                </div>\n              </div>\n              <div class=\"col-md-12 btnAction\" *ngIf=\"edit.remark\">\n                <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"cancleEdit()\">ยกเลิก</button>\n                <button type=\"button\" class=\"btn btn-success btn-square\" (click)=\"editData()\">บันทึก</button>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"col-md-6 \">\n        <div class=\"row\">\n          <div class=\"col-md-7\">\n            <div class=\"portlet box portlet-primary status\">\n              <div class=\"portlet-header\">\n                <div class=\"caption\">สถานะ</div>\n                <div class=\"tools\">\n                  <i class=\"fa fa-pencil\" (click)=\"edit.status = !edit.status\"></i>\n                </div>\n              </div>\n              <div class=\"portlet-body \">\n                <div class=\"statusText\" [ngSwitch]=\"data.status\">\n                  <h4 class=\"statusShow text-blue\" *ngSwitchCase=\"1\">รับเรื่อง</h4>\n                  <h4 class=\"statusShow text-yellow\" *ngSwitchCase=\"2\">กำลังเปลี่ยน</h4>\n                  <h4 class=\"statusShow text-green\" *ngSwitchCase=\"3\">สำเร็จ</h4>\n                  <h4 class=\"statusShow text-red\" *ngSwitchCase=\"4\">ข้อผิดพลาด</h4>\n                </div>\n                <div class=\"statusEdit\" *ngIf=\"edit.status\">\n                  <div class=\"row form-horizontal\">\n                    <div class=\"col-md-12\">\n                      <div class=\"form-group\">\n                        <label for=\"status\" class=\"col-md-3 control-label\">\n                  สถานะ <span *ngIf=\"!edit.status\">:</span><span class='require' *ngIf=\"edit.status\">*</span></label>\n                        <div class=\"col-md-6\">\n                          <select [(ngModel)]=\"data.status\" class=\"selectCenter\" name=\"status\" style=\"width:100%\">\n                              <option *ngFor=\"let status of statusText\" [value]=\"status.id\">{{status.message}}</option>\n                          </select>\n                        </div>\n                      </div>\n                      <div class=\"col-md-12 btnAction\">\n                        <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"cancleEdit()\">ยกเลิก</button>\n                        <button type=\"button\" class=\"btn btn-success btn-square\" (click)=\"editData()\">บันทึก</button>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n          <div class=\"col-md-5\">\n            <div class=\"portlet box portlet-primary action\">\n              <div class=\"portlet-header\">\n                <div class=\"caption\">Action</div>\n              </div>\n              <div class=\"portlet-body\">\n                <button type=\"button\" class=\"btn btn-violet btn-outlined\" (click)=\"openPagePrint()\"><i class=\"fa fa-print\"></i> พิมพ์ใบเปลี่ยน</button>\n                <button type=\"button\" class=\"btn btn-danger btn-outlined\" (click)=\"openDeleteModal()\"><i class=\"fa fa-trash\"></i> ลบใบเปลี่ยน</button>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"portlet box portlet-primary productOriginal\">\n          <div class=\"portlet-header\">\n            <div class=\"caption\">สินค้าที่นำมาเปลี่ยน</div>\n            <div class=\"tools\">\n              <i class=\"fa fa-pencil\" (click)=\"edit.source_product = !edit.source_product\"></i>\n            </div>\n          </div>\n          <div class=\"portlet-body\">\n            <div class=\"row form-horizontal\">\n              <div class=\"col-md-12\">\n                <div class=\"form-group\">\n                  <label for=\"source_product_id\" class=\"col-md-3 control-label\">\n                  รหัสสินค้า <span *ngIf=\"!edit.source_product\">:</span><span class='require' *ngIf=\"edit.source_product\">*</span></label>\n                  <div class=\"col-md-8\">\n                    <input type=\"text\" placeholder=\"รหัสสินค้า\" class=\"inputUsername form-control\" name=\"source_product_id\" id=\"source_product_id\"\n                      [(ngModel)]=\"data.source_product.code\" [readonly]=\"!edit.source_product\" />\n                  </div>\n                </div>\n              </div>\n              <div class=\"col-md-12\">\n                <div class=\"form-group\">\n                  <label for=\"source_product_name\" class=\"col-md-3 control-label\">\n                  ชื่อสินค้า <span *ngIf=\"!edit.source_product\">:</span><span class='require' *ngIf=\"edit.source_product\">*</span></label>\n                  <div class=\"col-md-8\">\n                    <input type=\"text\" placeholder=\"ชื่อสินค้า\" class=\"inputUsername form-control\" name=\"source_product_name\" id=\"source_product_name\"\n                      [(ngModel)]=\"data.source_product.name\" [readonly]=\"!edit.source_product\" />\n                  </div>\n                </div>\n              </div>\n              <div class=\"col-md-12\">\n                <div class=\"form-group\">\n                  <label for=\"source_product_price\" class=\"col-md-3 control-label\">\n                  ราคาซื้อสินค้า <span *ngIf=\"!edit.source_product\">:</span><span class='require' *ngIf=\"edit.source_product\">*</span></label>\n                  <div class=\"col-md-8\">\n                    <input type=\"text\" placeholder=\"ราคาซื้อสินค้า\" class=\"inputUsername form-control\" name=\"source_product_price\" id=\"source_product_price\"\n                      [(ngModel)]=\"data.source_product.sell_price\" [readonly]=\"!edit.source_product\" />\n                  </div>\n                </div>\n              </div>\n              <div class=\"col-md-12\">\n                <div class=\"form-group\">\n                  <label for=\"source_product_type\" class=\"col-md-3 control-label\">\n                  ประเภทสินค้า <span *ngIf=\"!edit.source_product\">:</span><span class='require' *ngIf=\"edit.source_product\">*</span></label>\n                  <div class=\"col-md-8\">\n                    <input type=\"text\" placeholder=\"ประเภทสินค้า\" class=\"inputUsername form-control\" name=\"source_product_type\" id=\"source_product_type\"\n                      [(ngModel)]=\"data.source_product.product_type.name\" [readonly]=\"!edit.source_product\" />\n                  </div>\n                </div>\n              </div>\n              <div class=\"col-md-12\">\n                <div class=\"form-group\">\n                  <label for=\"source_productDateBuy\" class=\"col-md-3 control-label\">\n                  วันที่ซื้อสินค้า <span *ngIf=\"!edit.source_product\">:</span><span class='require' *ngIf=\"edit.source_product\">*</span></label>\n                  <div class=\"col-md-8\">\n                    <div class=\"input-group\" *ngIf=\"edit.source_product\">\n                      <input type=\"text\" name=\"source_productDateBuy\" class=\"datepicker-default form-control\" ngbDatepicker #source_productDateBuy=\"ngbDatepicker\"\n                        (click)=\"source_productDateBuy.toggle()\" [(ngModel)]=\"data.source_product.dateBuyFormat\" />\n                      <div class=\"input-group-addon\" (click)=\"date.toggle()\"><i class=\"fa fa-calendar\"></i></div>\n                    </div>\n                    <input type=\"text\" class=\"inputUsername form-control\" name=\"source_productDateBuy\" id=\"source_productDateBuy\" [(ngModel)]=\"data.source_product.dateBuy\"\n                      [readonly]=\"!edit.info\" *ngIf=\"!edit.source_product\" />\n                  </div>\n                </div>\n              </div>\n              <div class=\"col-md-12 btnAction\" *ngIf=\"edit.source_product\">\n                <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"cancleEdit()\">ยกเลิก</button>\n                <button type=\"button\" class=\"btn btn-success btn-square\" (click)=\"editData()\">บันทึก</button>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"portlet box portlet-primary productChange\">\n          <div class=\"portlet-header\">\n            <div class=\"caption\">สินค้าที่เปลี่ยน</div>\n            <div class=\"tools\">\n              <i class=\"fa fa-pencil\" (click)=\"edit.target_product = !edit.target_product\"></i>\n            </div>\n          </div>\n          <div class=\"portlet-body portlet-product\">\n            <div class=\"row form-horizontal\">\n              <div class=\"col-md-12\">\n                <div class=\"form-group\">\n                  <label for=\"target_product_id\" class=\"col-md-3 control-label\">\n                  รหัสสินค้า <span *ngIf=\"!edit.target_product\">:</span><span class='require' *ngIf=\"edit.target_product\">*</span></label>\n                  <div class=\"col-md-8\">\n                    <input type=\"text\" placeholder=\"รหัสสินค้า\" class=\"inputUsername form-control\" name=\"target_product_id\" id=\"target_product_id\"\n                      [(ngModel)]=\"data.target_product.code\" [readonly]=\"!edit.target_product\" />\n                  </div>\n                </div>\n              </div>\n              <div class=\"col-md-12\">\n                <div class=\"form-group\">\n                  <label for=\"target_product_name\" class=\"col-md-3 control-label\">\n                  ชื่อสินค้า <span *ngIf=\"!edit.target_product\">:</span><span class='require' *ngIf=\"edit.target_product\">*</span></label>\n                  <div class=\"col-md-8\">\n                    <input type=\"text\" placeholder=\"ชื่อสินค้า\" class=\"inputUsername form-control\" name=\"target_product_name\" id=\"target_product_name\"\n                      [(ngModel)]=\"data.target_product.name\" [readonly]=\"!edit.target_product\" />\n                  </div>\n                </div>\n              </div>\n              <div class=\"col-md-12\">\n                <div class=\"form-group\">\n                  <label for=\"target_product_price\" class=\"col-md-3 control-label\">\n                  ราคาซื้อสินค้า <span *ngIf=\"!edit.target_product\">:</span><span class='require' *ngIf=\"edit.target_product\">*</span></label>\n                  <div class=\"col-md-8\">\n                    <input type=\"text\" placeholder=\"ราคาซื้อสินค้า\" class=\"inputUsername form-control\" name=\"target_product_price\" id=\"target_product_price\"\n                      [(ngModel)]=\"data.target_product.sell_price\" [readonly]=\"!edit.target_product\" />\n                  </div>\n                </div>\n              </div>\n              <div class=\"col-md-12\">\n                <div class=\"form-group\">\n                  <label for=\"target_product_type\" class=\"col-md-3 control-label\">\n                  ประเภทสินค้า <span *ngIf=\"!edit.target_product\">:</span><span class='require' *ngIf=\"edit.target_product\">*</span></label>\n                  <div class=\"col-md-8\">\n                    <input type=\"text\" placeholder=\"ประเภทสินค้า\" class=\"inputUsername form-control\" name=\"target_product_type\" id=\"target_product_type\"\n                      [(ngModel)]=\"data.target_product.product_type.name\" [readonly]=\"!edit.target_product\" />\n                  </div>\n                </div>\n              </div>\n              <div class=\"col-md-12 btnAction\" *ngIf=\"edit.target_product\">\n                <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"cancleEdit()\">ยกเลิก</button>\n                <button type=\"button\" class=\"btn btn-success btn-square\" (click)=\"editData()\">บันทึก</button>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n<template #customItemCustomerTemplate let-model=\"item\" let-index=\"index\">\n  <div class=\"nameAutocomplete\">\n    <h5><strong>ชื่อ :: </strong> {{model.name}}</h5>\n    <h5><strong>เบอร์ :: </strong> {{model.tel}}</h5>\n  </div>\n</template>\n\n<template #customItemProductTemplate let-model=\"item\" let-index=\"index\">\n  <div class=\"nameAutocomplete\">\n    <h5><strong>barcode :: </strong> {{model.code}}</h5>\n    <h5><strong>ชื่อสินค้า :: </strong> {{model.name}}</h5>\n    <h5><strong>ประเภท :: </strong> {{model.product_type.name}}</h5>\n  </div>\n</template>\n\n<div class=\"modal fade\" bsModal #deleteModal=\"bs-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"mySmallModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-sm modalEditProduct\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title pull-left\">ยืนยันการลบการเปลี่ยนสินค้า</h4>\n        <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"deleteModal.hide()\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\">\n        <p>คุณแน่ใจที่ต้องการจะลบ \"{{data.code}}\"</p>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"deleteModal.hide()\">ยกเลิก</button>\n        <button type=\"button\" class=\"btn btn-success btn-square\" (click)=\"deleteItem()\">แน่ใจ</button>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ 1215:
/***/ (function(module, exports) {

module.exports = "<!-- title -->\n<div id=\"title-breadcrumb-option-demo\" class=\"page-title-breadcrumb\">\n  <div class=\"page-header pull-left\">\n    <div class=\"page-title\">{{Heading}}</div>\n  </div>\n  <ol class=\"breadcrumb page-breadcrumb\">\n    <li>\n      <i class=\"fa fa-home\"></i>&nbsp;\n      <a [routerLink]=\"['/dashboard']\">Home</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n    </li>\n    <li class=\"active\">{{Heading}}</li>\n  </ol>\n  <div class=\"btn btn-blue reportrange hide\">\n    <i class=\"fa fa-calendar\"></i>&nbsp;\n    <span></span>&nbsp;report&nbsp;\n    <i class=\"fa fa-angle-down\"></i>\n    <input type=\"hidden\" name=\"datestart\" />\n    <input type=\"hidden\" name=\"endstart\" />\n  </div>\n  <div class=\"clearfix\"></div>\n</div>\n\n<!-- content -->\n<div class=\"page-content\" id=\"claim\">\n  <ul id=\"claimTab\" class=\"nav nav-tabs ul-edit responsive\">\n    <li class=\"active\"><a href=\"#claimTab\" data-toggle=\"tab\">จัดการ{{Heading}}</a></li>\n  </ul>\n  <div id=\"claimTabContent\" class=\"tab-content\">\n    <app-loading *ngIf=\"isLoading\"></app-loading>\n    <div id=\"listClaim\" class=\"tab-pane fade in active\">\n      <div class=\"row dateSelect\">\n        <label class=\"control-label\">เลือกช่วงวัน</label>\n        <div>\n          <my-date-range-picker [options]=\"myDateRangePickerOptions\" (dateRangeChanged)=\"onDateRangeChanged($event)\" [(ngModel)]=\"dateRange\"></my-date-range-picker>\n        </div>\n      </div>\n      <div class=\"row\" *ngIf=\"!isLoading\">\n        <div class=\"col-lg-12 datatable-simple\">\n          <table datatable class=\"table table-bordered\" [dtOptions]=\"dtOptions\" *ngIf=\"data\">\n            <thead>\n              <tr>\n                <th>รหัสเปลี่ยน</th>\n                <th>วันที่</th>\n                <th>สินค้า</th>\n                <th>สินค้าที่เปลี่ยน</th>\n                <th>ลูกค้า</th>\n                <th>สถานะ</th>\n                <th>Actions</th>\n              </tr>\n            </thead>\n            <tbody>\n              <tr *ngFor=\"let item of data\">\n                <td>{{item.code}}</td>\n                <td>{{item.exchange_date}}</td>\n                <td>{{item.source_product_name}}</td>\n                <td>{{item.target_product_name}}</td>\n                <td>{{item.customer_name}}</td>\n                <td [ngSwitch]=\"item.status\">\n                  <span class=\"label label-blue\" *ngSwitchCase=\"1\">รับเรื่อง</span>\n                  <span class=\"label label-yellow\" *ngSwitchCase=\"2\">กำลังเปลี่ยน</span>\n                  <span class=\"label label-green\" *ngSwitchCase=\"3\">สำเร็จ</span>\n                  <span class=\"label label-red\" *ngSwitchCase=\"4\">ผิดพลาด</span>\n                </td>\n                <td>\n                  <div class=\"actionsBtn\">\n                    <a class=\"btn btn-yellow btn-outlined\" (click)=\"showDetail(item)\"><i class=\"fa fa-info\"></i></a>\n                    <a class=\"btn btn-danger btn-outlined\" (click)=\"openDeleteModal(item)\"><i class=\"fa fa-trash\"></i></a>\n                  </div>\n                </td>\n              </tr>\n            </tbody>\n          </table>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n\n<div class=\"modal fade\" bsModal #deleteModal=\"bs-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"mySmallModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-sm modalEditProduct\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title pull-left\">ยืนยันการลบการเปลี่ยนสินค้า</h4>\n        <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"deleteModal.hide()\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\" *ngIf=\"selectedItem\">\n        <p>คุณแน่ใจที่ต้องการจะลบ \"{{selectedItem.code}}\"</p>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"deleteModal.hide()\">ยกเลิก</button>\n        <button type=\"button\" class=\"btn btn-success btn-square\" (click)=\"deleteItem()\">แน่ใจ</button>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ 1216:
/***/ (function(module, exports) {

module.exports = "<!-- title -->\n<div id=\"title-breadcrumb-option-demo\" class=\"page-title-breadcrumb\">\n  <div class=\"page-header pull-left\">\n    <div class=\"page-title\">{{Heading}}</div>\n  </div>\n  <ol class=\"breadcrumb page-breadcrumb\">\n    <li>\n      <i class=\"fa fa-home\"></i>&nbsp;\n      <a [routerLink]=\"['/dashboard']\">Home</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n      <a [routerLink]=\"['/claim/change/list']\">รายการเปลี่ยนสินค้า</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n    </li>\n    <li class=\"active\">{{Heading}}</li>\n  </ol>\n\n  <div class=\"clearfix\"></div>\n</div>\n\n<!-- content -->\n<div class=\"page-content\" id=\"addClaim\">\n  <div id=\"claimPrint-page\" class=\"row\">\n    <div class=\"col-md-9\">\n      <div class=\"panel\">\n        <app-loading *ngIf=\"isLoading\"></app-loading>\n        <div id=\"print-section\" *ngIf=\"!isLoading && data\">\n          <div class=\"panel-body\">\n            <div class=\"claimPrint-title\">\n              <h2>{{store.name}}</h2>\n              <p class=\"mbn text-center\">{{store.tel}}</p>\n            </div>\n            <table class=\"table table-claim-info\">\n              <tr>\n                <td>\n                  <address>\n                    <strong>รหัสซ่อม:</strong><br/>{{data.code}}\n                  </address>\n                </td>\n                <td>\n                  <address>\n                    <strong>วันที่รับซ่อม:</strong><br/>{{data.exchange_date}}\n                  </address>\n                </td>\n                <td>\n                  <address><strong>ชื่อ - นามสกุล:</strong><br/>{{data.customer.name}}</address>\n                </td>\n                <td>\n                  <address><strong>เบอร์:</strong><br/>{{data.tel}}</address>\n                </td>\n              </tr>\n            </table>\n            <hr/>\n            <div class=\"table-responsive\">\n              <table class=\"table table-product\">\n                <tbody>\n                  <tr>\n                    <td colspan=\"6\">\n                      <h4 class=\"header\">สินค้าที่ต้องการเปลี่ยน</h4>\n                    </td>\n                  </tr>\n                  <tr>\n                    <td colspan=\"3\"><strong>สินค้า :</strong>&nbsp;&nbsp;{{data.source_product.name}}</td>\n                    <td colspan=\"3\"><strong>ประเภท :</strong>&nbsp;&nbsp;{{data.source_product.product_type.name}}</td>\n                    <!--<td colspan=\"2\"><strong>สี :</strong>&nbsp;&nbsp;Samsung</td>-->\n                  </tr>\n                  <tr *ngIf=\"data.source_product.detail\">\n                    <td colspan=\"6\"><strong>รายละเอียด :</strong>&nbsp;&nbsp;{{data.source_product.detail}}</td>\n                  </tr>\n                  <tr>\n                    <td colspan=\"6\"><strong>Barcode :</strong>&nbsp;&nbsp;<span class=\"imei\" *ngFor=\"let item of data.source_product_block\">{{item}}</span></td>\n                  </tr>\n                  <tr>\n                    <td colspan=\"3\" class=\"table-list\">\n                      <p><strong>อาการ :</strong></p>\n                      <ol>\n                        <li *ngFor=\"let item of data.broken\">{{item.message || '-'}}</li>\n                      </ol>\n                    </td>\n                    <td colspan=\"3\" class=\"table-list\">\n                      <p><strong>หมายเหตุ :</strong>&nbsp;&nbsp;</p>\n                      <p class=\"textextra\">{{data.remark}}</p>\n                    </td>\n                  </tr>\n                </tbody>\n              </table>\n              <hr class=\"mbm\" />\n              <table class=\"table table-product\">\n                <tbody>\n                  <tr>\n                    <td colspan=\"6\">\n                      <h4 class=\"header\">สินค้าที่เปลี่ยน</h4>\n                    </td>\n                  </tr>\n                  <tr>\n                    <td colspan=\"3\"><strong>สินค้า :</strong>&nbsp;&nbsp;{{data.target_product.name}}</td>\n                    <td colspan=\"3\"><strong>ประเภท :</strong>&nbsp;&nbsp;{{data.target_product.product_type.name}}</td>\n                  </tr>\n                  <tr *ngIf=\"data.target_product.detail\">\n                    <td colspan=\"6\"><strong>รายละเอียด :</strong>&nbsp;&nbsp;{{data.target_product.detail}}</td>\n                  </tr>\n                  <tr>\n                    <td colspan=\"6\"><strong>Barcode :</strong>&nbsp;&nbsp;<span class=\"imei\" *ngFor=\"let item of data.target_product_block\">{{item}}</span></td>\n                  </tr>\n                  <!--<tr>\n                    <td colspan=\"6\" class=\"table-list\">\n                      <p><strong>หมายเหตุ :</strong>&nbsp;&nbsp;</p>\n                      <p class=\"textextra\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare dolor, quis ullamcorper\n                        ligula sodales at. Nulla tellus elit, varius non commodo eget, mattis vel eros. In sed ornare nulla.</p>\n                    </td>\n                  </tr>-->\n                </tbody>\n              </table>\n              <hr class=\"mbm\" />\n              <table class=\"table table-signature\">\n                <tbody>\n                  <tr class=\"signatureHeader\">\n                    <td colspan=\"6\">ลายเซ็นลูกค้า</td>\n                    <td colspan=\"6\">ลายเซ็นผู้รับเครื่อง</td>\n                    <td colspan=\"6\">ลายเซ็นช่างซ่อม</td>\n                  </tr>\n                  <tr class=\"signature\">\n                    <td></td>\n                    <td colspan=\"4\" class=\"borderShow\"></td>\n                    <td></td>\n                    <td></td>\n                    <td colspan=\"4\" class=\"borderShow\"></td>\n                    <td></td>\n                    <td></td>\n                    <td colspan=\"4\" class=\"borderShow\"></td>\n                    <td></td>\n                  </tr>\n                  <tr class=\"signatureDate\">\n                    <td colspan=\"2\">\n                      <p>วันที่</p>\n                    </td>\n                    <td colspan=\"3\" class=\"borderShow\"></td>\n                    <td></td>\n                    <td colspan=\"2\">\n                      <p>วันที่</p>\n                    </td>\n                    <td colspan=\"3\" class=\"borderShow\"></td>\n                    <td></td>\n                    <td colspan=\"2\">\n                      <p>วันที่</p>\n                    </td>\n                    <td colspan=\"3\" class=\"borderShow\"></td>\n                    <td></td>\n                  </tr>\n                  <tr class=\"signatureSpace\"></tr>\n                  <tr class=\"signatureHeader\">\n                    <td colspan=\"6\">ลายเซ็นผู้รับเครื่องซ่อม</td>\n                    <td colspan=\"6\">ลายเซ็นผู้ส่งเครื่องซ่อม</td>\n                    <td colspan=\"6\">วันที่ส่งเครื่องซ่อม</td>\n                  </tr>\n                  <tr class=\"signature\">\n                    <td></td>\n                    <td colspan=\"4\" class=\"borderShow\"></td>\n                    <td></td>\n                    <td></td>\n                    <td colspan=\"4\" class=\"borderShow\"></td>\n                    <td></td>\n                    <td></td>\n                    <td colspan=\"4\" class=\"borderShow\"></td>\n                    <td></td>\n                  </tr>\n                  <tr class=\"signatureDate\">\n                    <td colspan=\"2\">\n                      <p>วันที่</p>\n                    </td>\n                    <td colspan=\"3\" class=\"borderShow\"></td>\n                    <td></td>\n                    <td colspan=\"2\">\n                      <p>วันที่</p>\n                    </td>\n                    <td colspan=\"3\" class=\"borderShow\"></td>\n                    <td></td>\n                    <td colspan=\"6\"></td>\n                  </tr>\n                </tbody>\n              </table>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"col-lg-3\">\n      <button class=\"btn btn-success mrm\" (click)=\"print()\"><i class=\"fa fa-print\"></i>&nbsp; Print\n                            </button>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ 1217:
/***/ (function(module, exports) {

module.exports = "<!-- title -->\n<div id=\"title-breadcrumb-option-demo\" class=\"page-title-breadcrumb\">\n  <div class=\"page-header pull-left\">\n    <div class=\"page-title\">{{Heading}}</div>\n  </div>\n  <ol class=\"breadcrumb page-breadcrumb\">\n    <li>\n      <i class=\"fa fa-home\"></i>&nbsp;\n      <a [routerLink]=\"['/dashboard']\">Home</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n      <a [routerLink]=\"['/claim/list']\">การเคลมสินค้า</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n    </li>\n    <li class=\"active\">{{Heading}}</li>\n  </ol>\n  <div class=\"btn btn-blue reportrange hide\">\n    <i class=\"fa fa-calendar\"></i>&nbsp;\n    <span></span>&nbsp;report&nbsp;\n    <i class=\"fa fa-angle-down\"></i>\n    <input type=\"hidden\" name=\"datestart\" />\n    <input type=\"hidden\" name=\"endstart\" />\n  </div>\n  <div class=\"clearfix\"></div>\n</div>\n\n<!-- content -->\n<div class=\"page-content\" id=\"addClaim\">\n  <ul id=\"addClaimTab\" class=\"nav nav-tabs ul-edit responsive\">\n    <li class=\"active\"><a href=\"#addFixClaimStore\" data-toggle=\"tab\">เพิ่มรายการซ่อมสินค้า</a></li>\n  </ul>\n  <div id=\"addClaimTabContent\" class=\"tab-content\">\n    <div id=\"addFixClaimStore\" class=\"tab-pane fade in active\">\n      <div id=\"rootwizard-custom-circle\">\n        <div class=\"navbar\">\n          <div class=\"navbar-inner\">\n            <ul>\n              <li [class.active]=\"i == 0\" *ngFor=\"let formList of formName; let i = index;\" [ngClass]=\"{'active': selectedTab === (i+1)}\">\n                <a>\n                  <i class=\"fa\" [ngClass]=\"formList.icon\"></i>\n                  <p class=\"anchor\">{{formList.name}}</p>\n                </a>\n              </li>\n            </ul>\n          </div>\n        </div>\n        <div id=\"bar\" class=\"progress active\">\n          <progressbar value=\"{{progress}}\" class=\"bar progress-bar-primary\"></progressbar>\n        </div>\n        <div class=\"tab-content form-horizontal\">\n          <app-loading *ngIf=\"isLoading\"></app-loading>\n          <div id=\"tab-1-info\" class=\"tab-pane fadeIn in\" [ngClass]=\"{'active': selectedTab === 1}\">\n            <form [formGroup]=\"infoForm\" class=\"form-horizontal\">\n              <div class=\"form-body pal\">\n                <h3>ข้อมูลทั่วไป</h3>\n                <div class=\"row\">\n                  <!--<div class=\"col-md-6\">\n                    <div class=\"form-group\">\n                      <label for=\"code\" class=\"col-md-4 control-label\">รหัสซ่อม<span class='require'>*</span></label>\n                      <div class=\"col-md-8\">\n                        <input type=\"text\" name=\"code\" placeholder=\"รหัสซ่อม\" class=\"inputUsername form-control\" formControlName=\"code\" />\n                        <p class=\"text-danger\" *ngIf=\"code.hasError('required') && code.dirty\">กรุณากรอกรหัสซ่อม</p>\n                      </div>\n                    </div>\n                  </div>-->\n                  <div class=\"col-md-6\">\n                    <div class=\"form-group\">\n                      <label for=\"\" class=\"col-md-4 control-label\">วันที่รับซ่อม<span class='require'>*</span></label>\n                      <div class=\"col-md-8\">\n                        <div>\n                          <my-date-picker [options]=\"myDatePickerOptions\" [selDate]=\"selDate\" (dateChanged)=\"onDateChanged($event)\" formControlName=\"repair_date\"\n                            [(ngModel)]=\"date\"></my-date-picker>\n                        </div>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n              </div>\n              <div class=\"form-body pal\">\n                <h3>ข้อมูลลูกค้า</h3>\n                <div class=\"row\">\n                  <div class=\"col-md-6\">\n                    <div class=\"form-group\"><label class=\"col-md-4 control-label\">ชื่อลูกค้า</label>\n                      <div class=\"col-md-6\" id=\"addCustomerInput\">\n                        <input name=\"nameCustomer\" placeholder=\"กรอกชื่อลูกค้า\" [(ngModel)]=\"customerSelected.name\" [typeahead]=\"customers\" [typeaheadItemTemplate]=\"customItemCustomerTemplate\"\n                          [typeaheadOptionField]=\"'name'\" (typeaheadNoResults)=\"changeTypeaheadNoResults(0,$event)\" (typeaheadOnSelect)=\"typeaheadOnSelect(0,$event)\"\n                          class=\"form-control\" formControlName=\"customer_id\" />\n                        <div *ngIf=\"typeaheadLoading\">\n                          <i class=\"fa fa-refresh ng-hide\"></i>\n                        </div>\n                        <div class=\"no-data\" *ngIf=\"typeaheadNoResults.customer\">\n                          <i class=\"fa fa-remove\"></i> ไม่พบข้อมูลลูกค้า\n                        </div>\n                      </div>\n                      <div class=\"col-md-2 addCustomerBtn\">\n                        <button type=\"button\" class=\"btn btn-success btn-outlined btn-sm\" (click)=\"openAddCustomerModal()\">เพิ่ม</button>\n                      </div>\n                    </div>\n                  </div>\n                  <div class=\"col-md-6\">\n                    <div class=\"form-group\">\n                      <label for=\"tel\" class=\"col-md-4 control-label\">เบอร์โทร<span class='require'>*</span></label>\n                      <div class=\"col-md-8\">\n                        <input type=\"text\" name=\"tel\" placeholder=\"เบอร์โทร\" class=\"inputUsername form-control\" formControlName=\"tel\" [(ngModel)]=\"customerSelected.tel\"\n                        />\n                        <p class=\"text-danger\" *ngIf=\"tel.hasError('required') && tel.dirty\">กรุณากรอกเบอร์โทรศัพท์</p>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n              </div>\n              <div class=\"action text-right\">\n                <button type=\"button\" name=\"previous\" value=\"Previous\" class=\"btn btn-primary btn-outlined button-previous disabled\">\n                  <i class=\"fa fa-arrow-circle-o-left mrx\"></i>ย้อนกลับ\n                </button>\n                <button type=\"button\" name=\"next\" value=\"Next\" class=\"btn btn-primary btn-outlined button-next\" *ngIf=\"!isLastTab\" (click)=\"setTab(0,'next')\"\n                  [ngClass]=\"{'disabled': !infoForm.valid}\" [disabled]=\"!infoForm.valid\">\n                  ถัดไป<i class=\"fa fa-arrow-circle-o-right mlx\"></i>\n                </button>\n              </div>\n            </form>\n          </div>\n          <div id=\"tab-2-product\" class=\"tab-pane fadeIn\" [ngClass]=\"{'active': selectedTab === 2}\">\n            <form [formGroup]=\"productForm\" class=\"form-horizontal\">\n              <div class=\"form-body pal\">\n                <h3>สินค้าที่ต้องการซ่อม</h3>\n                <div class=\"row\">\n                  <div class=\"col-md-6\">\n                    <div class=\"form-group\">\n                      <label for=\"productSelected\" class=\"col-md-4 control-label\">ค้นหาสินค้า</label>\n                      <div class=\"col-md-8\" id=\"addCustomerInput\">\n                        <input name=\"productSelected\" placeholder=\"กรุณากรอก barcode\" [(ngModel)]=\"productSelected.name\" [typeahead]=\"products\" [typeaheadItemTemplate]=\"customItemProductTemplate\"\n                          [typeaheadOptionField]=\"'code'\" (typeaheadNoResults)=\"changeTypeaheadNoResults(1,$event)\" (typeaheadOnSelect)=\"typeaheadOnSelect(1,$event)\"\n                          class=\"form-control\" formControlName=\"product_id\" />\n                        <div *ngIf=\"typeaheadLoading\">\n                          <i class=\"fa fa-refresh ng-hide\"></i>\n                        </div>\n                        <div class=\"no-data\" *ngIf=\"typeaheadNoResults.product\">\n                          <i class=\"fa fa-remove\"></i> ไม่พบสินค้า\n                        </div>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n              </div>\n              <div class=\"form-body pal\">\n                <h3>ข้อมูลสินค้า</h3>\n                <div class=\"row\">\n                  <div class=\"col-md-6\">\n                    <div class=\"form-group\">\n                      <label for=\"item_brand\" class=\"col-md-4 control-label\">ยี่ห้อ<span class='require'>*</span></label>\n                      <div class=\"col-md-8\">\n                        <input type=\"text\" placeholder=\"ยี่ห้อ\" class=\"inputUsername form-control\" name=\"item_brand\" formControlName=\"item_brand\"\n                        />\n                      </div>\n                    </div>\n                  </div>\n                  <div class=\"col-md-6\">\n                    <div class=\"form-group\">\n                      <label for=\"item_generation\" class=\"col-md-4 control-label\">รุ่น<span class='require'>*</span></label>\n                      <div class=\"col-md-8\">\n                        <input type=\"text\" placeholder=\"รุ่น\" class=\"inputUsername form-control\" name=\"item_generation\" formControlName=\"item_generation\"\n                        />\n                      </div>\n                    </div>\n                  </div>\n                </div>\n                <div class=\"row\">\n                  <div class=\"col-md-6\">\n                    <div class=\"form-group\">\n                      <label for=\"item_color\" class=\"col-md-4 control-label\">สี<span class='require'>*</span></label>\n                      <div class=\"col-md-8\">\n                        <input type=\"text\" placeholder=\"สี\" class=\"inputUsername form-control\" name=\"item_color\" formControlName=\"item_color\" />\n                      </div>\n                    </div>\n                  </div>\n                  <div class=\"col-md-6\">\n                    <div class=\"form-group\">\n                      <label for=\"item_imei\" class=\"col-md-4 control-label\">IMEI<span class='require'>*</span></label>\n                      <div class=\"col-md-8\">\n                        <input type=\"text\" placeholder=\"IMEI\" class=\"inputUsername form-control\" name=\"item_imei\" formControlName=\"item_imei\" [(ngModel)]=\"productSelected.code\"\n                        />\n                      </div>\n                    </div>\n                  </div>\n                </div>\n                <div class=\"row\">\n                  <div class=\"col-md-6\">\n                    <div class=\"form-group\">\n                      <label for=\"item_blame\" class=\"col-md-4 control-label\">ตำหนิตัวเครื่อง<span class='require'>*</span></label>\n                      <div class=\"col-md-8\">\n                        <textarea id=\"inputDetail\" rows=\"5\" class=\"form-control\" name=\"item_blame\" formControlName=\"item_blame\"></textarea>\n                      </div>\n                    </div>\n                  </div>\n                  <div class=\"col-md-6\">\n                    <div class=\"form-group\">\n                      <label for=\"inputDetail\" class=\"col-md-4 control-label\">Sim card<span class='require'>*</span></label>\n                      <div class=\"col-md-8 radioButtonCustom\">\n                        <div>\n                          <label class=\"btn active\">\n                            <input type=\"radio\" name=\"item_has_sim\" [value]=\"1\" formControlName=\"item_has_sim\" checked/>\n                            <i class=\"fa fa-circle-o notCheck\"></i>\n                            <i class=\"fa fa-check-circle-o selected\"></i>\n                            <span> มี</span>\n                          </label>\n                          <label class=\"btn\">\n                            <input type=\"radio\" name=\"item_has_sim\" [value]=\"0\" formControlName=\"item_has_sim\"/>\n                            <i class=\"fa fa-circle-o notCheck\"></i>\n                            <i class=\"fa fa-check-circle-o selected\"></i>\n                            <span> ไม่มี</span>\n                          </label>\n                        </div>\n                      </div>\n                    </div>\n                    <div class=\"form-group\">\n                      <label for=\"inputDetail\" class=\"col-md-4 control-label\">Memory card<span class='require'>*</span></label>\n                      <div class=\"col-md-8 radioButtonCustom\">\n                        <div>\n                          <label class=\"btn active\">\n                          <input type=\"radio\" name=\"item_has_mem\" [value]=\"1\" formControlName=\"item_has_mem\" checked />\n                          <i class=\"fa fa-circle-o notCheck\"></i>\n                          <i class=\"fa fa-check-circle-o selected\"></i>\n                          <span> มี</span>\n                        </label>\n                          <label class=\"btn\">\n                          <input type=\"radio\" name=\"item_has_mem\" [value]=\"0\" formControlName=\"item_has_mem\" />\n                          <i class=\"fa fa-circle-o notCheck\"></i>\n                          <i class=\"fa fa-check-circle-o selected\"></i>\n                          <span> ไม่มี</span>\n                        </label>\n                        </div>\n                      </div>\n                    </div>\n                    <div class=\"form-group\">\n                      <label for=\"inputDetail\" class=\"col-md-4 control-label\">Battery<span class='require'>*</span></label>\n                      <div class=\"col-md-8 radioButtonCustom\">\n                        <div>\n                          <label class=\"btn active\">\n                          <input type=\"radio\" name=\"item_has_battery\" [value]=\"1\" formControlName=\"item_has_battery\" checked/>\n                          <i class=\"fa fa-circle-o notCheck\"></i>\n                          <i class=\"fa fa-check-circle-o selected\"></i>\n                          <span> มี</span>\n                        </label>\n                          <label class=\"btn\">\n                          <input type=\"radio\" name=\"item_has_battery\" [value]=\"0\" formControlName=\"item_has_battery\"/>\n                          <i class=\"fa fa-circle-o notCheck\"></i>\n                          <i class=\"fa fa-check-circle-o selected\"></i>\n                          <span> ไม่มี</span>\n                        </label>\n                        </div>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n              </div>\n              <div class=\"action text-right\">\n                <button type=\"button\" name=\"previous\" value=\"Previous\" class=\"btn btn-primary btn-outlined button-previous\" (click)=\"setTab(0,'previous')\">\n                  <i class=\"fa fa-arrow-circle-o-left mrx\"></i>ย้อนกลับ\n                </button>\n                <button type=\"button\" name=\"next\" value=\"Next\" class=\"btn btn-primary btn-outlined button-next\" (click)=\"setTab(0,'next')\"\n                  [ngClass]=\"{'disabled': !productForm.valid}\" [disabled]=\"!productForm.valid\">\n                  ถัดไป<i class=\"fa fa-arrow-circle-o-right mlx\"></i>\n                </button>\n              </div>\n            </form>\n          </div>\n          <div id=\"tab-3-detail\" class=\"tab-pane fadeIn\" [ngClass]=\"{'active': selectedTab === 3}\">\n            <form [formGroup]=\"causeForm\" class=\"form-horizontal\">\n              <div class=\"form-body pal\">\n                <h3>อาการ</h3>\n                <div class=\"row\">\n                  <div class=\"col-md-6\">\n                    <div class=\"form-group broken\">\n                      <!--<label for=\"inputEmail\" class=\"col-md-3 control-label\">Email<span class='require'>*</span></label>-->\n                      <div class=\"col-md-11 col-md-offset-1 brokenInput\" *ngFor=\"let item of brokens; let i = index\">\n                        <div class=\"input-group\">\n                          <span class=\"input-group-addon\">{{i+1}}</span>\n                          <div class=\"input-icon right\">\n                            <input type=\"text\" placeholder=\"อาการ\" class=\"form-control\" [(ngModel)]=\"brokens[i].message\" [ngModelOptions]=\"{standalone: true}\"\n                            />\n                            <i class=\"fa fa-times\" (click)=deleteBrokens(i)></i>\n                          </div>\n                        </div>\n                      </div>\n                      <div class=\"col-md-11 col-md-offset-1\">\n                        <button type=\"button\" class=\"btn btn-green btn-outlined btn-block brokenAddBtn\" (click)=\"addBrokens()\"><i class=\"fa fa-plus\"></i>&nbsp;&nbsp;เพิ่มอาการ</button>\n                      </div>\n                    </div>\n                  </div>\n                  <div class=\"col-md-6\">\n                    <div class=\"form-group\">\n                      <label for=\"inputUsername\" class=\"col-md-4 control-label\">ราคาประเมิน <span class='require'>*</span></label>\n                      <div class=\"col-md-8\">\n                        <input type=\"text\" placeholder=\"00.00\" class=\"inputUsername form-control\" formControlName=\"estimate_price\" />\n                        <p class=\"text-danger\" *ngIf=\"estimate_price.errors?.min\">ราคาประเมินต้องมีค่ามากกว่า 0</p>\n                        <p class=\"text-danger\" *ngIf=\"estimate_price.errors?.number\">กรุณากรอกตัวเลข</p>\n                      </div>\n                    </div>\n                    <div class=\"form-group\"><label for=\"inputDetail\" class=\"col-md-4 control-label\">หมายเหตุ</label>\n                      <div class=\"col-md-8\">\n                        <textarea id=\"inputDetail\" rows=\"3\" class=\"form-control\" formControlName=\"cause_remark\"></textarea>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n              </div>\n              <div class=\"form-body pal\">\n                <h3>รายการซ่อม</h3>\n                <div class=\"row\">\n                  <div class=\"col-md-6\">\n                    <div class=\"form-group broken\">\n                      <div class=\"col-md-11 col-md-offset-1 brokenInput\" *ngFor=\"let item of fixs; let i = index\">\n                        <div class=\"input-group\">\n                          <span class=\"input-group-addon\">{{i+1}}</span>\n                          <div class=\"input-icon right\">\n                            <input type=\"text\" placeholder=\"รายการซ่อม\" class=\"form-control\" [(ngModel)]=\"fixs[i].message\" [ngModelOptions]=\"{standalone: true}\"\n                            />\n                            <i class=\"fa fa-times\" (click)=deleteFixs(i)></i>\n                          </div>\n                        </div>\n                      </div>\n                      <div class=\"col-md-11 col-md-offset-1\">\n                        <button type=\"button\" class=\"btn btn-green btn-outlined btn-block brokenAddBtn\" (click)=\"addFixs()\"><i class=\"fa fa-plus\"></i>&nbsp;&nbsp;เพิ่มรายการซ่อม</button>\n                      </div>\n                    </div>\n                  </div>\n                  <div class=\"col-md-6\">\n                    <div class=\"form-group\">\n                      <label for=\"inputUsername\" class=\"col-md-4 control-label\">ราคาซ่อมจริง</label>\n                      <div class=\"col-md-8\">\n                        <input type=\"text\" placeholder=\"00.00\" class=\"inputUsername form-control\" formControlName=\"repair_price\" />\n                        <p class=\"text-danger\" *ngIf=\"repair_price.errors?.min\">ราคาซ่อมจริงต้องมีค่ามากกว่า 0</p>\n                        <p class=\"text-danger\" *ngIf=\"repair_price.errors?.number\">กรุณากรอกตัวเลข</p>\n                      </div>\n                    </div>\n                    <div class=\"form-group\"><label for=\"inputDetail\" class=\"col-md-4 control-label\">หมายเหตุ</label>\n                      <div class=\"col-md-8\">\n                        <textarea id=\"inputDetail\" rows=\"3\" class=\"form-control\" formControlName=\"repair_remark\"></textarea>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n              </div>\n              <div class=\"form-body pal\">\n                <h3>ข้อตกลง</h3>\n                <div class=\"row\">\n                  <div class=\"col-md-6\">\n                    <div class=\"form-group broken\">\n                      <!--<label for=\"inputEmail\" class=\"col-md-3 control-label\">Email<span class='require'>*</span></label>-->\n                      <div class=\"col-md-11 col-md-offset-1 brokenInput\" *ngFor=\"let item of agreements; let i = index\">\n                        <div class=\"input-group\">\n                          <span class=\"input-group-addon\">{{i+1}}</span>\n                          <div class=\"input-icon right\">\n                            <input type=\"text\" placeholder=\"ข้อตกลง\" class=\"form-control\" [(ngModel)]=\"agreements[i].message\" [ngModelOptions]=\"{standalone: true}\"\n                            />\n                            <i class=\"fa fa-times\" (click)=deleteAgreements(i)></i>\n                          </div>\n                        </div>\n                      </div>\n                      <div class=\"col-md-11 col-md-offset-1\">\n                        <button type=\"button\" class=\"btn btn-green btn-outlined btn-block brokenAddBtn\" (click)=\"addAgreements()\"><i class=\"fa fa-plus\"></i>&nbsp;&nbsp;เพิ่มข้อตกลง</button>\n                      </div>\n                    </div>\n                  </div>\n                  <div class=\"col-md-6\">\n                    <div class=\"form-group\"><label for=\"inputDetail\" class=\"col-md-4 control-label\">หมายเหตุ</label>\n                      <div class=\"col-md-8\">\n                        <textarea id=\"inputDetail\" rows=\"3\" class=\"form-control\" formControlName=\"agreement_remark\"></textarea>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n              </div>\n              <div class=\"action text-right\">\n                <button type=\"button\" name=\"previous\" value=\"Previous\" class=\"btn btn-primary btn-outlined button-previous\" (click)=\"setTab(0,'previous')\">\n                  <i class=\"fa fa-arrow-circle-o-left mrx\"></i>ย้อนกลับ\n                </button>\n                <button type=\"button\" name=\"next\" value=\"Next\" class=\"btn btn-primary btn-outlined button-next\" (click)=\"setTab(0,'next')\"\n                  [ngClass]=\"{'disabled': !causeForm.valid}\" [disabled]=\"!causeForm.valid\">\n                  ถัดไป<i class=\"fa fa-arrow-circle-o-right mlx\"></i>\n                </button>\n              </div>\n            </form>\n          </div>\n          <div id=\"tab-4-employee\" class=\"tab-pane fadeIn\" [ngClass]=\"{'active': selectedTab === 4}\">\n            <form [formGroup]=\"employeeForm\" class=\"form-horizontal\">\n              <div class=\"form-body pal\">\n                <h3>ข้อมูลพนักงาน</h3>\n                <div class=\"row\">\n                  <div class=\"col-md-6\">\n                    <div class=\"form-group\">\n                      <label for=\"inputUsername\" class=\"col-md-4 control-label\">พนักงาน</label>\n                      <div class=\"col-md-8\">\n                        <input type=\"text\" placeholder=\"ชื่อ\" class=\"inputUsername form-control\" formControlName=\"staff_name\" />\n                      </div>\n                    </div>\n                  </div>\n                </div>\n              </div>\n              <div class=\"action text-right\">\n                <button type=\"button\" name=\"previous\" value=\"Previous\" class=\"btn btn-primary btn-outlined button-previous\" (click)=\"setTab(0,'previous')\">\n                  <i class=\"fa fa-arrow-circle-o-left mrx\"></i>ย้อนกลับ\n                </button>\n                <button type=\"button\" name=\"next\" value=\"Next\" class=\"btn btn-success btn-outlined button-next\" (click)=\"saveData()\" [ngClass]=\"{'disabled': !employeeForm.valid}\"\n                  [disabled]=\"!employeeForm.valid\">\n                  บันทึก<i class=\"fa fa-floppy-o mlx\"></i>\n                </button>\n              </div>\n            </form>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n<template #customItemCustomerTemplate let-model=\"item\" let-index=\"index\">\n  <div class=\"nameAutocomplete\">\n    <h5><strong>ชื่อ :: </strong> {{model.name}}</h5>\n    <h5><strong>เบอร์ :: </strong> {{model.tel}}</h5>\n  </div>\n</template>\n\n<template #customItemProductTemplate let-model=\"item\" let-index=\"index\">\n  <div class=\"nameAutocomplete\">\n    <h5><strong>barcode :: </strong> {{model.code}}</h5>\n    <h5><strong>ชื่อสินค้า :: </strong> {{model.name}}</h5>\n    <h5><strong>ประเภท :: </strong> {{model.product_type.name}}</h5>\n  </div>\n</template>\n\n<div class=\"modal fade\" bsModal #editModal=\"bs-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"mySmallModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-lg modalEditProduct\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title pull-left\">เพิ่มลูกค้า</h4>\n        <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"editModal.hide()\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <form [formGroup]=\"customerForm\" class=\"form-horizontal\" (ngSubmit)=\"saveCustomer()\">\n        <div class=\"modal-body\">\n          <div class=\"form-body pal\">\n            <div class=\"form-group\">\n              <label for=\"name_customer\" class=\"col-md-3 control-label\">ชื่อ<span class='require'>*</span></label>\n              <div class=\"col-md-5\">\n                <div class=\"input-icon right\">\n                  <input type=\"text\" id=\"name_customer\" name=\"name_customer\" class=\"form-control\" formControlName=\"name_customer\" />\n                  <p class=\"text-danger\" *ngIf=\"name_customer.hasError('required') && name_customer.dirty\">กรุณากรอกชื่อ</p>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <label for=\"tel_customer\" class=\"col-md-3 control-label\">เบอร์ติดต่อ<span class='require'>*</span></label>\n              <div class=\"col-md-5\">\n                <div class=\"input-icon right\">\n                  <input type=\"tel\" id=\"tel_customer\" name=\"tel_customer\" class=\"form-control\" formControlName=\"tel_customer\" />\n                  <p class=\"text-danger\" *ngIf=\"tel_customer.hasError('required') && tel_customer.dirty\">กรุณากรอกเบอร์ติดต่อ</p>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"editModal.hide()\">ยกเลิก</button>\n          <button type=\"submit\" class=\"btn btn-success btn-square\" [disabled]=\"!customerForm.valid\">บันทึก</button>\n        </div>\n      </form>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ 1218:
/***/ (function(module, exports) {

module.exports = "<!-- title -->\n<div id=\"title-breadcrumb-option-demo\" class=\"page-title-breadcrumb\">\n  <div class=\"page-header pull-left\">\n    <div class=\"page-title\">{{Heading}}</div>\n  </div>\n  <ol class=\"breadcrumb page-breadcrumb\">\n    <li>\n      <i class=\"fa fa-home\"></i>&nbsp;\n      <a [routerLink]=\"['/dashboard']\">Home</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n      <a [routerLink]=\"['/claim/change/list']\">รายการซ่อมสินค้า</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n    </li>\n    <li class=\"active\">{{Heading}}</li>\n  </ol>\n</div>\n\n<!-- content -->\n<div class=\"page-content\" id=\"change-info\">\n  <div id=\"changeInfoContent\">\n    <app-loading *ngIf=\"isLoading\"></app-loading>\n    <div class=\"row\" *ngIf=\"!isLoading\">\n      <div class=\"col-md-6\">\n        <div class=\"portlet box portlet-primary information\">\n          <div class=\"portlet-header\">\n            <div class=\"caption\">ข้อมูลทั่วไป</div>\n            <div class=\"tools\">\n              <i class=\"fa fa-pencil\" (click)=\"setEdit('info')\"></i>\n            </div>\n          </div>\n          <div class=\"portlet-body\">\n            <div class=\"row form-horizontal\">\n              <div class=\"col-md-12\">\n                <div class=\"form-group\">\n                  <label for=\"code\" class=\"col-md-3 control-label\">\n                  รหัสซ่อม <span *ngIf=\"!edit.info\">:</span><span class='require' *ngIf=\"edit.info\">*</span></label>\n                  <div class=\"col-md-8\">\n                    <input type=\"text\" placeholder=\"รหัสซ่อม\" class=\"inputUsername form-control\" name=\"code\" id=\"code\" [(ngModel)]=\"data.code\"\n                      [readonly]=\"!edit.info\" />\n                  </div>\n                </div>\n              </div>\n              <div class=\"col-md-12\">\n                <div class=\"form-group\">\n                  <label for=\"repair_date\" class=\"col-md-3 control-label\">\n                  วันที่รับซ่อม <span *ngIf=\"!edit.info\">:</span><span class='require' *ngIf=\"edit.info\">*</span></label>\n                  <div class=\"col-md-8\">\n                    <div class=\"input-group\" *ngIf=\"edit.info\">\n                      <input type=\"text\" data-date-format=\"yyyy-mm-dd\" placeholder=\"yyyy-mm-dd\" class=\"datepicker-default form-control\" ngbDatepicker\n                        #d=\"ngbDatepicker\" (click)=\"d.toggle()\" [(ngModel)]=\"data.dateFormat\" />\n                      <div class=\"input-group-addon\" (click)=\"d.toggle()\"><i class=\"fa fa-calendar\"></i></div>\n\n                      <!--<input type=\"text\" name=\"repair_date\" class=\"datepicker-default form-control\" ngbDatepicker #date=\"ngbDatepicker\" (click)=\"date.toggle()\"\n                        [(ngModel)]=\"data.dateFormat\" />\n                      <div class=\"input-group-addon\" (click)=\"date.toggle()\"><i class=\"fa fa-calendar\"></i></div>-->\n                    </div>\n                    <input type=\"text\" class=\"inputUsername form-control\" name=\"repair_date\" id=\"repair_date\" [(ngModel)]=\"data.repair_date\"\n                      [readonly]=\"!edit.info\" *ngIf=\"!edit.info\" />\n                  </div>\n                </div>\n              </div>\n              <div class=\"col-md-12\">\n                <div class=\"form-group\">\n                  <label for=\"customerName\" class=\"col-md-3 control-label\">\n                ชื่อลูกค้า <span *ngIf=\"!edit.info\">:</span><span class='require' *ngIf=\"edit.info\">*</span></label>\n                  <div *ngIf=\"edit.info\" class=\"col-md-8 inputTypeahead\">\n                    <div class=\"col-md-9 divInputAhead\" id=\"addCustomerInput\">\n                      <input name=\"nameCustomer\" placeholder=\"กรอกชื่อลูกค้า\" [(ngModel)]=\"customerSelected.name\" [typeahead]=\"customers\" [typeaheadItemTemplate]=\"customItemCustomerTemplate\"\n                        [typeaheadOptionField]=\"'name'\" (typeaheadNoResults)=\"changeTypeaheadNoResults($event)\" (typeaheadOnSelect)=\"typeaheadOnSelect(0,$event)\"\n                        class=\"form-control\" />\n                      <div *ngIf=\"typeaheadLoading===true\">\n                        <i class=\"fa fa-refresh ng-hide\"></i>\n                      </div>\n                    </div>\n                    <div class=\"col-md-3 addCustomerBtn divButtonAhead\">\n                      <button type=\"button\" class=\"btn btn-success btn-outlined btn-sm\" (click)=\"openModal(addCustomer)\">เพิ่ม</button>\n                    </div>\n                  </div>\n                  <div class=\"col-md-8\" *ngIf=\"!edit.info\">\n                    <input type=\"text\" class=\"inputUsername form-control\" name=\"customerName\" id=\"customerName\" [(ngModel)]=\"data.customer.name\"\n                      [readonly]=\"!edit.info\" />\n                  </div>\n                </div>\n\n                <div class=\"no-data col-md-8 col-md-offset-3\" *ngIf=\"typeaheadNoResults===true\">\n                  <i class=\"fa fa-remove\"></i> ไม่พบข้อมูลลูกค้า\n                </div>\n              </div>\n              <div class=\"col-md-12\">\n                <div class=\"form-group\">\n                  <label for=\"tel\" class=\"col-md-3 control-label\">\n                  เบอร์โทร <span *ngIf=\"!edit.info\">:</span><span class='require' *ngIf=\"edit.info\">*</span></label>\n                  <div class=\"col-md-8\">\n                    <input type=\"text\" placeholder=\"เบอร์โทร\" class=\"inputUsername form-control\" name=\"tel\" id=\"tel\" [(ngModel)]=\"data.tel\" [readonly]=\"!edit.info\"\n                    />\n                  </div>\n                </div>\n              </div>\n              <div class=\"col-md-12 btnAction\" *ngIf=\"edit.info\">\n                <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"cancleEdit()\">ยกเลิก</button>\n                <button type=\"button\" class=\"btn btn-success btn-square\" (click)=\"editData()\">บันทึก</button>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"portlet box portlet-primary broken\">\n          <div class=\"portlet-header\">\n            <div class=\"caption\">อาการ</div>\n            <div class=\"tools\">\n              <i class=\"fa fa-pencil\" (click)=\"setEdit('broken')\"></i>\n            </div>\n          </div>\n          <div class=\"portlet-body\">\n            <div class=\"row form-horizontal\">\n              <div class=\"col-md-12\">\n                <div class=\"form-group\">\n                  <label for=\"estimate_price\" class=\"col-md-3 control-label\">\n                  ราคาประเมิน <span *ngIf=\"!edit.broken\">:</span></label>\n                  <div class=\"col-md-8\">\n                    <input type=\"text\" placeholder=\"ราคาประเมิน\" class=\"inputUsername form-control\" name=\"estimate_price\" id=\"estimate_price\"\n                      [(ngModel)]=\"data.estimate_price\" [readonly]=\"!edit.broken\" />\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"row\">\n              <div class=\"col-md-12\">\n                <div class=\"form-group broken\">\n                  <div class=\"col-md-12 brokenInput\" *ngFor=\"let item of data.broken; let i = index\">\n                    <div class=\"input-group\">\n                      <span class=\"input-group-addon\">{{i+1}}</span>\n                      <input type=\"text\" placeholder=\"อาการ\" class=\"form-control\" [(ngModel)]=\"data.broken[i].message\" [readonly]=\"!edit.broken\"\n                      />\n                    </div>\n                  </div>\n                  <div class=\"col-md-12\" *ngIf=\"edit.broken\">\n                    <button type=\"button\" class=\"btn btn-green btn-outlined btn-block brokenAddBtn\" (click)=\"addBrokens()\"><i class=\"fa fa-plus\"></i>&nbsp;&nbsp;เพิ่มอาการ</button>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"extra\">\n              <div class=\"row form-horizontal\">\n                <div class=\"col-md-12\">\n                  <div class=\"form-group\">\n                    <label for=\"cause_remark\" class=\"col-md-3 control-label\">หมายเหตุ <span *ngIf=\"!edit.broken\">:</span></label>\n                    <div class=\"col-md-8\">\n                      <textarea id=\"extra\" rows=\"3\" class=\"form-control\" [(ngModel)]=\"data.cause_remark\" name=\"cause_remark\" *ngIf=\"edit.broken\"></textarea>\n                      <p *ngIf=\"!edit.broken\">{{data.cause_remark}}</p>\n                    </div>\n                  </div>\n                  <div class=\"col-md-12 btnAction\" *ngIf=\"edit.broken\">\n                    <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"cancleEdit()\">ยกเลิก</button>\n                    <button type=\"button\" class=\"btn btn-success btn-square\" (click)=\"editData()\">บันทึก</button>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"portlet box portlet-primary fix\">\n          <div class=\"portlet-header\">\n            <div class=\"caption\">รายการซ่อม</div>\n            <div class=\"tools\">\n              <i class=\"fa fa-pencil\" (click)=\"setEdit('fix')\"></i>\n            </div>\n          </div>\n          <div class=\"portlet-body\">\n            <div class=\"row form-horizontal\">\n              <div class=\"col-md-12\">\n                <div class=\"form-group\">\n                  <label for=\"repair_price\" class=\"col-md-3 control-label\">\n                  ราคาซ่อมจริง <span *ngIf=\"!edit.fix\">:</span></label>\n                  <div class=\"col-md-8\">\n                    <input type=\"text\" placeholder=\"ราคาซ่อมจริง\" class=\"inputUsername form-control\" name=\"repair_price\" id=\"repair_price\" [(ngModel)]=\"data.repair_price\"\n                      [readonly]=\"!edit.fix\" />\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"row\">\n              <div class=\"col-md-12\">\n                <div class=\"form-group fix\">\n                  <div class=\"col-md-12 brokenInput\" *ngFor=\"let item of data.fix; let i = index\">\n                    <div class=\"input-group\">\n                      <span class=\"input-group-addon\">{{i+1}}</span>\n                      <input type=\"text\" placeholder=\"รายการซ่อม\" class=\"form-control\" [(ngModel)]=\"data.fix[i].message\" [readonly]=\"!edit.fix\"\n                      />\n                    </div>\n                  </div>\n                  <div class=\"col-md-12\" *ngIf=\"edit.fix\">\n                    <button type=\"button\" class=\"btn btn-green btn-outlined btn-block fixAddBtn\" (click)=\"addFixs()\"><i class=\"fa fa-plus\"></i>&nbsp;&nbsp;เพิ่มรายการซ่อม</button>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"extra\">\n              <div class=\"row form-horizontal\">\n                <div class=\"col-md-12\">\n                  <div class=\"form-group\">\n                    <label for=\"repair_remark\" class=\"col-md-3 control-label\">หมายเหตุ <span *ngIf=\"!edit.fix\">:</span></label>\n                    <div class=\"col-md-8\">\n                      <textarea id=\"repair_remark\" rows=\"3\" class=\"form-control\" [(ngModel)]=\"data.repair_remark\" name=\"repair_remark\" *ngIf=\"edit.fix\"></textarea>\n                      <p *ngIf=\"!edit.fix\">{{data.repair_remark}}</p>\n                    </div>\n                  </div>\n                  <div class=\"col-md-12 btnAction\" *ngIf=\"edit.fix\">\n                    <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"cancleEdit()\">ยกเลิก</button>\n                    <button type=\"button\" class=\"btn btn-success btn-square\" (click)=\"editData()\">บันทึก</button>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"col-md-6 \">\n        <div class=\"row\">\n          <div class=\"col-md-7\">\n            <div class=\"portlet box portlet-primary status\">\n              <div class=\"portlet-header\">\n                <div class=\"caption\">สถานะ</div>\n                <div class=\"tools\">\n                  <i class=\"fa fa-pencil\" (click)=\"setEdit('status')\"></i>\n                </div>\n              </div>\n              <div class=\"portlet-body \">\n                <div class=\"statusText\" [ngSwitch]=\"data.status\">\n                  <h4 class=\"statusShow text-blue\" *ngSwitchCase=\"1\">รับเรื่อง</h4>\n                  <h4 class=\"statusShow text-yellow\" *ngSwitchCase=\"2\">กำลังซ่อม</h4>\n                  <h4 class=\"statusShow text-green\" *ngSwitchCase=\"3\">สำเร็จ</h4>\n                  <h4 class=\"statusShow text-red\" *ngSwitchCase=\"4\">ข้อผิดพลาด</h4>\n                </div>\n                <div class=\"statusEdit\" *ngIf=\"edit.status\">\n                  <div class=\"row form-horizontal\">\n                    <div class=\"col-md-12\">\n                      <div class=\"form-group\">\n                        <label for=\"status\" class=\"col-md-2 control-label\">\n                  สถานะ <span *ngIf=\"!edit.status\">:</span></label>\n                        <div class=\"col-md-6\">\n                          <select [(ngModel)]=\"data.status\" class=\"selectCenter\" name=\"status\" style=\"width:100%\">\n                          <option *ngFor=\"let status of statusText\" [value]=\"status.id\">{{status.message}}</option>\n                      </select>\n                        </div>\n                      </div>\n                      <div class=\"col-md-12 btnAction\">\n                        <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"cancleEdit()\">ยกเลิก</button>\n                        <button type=\"button\" class=\"btn btn-success btn-square\" (click)=\"editData()\">บันทึก</button>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n          <div class=\"col-md-5\">\n            <div class=\"portlet box portlet-primary action\">\n              <div class=\"portlet-header\">\n                <div class=\"caption\">Action</div>\n              </div>\n              <div class=\"portlet-body\">\n                <button type=\"button\" class=\"btn btn-violet btn-outlined\" (click)=\"openPagePrint()\"><i class=\"fa fa-print\"></i> พิมพ์ใบซ่อม</button>\n                <button type=\"button\" class=\"btn btn-danger btn-outlined\"><i class=\"fa fa-trash\"></i> ลบใบซ่อม</button>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"portlet box portlet-primary productOriginal\">\n          <div class=\"portlet-header\">\n            <div class=\"caption\">สินค้าที่นำมาซ่อม</div>\n            <div class=\"tools\">\n              <i class=\"fa fa-pencil\" (click)=\"setEdit('product')\"></i>\n            </div>\n          </div>\n          <div class=\"portlet-body\">\n            <div class=\"row form-horizontal\">\n              <div class=\"col-md-12\">\n                <div class=\"form-group\">\n                  <label for=\"product_imei\" class=\"col-md-4 control-label\">\n                  IMEI <span *ngIf=\"!edit.product\">:</span></label>\n                  <div class=\"col-md-8\">\n                    <input type=\"text\" placeholder=\"รหัสสินค้า\" class=\"inputUsername form-control\" name=\"product_imei\" id=\"product_imei\" [(ngModel)]=\"data.item_imei\"\n                      readonly />\n                  </div>\n                </div>\n              </div>\n              <div class=\"col-md-12\">\n                <div class=\"form-group\">\n                  <label for=\"product_brand\" class=\"col-md-4 control-label\">\n                  ยี่ห้อ <span *ngIf=\"!edit.product\">:</span><span class='require' *ngIf=\"edit.product\">*</span></label>\n                  <div class=\"col-md-8\">\n                    <input type=\"text\" placeholder=\"ยี่ห้อ\" class=\"inputUsername form-control\" name=\"product_brand\" id=\"product_brand\" [(ngModel)]=\"data.item_brand\"\n                      [readonly]=\"!edit.product\" />\n                  </div>\n                </div>\n              </div>\n              <div class=\"col-md-12\">\n                <div class=\"form-group\">\n                  <label for=\"product_generation\" class=\"col-md-4 control-label\">\n                  รุ่น <span *ngIf=\"!edit.product\">:</span><span class='require' *ngIf=\"edit.product\">*</span></label>\n                  <div class=\"col-md-8\">\n                    <input type=\"text\" placeholder=\"รุ่น\" class=\"inputUsername form-control\" name=\"product_generation\" id=\"product_generation\"\n                      [(ngModel)]=\"data.item_generation\" [readonly]=\"!edit.product\" />\n                  </div>\n                </div>\n              </div>\n              <div class=\"col-md-12\">\n                <div class=\"form-group\">\n                  <label for=\"product_color\" class=\"col-md-4 control-label\">\n                  สี <span *ngIf=\"!edit.product\">:</span><span class='require' *ngIf=\"edit.product\">*</span></label>\n                  <div class=\"col-md-8\">\n                    <input type=\"text\" placeholder=\"สี\" class=\"inputUsername form-control\" name=\"product_color\" id=\"product_color\" [(ngModel)]=\"data.item_color\"\n                      [readonly]=\"!edit.product\" />\n                  </div>\n                </div>\n              </div>\n              <div class=\"col-md-12\">\n                <div class=\"form-group\">\n                  <label for=\"product_blame\" class=\"col-md-4 control-label\">\n                  ตำหนิตัวเครื่อง <span *ngIf=\"!edit.product\">:</span><span class='require' *ngIf=\"edit.product\">*</span></label>\n                  <div class=\"col-md-8\">\n                    <textarea id=\"product_blame\" rows=\"3\" class=\"inputUsername form-control\" [(ngModel)]=\"data.item_blame\" name=\"product_blame\"\n                      *ngIf=\"edit.product\"></textarea>\n                    <p *ngIf=\"!edit.product\">{{data.item_blame}}</p>\n                  </div>\n                </div>\n              </div>\n              <div class=\"col-md-12\">\n                <!--<form [formGroup]=\"productHasItem\" class=\"form-horizontal\">-->\n                  <div class=\"form-group\">\n                    <label for=\"inputDetail\" class=\"col-md-4 control-label\">Sim card :<span class='require' *ngIf=\"edit.product\">*</span></label>\n                    <div class=\"col-md-8 radioButtonCustom\">\n                      <div *ngIf=\"edit.product\">\n                        <label class=\"btn active\">\n                          <input type=\"radio\" name=\"item_has_sim\" [value]=\"1\" [(ngModel)]=\"data.item_has_sim\" [checked]=\"data.item_has_sim === '1'\" [ngModelOptions]=\"{standalone: true}\"/>\n                          <i class=\"fa fa-circle-o notCheck\"></i>\n                          <i class=\"fa fa-check-circle-o selected\"></i>\n                          <span> มี</span>\n                        </label>\n                        <label class=\"btn\">\n                          <input type=\"radio\" name=\"item_has_sim\" [value]=\"0\" [(ngModel)]=\"data.item_has_sim\" [checked]=\"data.item_has_sim === '0'\" [ngModelOptions]=\"{standalone: true}\"/>\n                          <i class=\"fa fa-circle-o notCheck\"></i>\n                          <i class=\"fa fa-check-circle-o selected\"></i>\n                          <span> ไม่มี</span>\n                        </label>\n                      </div>\n                      <div *ngIf=\"!edit.product\">\n                        <label class=\"btn active\">\n                          <span *ngIf=\"!edit.product\"> \n                            <i class=\"fa fa-circle-o\" *ngIf=\"data.item_has_sim === 0\"></i>\n                            <i class=\"fa fa-check-circle-o\" *ngIf=\"data.item_has_sim === 1\"></i>\n                          </span>\n                          <span> มี</span>\n                        </label>\n                        <label class=\"btn\">\n                          <span *ngIf=\"!edit.product\"> \n                            <i class=\"fa fa-circle-o\" *ngIf=\"data.item_has_sim === 1\"></i>\n                            <i class=\"fa fa-check-circle-o\" *ngIf=\"data.item_has_sim === 0\"></i>\n                          </span>\n                          <span> ไม่มี</span>\n                        </label>\n                      </div>\n                    </div>\n                  </div>\n                  <div class=\"form-group\">\n                    <label for=\"item_has_mem\" class=\"col-md-4 control-label\">Memory card :<span class='require' *ngIf=\"edit.product\">*</span></label>\n                    <div class=\"col-md-8 radioButtonCustom\">\n                      <div *ngIf=\"edit.product\">\n                        <label class=\"btn active\">\n                          <input type=\"radio\" name=\"item_has_mem\" [value]=\"1\" [(ngModel)]=\"data.item_has_mem\" [checked]=\"data.item_has_mem === '1'\" [ngModelOptions]=\"{standalone: true}\"/>\n                          <i class=\"fa fa-circle-o notCheck\"></i>\n                          <i class=\"fa fa-check-circle-o selected\"></i>\n                          <span> มี</span>\n                        </label>\n                        <label class=\"btn\">\n                          <input type=\"radio\" name=\"item_has_mem\" [value]=\"0\" [(ngModel)]=\"data.item_has_mem\" [checked]=\"data.item_has_mem === '0'\" [ngModelOptions]=\"{standalone: true}\"/>\n                          <i class=\"fa fa-circle-o notCheck\"></i>\n                          <i class=\"fa fa-check-circle-o selected\"></i>\n                          <span> ไม่มี</span>\n                        </label>\n                      </div>\n                      <div *ngIf=\"!edit.product\">\n                        <label class=\"btn active\">\n                          <span *ngIf=\"!edit.product\"> \n                            <i class=\"fa fa-circle-o\" *ngIf=\"data.item_has_mem === 0\"></i>\n                            <i class=\"fa fa-check-circle-o\" *ngIf=\"data.item_has_mem === 1\"></i>\n                          </span>\n                          <span> มี</span>\n                        </label>\n                        <label class=\"btn\">\n                          <span *ngIf=\"!edit.product\"> \n                            <i class=\"fa fa-circle-o\" *ngIf=\"data.item_has_mem === 1\"></i>\n                            <i class=\"fa fa-check-circle-o\" *ngIf=\"data.item_has_mem === 0\"></i>\n                          </span>\n                          <span> ไม่มี</span>\n                        </label>\n                      </div>\n                    </div>\n                  </div>\n                  <div class=\"form-group\">\n                    <label for=\"item_has_battery\" class=\"col-md-4 control-label\">Battery :<span class='require' *ngIf=\"edit.product\">*</span></label>\n                    <div class=\"col-md-8 radioButtonCustom\">\n                      <div *ngIf=\"edit.product\">\n                        <label class=\"btn active\">\n                          <input type=\"radio\" name=\"item_has_battery\" [value]=\"1\" [(ngModel)]=\"data.item_has_battery\" [checked]=\"data.item_has_battery === '1'\" [ngModelOptions]=\"{standalone: true}\"/>\n                          <i class=\"fa fa-circle-o notCheck\"></i>\n                          <i class=\"fa fa-check-circle-o selected\"></i>\n                          <span> มี</span>\n                        </label>\n                        <label class=\"btn\">\n                          <input type=\"radio\" name=\"item_has_battery\" [value]=\"0\" [(ngModel)]=\"data.item_has_battery\" [checked]=\"data.item_has_battery === '0'\" [ngModelOptions]=\"{standalone: true}\"/>\n                          <i class=\"fa fa-circle-o notCheck\"></i>\n                          <i class=\"fa fa-check-circle-o selected\"></i>\n                          <span> ไม่มี</span>\n                        </label>\n                      </div>\n                      <div *ngIf=\"!edit.product\">\n                        <label class=\"btn active\">\n                          <span *ngIf=\"!edit.product\"> \n                            <i class=\"fa fa-circle-o\" *ngIf=\"data.item_has_battery === 0\"></i>\n                            <i class=\"fa fa-check-circle-o\" *ngIf=\"data.item_has_battery === 1\"></i>\n                          </span>\n                          <span> มี</span>\n                        </label>\n                        <label class=\"btn\">\n                          <span *ngIf=\"!edit.product\"> \n                            <i class=\"fa fa-circle-o\" *ngIf=\"data.item_has_battery === 1\"></i>\n                            <i class=\"fa fa-check-circle-o\" *ngIf=\"data.item_has_battery === 0\"></i>\n                          </span>\n                          <span> ไม่มี</span>\n                        </label>\n                      </div>\n                    </div>\n                  </div>\n                <!--</form>-->\n              </div>\n              <div class=\"col-md-12 btnAction\" *ngIf=\"edit.product\">\n                <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"cancleEdit()\">ยกเลิก</button>\n                <button type=\"button\" class=\"btn btn-success btn-square\" (click)=\"editData()\">บันทึก</button>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"portlet box portlet-primary agreement\">\n          <div class=\"portlet-header\">\n            <div class=\"caption\">ข้อตกลง</div>\n            <div class=\"tools\">\n              <i class=\"fa fa-pencil\" (click)=\"setEdit('agreement')\"></i>\n            </div>\n          </div>\n          <div class=\"portlet-body\">\n            <div class=\"row\">\n              <div class=\"col-md-12\">\n                <div class=\"form-group fix\">\n                  <div class=\"col-md-12 brokenInput\" *ngFor=\"let item of data.agreement; let i = index\">\n                    <div class=\"input-group\">\n                      <span class=\"input-group-addon\">{{i+1}}</span>\n                      <input type=\"text\" placeholder=\"ข้อตกลง\" class=\"form-control\" [(ngModel)]=\"data.agreement[i].message\" [readonly]=\"!edit.agreement\"\n                      />\n                    </div>\n                  </div>\n                  <div class=\"col-md-12\" *ngIf=\"edit.agreement\">\n                    <button type=\"button\" class=\"btn btn-green btn-outlined btn-block agreementAddBtn\" (click)=\"addAgreements()\"><i class=\"fa fa-plus\"></i>&nbsp;&nbsp;เพิ่มข้อตกลง</button>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"extra\">\n              <div class=\"row form-horizontal\">\n                <div class=\"col-md-12\">\n                  <div class=\"form-group\">\n                    <label for=\"agreement_remark\" class=\"col-md-3 control-label\">หมายเหตุ <span *ngIf=\"!edit.agreement\">:</span></label>\n                    <div class=\"col-md-8\">\n                      <textarea id=\"agreement_remark\" rows=\"3\" class=\"form-control\" [(ngModel)]=\"data.agreement_remark\" name=\"agreement_remark\"\n                        *ngIf=\"edit.agreement\"></textarea>\n                      <p *ngIf=\"!edit.agreement\">{{data.agreement_remark}}</p>\n                    </div>\n                  </div>\n                  <div class=\"col-md-12 btnAction\" *ngIf=\"edit.agreement\">\n                    <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"cancleEdit()\">ยกเลิก</button>\n                    <button type=\"button\" class=\"btn btn-success btn-square\" (click)=\"editData()\">บันทึก</button>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n<template #customItemCustomerTemplate let-model=\"item\" let-index=\"index\">\n  <div class=\"nameAutocomplete\">\n    <h5><strong>ชื่อ :: </strong> {{model.name}}</h5>\n    <h5><strong>เบอร์ :: </strong> {{model.tel}}</h5>\n  </div>\n</template>\n\n<template #customItemProductTemplate let-model=\"item\" let-index=\"index\">\n  <div class=\"nameAutocomplete\">\n    <h5><strong>barcode :: </strong> {{model.barcode}}</h5>\n    <h5><strong>ชื่อสินค้า :: </strong> {{model.name}}</h5>\n    <h5><strong>ประเภท :: </strong> {{model.type}}</h5>\n  </div>\n</template>"

/***/ }),

/***/ 1219:
/***/ (function(module, exports) {

module.exports = "<!-- title -->\n<div id=\"title-breadcrumb-option-demo\" class=\"page-title-breadcrumb\">\n  <div class=\"page-header pull-left\">\n    <div class=\"page-title\">{{Heading}}</div>\n  </div>\n  <ol class=\"breadcrumb page-breadcrumb\">\n    <li>\n      <i class=\"fa fa-home\"></i>&nbsp;\n      <a [routerLink]=\"['/dashboard']\">Home</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n    </li>\n    <li class=\"active\">{{Heading}}</li>\n  </ol>\n  <div class=\"btn btn-blue reportrange hide\">\n    <i class=\"fa fa-calendar\"></i>&nbsp;\n    <span></span>&nbsp;report&nbsp;\n    <i class=\"fa fa-angle-down\"></i>\n    <input type=\"hidden\" name=\"datestart\" />\n    <input type=\"hidden\" name=\"endstart\" />\n  </div>\n  <div class=\"clearfix\"></div>\n</div>\n\n<!-- content -->\n<div class=\"page-content\" id=\"claim\">\n  <ul id=\"claimTab\" class=\"nav nav-tabs ul-edit responsive\">\n    <li class=\"active\"><a href=\"#claimTab\" data-toggle=\"tab\">จัดการ{{Heading}}</a></li>\n  </ul>\n  <div id=\"claimTabContent\" class=\"tab-content\">\n    <app-loading *ngIf=\"isLoading\"></app-loading>\n    <div id=\"listClaim\" class=\"tab-pane fade in active\">\n      <div class=\"row dateSelect\">\n        <label class=\"control-label\">เลือกช่วงวัน</label>\n        <div>\n          <my-date-range-picker [options]=\"myDateRangePickerOptions\" (dateRangeChanged)=\"onDateRangeChanged($event)\" [(ngModel)]=\"dateRange\"></my-date-range-picker>\n        </div>\n      </div>\n      <div class=\"row\" *ngIf=\"!isLoading\">\n        <div class=\"col-lg-12 datatable-simple\">\n          <table datatable class=\"table table-bordered\" [dtOptions]=\"dtOptions\" *ngIf=\"data\">\n            <thead>\n              <tr>\n                <th>รหัสซ่อม</th>\n                <th>วันที่</th>\n                <th>สินค้า</th>\n                <th>ลูกค้า</th>\n                <th>สถานะ</th>\n                <th>Actions</th>\n              </tr>\n            </thead>\n            <tbody>\n              <tr *ngFor=\"let item of data\">\n                <td>{{item.code}}</td>\n                <td>{{item.repair_date}}</td>\n                <td>{{item.item_brand}}</td>\n                <td>{{item.customer && item.customer.name}}</td>\n                <td [ngSwitch]=\"item.status\">\n                  <span class=\"label label-blue\" *ngSwitchCase=\"1\">รับเรื่อง</span>\n                  <span class=\"label label-yellow\" *ngSwitchCase=\"2\">กำลังเปลี่ยน</span>\n                  <span class=\"label label-green\" *ngSwitchCase=\"3\">สำเร็จ</span>\n                  <span class=\"label label-red\" *ngSwitchCase=\"4\">ผิดพลาด</span>\n                </td>\n                <td>\n                  <div class=\"actionsBtn\">\n                    <a class=\"btn btn-yellow btn-outlined\" (click)=\"showDetail(item)\"><i class=\"fa fa-info\"></i></a>\n                    <a class=\"btn btn-danger btn-outlined\" (click)=\"openDeleteModal(item)\"><i class=\"fa fa-trash\"></i></a>\n                  </div>\n                </td>\n              </tr>\n            </tbody>\n          </table>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n\n<div class=\"modal fade\" bsModal #deleteModal=\"bs-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"mySmallModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-sm modalEditProduct\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title pull-left\">ยืนยันการลบการซ่อมสินค้า</h4>\n        <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"deleteModal.hide()\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\" *ngIf=\"selectedItem\">\n        <p>คุณแน่ใจที่ต้องการจะลบ \"{{selectedItem.code}}\"</p>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"deleteModal.hide()\">ยกเลิก</button>\n        <button type=\"button\" class=\"btn btn-success btn-square\" (click)=\"deleteItem()\">แน่ใจ</button>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ 1220:
/***/ (function(module, exports) {

module.exports = "<!-- title -->\n<div id=\"title-breadcrumb-option-demo\" class=\"page-title-breadcrumb\">\n  <div class=\"page-header pull-left\">\n    <div class=\"page-title\">{{Heading}}</div>\n  </div>\n  <ol class=\"breadcrumb page-breadcrumb\">\n    <li>\n      <i class=\"fa fa-home\"></i>&nbsp;\n      <a [routerLink]=\"['/dashboard']\">Home</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n      <a [routerLink]=\"['/claim/fix/list']\">การซ่อมสินค้า</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n    </li>\n    <li class=\"active\">{{Heading}}</li>\n  </ol>\n\n  <div class=\"clearfix\"></div>\n</div>\n\n<!-- content -->\n<div class=\"page-content\" id=\"addClaim\">\n  <div id=\"claimPrint-page\" class=\"row\">\n    <div class=\"col-md-9\">\n      <div class=\"panel\">\n        <div id=\"print-section\" *ngIf=\"data\">\n          <div class=\"panel-body\">\n            <div class=\"claimPrint-title\">\n              <h2>{{store.name}}</h2>\n              <p class=\"mbn text-center\">{{store.tel}}</p>\n            </div>\n            <table class=\"table table-claim-info\">\n              <tr>\n                <td>\n                  <address>\n                    <strong>รหัสซ่อม:</strong><br/>{{data.code}}\n                  </address>\n                </td>\n                <td>\n                  <address>\n                    <strong>วันที่รับซ่อม:</strong><br/>{{data.repair_date}}\n                  </address>\n                </td>\n                <td>\n                  <address><strong>ชื่อ - นามสกุล:</strong><br/>{{data.customer.name}}</address>\n                </td>\n                <td>\n                  <address><strong>เบอร์:</strong><br/>{{data.tel}}</address>\n                </td>\n              </tr>\n            </table>\n            <hr/>\n            <div class=\"table-responsive\">\n              <table class=\"table table-product\">\n                <tbody>\n                  <tr>\n                    <td colspan=\"2\"><strong>ยี่ห้อ :</strong>&nbsp;&nbsp;{{data.item_brand}}</td>\n                    <td colspan=\"2\"><strong>รุ่น :</strong>&nbsp;&nbsp;{{data.item_generation}}</td>\n                    <td colspan=\"2\"><strong>สี :</strong>&nbsp;&nbsp;{{data.item_color}}</td>\n                  </tr>\n                  <tr>\n                    <td colspan=\"6\"><strong>IMEI :</strong>&nbsp;&nbsp;<span class=\"imei\" *ngFor=\"let item of data.product_block\">{{item}}</span></td>\n                  </tr>\n                  <tr class=\"borderDiv\">\n                    <td colspan=\"3\" class=\"table-list\">\n                      <p><strong>ตำหนิตัวเครื่อง</strong></p>\n                      <p>\n                        {{data.item_blame}}\n                      </p>\n                    </td>\n                    <td colspan=\"2\">\n                      <table class=\"table table-option\">\n                        <thead>\n                          <tr>\n                            <td></td>\n                            <td><strong>มี</strong></td>\n                            <td><strong>ไม่มี</strong></td>\n                          </tr>\n                        </thead>\n                        <tbody>\n                          <tr>\n                            <td>Simcard</td>\n                            <td class=\"hasIcon\"><i class=\"icon fa\" [ngClass]=\"data.item_has_sim ? 'fa-check-circle-o':'fa-circle-o'\"></i></td>\n                            <td class=\"hasIcon\"><i class=\"icon fa\" [ngClass]=\"!data.item_has_sim ? 'fa-check-circle-o':'fa-circle-o'\"></i></td>\n                          </tr>\n                          <tr>\n                            <td>Memory</td>\n                            <td class=\"hasIcon\"><i class=\"icon fa\" [ngClass]=\"data.item_has_mem ? 'fa-check-circle-o':'fa-circle-o'\"></i></td>\n                            <td class=\"hasIcon\"><i class=\"icon fa\" [ngClass]=\"!data.item_has_mem ? 'fa-check-circle-o':'fa-circle-o'\"></i></td>\n                          </tr>\n                          <tr>\n                            <td>Battery</td>\n                            <td class=\"hasIcon\"><i class=\"icon fa\" [ngClass]=\"data.item_has_battery ? 'fa-check-circle-o':'fa-circle-o'\"></i></td>\n                            <td class=\"hasIcon\"><i class=\"icon fa\" [ngClass]=\"!data.item_has_battery ? 'fa-check-circle-o':'fa-circle-o'\"></i></td>\n                          </tr>\n                        </tbody>\n                      </table>\n                    </td>\n                  </tr>\n                  <tr class=\"borderDiv\">\n                    <td colspan=\"3\" class=\"table-list\">\n                      <p><strong>อาการ</strong></p>\n                      <ol>\n                        <li *ngFor=\"let cause of data.cause\">{{cause ? cause.message : ''}}</li>\n                      </ol>\n                    </td>\n                    <td colspan=\"3\" class=\"cost\">\n                      <p><strong>ราคาประเมิน :</strong>&nbsp;&nbsp; {{ data.estimate_price || '0' | number : fractionSize}} บาท</p>\n                      <p><strong>หมายเหตุ :</strong>&nbsp;&nbsp;</p>\n                      <p class=\"textextra\">{{data.cause_remark || '-'}}</p>\n                    </td>\n                  </tr>\n                  <tr class=\"borderDiv\">\n                    <td colspan=\"3\" class=\"table-list\">\n                      <p><strong>รายการซ่อม</strong></p>\n                      <ol>\n                        <li *ngFor=\"let repair of data.repair\">{{repair ? repair.message : ''}}</li>\n                      </ol>\n                    </td>\n                    <td colspan=\"3\" class=\"cost\">\n                      <p><strong>ราคาซ่อมจริง :</strong>&nbsp;&nbsp; {{ data.repair_price || '0' | number : fractionSize}} บาท</p>\n                      <p><strong>หมายเหตุ :</strong>&nbsp;&nbsp;</p>\n                      <p class=\"textextra\">{{data.repair_remark || '-'}}</p>\n                    </td>\n                  </tr>\n                  <tr>\n                    <td colspan=\"6\" class=\"table-list\">\n                      <p><strong>ข้อตกลง</strong></p>\n                      <ol>\n                        <li *ngFor=\"let agreement of data.agreement\">{{ agreement ? agreement.message : ''}}</li>\n                      </ol>\n                    </td>\n                  </tr>\n                  <tr>\n                    <td colspan=\"6\" class=\"cost\">\n                      <p><strong>หมายเหตุ :</strong>&nbsp;&nbsp;</p>\n                      <p class=\"textextra\">{{data.agreement_remark}}</p>\n                    </td>\n                  </tr>\n                </tbody>\n              </table>\n              <hr class=\"mbm\" />\n              <table class=\"table table-signature\">\n                <tbody>\n                  <tr class=\"signatureHeader\">\n                    <td colspan=\"6\">ลายเซ็นลูกค้า</td>\n                    <td colspan=\"6\">ลายเซ็นผู้รับเครื่อง</td>\n                    <td colspan=\"6\">ลายเซ็นช่างซ่อม</td>\n                  </tr>\n                  <tr class=\"signature\">\n                    <td></td>\n                    <td colspan=\"4\" class=\"borderShow\"></td>\n                    <td></td>\n                    <td></td>\n                    <td colspan=\"4\" class=\"borderShow\"></td>\n                    <td></td>\n                    <td></td>\n                    <td colspan=\"4\" class=\"borderShow\"></td>\n                    <td></td>\n                  </tr>\n                  <tr class=\"signatureDate\">\n                    <td colspan=\"2\">\n                      <p>วันที่</p>\n                    </td>\n                    <td colspan=\"3\" class=\"borderShow\"></td>\n                    <td></td>\n                    <td colspan=\"2\">\n                      <p>วันที่</p>\n                    </td>\n                    <td colspan=\"3\" class=\"borderShow\"></td>\n                    <td></td>\n                    <td colspan=\"2\">\n                      <p>วันที่</p>\n                    </td>\n                    <td colspan=\"3\" class=\"borderShow\"></td>\n                    <td></td>\n                  </tr>\n                  <tr class=\"signatureSpace\"></tr>\n                  <tr class=\"signatureHeader\">\n                    <td colspan=\"6\">ลายเซ็นผู้รับเครื่องซ่อม</td>\n                    <td colspan=\"6\">ลายเซ็นผู้ส่งเครื่องซ่อม</td>\n                    <td colspan=\"6\">วันที่ส่งเครื่องซ่อม</td>\n                  </tr>\n                  <tr class=\"signature\">\n                    <td></td>\n                    <td colspan=\"4\" class=\"borderShow\"></td>\n                    <td></td>\n                    <td></td>\n                    <td colspan=\"4\" class=\"borderShow\"></td>\n                    <td></td>\n                    <td></td>\n                    <td colspan=\"4\" class=\"borderShow\"></td>\n                    <td></td>\n                  </tr>\n                  <tr class=\"signatureDate\">\n                    <td colspan=\"2\">\n                      <p>วันที่</p>\n                    </td>\n                    <td colspan=\"3\" class=\"borderShow\"></td>\n                    <td></td>\n                    <td colspan=\"2\">\n                      <p>วันที่</p>\n                    </td>\n                    <td colspan=\"3\" class=\"borderShow\"></td>\n                    <td></td>\n                    <td colspan=\"6\"></td>\n                  </tr>\n                </tbody>\n              </table>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"col-lg-3\">\n      <button class=\"btn btn-success mrm\" (click)=\"print()\"><i class=\"fa fa-print\"></i>&nbsp; Print\n                            </button>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ 1221:
/***/ (function(module, exports) {

module.exports = "<!-- title -->\n<div id=\"title-breadcrumb-option-demo\" class=\"page-title-breadcrumb\">\n  <div class=\"page-header pull-left\">\n    <div class=\"page-title\">{{Heading}}</div>\n  </div>\n  <ol class=\"breadcrumb page-breadcrumb\">\n    <li>\n      <i class=\"fa fa-home\"></i>&nbsp;\n      <a [routerLink]=\"['/dashboard']\">Home</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n    </li>\n    <li class=\"active\">{{Heading}}</li>\n  </ol>\n  <div class=\"btn btn-blue reportrange hide\">\n    <i class=\"fa fa-calendar\"></i>&nbsp;\n    <span></span>&nbsp;report&nbsp;\n    <i class=\"fa fa-angle-down\"></i>\n    <input type=\"hidden\" name=\"datestart\" />\n    <input type=\"hidden\" name=\"endstart\" />\n  </div>\n  <div class=\"clearfix\"></div>\n</div>\n\n<!-- content -->\n<div class=\"page-content\" id=\"customer\">\n  <ul id=\"customerTab\" class=\"nav nav-tabs ul-edit responsive\">\n    <li class=\"active\"><a href=\"#addCustomer\" data-toggle=\"tab\">เพิ่ม{{Heading}}</a></li>\n  </ul>\n  <div id=\"customerTabContent\" class=\"tab-content\">\n\n    <div id=\"addCustomer\" class=\"tab-pane fade in active\">\n      <form [formGroup]=\"customerForm\" class=\"form-horizontal\" (ngSubmit)=\"sendForm()\">\n        <div class=\"form-body pal\">\n          <div class=\"form-group\">\n            <label for=\"name\" class=\"col-md-3 control-label\">ชื่อ<span class='require'>*</span></label>\n            <div class=\"col-md-5\">\n              <div class=\"input-icon right\">\n                <input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" formControlName=\"name\" [(ngModel)]=\"data.name\" />\n                <p class=\"text-danger\" *ngIf=\"name.hasError('required') && name.dirty\">กรุณากรอกชื่อ</p>\n              </div>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label for=\"tel\" class=\"col-md-3 control-label\">เบอร์ติดต่อ<span class='require'>*</span></label>\n            <div class=\"col-md-5\">\n              <div class=\"input-icon right\">\n                <input type=\"tel\" id=\"tel\" name=\"tel\" class=\"form-control\" formControlName=\"tel\" [(ngModel)]=\"data.tel\" />\n                <p class=\"text-danger\" *ngIf=\"tel.hasError('required') && tel.dirty\">กรุณากรอกเบอร์ติดต่อ</p>\n              </div>\n            </div>\n          </div>\n          <div class=\"form-actions\">\n            <div class=\"col-md-5 col-md-offset-3\">\n              <button type=\"submit\" class=\"btn btn-success btn-outlined\" [disabled]=\"!customerForm.valid\">บันทึก</button> &nbsp;\n              <button type=\"reset\" class=\"btn btn-danger btn-outlined\">ยกเลิก</button>\n            </div>\n          </div>\n        </div>\n      </form>\n    </div>\n  </div>\n</div>\n\n"

/***/ }),

/***/ 1222:
/***/ (function(module, exports) {

module.exports = "<!-- title -->\n<div id=\"title-breadcrumb-option-demo\" class=\"page-title-breadcrumb\">\n  <div class=\"page-header pull-left\">\n    <div class=\"page-title\">{{Heading}}</div>\n  </div>\n  <ol class=\"breadcrumb page-breadcrumb\">\n    <li>\n      <i class=\"fa fa-home\"></i>&nbsp;\n      <a [routerLink]=\"['/dashboard']\">Home</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n    </li>\n    <li class=\"active\">{{Heading}}</li>\n  </ol>\n  <div class=\"btn btn-blue reportrange hide\">\n    <i class=\"fa fa-calendar\"></i>&nbsp;\n    <span></span>&nbsp;report&nbsp;\n    <i class=\"fa fa-angle-down\"></i>\n    <input type=\"hidden\" name=\"datestart\" />\n    <input type=\"hidden\" name=\"endstart\" />\n  </div>\n  <div class=\"clearfix\"></div>\n</div>\n\n<!-- content -->\n<div class=\"page-content\" id=\"customer\">\n  <ul id=\"customerTab\" class=\"nav nav-tabs ul-edit responsive\">\n    <li class=\"active\"><a href=\"#manageCustomer\" data-toggle=\"tab\">จัดการ{{Heading}}</a></li>\n  </ul>\n  <div id=\"customerTabContent\" class=\"tab-content\">\n    <app-loading *ngIf=\"isLoading\"></app-loading>\n    <div id=\"manageCustomer\" class=\"tab-pane fade in active\">\n      <div class=\"row\" *ngIf=\"!isLoading\">\n        <div class=\"col-lg-12 datatable-simple\" [ngClass]=\"{'tableWhenHideMenu': !isHideMenu}\">\n          <table datatable class=\"table table-bordered\" [dtOptions]=\"dtOptions\" *ngIf=\"data\">\n            <thead>\n              <tr>\n                <th>ID</th>\n                <th>ชื่อ - นามสกุล</th>\n                <th>เบอร์</th>\n                <th>Actions</th>\n              </tr>\n            </thead>\n            <tbody>\n              <tr *ngFor=\"let item of data\">\n                <td>{{item.id}}</td>\n                <td>{{item.name}}</td>\n                <td>{{item.tel}}</td>\n                <td>\n                  <div class=\"actionsBtn\">\n                    <a class=\"btn btn-info btn-outlined\" (click)=\"openEditModal(item.id)\"><i class=\"fa fa-edit\"></i></a>\n                    <a class=\"btn btn-danger btn-outlined\" (click)=\"openDeleteModal(item.id)\"><i class=\"fa fa-trash\"></i></a>\n                  </div>\n                </td>\n              </tr>\n            </tbody>\n          </table>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n<div class=\"modal fade\" bsModal #editModal=\"bs-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"mySmallModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-lg modalEditProduct\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title pull-left\">แก้ไข{{Heading}}</h4>\n        <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"editModal.hide()\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <form [formGroup]=\"customerForm\" class=\"form-horizontal\" (ngSubmit)=\"saveChange()\">\n        <div class=\"modal-body\" *ngIf=\"selectedItem\">\n          <div class=\"form-body pal\">\n            <div class=\"form-group\">\n              <label for=\"name\" class=\"col-md-3 control-label\">ชื่อ<span class='require'>*</span></label>\n              <div class=\"col-md-5\">\n                <div class=\"input-icon right\">\n                  <input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" formControlName=\"name\" [(ngModel)]=\"selectedItem.name\" />\n                  <p class=\"text-danger\" *ngIf=\"name.hasError('required') && name.dirty\">กรุณากรอกชื่อ</p>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <label for=\"tel\" class=\"col-md-3 control-label\">เบอร์ติดต่อ<span class='require'>*</span></label>\n              <div class=\"col-md-5\">\n                <div class=\"input-icon right\">\n                  <input type=\"tel\" id=\"tel\" name=\"tel\" class=\"form-control\" formControlName=\"tel\" [(ngModel)]=\"selectedItem.tel\" />\n                  <p class=\"text-danger\" *ngIf=\"tel.hasError('required') && tel.dirty\">กรุณากรอกเบอร์ติดต่อ</p>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"editModal.hide()\">ยกเลิก</button>\n          <button type=\"submit\" class=\"btn btn-success btn-square\" [disabled]=\"!customerForm.valid\">บันทึก</button>\n        </div>\n      </form>\n    </div>\n  </div>\n</div>\n\n<div class=\"modal fade\" bsModal #deleteModal=\"bs-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"mySmallModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-sm modalEditProduct\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title pull-left\">ยืนยันการลบ{{Heading}}</h4>\n        <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"deleteModal.hide()\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\" *ngIf=\"selectedItem\">\n        <p>คุณแน่ใจที่ต้องการจะลบ \"{{selectedItem.name}}\"</p>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"deleteModal.hide()\">ยกเลิก</button>\n        <button type=\"button\" class=\"btn btn-success btn-square\" (click)=\"deleteItem()\">แน่ใจ</button>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ 1223:
/***/ (function(module, exports) {

module.exports = "<!-- title -->\n<div id=\"title-breadcrumb-option-demo\" class=\"page-title-breadcrumb\">\n  <div class=\"page-header pull-left\">\n    <div class=\"page-title\">{{Heading}}</div>\n  </div>\n  <ol class=\"breadcrumb page-breadcrumb\">\n    <li>\n      <i class=\"fa fa-home\"></i>&nbsp;\n      <a [routerLink]=\"['/dashboard']\">Home</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n      <a [routerLink]=\"['/product/type/list']\">{{Heading}}</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n    </li>\n    <li class=\"active\">เพิ่ม{{Heading}}</li>\n  </ol>\n</div>\n\n<!-- content -->\n<div class=\"page-content\" id=\"productType\">\n  <ul id=\"productTypeTab\" class=\"nav nav-tabs ul-edit responsive\">\n    <li class=\"active\"><a href=\"#addProductType\" data-toggle=\"tab\">เพิ่ม{{Heading}}</a></li>\n  </ul>\n  <div id=\"productTypeTabContent\" class=\"tab-content\">\n    <app-loading *ngIf=\"isLoading\"></app-loading>\n    <div id=\"addProductType\" class=\"tab-pane  fade in active\">\n      <form [formGroup]=\"expenseForm\" class=\"form-horizontal\" (ngSubmit)=\"sendForm()\">\n        <div class=\"form-body pal\">\n          <div class=\"form-group\">\n            <label for=\"detail\" class=\"col-md-3 control-label\">รายการ<span class='require'>*</span></label>\n            <div class=\"col-md-6\">\n              <div>\n                <input type=\"text\" id=\"detail\" name=\"detail\" class=\"form-control\" formControlName=\"detail\" />\n                <p class=\"text-danger\" *ngIf=\"detail.hasError('required') && detail.dirty\">กรุณากรอกรายการ</p>\n              </div>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label for=\"date\" class=\"col-md-3 control-label\">วันที่<span class='require'>*</span></label>\n            <div class=\"col-md-6\">\n              <div>\n                <my-date-picker [selDate]=\"selDate\" [options]=\"myDatePickerOptions\" (dateChanged)=\"onDateChanged($event)\" formControlName=\"expense_date\" [(ngModel)]=\"date\"></my-date-picker>\n              </div>\n            </div>\n          </div>\n          <div class=\"form-group mbn\"><label for=\"inputDetail\" class=\"col-md-3 control-label\">จำนวนเงิน<span class='require'>*</span></label>\n            <div class=\"col-md-6\">\n              <div>\n                <input id=\"inputNameType\" type=\"number\" class=\"form-control\" formControlName=\"amount\" />\n                <p class=\"text-danger\" *ngIf=\"amount.hasError('required') && amount.dirty\">กรุณากรอกจำนวนเงิน</p>\n              </div>\n            </div>\n          </div>\n          <div class=\"form-actions\">\n            <div class=\"col-md-12\">\n              <button type=\"submit\" class=\"btn btn-success btn-outlined\" [disabled]=\"!expenseForm.valid\">บันทึก</button>&nbsp;\n              <button type=\"reset\" class=\"btn btn-danger btn-outlined\">ยกเลิก</button>\n            </div>\n          </div>\n        </div>\n      </form>\n    </div>\n  </div>\n</div>\n\n<template ngbModalContainer></template>\n\n<template #confirmDeleteProducType let-c=\"close\" let-d=\"dismiss\" class=\"fade\">\n  <div class=\"modal-dialog\" id=\"modalConfirmDelete\">\n    <div class=\"modal-header modal-header-primary\">\n      <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"d('Cross click')\">\n        <span aria-hidden=\"true\">&times;</span>\n      </button>\n      <h4 id=\"modal-header-primary-label\" class=\"modal-title\">ยืนยันการลบ{{Heading}}</h4>\n    </div>\n    <div class=\"modal-body\">\n      <p>คุณแน่ใจที่ต้องการจะลบ \"{{selectedItem.name}}\"</p>\n    </div>\n    <div class=\"modal-footer\">\n      <button type=\"button\" data-dismiss=\"modal\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"c('Close click')\">ยกเลิก</button>\n      <button type=\"button\" class=\"btn btn-primary btn-square\" (click)=\"c('Close click')\">แน่ใจ</button>\n    </div>\n  </div>\n</template>\n\n<template #editProductType let-c=\"close\" let-d=\"dismiss\" class=\"fade\">\n  <div class=\"modal-dialog\" id=\"modalConfirmDelete\">\n    <div class=\"modal-header modal-header-primary\">\n      <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"d('Cross click')\">\n        <span aria-hidden=\"true\">&times;</span>\n      </button>\n      <h4 id=\"modal-header-primary-label\" class=\"modal-title\">แก้ไข{{Heading}}</h4>\n    </div>\n    <div class=\"modal-body\">\n      <form action=\"#\" class=\"form-horizontal\">\n        <div class=\"form-body pal\">\n          <div class=\"form-group\"><label for=\"inputNameType\" class=\"col-md-3 control-label\">ชื่อ{{Heading}}<span class='require'>*</span></label>\n            <div class=\"col-md-8\">\n              <div>\n                <input id=\"inputNameType\" type=\"text\" placeholder=\"ชื่อ{{Heading}}\" class=\"form-control\" [value]=\"selectedItem.name\" />\n              </div>\n            </div>\n          </div>\n          <div class=\"form-group mbn\"><label for=\"inputDetail\" class=\"col-md-3 control-label\">รายละเอียด<span class='require'>*</span></label>\n            <div class=\"col-md-8\">\n              <textarea id=\"inputDetail\" rows=\"3\" class=\"form-control\" [value]=\"selectedItem.phone\"></textarea>\n            </div>\n          </div>\n        </div>\n      </form>\n    </div>\n    <div class=\"modal-footer\">\n      <button type=\"button\" data-dismiss=\"modal\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"c('Close click')\">ยกเลิก</button>\n      <button type=\"button\" class=\"btn btn-success btn-square\" (click)=\"c('Close click')\">บันทึก</button>\n    </div>\n  </div>\n</template>"

/***/ }),

/***/ 1224:
/***/ (function(module, exports) {

module.exports = "<!-- title -->\n<div id=\"title-breadcrumb-option-demo\" class=\"page-title-breadcrumb\">\n  <div class=\"page-header pull-left\">\n    <div class=\"page-title\">{{Heading}}</div>\n  </div>\n  <ol class=\"breadcrumb page-breadcrumb\">\n    <li>\n      <i class=\"fa fa-home\"></i>&nbsp;\n      <a [routerLink]=\"['/dashboard']\">Home</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n    </li>\n    <li class=\"active\">{{Heading}}</li>\n  </ol>\n  <div class=\"btn btn-blue reportrange hide\">\n    <i class=\"fa fa-calendar\"></i>&nbsp;\n    <span></span>&nbsp;report&nbsp;\n    <i class=\"fa fa-angle-down\"></i>\n    <input type=\"hidden\" name=\"datestart\" />\n    <input type=\"hidden\" name=\"endstart\" />\n  </div>\n  <div class=\"clearfix\"></div>\n</div>\n\n<!-- content -->\n<div class=\"page-content\" id=\"productType\">\n  <ul id=\"productTypeTab\" class=\"nav nav-tabs ul-edit responsive\">\n    <li class=\"active\"><a href=\"#manageProductType\" data-toggle=\"tab\">จัดการ{{Heading}}</a></li>\n  </ul>\n  <div id=\"productTypeTabContent\" class=\"tab-content\">\n    <app-loading *ngIf=\"isLoading\"></app-loading>\n    <div id=\"manageProductType\" class=\"tab-pane fade in active\">\n      <div class=\"row dateSelect\">\n        <label class=\"control-label\">เลือกช่วงวัน</label>\n        <div>\n          <my-date-range-picker [options]=\"myDateRangePickerOptions\" (dateRangeChanged)=\"onDateRangeChanged($event)\" [(ngModel)]=\"dateRange\"></my-date-range-picker>\n        </div>\n      </div>\n      <div class=\"row\" *ngIf=\"!isLoading\">\n        <div class=\"col-lg-12 datatable-simple\">\n          <table datatable class=\"table table-bordered\" [dtOptions]=\"dtOptions\" *ngIf=\"data\">\n            <thead>\n              <tr>\n                <th>#</th>\n                <th>รายการ</th>\n                <th>จำนวนเงิน</th>\n                <th>วันที่</th>\n                <th>Actions</th>\n              </tr>\n            </thead>\n            <tbody>\n              <tr *ngFor=\"let item of data; let i = index\">\n                <td>{{i}}</td>\n                <td>{{item.detail}}</td>\n                <td>{{item.amount}}</td>\n                <td>{{item.expense_date}}</td>\n                <td>\n                  <div class=\"actionsBtn\">\n                    <a class=\"btn btn-info btn-outlined\" (click)=\"openEditModal(item.id)\"><i class=\"fa fa-edit\"></i></a>\n                    <a class=\"btn btn-danger btn-outlined\" (click)=\"openDeleteModal(item.id)\"><i class=\"fa fa-trash\"></i></a>\n                  </div>\n                </td>\n              </tr>\n            </tbody>\n          </table>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n\n<div class=\"modal fade\" bsModal #editModal=\"bs-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"mySmallModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-lg modalEditProduct\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title pull-left\">แก้ไข{{Heading}}</h4>\n        <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"editModal.hide()\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <form [formGroup]=\"expenseForm\" class=\"form-horizontal\" (ngSubmit)=\"saveChange()\">\n        <div class=\"modal-body\" *ngIf=\"selectedItem\">\n          <div class=\"form-body pal\">\n            <div class=\"form-group\">\n              <label for=\"detail\" class=\"col-md-3 control-label\">รายการ<span class='require'>*</span></label>\n              <div class=\"col-md-6\">\n                <div>\n                  <input type=\"text\" id=\"detail\" name=\"detail\" class=\"form-control\" formControlName=\"detail\" [(ngModel)]=\"selectedItem.detail\"\n                  />\n                  <p class=\"text-danger\" *ngIf=\"detail.hasError('required') && detail.dirty\">กรุณากรอกรายการ</p>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <label for=\"date\" class=\"col-md-3 control-label\">วันที่<span class='require'>*</span></label>\n              <div class=\"col-md-6\">\n                <div class=\"input-group showDatepicker\">\n                  <ngb-datepicker #dp [(ngModel)]=\"ex_date\" (navigate)=\"date = $event.next\" formControlName=\"expense_date\"></ngb-datepicker>\n                  <!--<input type=\"text\" data-date-format=\"yyyy-mm-dd\" placeholder=\"yyyy-mm-dd\" class=\"datepicker-default form-control\" ngbDatepicker\n                    #d=\"ngbDatepicker\" (click)=\"d.toggle()\" formControlName=\"expense_date\" [(ngModel)]=\"selectedItem.expense_date\" />\n                  <div class=\"input-group-addon\" (click)=\"d.toggle()\"><i class=\"fa fa-calendar\"></i></div>-->\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group mbn\"><label for=\"inputDetail\" class=\"col-md-3 control-label\">จำนวนเงิน<span class='require'>*</span></label>\n              <div class=\"col-md-6\">\n                <div>\n                  <input id=\"inputNameType\" type=\"number\" class=\"form-control\" formControlName=\"amount\" [(ngModel)]=\"selectedItem.amount\" />\n                  <p class=\"text-danger\" *ngIf=\"amount.hasError('required') && amount.dirty\">กรุณากรอกจำนวนเงิน</p>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"editModal.hide()\">ยกเลิก</button>\n          <button type=\"submit\" class=\"btn btn-success btn-square\" [disabled]=\"!expenseForm.valid\">บันทึก</button>\n        </div>\n      </form>\n    </div>\n  </div>\n</div>\n\n<div class=\"modal fade\" bsModal #deleteModal=\"bs-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"mySmallModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-sm modalEditProduct\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title pull-left\">ยืนยันการลบ{{Heading}}</h4>\n        <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"deleteModal.hide()\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\" *ngIf=\"selectedItem\">\n        <p>คุณแน่ใจที่ต้องการจะลบ \"{{selectedItem.name}}\"</p>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"deleteModal.hide()\">ยกเลิก</button>\n        <button type=\"button\" class=\"btn btn-success btn-square\" (click)=\"deleteItem()\">แน่ใจ</button>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ 1225:
/***/ (function(module, exports) {

module.exports = "<nav id=\"topbar\" role=\"navigation\" style=\"margin-bottom: 0;\" data-step=\"1\" data-intro=\"&lt;b&gt;Topbar&lt;/b&gt; has other styles with live demo. Go to &lt;b&gt;Layouts-&gt;Header&amp;Topbar&lt;/b&gt; and check it out.\"\n  class=\"navbar navbar-default navbar-static-top\">\n  <div class=\"navbar-header\" [ngClass]=\"{'hideMenu': isHideMenu}\">\n    <button type=\"button\" data-toggle=\"collapse\" data-target=\".sidebar-collapse\" class=\"navbar-toggle\" >\n      <span class=\"sr-only\">Toggle navigation</span>\n      <span class=\"icon-bar\"></span>\n      <span class=\"icon-bar\"></span>\n      <span class=\"icon-bar\"></span>\n    </button>\n    <a id=\"logo\" (click)=\"goToReport()\" class=\"navbar-brand\">\n      <span class=\"fa fa-rocket\"></span>\n      <span class=\"logo-text\">stock</span>\n      <span class=\"logo-text-icon\" [ngClass]=\"{'iconHideMenu': isHideMenu}\">S</span>\n    </a>\n  </div>\n  <div class=\"topbar-main\">\n    <a id=\"menu-toggle\" (click)=\"hideMenu()\" class=\"hidden-xs\">\n      <i class=\"fa fa-bars\"></i>\n    </a>\n    <ul class=\"nav navbar navbar-top-links navbar-right mbn\">\n      <li class=\"dropdown topbar-user\">\n        <p class=\"headerP\" *ngIf=\"user\">{{user.name}}</p>\n      </li>\n      <li class=\"dropdown topbar-user topbar-logout\" (click)=\"logout()\">\n        <p class=\"headerP\"><i class=\"fa fa-key\"></i>Log Out</p>\n      </li>\n    </ul>\n  </div>\n</nav>"

/***/ }),

/***/ 1226:
/***/ (function(module, exports) {

module.exports = "<div id=\"header-topbar-option-demo\" class=\"page-header-topbar\">\n  <app-header></app-header>\n</div>\n<div id=\"wrapper\">\n  <app-sidebar></app-sidebar>\n  <div id=\"page-wrapper\" [ngClass]=\"{'hideMenuContent': !isHideMenu}\">\n    <router-outlet></router-outlet>\n  </div>\n</div>"

/***/ }),

/***/ 1227:
/***/ (function(module, exports) {

module.exports = "<div class=\"loader-box\">\n  <div class=\"loader\">Loading...</div>\n</div>\n"

/***/ }),

/***/ 1228:
/***/ (function(module, exports) {

module.exports = "<div class=\"loginBody\">\n  <div class=\"page-form\">\n    <form [formGroup]=\"userForm\" (ngSubmit)=\"sendForm()\" class=\"form\">\n      <div class=\"header-content\">\n        <h1>Log In</h1>\n      </div>\n      <div class=\"body-content\">\n        <app-loading *ngIf=\"isLoading\" class=\"load\"></app-loading>\n        <div class=\"alert alert-danger\" *ngIf=\"message\" >\n         {{message}}\n        </div>\n        <div class=\"form-body\">\n          <div class=\"form-group\">\n            <div class=\"input-icon right\">\n              <i class=\"fa fa-user\"></i>\n              <input type=\"text\" placeholder=\"Username\" name=\"username\" class=\"form-control\" formControlName=\"username\">\n              <p class=\"text-danger\" *ngIf=\"username.hasError('required') && username.dirty\">กรุณากรอกชื่อผู้ใช้</p>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <div class=\"input-icon right\">\n              <i class=\"fa fa-key\"></i>\n              <input type=\"password\" placeholder=\"Password\" name=\"password\" class=\"form-control\" formControlName=\"password\">\n              <p class=\"text-danger\" *ngIf=\"password.hasError('required') && password.dirty\">กรุณากรอกรหัสผ่าน</p>\n            </div>\n          </div>\n          <div class=\"form-group pull-right\">\n            <button type=\"submit\" class=\"btn btn-success\">Log In&nbsp;<i class=\"fa fa-chevron-circle-right\"></i></button>\n          </div>\n        </div>\n        <div class=\"clearfix\"></div>\n      </div>\n    </form>\n  </div>\n</div>\n"

/***/ }),

/***/ 1229:
/***/ (function(module, exports) {

module.exports = "<div class=\"text-right\">\n  <ul class=\"pagination mtm mbm\">\n    <li [ngClass]=\"{'disabled' : selectedIdx === 0}\" ><a (click)=\"selectPage(selectedIdx-1)\">&laquo;</a></li>\n    <li *ngFor=\"let item of pages; let i = index\" [ngClass]=\"{'active': selectedIdx === i}\"><a (click)=\"selectPage(i)\">{{item}}</a></li>\n    <li [ngClass]=\"{'disabled' : selectedIdx === (pages.length - 1)}\" ><a (click)=\"selectPage(selectedIdx+1)\">&raquo;</a></li>\n  </ul>\n</div>\n"

/***/ }),

/***/ 1230:
/***/ (function(module, exports) {

module.exports = "<!-- title -->\n<div id=\"title-breadcrumb-option-demo\" class=\"page-title-breadcrumb\">\n  <div class=\"page-header pull-left\">\n    <div class=\"page-title\">{{Heading}}</div>\n  </div>\n  <ol class=\"breadcrumb page-breadcrumb\">\n    <li>\n      <i class=\"fa fa-home\"></i>&nbsp;\n      <a [routerLink]=\"['/dashboard']\">Home</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n      <a [routerLink]=\"['/product/type/list']\">{{Heading}}</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n    </li>\n    <li class=\"active\">เพิ่ม{{Heading}}</li>\n  </ol>\n</div>\n\n<!-- content -->\n<div class=\"page-content\" id=\"productType\">\n  <ul id=\"productTypeTab\" class=\"nav nav-tabs ul-edit responsive\">\n    <li class=\"active\"><a href=\"#addProductType\" data-toggle=\"tab\">เพิ่ม{{Heading}}</a></li>\n  </ul>\n  <div id=\"productTypeTabContent\" class=\"tab-content\">\n    <app-loading *ngIf=\"isLoading\"></app-loading>\n    <div id=\"addProductType\" class=\"tab-pane fade in active\">\n      <form [formGroup]=\"productTypeForm\" class=\"form-horizontal\" (ngSubmit)=\"sendForm()\">\n        <div class=\"form-body pal\">\n          <div class=\"form-group\">\n            <label for=\"name\" class=\"col-md-3 control-label\">ชื่อ{{Heading}}<span class='require'>*</span></label>\n            <div class=\"col-md-8\">\n              <div class=\"input-icon right\">\n                <input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" formControlName=\"name\" [(ngModel)]=\"data.name\"\n                />\n                <p class=\"text-danger\" *ngIf=\"name.hasError('required') && name.dirty\">กรุณากรอกชื่อ{{Heading}}</p>\n              </div>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label for=\"detail\" class=\"col-md-3 control-label\">รายละเอียด<span class='require'>*</span></label>\n            <div class=\"col-md-8\">\n              <div class=\"input-icon right\">\n                <textarea id=\"detail\" rows=\"3\" class=\"form-control\" name=\"detail\" formControlName=\"detail\" [(ngModel)]=\"data.detail\"></textarea>\n                <p class=\"text-danger\" *ngIf=\"detail.hasError('required') && detail.dirty\">กรุณากรอกที่อยู่</p>\n              </div>\n            </div>\n          </div>\n          <div class=\"form-actions\">\n            <div class=\"col-md-8 col-md-offset-3\">\n              <button type=\"submit\" class=\"btn btn-success btn-outlined\" [disabled]=\"!productTypeForm.valid\">บันทึก</button> &nbsp;\n              <button type=\"reset\" class=\"btn btn-danger btn-outlined\">ยกเลิก</button>\n            </div>\n          </div>\n        </div>\n      </form>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ 1231:
/***/ (function(module, exports) {

module.exports = "<!-- title -->\n<div id=\"title-breadcrumb-option-demo\" class=\"page-title-breadcrumb\">\n  <div class=\"page-header pull-left\">\n    <div class=\"page-title\">{{Heading}}</div>\n  </div>\n  <ol class=\"breadcrumb page-breadcrumb\">\n    <li>\n      <i class=\"fa fa-home\"></i>&nbsp;\n      <a [routerLink]=\"['/dashboard']\">Home</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n    </li>\n    <li class=\"active\">{{Heading}}</li>\n  </ol>\n  <div class=\"btn btn-blue reportrange hide\">\n    <i class=\"fa fa-calendar\"></i>&nbsp;\n    <span></span>&nbsp;report&nbsp;\n    <i class=\"fa fa-angle-down\"></i>\n    <input type=\"hidden\" name=\"datestart\" />\n    <input type=\"hidden\" name=\"endstart\" />\n  </div>\n  <div class=\"clearfix\"></div>\n</div>\n\n<!-- content -->\n<div class=\"page-content\" id=\"productType\">\n  <ul id=\"productTypeTab\" class=\"nav nav-tabs ul-edit responsive\">\n    <li class=\"active\"><a href=\"#manageProductType\" data-toggle=\"tab\">จัดการ{{Heading}}</a></li>\n  </ul>\n  <div id=\"productTypeTabContent\" class=\"tab-content\">\n    <app-loading *ngIf=\"isLoading\"></app-loading>\n    <div id=\"manageProductType\" class=\"tab-pane fade in active\">\n      <div class=\"row\" *ngIf=\"!isLoading\">\n        <div class=\"col-lg-12 datatable-simple\">\n          <table datatable class=\"table table-bordered\" [dtOptions]=\"dtOptions\" *ngIf=\"data\">\n            <thead>\n              <tr>\n                <th>ID</th>\n                <th>ประเภทสินค้า</th>\n                <th>รายละเอียด</th>\n                <th>Actions</th>\n              </tr>\n            </thead>\n            <tbody>\n              <tr *ngFor=\"let item of data; let i = index\">\n                <td>{{i+1}}</td>\n                <td>{{item.name}}</td>\n                <td>{{item.detail}}</td>\n                <td>\n                  <div class=\"actionsBtn\">\n                    <a class=\"btn btn-info btn-outlined\" (click)=\"openEditModal(item.id)\"><i class=\"fa fa-edit\"></i></a>\n                    <a class=\"btn btn-danger btn-outlined\" (click)=\"openDeleteModal(item.id)\"><i class=\"fa fa-trash\"></i></a>\n                  </div>\n                </td>\n              </tr>\n            </tbody>\n          </table>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n\n<div class=\"modal fade\" bsModal #editModal=\"bs-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"mySmallModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-lg modalEditProduct\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title pull-left\">แก้ไข{{Heading}}</h4>\n        <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"editModal.hide()\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <form [formGroup]=\"productTypeForm\" class=\"form-horizontal\" (ngSubmit)=\"saveChange()\">\n        <div class=\"modal-body\" *ngIf=\"selectedItem\">\n          <div class=\"form-body pal\">\n            <div class=\"form-group\">\n              <label for=\"name\" class=\"col-md-3 control-label\">ชื่อ{{Heading}}<span class='require'>*</span></label>\n              <div class=\"col-md-5\">\n                <div class=\"input-icon right\">\n                  <input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" formControlName=\"name\" [(ngModel)]=\"selectedItem.name\" />\n                  <p class=\"text-danger\" *ngIf=\"name.hasError('required') && name.dirty\">กรุณากรอกชื่อ{{Heading}}</p>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <label for=\"detail\" class=\"col-md-3 control-label\">รายละเอียด<span class='require'>*</span></label>\n              <div class=\"col-md-5\">\n                <div class=\"input-icon right\">\n                  <textarea id=\"detail\" rows=\"3\" class=\"form-control\" name=\"detail\" formControlName=\"detail\" [(ngModel)]=\"selectedItem.detail\"></textarea>\n                  <p class=\"text-danger\" *ngIf=\"detail.hasError('required') && detail.dirty\">กรุณากรอกที่อยู่</p>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"editModal.hide()\">ยกเลิก</button>\n          <button type=\"submit\" class=\"btn btn-success btn-square\" [disabled]=\"!productTypeForm.valid\">บันทึก</button>\n        </div>\n      </form>\n    </div>\n  </div>\n</div>\n\n<div class=\"modal fade\" bsModal #deleteModal=\"bs-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"mySmallModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-sm modalEditProduct\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title pull-left\">ยืนยันการลบ{{Heading}}</h4>\n        <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"deleteModal.hide()\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\" *ngIf=\"selectedItem\">\n        <p>คุณแน่ใจที่ต้องการจะลบ \"{{selectedItem.name}}\"</p>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"deleteModal.hide()\">ยกเลิก</button>\n        <button type=\"button\" class=\"btn btn-success btn-square\" (click)=\"deleteItem()\">แน่ใจ</button>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ 1232:
/***/ (function(module, exports) {

module.exports = "<!-- title -->\n<div id=\"title-breadcrumb-option-demo\" class=\"page-title-breadcrumb\">\n  <div class=\"page-header pull-left\">\n    <div class=\"page-title\">{{Heading}}</div>\n  </div>\n  <ol class=\"breadcrumb page-breadcrumb\">\n    <li>\n      <i class=\"fa fa-home\"></i>&nbsp;\n      <a [routerLink]=\"['/dashboard']\">Home</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n      <a [routerLink]=\"['/product/manage']\">สินค้า</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n    </li>\n    <li class=\"active\">{{Heading}}</li>\n  </ol>\n  <div class=\"clearfix\"></div>\n</div>\n\n<!-- content -->\n<div class=\"page-content\" id=\"acceptProduct\">\n  <ul id=\"acceptProductTab\" class=\"nav nav-tabs ul-edit responsive\">\n    <li class=\"active\"><a href=\"#listAcceptProduct\" data-toggle=\"tab\">จัดการ{{Heading}}</a></li>\n  </ul>\n  <div id=\"acceptProductTabContent\" class=\"tab-content\">\n    <app-loading *ngIf=\"isLoading\"></app-loading>\n    <div id=\"listAcceptProduct\" class=\"tab-pane fade in active\">\n      <div class=\"row dateSelect\">\n        <label class=\"control-label\">เลือกช่วงวัน</label>\n        <div>\n          <my-date-range-picker [options]=\"myDateRangePickerOptions\" (dateRangeChanged)=\"onDateRangeChanged($event)\" [(ngModel)]=\"dateRange\"></my-date-range-picker>\n        </div>\n      </div>\n      <div class=\"row\" *ngIf=\"!isLoading\">\n        <div class=\"col-lg-12 datatable-simple\">\n          <table datatable class=\"table table-bordered\" [dtOptions]=\"dtOptions\" *ngIf=\"data\">\n            <thead>\n              <tr>\n                <th>#</th>\n                <th>วันที่</th>\n                <th>Barcode</th>\n                <th>สินค้า</th>\n                <th>จำนวน</th>\n                <th>หมายเหตุ</th>\n                <th>Actions</th>\n              </tr>\n            </thead>\n            <tbody>\n              <tr *ngFor=\"let item of data; let i = index\">\n                <td>{{i + 1}}</td>\n                <td>{{item.date}}</td>\n                <td>{{item.product.code}}</td>\n                <td>{{item.product.name}}</td>\n                <td>{{item.amount}}</td>\n                <td class=\"remark\">\n                  <p>{{item.remark}}</p>\n                </td>\n                <td>\n                  <div class=\"actionsBtn\">\n                    <a class=\"btn btn-info btn-outlined\" (click)=\"openEditModal(item.id)\"><i class=\"fa fa-edit\"></i></a>\n                    <a class=\"btn btn-danger btn-outlined\" (click)=\"openDeleteModal(item.id)\"><i class=\"fa fa-trash\"></i></a>\n                  </div>\n                </td>\n              </tr>\n            </tbody>\n          </table>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n\n<div class=\"modal fade\" bsModal #editModal=\"bs-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"mySmallModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-lg modalEditProduct\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title pull-left\">แก้ไข{{Heading}}</h4>\n        <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"editModal.hide()\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <form [formGroup]=\"acceptProductTypeForm\" class=\"form-horizontal\">\n        <div class=\"modal-body\" *ngIf=\"selectedItem\">\n          <div class=\"form-body pal\">\n            <div class=\"form-group\">\n              <label for=\"productSelected\" class=\"col-md-3 control-label\">ค้นหาสินค้า</label>\n              <div class=\"col-md-8\" id=\"addCustomerInput\">\n                <input name=\"productSelected\" placeholder=\"กรุณากรอก barcode\" [(ngModel)]=\"productSelected.name\" [typeahead]=\"products\" [typeaheadItemTemplate]=\"customItemProductTemplate\"\n                  [typeaheadOptionField]=\"'code'\" (typeaheadNoResults)=\"changeTypeaheadNoResults($event)\" (typeaheadOnSelect)=\"typeaheadOnSelect($event)\"\n                  class=\"form-control\" formControlName=\"product_id\" />\n                <div class=\"no-data\" *ngIf=\"typeaheadNoResults\">\n                  <i class=\"fa fa-remove\"></i> ไม่พบสินค้า\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <label for=\"amount\" class=\"col-md-3 control-label\">จำนวน<span class='require'>*</span></label>\n              <div class=\"col-md-8\">\n                <div class=\"input-icon right\">\n                  <input type=\"text\" id=\"amount\" name=\"amount\" class=\"form-control\" formControlName=\"amount\" [(ngModel)]=\"selectedItem.amount\"\n                  />\n                  <p class=\"text-danger\" *ngIf=\"amount.hasError('required') && amount.dirty\">กรุณากรอกราคารวม</p>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <label for=\"remark\" class=\"col-md-3 control-label\">หมายเหตุ</label>\n              <div class=\"col-md-8\">\n                <div class=\"input-icon right\">\n                  <textarea id=\"remark\" rows=\"3\" class=\"form-control\" name=\"remark\" formControlName=\"remark\" [(ngModel)]=\"selectedItem.remark\"></textarea>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"editModal.hide()\">ยกเลิก</button>\n          <button type=\"button\" class=\"btn btn-success btn-square\" (click)=\"saveChange()\" [disabled]=\"!acceptProductTypeForm.valid\">บันทึก</button>\n        </div>\n      </form>\n    </div>\n  </div>\n</div>\n\n<div class=\"modal fade\" bsModal #deleteModal=\"bs-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"mySmallModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-sm modalEditProduct\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title pull-left\">ยืนยันการลบ{{Heading}}</h4>\n        <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"deleteModal.hide()\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\" *ngIf=\"selectedItem\">\n        <p>คุณแน่ใจที่ต้องการจะลบ \"{{selectedItem.product.name}}\" จำนวน {{selectedItem.amount}} ชิ้น</p>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"deleteModal.hide()\">ยกเลิก</button>\n        <button type=\"button\" class=\"btn btn-success btn-square\" (click)=\"deleteItem()\">แน่ใจ</button>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ 1233:
/***/ (function(module, exports) {

module.exports = "<!-- title -->\n<div id=\"title-breadcrumb-option-demo\" class=\"page-title-breadcrumb\">\n  <div class=\"page-header pull-left\">\n    <div class=\"page-title\">{{Heading}}</div>\n  </div>\n  <ol class=\"breadcrumb page-breadcrumb\">\n    <li>\n      <i class=\"fa fa-home\"></i>&nbsp;\n      <a [routerLink]=\"['/dashboard']\">Home</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n      <a [routerLink]=\"['/product/manage']\">สินค้า</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n      <a [routerLink]=\"['/product/accept/list']\">นำเข้าสินค้า</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n    </li>\n    <li class=\"active\">{{Heading}}</li>\n  </ol>\n  <div class=\"clearfix\"></div>\n</div>\n\n<!-- content -->\n<div class=\"page-content\" id=\"addAcceptProduct\">\n  <ul id=\"addAcceptProductTab\" class=\"nav nav-tabs ul-edit responsive\">\n    <li class=\"active\"><a href=\"#add\" data-toggle=\"tab\">{{Heading}}</a></li>\n  </ul>\n  <div id=\"addAcceptProductTabContent\" class=\"tab-content\">\n    <app-loading *ngIf=\"isLoading\"></app-loading>\n    <div id=\"add\" class=\"tab-pane fade in active\">\n      <form [formGroup]=\"acceptProductTypeForm\" class=\"form-horizontal\" (ngSubmit)=\"sendForm()\">\n        <div class=\"form-body pal\">\n          <div class=\"form-group\">\n            <label for=\"productSelected\" class=\"col-md-3 control-label\">ค้นหาสินค้า</label>\n            <div class=\"col-md-8\" id=\"addCustomerInput\">\n              <input name=\"productSelected\" placeholder=\"กรุณากรอก barcode\" [(ngModel)]=\"productSelected.name\" [typeahead]=\"products\" [typeaheadItemTemplate]=\"customItemProductTemplate\"\n                [typeaheadOptionField]=\"'code'\" (typeaheadNoResults)=\"changeTypeaheadNoResults($event)\" (typeaheadOnSelect)=\"typeaheadOnSelect($event)\"\n                class=\"form-control\" formControlName=\"product_id\" />\n              <div class=\"no-data\" *ngIf=\"typeaheadNoResults\">\n                <i class=\"fa fa-remove\"></i> ไม่พบสินค้า\n              </div>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label for=\"amount\" class=\"col-md-3 control-label\">จำนวน<span class='require'>*</span></label>\n            <div class=\"col-md-8\">\n              <div class=\"input-icon right\">\n                <input type=\"text\" id=\"amount\" name=\"amount\" class=\"form-control\" formControlName=\"amount\" [(ngModel)]=\"data.amount\" />\n                <p class=\"text-danger\" *ngIf=\"amount.hasError('required') && amount.dirty\">กรุณากรอกจำนวน</p>\n              </div>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label for=\"remark\" class=\"col-md-3 control-label\">หมายเหตุ</label>\n            <div class=\"col-md-8\">\n              <div class=\"input-icon right\">\n                <textarea id=\"remark\" rows=\"3\" class=\"form-control\" name=\"remark\" formControlName=\"remark\" [(ngModel)]=\"data.remark\"></textarea>\n              </div>\n            </div>\n          </div>\n          <div class=\"form-actions\">\n            <div class=\"col-md-8 col-md-offset-3\">\n              <button type=\"submit\" class=\"btn btn-success btn-outlined\" [disabled]=\"!acceptProductTypeForm.valid\">บันทึก</button>              &nbsp;\n              <button type=\"reset\" class=\"btn btn-danger btn-outlined\">ยกเลิก</button>\n            </div>\n          </div>\n        </div>\n      </form>\n    </div>\n  </div>\n</div>\n\n<template #customItemProductTemplate let-model=\"item\" let-index=\"index\">\n  <div class=\"nameAutocomplete\">\n    <h5><strong>barcode :: </strong> {{model.code}}</h5>\n    <h5><strong>ชื่อสินค้า :: </strong> {{model.name}}</h5>\n    <h5><strong>ประเภท :: </strong> {{model.product_type.name}}</h5>\n  </div>\n</template>"

/***/ }),

/***/ 1234:
/***/ (function(module, exports) {

module.exports = "<!-- title -->\n<div id=\"title-breadcrumb-option-demo\" class=\"page-title-breadcrumb\">\n  <div class=\"page-header pull-left\">\n    <div class=\"page-title\">{{Heading}}</div>\n  </div>\n  <ol class=\"breadcrumb page-breadcrumb\">\n    <li>\n      <i class=\"fa fa-home\"></i>&nbsp;\n      <a [routerLink]=\"['/dashboard']\">Home</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n      <a [routerLink]=\"['/product/manage/list']\">สินค้า</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n    </li>\n    <li class=\"active\">{{Heading}}</li>\n  </ol>\n  <div class=\"clearfix\"></div>\n</div>\n\n<!-- content -->\n<div class=\"page-content\" id=\"productInfo\" *ngIf=\"productDetail\">\n  <app-loading *ngIf=\"isProductDetailLoading\"></app-loading>\n  <div class=\"row\" id=\"sum_box\">\n    <div class=\"col-sm-6 col-md-4\">\n      <div class=\"panel green db mbm\">\n        <div class=\"panel-body\">\n          <p class=\"icon\"><i class=\"icon fa fa-cube\"></i></p>\n          <h4 class=\"value\">{{productDetail.name}}</h4>\n          <p class=\"description\">{{productDetail.product_type.name}}</p>\n        </div>\n      </div>\n    </div>\n    <!--<div class=\"col-sm-6 col-md-3\">\n      <div class=\"panel yellow db mbm\">\n        <div class=\"panel-body\">\n          <p class=\"icon\"><i class=\"icon fa fa-shopping-basket\"></i></p>\n          <h4 class=\"value\"><span>812</span><span> ครั้ง</span></h4>\n          <p class=\"description\">จำนวนการซื้อ</p>\n        </div>\n      </div>\n    </div>-->\n    <div class=\"col-sm-6 col-md-4\">\n      <div class=\"panel red db mbm\">\n        <div class=\"panel-body\">\n          <p class=\"icon\"><i class=\"icon fa fa-cubes\"></i></p>\n          <h4 class=\"value\"><span>{{productDetail.count_in_stores}}</span><span> ชิ้น</span></h4>\n          <p class=\"description\">จำนวนในคลัง</p>\n        </div>\n      </div>\n    </div>\n    <div class=\"col-sm-6 col-md-4\">\n      <div class=\"panel blue db mbm\">\n        <div class=\"panel-body\">\n          <p class=\"icon\"><i class=\"icon fa fa-money\"></i></p>\n          <h4 class=\"value\"><span>{{productDetail.sell_price || 0}}</span><span> บาท</span></h4>\n          <p class=\"description\">ราคาขาย</p>\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"row\" id=\"productInfoContent\">\n    <div class=\"col-md-6\">\n      <ul id=\"saleHistoryTab\" class=\"nav nav-tabs ul-edit responsive\">\n        <!--<li class=\"active\"><a href=\"#manageCustomer\" data-toggle=\"tab\">ประวัติการซื้อ</a></li>-->\n      </ul>\n      <div class=\"tab-content\">\n        <div class=\"tab-pane fade in active\">\n          <!--<app-loading *ngIf=\"isProductDetailLoading\"></app-loading>-->\n          <div class=\"row\" *ngIf=\"productDetail\">\n            <div class=\"col-md-12\">\n              <h3 class=\"title\">รายละเอียด</h3>\n            </div>\n            <div class=\"col-md-12 detail\">\n              <form [formGroup]=\"productForm\" class=\"form-horizontal\">\n                <div class=\"row\">\n                  <h4 class=\"col-md-4 nameDatail\">Barcode :</h4>\n                  <p class=\"col-md-8 contentDetail\" *ngIf=\"!edit\">{{productDetail.code}}</p>\n                  <input id=\"code\" type=\"text\" placeholder=\"barcode\" class=\"col-md-6 contentDetail\" formControlName=\"code\" *ngIf=\"edit\" [(ngModel)]=\"productDetail.code\"\n                  />\n                </div>\n                <div class=\"row\">\n                  <h4 class=\"col-md-4 nameDatail\">ชื่อสินค้า :</h4>\n                  <p class=\"col-md-8 contentDetail\" *ngIf=\"!edit\">{{productDetail.name}}</p>\n                  <input id=\"name\" type=\"text\" placeholder=\"ชื่อสินค้า\" class=\"col-md-6 contentDetail\" formControlName=\"name\" *ngIf=\"edit\"\n                    [(ngModel)]=\"productDetail.name\" />\n                </div>\n                <div class=\"row\">\n                  <h4 class=\"col-md-4 nameDatail\">ประเภท :</h4>\n                  <p class=\"col-md-8 contentDetail\" *ngIf=\"!edit\">{{productDetail.product_type.name}}</p>\n                  <ng-select name=\"product_type_id\" formControlName=\"product_type_id\" [options]=\"product_type_data\" [multiple]=\"false\" highlightColor=\"#d94e37\"\n                    highlightTextColor=\"#ffffff\" *ngIf=\"product_type_datas && edit\" notFoundMsg=\"ไม่พบประเภทสินค้า\" class=\"col-md-6 contentDetail\">\n                  </ng-select>\n                </div>\n                <div class=\"row\">\n                  <h4 class=\"col-md-4 nameDatail\">ต้นทุน :</h4>\n                  <p class=\"col-md-8 contentDetail\" *ngIf=\"!edit\">{{productDetail.cost}}</p>\n                  <input id=\"cost\" type=\"text\" placeholder=\"ต้นทุน\" class=\"col-md-6 contentDetail\" formControlName=\"cost\" *ngIf=\"edit\" [(ngModel)]=\"productDetail.cost\"\n                  />\n                </div>\n                <div class=\"row\">\n                  <h4 class=\"col-md-4 nameDatail\">ขายจริง :</h4>\n                  <p class=\"col-md-8 contentDetail\" *ngIf=\"!edit\">{{productDetail.sell_price}}</p>\n                  <input id=\"sell_price\" type=\"text\" placeholder=\"ชื่อสินค้า\" class=\"col-md-6 contentDetail\" name=\"sell_price\" formControlName=\"sell_price\"\n                    [(ngModel)]=\"productDetail.sell_price\" *ngIf=\"edit\" />\n                </div>\n                <div class=\"row\">\n                  <h4 class=\"col-md-4 nameDatail\">รายละเอียด :</h4>\n                  <p class=\"col-md-8 contentDetail\" *ngIf=\"!edit\">{{productDetail.detail}}</p>\n                  <textarea id=\"detail\" rows=\"3\" class=\"col-md-6 contentDetail\" name=\"detail\" formControlName=\"detail\" *ngIf=\"edit\" [(ngModel)]=\"productDetail.detail\"></textarea>\n                </div>\n                <div class=\"row\">\n                  <h4 class=\"col-md-4 nameDatail\">หมายเหตุ :</h4>\n                  <p class=\"col-md-8 contentDetail\" *ngIf=\"!edit\">{{productDetail.remark}}</p>\n                  <textarea id=\"remark\" rows=\"3\" class=\"col-md-6 contentDetail\" name=\"remark\" formControlName=\"remark\" *ngIf=\"edit\" [(ngModel)]=\"productDetail.remark\"></textarea>\n                </div>\n                <div class=\"row\" *ngIf=\"edit\">\n                  <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"edit = !edit\">ยกเลิก</button>\n                  <button type=\"button\" class=\"btn btn-success btn-square\" (click)=\"saveChange()\">บันทึก</button>\n                </div>\n                <div class=\"row\" *ngIf=\"!edit\">\n                  <button class=\"btn btn-info btn-outlined\" (click)=\"edit = !edit\"><i class=\"fa fa-edit\"></i>  แก้ไข</button>\n                  <button class=\"btn btn-violet btn-outlined\" (click)=\"printBarcode()\"><i class=\"fa fa-barcode\"></i>  barcode</button>\n                </div>\n              </form>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"col-md-6\">\n      <ul id=\"saleHistoryTab\" class=\"nav nav-tabs ul-edit responsive\">\n        <!--<li class=\"active\"><a href=\"#manageCustomer\" data-toggle=\"tab\">ประวัติการซื้อ</a></li>-->\n      </ul>\n      <div class=\"tab-content\">\n        <div class=\"tab-pane fade in active\">\n          <div class=\"row\">\n            <div class=\"col-md-12\">\n              <h3 class=\"title\">สินค้าในคลัง</h3>\n            </div>\n            <div class=\"col-md-12 detail\">\n              <table class=\"table table-hover\">\n                <thead>\n                  <tr>\n                    <th width=\"10%\">#</th>\n                    <th width=\"30%\">สาขา</th>\n                    <th width=\"20%\">เข้า</th>\n                    <th width=\"20%\">ออก</th>\n                    <th width=\"20%\">เหลือ</th>\n                  </tr>\n                </thead>\n                <tbody>\n                  <tr *ngFor=\"let item of productDetail.shops; let i = index\">\n                    <td>{{i+1}}</td>\n                    <td>{{item.name}}</td>\n                    <td>{{item.transectionIn}}</td>\n                    <td>{{item.transectionOut}}</td>\n                    <td>\n                      <span class=\"label label-warning\" *ngIf=\"item.amount === 0\">{{item.amount}}</span>\n                      <span class=\"label label-success\" *ngIf=\"item.amount > 0\">{{item.amount}}</span>\n                      <span class=\"label label-danger\" *ngIf=\"item.amount < 0\">{{item.amount}}</span>\n                    </td>\n                  </tr>\n                </tbody>\n              </table>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ 1235:
/***/ (function(module, exports) {

module.exports = "<!-- title -->\n<div id=\"title-breadcrumb-option-demo\" class=\"page-title-breadcrumb\">\n  <div class=\"page-header pull-left\">\n    <div class=\"page-title\">{{Heading}}</div>\n  </div>\n  <ol class=\"breadcrumb page-breadcrumb\">\n    <li>\n      <i class=\"fa fa-home\"></i>&nbsp;\n      <a [routerLink]=\"['/dashboard']\">Home</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n      <a [routerLink]=\"['/product/manage/list']\">{{Heading}}</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n    </li>\n    <li class=\"active\">เพิ่ม{{Heading}}</li>\n  </ol>\n\n  <div class=\"clearfix\"></div>\n</div>\n\n<!-- content -->\n<div class=\"page-content\" id=\"manageProduct\">\n  <ul id=\"manageProductTab\" class=\"nav nav-tabs ul-edit responsive\">\n    <li class=\"active\"><a href=\"#addProduct\" data-toggle=\"tab\">เพิ่ม{{Heading}}</a></li>\n  </ul>\n  <div id=\"manageProductTabContent\" class=\"tab-content\">\n    <div id=\"addProduct\" class=\"tab-pane fade in active\">\n      <form [formGroup]=\"productForm\" class=\"form-horizontal\" (ngSubmit)=\"sendForm()\">\n        <div class=\"form-body pal\">\n          <div class=\"form-group\">\n            <label for=\"code\" class=\"col-md-3 control-label\">code</label>\n            <div class=\"col-md-8\">\n              <div class=\"row codesRow\">\n                <div class=\"col-md-6 codesDiv\" *ngFor=\"let item of codes; let i = index\">\n                  <div class=\"input-group\">\n                    <input id=\"code\" type=\"text\" placeholder=\"ID\" class=\"form-control\" [(ngModel)]=\"codes[i].code\" [ngModelOptions]=\"{standalone: true}\"\n                      [disabled]=\"autoCodeProduct\" />\n                    <span class=\"input-group-addon\" (click)=deleteCodes(i)><i class=\"fa fa-times\"></i></span>\n                  </div>\n                </div>\n              </div>\n              <div class=\"row codesRow radioButtonCustom\">\n                <button class=\"btn btn-success btn-outlined btn addCode\" (click)=\"addCodes()\" type=\"button\" [disabled]=\"autoCodeProduct\">เพิ่ม code</button>\n                <label class=\"btn active\">\n                  <input type=\"checkbox\" (change)=\"checkAutoCode()\" name=\"autoCode\" formControlName=\"autoCode\" [(ngModel)]=\"autoCodeProduct\"  >\n                  <i class=\"fa fa-square-o notCheck\"></i>\n                  <i class=\"fa fa-check-square selected\"></i>\n                  <span>สร้าง ID อัตโนมัติ</span>\n                </label>\n              </div>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label for=\"name\" class=\"col-md-3 control-label\">ชื่อ{{Heading}}<span class='require'>*</span></label>\n            <div class=\"col-md-8\">\n              <div>\n                <div class=\"row\">\n                  <div class=\"col-md-2 selectCenter\">\n                    <select [(ngModel)]=\"brands_select\" class=\"selectCenter\" name=\"brands\" style=\"width:100%\" [ngModelOptions]=\"{standalone: true}\">\n                        <option value=\"\">อื่นๆ</option>\n                        <option *ngFor=\"let brand of brands\" [value]=\"brand\">{{brand}}</option>\n                    </select>\n                    <!--<select class=\"selectCenter ng-pristine ng-valid ng-touched\" name=\"status\" style=\"width:100%\">\n                      <option value=\"\" ng-reflect-value=\"1\">อื่นๆ</option>\n                      <option value=\"2\" ng-reflect-value=\"2\">กำลังเปลี่ยน</option>\n                      <option value=\"3\" ng-reflect-value=\"3\">สำเร็จ</option>\n                      <option value=\"4\" ng-reflect-value=\"4\">ข้อผิดพลาด</option>\n                    </select>-->\n                  </div>\n                  <div class=\"col-md-10\">\n                    <input id=\"name\" type=\"text\" placeholder=\"ชื่อ{{Heading}}\" name=\"name\" class=\"form-control\" formControlName=\"name\" />\n                  </div>\n                </div>\n                <p class=\"text-danger\" *ngIf=\"name.hasError('required') && name.dirty\">กรุณากรอกชื่อสินค้า</p>\n              </div>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label for=\"product_type_id\" class=\"col-md-3 control-label\">ประเภทสินค้า<span class='require'>*</span></label>\n            <div class=\"col-md-7 selectProductType\">\n              <ng-select name=\"product_type_id\" formControlName=\"product_type_id\" [options]=\"product_type_data\" [multiple]=\"false\" highlightColor=\"#d94e37\"\n                highlightTextColor=\"#ffffff\" *ngIf=\"product_type_datas\" notFoundMsg=\"ไม่พบประเภทสินค้า\">\n              </ng-select>\n              <p class=\"text-danger\" *ngIf=\"product_type_id.hasError('required') && product_type_id.dirty\">กรุณาเลือกประเภทสินค้า</p>\n            </div>\n            <div class=\"col-md-1 addProductTypeBtn\">\n              <button class=\"btn col-md-1 btn-success btn-outlined\" (click)=\"openEditModal()\" type=\"button\">เพิ่มประเภท</button>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label for=\"cost\" class=\"col-md-3 control-label\">ราคาต้นทุน<span class='require'>*</span></label>\n            <div class=\"col-md-8\">\n              <div>\n                <input id=\"cost\" type=\"text\" placeholder=\"ราคาต้นทุน\" name=\"cost\" class=\"form-control\" formControlName=\"cost\" />\n                <p class=\"text-danger\" *ngIf=\"cost.hasError('required') && cost.dirty\">กรุณากรอกราคาขายจริง</p>\n              </div>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label for=\"sell_price\" class=\"col-md-3 control-label\">ราคาขายจริง<span class='require'>*</span></label>\n            <div class=\"col-md-8\">\n              <div>\n                <input id=\"sell_price\" type=\"text\" placeholder=\"ราคาขายจริง\" name=\"sell_price\" class=\"form-control\" formControlName=\"sell_price\"\n                />\n                <p class=\"text-danger\" *ngIf=\"sell_price.hasError('required') && sell_price.dirty\">กรุณากรอกราคาขายจริง</p>\n              </div>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label for=\"detail\" class=\"col-md-3 control-label\">รายละเอียด</label>\n            <div class=\"col-md-8\">\n              <div>\n                <textarea id=\"detail\" rows=\"3\" class=\"form-control\" name=\"detail\" formControlName=\"detail\"></textarea>\n              </div>\n            </div>\n          </div>\n          <div class=\"form-group\"><label for=\"inputDetail\" class=\"col-md-3 control-label\">หมายเหตุ</label>\n            <div class=\"col-md-8\">\n              <textarea id=\"inputDetail\" rows=\"3\" class=\"form-control\" formControlName=\"remark\"></textarea>\n            </div>\n          </div>\n          <div class=\"form-group\" *ngIf=\"autoTranIn\">\n            <div class=\"col-md-8 radioButtonCustom col-md-offset-3 clearMargin\">\n              <div>\n                <label class=\"btn active\">\n                  <input type=\"checkbox\" name=\"isCreateTranIn\" formControlName=\"isCreateTranIn\" [(ngModel)]=\"isCreateTranIn\">\n                  <i class=\"fa fa-square-o notCheck\"></i>\n                  <i class=\"fa fa-check-square selected\"></i>\n                  <span>นำเข้าสินค้าอัตโนมัติ</span>\n                </label>\n              </div>\n            </div>\n          </div>\n          <div class=\"form-actions\">\n            <div class=\"col-md-12\">\n              <button type=\"submit\" class=\"btn btn-success btn-outlined\" [disabled]=\"!productForm.valid\">บันทึก</button>              &nbsp;\n              <!--<button type=\"submit\" class=\"btn btn-success btn-outlined\">บันทึก</button> &nbsp;-->\n              <button type=\"button\" class=\"btn btn-danger btn-outlined\">ยกเลิก</button>\n            </div>\n          </div>\n        </div>\n      </form>\n    </div>\n  </div>\n</div>\n\n<div class=\"modal fade\" bsModal #editModal=\"bs-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"mySmallModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-lg modalEditProduct\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title pull-left\">เพิ่มประเภทสินค้า</h4>\n        <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"editModal.hide()\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <form [formGroup]=\"productTypeForm\" class=\"form-horizontal\" (ngSubmit)=\"saveProductType()\">\n        <div class=\"modal-body\">\n          <div class=\"form-body pal\">\n            <div class=\"form-group\">\n              <label for=\"name\" class=\"col-md-3 control-label\">ชื่อประเภทสินค้า<span class='require'>*</span></label>\n              <div class=\"col-md-5\">\n                <div class=\"input-icon right\">\n                  <input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" formControlName=\"name_product_type\" />\n                  <p class=\"text-danger\" *ngIf=\"name_product_type.hasError('required') && name_product_type.dirty\">กรุณากรอกชื่อ{{Heading}}</p>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <label for=\"detail\" class=\"col-md-3 control-label\">รายละเอียด<span class='require'>*</span></label>\n              <div class=\"col-md-5\">\n                <div class=\"input-icon right\">\n                  <textarea id=\"detail\" rows=\"3\" class=\"form-control\" name=\"detail\" formControlName=\"detail_product_type\"></textarea>\n                  <p class=\"text-danger\" *ngIf=\"detail_product_type.hasError('required') && detail_product_type.dirty\">กรุณากรอกรายละเอียด</p>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"editModal.hide()\">ยกเลิก</button>\n          <button type=\"submit\" class=\"btn btn-success btn-square\" [disabled]=\"!productTypeForm.valid\">บันทึก</button>\n        </div>\n      </form>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ 1236:
/***/ (function(module, exports) {

module.exports = "<!-- title -->\n<div id=\"title-breadcrumb-option-demo\" class=\"page-title-breadcrumb\">\n  <div class=\"page-header pull-left\">\n    <div class=\"page-title\">{{Heading}}</div>\n  </div>\n  <ol class=\"breadcrumb page-breadcrumb\">\n    <li>\n      <i class=\"fa fa-home\"></i>&nbsp;\n      <a [routerLink]=\"['/dashboard']\">Home</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n    </li>\n    <li class=\"active\">{{Heading}}</li>\n  </ol>\n\n  <div class=\"clearfix\"></div>\n</div>\n\n<!-- content -->\n<div class=\"page-content\" id=\"manageProduct\">\n  <ul class=\"nav nav-tabs ul-edit responsive\">\n    <li class=\"active\">\n      <a href=\"#listProduct\" data-toggle=\"tab\">ค้นหา{{Heading}}</a>\n    </li>\n  </ul>\n  <div class=\"tab-content\">\n    <form [formGroup]=\"productSearchForm\" class=\"form-horizontal\" (ngSubmit)=\"sendSearchForm()\">\n      <div class=\"row\">\n        <div class=\"form-group  col-md-4\">\n          <label for=\"name\" class=\"col-md-5 control-label\">คำค้นหา</label>\n          <div class=\"col-md-7\">\n            <input id=\"name\" type=\"text\" placeholder=\"ชื่อสินค้าหรือรายละเอียด\" name=\"search_text\" class=\"form-control\" formControlName=\"search_text\"\n            />\n          </div>\n        </div>\n        <div class=\"form-group col-md-4\">\n          <label for=\"search_type\" class=\"col-md-5 control-label\">ประเภทสินค้า</label>\n          <div class=\"col-md-7 selectProductType\">\n            <!-- <ng-select name=\"search_type\" formControlName=\"search_type\" [options]=\"product_search_type\" [multiple]=\"false\" highlightColor=\"#d94e37\"\n              highlightTextColor=\"#ffffff\" *ngIf=\"product_type_datas\" notFoundMsg=\"ไม่พบประเภทสินค้า\">\n            </ng-select> -->\n            <select formControlName=\"search_type\" name=\"search_type\" class=\"selection\">\n              <option *ngFor=\"let item of product_search_type\" [attr.value]=\"item.value\" [attr.selected]=\"item.value == 0 ? true : null\">{{item.label}}</option>\n            </select>\n          </div>\n        </div>\n        <div class=\"form-group col-md-4\">\n          <label for=\"search_status\" class=\"col-md-5 control-label\">สถานะสินค้า</label>\n          <div class=\"col-md-7 selectProductType\">\n            <!-- <ng-select name=\"search_status\" formControlName=\"search_status\" [options]=\"product_status_data\" [multiple]=\"false\" highlightColor=\"#d94e37\"\n              highlightTextColor=\"#ffffff\" *ngIf=\"product_status_data\">\n            </ng-select> -->\n            <select formControlName=\"search_status\" name=\"search_status\" class=\"selection\">\n              <option *ngFor=\"let item of product_status_data\" [attr.value]=\"item.value\" [attr.selected]=\"item.value == 0 ? true : null\">{{item.label}}</option>\n            </select>\n          </div>\n        </div>\n      </div>\n      <div class=\"row\" style=\"text-align:end\">\n        <div class=\"col-md-12\">\n          <button type=\"submit\" class=\"btn btn-success btn-outlined\">ค้นหา</button> &nbsp;\n          <button type=\"reset\" class=\"btn btn-danger btn-outlined\">ล้าง</button>\n        </div>\n      </div>\n    </form>\n  </div>\n  <ul id=\"manageProductTab\" class=\"nav nav-tabs ul-edit responsive\" *ngIf=\"product_data\">\n    <li class=\"active\">\n      <a href=\"#listProduct\" data-toggle=\"tab\">จัดการ{{Heading}}</a>\n    </li>\n  </ul>\n  <div id=\"manageProductTabContent\" class=\"tab-content\" *ngIf=\"product_data\">\n    <app-loading *ngIf=\"isLoading\"></app-loading>\n    <div id=\"listProduct\" class=\"tab-pane fade in active\">\n      <div class=\"row\" [hidden]=\"!haveSelected\" style=\"height: 50px;\">\n        <div class=\"col-md-8\">\n          <div class=\"actionWhenSelect\">\n            <button class=\"btn btn-violet btn-outlined\" (click)=\"printBarcode()\">\n              <i class=\"fa fa-barcode\"></i>&nbsp;พิมพ์ Barcode ({{selectedLength}})</button>\n          </div>\n        </div>\n      </div>\n      <div class=\"row\" *ngIf=\"!isLoading\">\n        <div class=\"col-lg-12 datatable-simple\">\n          <table datatable class=\"table table-bordered\" [dtOptions]=\"dtOptions\" *ngIf=\"product_data\">\n            <thead>\n              <tr>\n                <th></th>\n                <th>barcode</th>\n                <th>ชื่อสินค้า</th>\n                <th>ประเภท</th>\n                <th>ต้นทุน</th>\n                <th>ราคา</th>\n                <th>Actions</th>\n              </tr>\n            </thead>\n            <tbody>\n              <tr *ngFor=\"let item of product_data\">\n                <td class=\"radioButtonCustom\">\n                  <label class=\"btn active\">\n                    <input type=\"checkbox\" class=\"form-control\" (change)=\"checkProduct()\" name=\"checkProduct\" />\n                    <i class=\"fa fa-square-o notCheck\"></i>\n                    <i class=\"fa fa-check-square selected\"></i>\n                  </label>\n                </td>\n                <td>{{item.code}}</td>\n                <td class=\"contentLeft\">{{item.name}}</td>\n                <td>{{item.product_type && item.product_type.name}}</td>\n                <td>{{item.cost}}</td>\n                <td>{{item.sell_price}}</td>\n                <td>\n                  <div class=\"actionsBtn\">\n                    <a class=\"btn btn-yellow btn-outlined\" (click)=\"showDetail(item.id)\">\n                      <i class=\"fa fa-info\"></i>\n                    </a>\n                    <a class=\"btn btn-violet btn-outlined\" (click)=\"printBarcode(item.id)\" target=\"_blank\">\n                      <i class=\"fa fa-barcode\"></i>\n                    </a>\n                    <a class=\"btn btn-info btn-outlined\" (click)=\"openEditModal(item.id)\">\n                      <i class=\"fa fa-edit\"></i>\n                    </a>\n                    <a class=\"btn btn-danger btn-outlined\" (click)=\"openDeleteModal(item.id)\">\n                      <i class=\"fa fa-trash\"></i>\n                    </a>\n                  </div>\n                </td>\n              </tr>\n            </tbody>\n          </table>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n<div class=\"modal fade\" bsModal #editModal=\"bs-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"mySmallModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-lg modalEditProduct\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title pull-left\">แก้ไข{{Heading}}</h4>\n        <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"editModal.hide()\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <form [formGroup]=\"productForm\" class=\"form-horizontal\" (ngSubmit)=\"saveChange()\">\n        <div class=\"modal-body\" *ngIf=\"selectedItem\">\n          <div class=\"form-body pal\">\n            <div class=\"form-group\">\n              <label for=\"code\" class=\"col-md-3 control-label\">code</label>\n              <div class=\"col-md-8 radioButtonCustom\">\n                <div>\n                  <input id=\"code\" type=\"text\" placeholder=\"ID\" class=\"form-control\" formControlName=\"code\" [(ngModel)]=\"selectedItem.code\"\n                  />\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <label for=\"name\" class=\"col-md-3 control-label\">ชื่อ{{Heading}}\n                <span class='require'>*</span>\n              </label>\n              <div class=\"col-md-8\">\n                <div>\n                  <input id=\"name\" type=\"text\" placeholder=\"ชื่อ{{Heading}}\" name=\"name\" class=\"form-control\" formControlName=\"name\" [(ngModel)]=\"selectedItem.name\"\n                  />\n                  <p class=\"text-danger\" *ngIf=\"name.hasError('required') && name.dirty\">กรุณากรอกชื่อสินค้า</p>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <label for=\"product_type_id\" class=\"col-md-3 control-label\">ประเภทสินค้า\n                <span class='require'>*</span>\n              </label>\n              <div class=\"col-md-8 selectProductType\">\n                <!--<ng-select name=\"product_type_id\" formControlName=\"product_type_id\" [options]=\"product_type_data\" [multiple]=\"false\" highlightColor=\"#d94e37\"\n                  highlightTextColor=\"#ffffff\" *ngIf=\"product_type_datas\" notFoundMsg=\"ไม่พบประเภทสินค้า\">\n                </ng-select>-->\n                <select [(ngModel)]=\"selectedItem.product_type_id\" formControlName=\"product_type_id\" name=\"Select name\" class=\"selection\">\n                  <option *ngFor=\"let item of product_type_datas\" [attr.value]=\"item.id\" [attr.selected]=\"item.id == selectedItem.product_type_id ? true : null\">{{item.name}}</option>\n                </select>\n                <p class=\"text-danger\" *ngIf=\"product_type_id.hasError('required') && product_type_id.dirty\">กรุณาเลือกประเภทสินค้า</p>\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <label for=\"cost\" class=\"col-md-3 control-label\">ราคาต้นทุน</label>\n              <div class=\"col-md-8\">\n                <div>\n                  <input id=\"cost\" type=\"text\" placeholder=\"ราคาต้นทุน\" name=\"cost\" class=\"form-control\" formControlName=\"cost\" [(ngModel)]=\"selectedItem.cost\"\n                  />\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <label for=\"sell_price\" class=\"col-md-3 control-label\">ราคาขายจริง\n                <span class='require'>*</span>\n              </label>\n              <div class=\"col-md-8\">\n                <div>\n                  <input id=\"sell_price\" type=\"text\" placeholder=\"ราคาขายจริง\" name=\"sell_price\" class=\"form-control\" formControlName=\"sell_price\"\n                    [(ngModel)]=\"selectedItem.sell_price\" />\n                  <p class=\"text-danger\" *ngIf=\"sell_price.hasError('required') && sell_price.dirty\">กรุณากรอกราคาขายจริง</p>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <label for=\"detail\" class=\"col-md-3 control-label\">รายละเอียด</label>\n              <div class=\"col-md-8\">\n                <div>\n                  <textarea id=\"detail\" rows=\"3\" class=\"form-control\" name=\"detail\" formControlName=\"detail\" [(ngModel)]=\"selectedItem.detail\"></textarea>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <label for=\"inputDetail\" class=\"col-md-3 control-label\">หมายเหตุ</label>\n              <div class=\"col-md-8\">\n                <textarea id=\"inputDetail\" rows=\"3\" class=\"form-control\" formControlName=\"remark\" [(ngModel)]=\"selectedItem.remark\"></textarea>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"editModal.hide()\">ยกเลิก</button>\n          <button type=\"submit\" class=\"btn btn-success btn-square\">บันทึก</button>\n        </div>\n      </form>\n    </div>\n  </div>\n</div>\n\n<div class=\"modal fade\" bsModal #deleteModal=\"bs-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"mySmallModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-sm modalEditProduct\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title pull-left\">ยืนยันการลบ{{Heading}}</h4>\n        <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"deleteModal.hide()\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\" *ngIf=\"selectedItem\">\n        <p>คุณแน่ใจที่ต้องการจะลบ \"{{selectedItem.name}}\"</p>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"deleteModal.hide()\">ยกเลิก</button>\n        <button type=\"button\" class=\"btn btn-success btn-square\" (click)=\"deleteItem()\">แน่ใจ</button>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ 1237:
/***/ (function(module, exports) {

module.exports = "<!-- title -->\n<div id=\"title-breadcrumb-option-demo\" class=\"page-title-breadcrumb\">\n  <div class=\"page-header pull-left\">\n    <div class=\"page-title\">{{Heading}}</div>\n  </div>\n  <ol class=\"breadcrumb page-breadcrumb\">\n    <li>\n      <i class=\"fa fa-home\"></i>&nbsp;\n      <a [routerLink]=\"['/dashboard']\">Home</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n      <a [routerLink]=\"['/product/manage/list']\">รายการสินค้า</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n    </li>\n    <li class=\"active\">{{Heading}}</li>\n  </ol>\n\n  <div class=\"clearfix\"></div>\n</div>\n\n<!-- content -->\n<div class=\"page-content\" id=\"barcodePrint\">\n  <div id=\"barcodePrint-page\" class=\"row\">\n    <div class=\"col-md-8\">\n      <div class=\"panel\">\n        <div id=\"print-section\">\n          <div class=\"panel-body\">\n            <div class=\"row \">\n              <div *ngFor=\"let item of barcodeSelect\">\n                <div class=\"eachBarcode col-md-3\" *ngFor=\"let i of item.quantity | arrayNumber\">\n                  <!--<img class=\"col-md-12\" src=\"{{item.barcode && 'http://www.pion.co.th/images/Knowledges/Barcode/barcode01.jpg'}}\" />-->\n                  <img class=\"col-md-12\" src=\"{{item.barcode}}\" />\n                  <p>{{item.code}}</p>\n                  <p>{{item.name}}</p>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"col-lg-4\">\n      <div class=\"row\">\n        <div class=\"col-md-12\">\n          <div class=\"panel barcodeForm\">\n            <div class=\"panel-heading\">เลือกจำนวน barcode\n            </div>\n            <div class=\"panel-body\">\n              <form action=\"#\" class=\"form-horizontal\">\n                <div class=\"list-group\">\n                  <div class=\"list-group-item\" *ngFor=\"let item of barcodeSelect; let i = index;\">\n                    <i class=\"fa fa-close\" (click)=\"removeItem(i)\"></i>\n                    <h5 class=\"list-group-item-heading\">{{item.name}}</h5>\n                    <div class=\"list-group-item-text\">\n                      <input name=\"'barcodeCount' + i\" type=\"number\" [(ngModel)]=\"item.quantity\" placeholder=\"จำนวน\" class=\"form-control\" />\n                    </div>\n                  </div>\n                </div>\n              </form>\n              <div class=\"row actionBtn\">\n                <!--<div class=\"col-md-4\">-->\n                <!--<button class=\"btn btn-info btn-outlined mrm\"><i class=\"fa fa-refresh\"></i>&nbsp; โหลดใหม่</button>-->\n                <!--</div>-->\n                <!--<div class=\"col-md-4\">-->\n                <button class=\"btn btn-success btn-outlined mrm\" (click)=\"print()\"><i class=\"fa fa-print\"></i>&nbsp; พิมพ์</button>\n                <button class=\"btn btn-success btn-outlined mrm\" (click)=\"printFormat()\"><i class=\"fa fa-print\"></i>&nbsp; พิมพ์ตาม format</button>\n                <!--</div>-->\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>"

/***/ }),

/***/ 1238:
/***/ (function(module, exports) {

module.exports = "<!-- title -->\n<div id=\"title-breadcrumb-option-demo\" class=\"page-title-breadcrumb\">\n  <div class=\"page-header pull-left\">\n    <div class=\"page-title\">{{Heading}}</div>\n  </div>\n  <ol class=\"breadcrumb page-breadcrumb\">\n    <li>\n      <i class=\"fa fa-home\"></i>&nbsp;\n      <a [routerLink]=\"['/dashboard']\">Home</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n      <a [routerLink]=\"['/product/manage']\">สินค้า</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n      <a [routerLink]=\"['/product/accept/list']\">การขายสินค้า</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n    </li>\n    <li class=\"active\">{{Heading}}</li>\n  </ol>\n  <div class=\"clearfix\"></div>\n</div>\n\n<!-- content -->\n<div class=\"page-content\" id=\"addSellProduct\">\n  <ul id=\"addSellProductTab\" class=\"nav nav-tabs ul-edit responsive\">\n    <li class=\"active\"><a href=\"#add\" data-toggle=\"tab\">{{Heading}}</a></li>\n  </ul>\n  <div id=\"addSellProductTabContent\" class=\"tab-content\">\n    <div id=\"add\" class=\"tab-pane fade in active\">\n      <form [formGroup]=\"sellProductForm\" class=\"form-horizontal\">\n        <div class=\"form-body pal\">\n          <div class=\"form-group\">\n            <label for=\"productSelected\" class=\"col-md-3 control-label\">สินค้า<span class='require'>*</span></label>\n            <div class=\"col-md-6\" id=\"addCustomerInput\">\n              <input name=\"productSelected\" placeholder=\"กรุณากรอก barcode\" [(ngModel)]=\"productSelected.name\" [typeahead]=\"products\" [typeaheadItemTemplate]=\"customItemProductTemplate\"\n                [typeaheadOptionField]=\"'code'\" (typeaheadNoResults)=\"changeTypeaheadNoResults('product',$event)\" (typeaheadOnSelect)=\"typeaheadOnSelect('product',$event)\"\n                class=\"form-control\" formControlName=\"product_id\" />\n              <div class=\"no-data\" *ngIf=\"typeaheadNoResults.product\">\n                <i class=\"fa fa-remove\"></i> ไม่พบสินค้า\n              </div>\n              <p class=\"text-danger\" *ngIf=\"product_id.hasError('required') && product_id.dirty\">กรุณากรอก barcode สินค้า</p>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label for=\"amount\" class=\"col-md-3 control-label\">จำนวน<span class='require'>*</span></label>\n            <div class=\"col-md-6\">\n              <div class=\"input-icon right\">\n                <input type=\"text\" id=\"amount\" name=\"amount\" class=\"form-control\" formControlName=\"amount\" (keyup)=\"multipleValue()\" [(ngModel)]=\"amount_value\" />\n                <p class=\"text-danger\" *ngIf=\"amount.hasError('required') && amount.dirty\">กรุณากรอกจำนวน</p>\n              </div>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label for=\"sell_price\" class=\"col-md-3 control-label\">ราคาขาย<span class='require'>*</span></label>\n            <div class=\"col-md-6\">\n              <div class=\"input-icon right\">\n                <input type=\"text\" id=\"sell_price\" name=\"sell_price\" class=\"form-control\" formControlName=\"sell_price\" [(ngModel)]=\"price\"\n                />\n                <p class=\"text-danger\" *ngIf=\"sell_price.hasError('required') && sell_price.dirty\">กรุณากรอกจำนวน</p>\n              </div>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label class=\"col-md-3 control-label\">ชื่อลูกค้า</label>\n            <div class=\"col-md-5\" id=\"addCustomerInput\">\n              <input name=\"nameCustomer\" placeholder=\"กรอกชื่อลูกค้า\" [(ngModel)]=\"customerSelected.name\" [typeahead]=\"customers\" [typeaheadItemTemplate]=\"customItemCustomerTemplate\"\n                [typeaheadOptionField]=\"'name'\" (typeaheadNoResults)=\"changeTypeaheadNoResults('customer',$event)\" (typeaheadOnSelect)=\"typeaheadOnSelect('customer',$event)\"\n                class=\"form-control\" formControlName=\"customer_id\" />\n              <div class=\"no-data\" *ngIf=\"typeaheadNoResults.customer\">\n                <i class=\"fa fa-remove\"></i> ไม่พบข้อมูลลูกค้า\n              </div>\n            </div>\n            <div class=\"col-md-1 addCustomerBtn\">\n              <button type=\"button\" class=\"btn btn-success btn-outlined btn-sm\" (click)=\"openAddCustomerModal()\">เพิ่ม</button>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label for=\"sell_type\" class=\"col-md-3 control-label\">ประเภทการขาย<span class='require'>*</span></label>\n            <div class=\"col-md-6 radioButtonCustom\">\n              <div>\n                <label class=\"btn active\">\n                    <input type=\"radio\" name=\"sell_type\" formControlName=\"sell_type\" [value]=\"1\" checked/>     \n                    <i class=\"fa fa-circle-o notCheck\"></i>\n                    <i class=\"fa fa-check-circle-o selected\"></i>\n                    <span> ขายปลีก</span>\n                  </label>\n                <label class=\"btn\">\n                    <input type=\"radio\" name=\"sell_type\" formControlName=\"sell_type\" [value]=\"0\"  />    \n                    <i class=\"fa fa-circle-o notCheck\"></i>\n                    <i class=\"fa fa-check-circle-o selected\"></i>\n                    <span> ขายส่ง</span>\n                  </label>\n              </div>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label for=\"remark\" class=\"col-md-3 control-label\">หมายเหตุ</label>\n            <div class=\"col-md-6\">\n              <div class=\"input-icon right\">\n                <textarea id=\"remark\" rows=\"3\" class=\"form-control\" name=\"remark\" formControlName=\"remark\" [(ngModel)]=\"data.remark\"></textarea>\n              </div>\n            </div>\n          </div>\n          <div class=\"form-actions\">\n            <div class=\"col-md-6 col-md-offset-3\">\n              <button type=\"button\" class=\"btn btn-success btn-outlined\" [disabled]=\"!sellProductForm.valid\" (click)=\"sendForm()\">บันทึก</button>              &nbsp;\n              <button type=\"reset\" class=\"btn btn-danger btn-outlined\">ยกเลิก</button>\n            </div>\n          </div>\n        </div>\n      </form>\n    </div>\n  </div>\n</div>\n\n<template #customItemCustomerTemplate let-model=\"item\" let-index=\"index\">\n  <div class=\"nameAutocomplete\">\n    <h5><strong>ชื่อ :: </strong> {{model.name}}</h5>\n    <h5><strong>เบอร์ :: </strong> {{model.tel}}</h5>\n  </div>\n</template>\n\n<template #customItemProductTemplate let-model=\"item\" let-index=\"index\">\n  <div class=\"nameAutocomplete\">\n    <h5><strong>barcode :: </strong> {{model.code}}</h5>\n    <h5><strong>ชื่อสินค้า :: </strong> {{model.name}}</h5>\n    <h5><strong>ประเภท :: </strong> {{model.product_type.name}}</h5>\n  </div>\n</template>\n\n<div class=\"modal fade\" bsModal #editModal=\"bs-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"mySmallModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-lg modalEditProduct\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title pull-left\">เพิ่มลูกค้า</h4>\n        <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"editModal.hide()\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <form [formGroup]=\"customerForm\" class=\"form-horizontal\" (ngSubmit)=\"saveCustomer()\">\n        <div class=\"modal-body\">\n          <div class=\"form-body pal\">\n            <div class=\"form-group\">\n              <label for=\"name_customer\" class=\"col-md-3 control-label\">ชื่อ<span class='require'>*</span></label>\n              <div class=\"col-md-5\">\n                <div class=\"input-icon right\">\n                  <input type=\"text\" id=\"name_customer\" name=\"name_customer\" class=\"form-control\" formControlName=\"name_customer\" />\n                  <p class=\"text-danger\" *ngIf=\"name_customer.hasError('required') && name_customer.dirty\">กรุณากรอกชื่อ</p>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <label for=\"tel_customer\" class=\"col-md-3 control-label\">เบอร์ติดต่อ<span class='require'>*</span></label>\n              <div class=\"col-md-5\">\n                <div class=\"input-icon right\">\n                  <input type=\"tel\" id=\"tel_customer\" name=\"tel_customer\" class=\"form-control\" formControlName=\"tel_customer\" />\n                  <p class=\"text-danger\" *ngIf=\"tel_customer.hasError('required') && tel_customer.dirty\">กรุณากรอกเบอร์ติดต่อ</p>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"editModal.hide()\">ยกเลิก</button>\n          <button type=\"submit\" class=\"btn btn-success btn-square\" [disabled]=\"!customerForm.valid\">บันทึก</button>\n        </div>\n      </form>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ 1239:
/***/ (function(module, exports) {

module.exports = "<!-- title -->\n<div id=\"title-breadcrumb-option-demo\" class=\"page-title-breadcrumb\">\n  <div class=\"page-header pull-left\">\n    <div class=\"page-title\">{{Heading}}</div>\n  </div>\n  <ol class=\"breadcrumb page-breadcrumb\">\n    <li>\n      <i class=\"fa fa-home\"></i>&nbsp;\n      <a [routerLink]=\"['/dashboard']\">Home</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n      <a [routerLink]=\"['/product/manage']\">สินค้า</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n    </li>\n    <li class=\"active\">{{Heading}}</li>\n  </ol>\n  <div class=\"clearfix\"></div>\n</div>\n\n<!-- content -->\n<div class=\"page-content\" id=\"sellProduct\">\n  <ul id=\"sellProductTab\" class=\"nav nav-tabs ul-edit responsive\">\n    <li class=\"active\"><a href=\"#listSellProduct\" data-toggle=\"tab\">จัดการ{{Heading}}</a></li>\n  </ul>\n  <div id=\"sellProductTabContent\" class=\"tab-content\">\n    <app-loading *ngIf=\"isLoading\"></app-loading>\n    <div id=\"listSellProduct\" class=\"tab-pane fade in active\">\n      <div class=\"row dateSelect\">\n        <label class=\"control-label\">เลือกช่วงวัน</label>\n        <div>\n          <my-date-range-picker [options]=\"myDateRangePickerOptions\" (dateRangeChanged)=\"onDateRangeChanged($event)\" [(ngModel)]=\"dateRange\"></my-date-range-picker>\n        </div>\n      </div>\n      <div class=\"row\" *ngIf=\"!isLoading\">\n        <div class=\"col-lg-12 datatable-simple\">\n          <table datatable class=\"table table-bordered\" [dtOptions]=\"dtOptions\" *ngIf=\"data\">\n            <thead>\n              <tr>\n                <th>#</th>\n                <th>วันที่</th>\n                <th>Barcode</th>\n                <th>จำนวน</th>\n                <th>ราคา</th>\n                <th>ลูกค้า</th>\n                <th>ประเภท</th>\n                <th>หมายเหตุ</th>\n                <th>Actions</th>\n              </tr>\n            </thead>\n            <tbody>\n              <tr *ngFor=\"let item of data; let i = index\">\n                <td>{{i + 1}}</td>\n                <td>{{item.date}}</td>\n                <td>{{item.product.code}}</td>\n                <td>{{item.amount}}</td>\n                <td>{{item.sell_price}}</td>\n                <td>{{item.customer ? item.customer.name : '-'}}</td>\n                <td>{{item.sell_type ? 'ขายปลีก' : 'ขายส่ง'}}</td>\n                <td class=\"remark\">\n                  <p>{{item.remark}}</p>\n                </td>\n                <td>\n                  <div class=\"actionsBtn\">\n                    <a class=\"btn btn-info btn-outlined\" (click)=\"openEditModal(item.id)\"><i class=\"fa fa-edit\"></i></a>\n                    <a class=\"btn btn-danger btn-outlined\" (click)=\"openDeleteModal(item.id)\"><i class=\"fa fa-trash\"></i></a>\n                  </div>\n                </td>\n              </tr>\n            </tbody>\n          </table>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n\n\n<div class=\"modal fade\" bsModal #editModal=\"bs-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"mySmallModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-lg modalEditProduct\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title pull-left\">แก้ไข{{Heading}}</h4>\n        <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"editModal.hide()\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <form [formGroup]=\"sellProductForm\" class=\"form-horizontal\">\n        <div class=\"modal-body\" *ngIf=\"selectedItem\">\n          <div class=\"form-body pal\">\n            <div class=\"form-group\">\n              <label for=\"product\" class=\"col-md-3 control-label\">สินค้า<span class='require'>*</span></label>\n              <div class=\"col-md-6\">\n                <div class=\"input-icon right\">\n                  <input type=\"text\" id=\"product\" name=\"product\" class=\"form-control\" [(ngModel)]=\"selectedItem.product.name\" [ngModelOptions]=\"{standalone: true}\"\n                    readonly/>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <label for=\"amount\" class=\"col-md-3 control-label\">จำนวน<span class='require'>*</span></label>\n              <div class=\"col-md-6\">\n                <div class=\"input-icon right\">\n                  <input type=\"text\" id=\"amount\" name=\"amount\" class=\"form-control\" formControlName=\"amount\" (keyup)=\"multipleValue()\" [(ngModel)]=\"amount_value\"\n                  />\n                  <p class=\"text-danger\" *ngIf=\"amount.hasError('required') && amount.dirty\">กรุณากรอกจำนวน</p>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <label for=\"sell_price\" class=\"col-md-3 control-label\">ราคาขาย<span class='require'>*</span></label>\n              <div class=\"col-md-6\">\n                <div class=\"input-icon right\">\n                  <input type=\"text\" id=\"sell_price\" name=\"sell_price\" class=\"form-control\" formControlName=\"sell_price\" [(ngModel)]=\"price\"\n                  />\n                  <p class=\"text-danger\" *ngIf=\"sell_price.hasError('required') && sell_price.dirty\">กรุณากรอกจำนวน</p>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <label class=\"col-md-3 control-label\">ชื่อลูกค้า</label>\n              <div class=\"col-md-6\" id=\"addCustomerInput\">\n                <input name=\"nameCustomer\" placeholder=\"กรอกชื่อลูกค้า\" [(ngModel)]=\"customerSelected.name\" [typeahead]=\"customers\" [typeaheadItemTemplate]=\"customItemCustomerTemplate\"\n                  [typeaheadOptionField]=\"'name'\" (typeaheadNoResults)=\"changeTypeaheadNoResults('customer',$event)\" (typeaheadOnSelect)=\"typeaheadOnSelect('customer',$event)\"\n                  class=\"form-control\" formControlName=\"customer_id\" />\n                <div class=\"no-data\" *ngIf=\"typeaheadNoResults.customer\">\n                  <i class=\"fa fa-remove\"></i> ไม่พบข้อมูลลูกค้า\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <label for=\"sell_type\" class=\"col-md-3 control-label\">ประเภทการขาย<span class='require'>*</span></label>\n              <div class=\"col-md-6 radioButtonCustom\">\n                <div>\n                  <label class=\"btn active\">\n                    <input type=\"radio\" name=\"sell_type\" [(ngModel)]=\"selectedItem.sell_type\" [value]=\"1\"  [ngModelOptions]=\"{standalone: true}\"/>     \n                    <i class=\"fa fa-circle-o notCheck\"></i>\n                    <i class=\"fa fa-check-circle-o selected\"></i>\n                    <span> ขายปลีก</span>\n                  </label>\n                  <label class=\"btn\">\n                    <input type=\"radio\" name=\"sell_type\"  [(ngModel)]=\"selectedItem.sell_type\" [value]=\"0\" [ngModelOptions]=\"{standalone: true}\" />    \n                    <i class=\"fa fa-circle-o notCheck\"></i>\n                    <i class=\"fa fa-check-circle-o selected\"></i>\n                    <span> ขายส่ง</span>\n                  </label>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <label for=\"remark\" class=\"col-md-3 control-label\">หมายเหตุ</label>\n              <div class=\"col-md-6\">\n                <div class=\"input-icon right\">\n                  <textarea id=\"remark\" rows=\"3\" class=\"form-control\" name=\"remark\" formControlName=\"remark\" [(ngModel)]=\"selectedItem.remark\"></textarea>\n                </div>\n              </div>\n            </div>\n          </div>\n          <div class=\"modal-footer\">\n            <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"editModal.hide()\">ยกเลิก</button>\n            <button type=\"button\" class=\"btn btn-success btn-square\" (click)=\"saveChange()\" [disabled]=\"!sellProductForm.valid\">บันทึก</button>\n          </div>\n        </div>\n      </form>\n      <!--<form [formGroup]=\"acceptProductTypeForm\" class=\"form-horizontal\" (ngSubmit)=\"saveChange()\">\n        <div class=\"modal-body\" *ngIf=\"selectedItem\">\n          <div class=\"form-body pal\">\n            <div class=\"form-group\">\n              <label for=\"productSelected\" class=\"col-md-3 control-label\">ค้นหาสินค้า</label>\n              <div class=\"col-md-8\" id=\"addCustomerInput\">\n                <input name=\"productSelected\" placeholder=\"กรุณากรอก barcode\" [(ngModel)]=\"productSelected.name\" [typeahead]=\"products\" [typeaheadItemTemplate]=\"customItemProductTemplate\"\n                  [typeaheadOptionField]=\"'code'\" (typeaheadNoResults)=\"changeTypeaheadNoResults($event)\" (typeaheadOnSelect)=\"typeaheadOnSelect($event)\"\n                  class=\"form-control\" formControlName=\"product_id\" />\n                <div class=\"no-data\" *ngIf=\"typeaheadNoResults\">\n                  <i class=\"fa fa-remove\"></i> ไม่พบสินค้า\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <label for=\"amount\" class=\"col-md-3 control-label\">จำนวน<span class='require'>*</span></label>\n              <div class=\"col-md-8\">\n                <div class=\"input-icon right\">\n                  <input type=\"text\" id=\"amount\" name=\"amount\" class=\"form-control\" formControlName=\"amount\" [(ngModel)]=\"selectedItem.amount\"\n                  />\n                  <p class=\"text-danger\" *ngIf=\"amount.hasError('required') && amount.dirty\">กรุณากรอกราคารวม</p>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <label for=\"remark\" class=\"col-md-3 control-label\">หมายเหตุ</label>\n              <div class=\"col-md-8\">\n                <div class=\"input-icon right\">\n                  <textarea id=\"remark\" rows=\"3\" class=\"form-control\" name=\"remark\" formControlName=\"remark\" [(ngModel)]=\"selectedItem.remark\"></textarea>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"modal-footer\">\n          <button type=\"submit\" class=\"btn btn-success btn-outlined\" [disabled]=\"!acceptProductTypeForm.valid\">บันทึก</button>          &nbsp;\n          <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"editModal.hide()\">ยกเลิก</button>\n        </div>\n      </form>-->\n    </div>\n  </div>\n</div>\n\n<div class=\"modal fade\" bsModal #deleteModal=\"bs-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"mySmallModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-sm modalEditProduct\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title pull-left\">ยืนยันการลบ{{Heading}}</h4>\n        <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"deleteModal.hide()\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\" *ngIf=\"selectedItem\">\n        <p>คุณแน่ใจที่ต้องการจะลบ \"{{selectedItem.product.name}}\" </p>\n        <p>จำนวน {{selectedItem.amount}} ชิ้น</p>\n        <p>ราคารวม {{selectedItem.sell_price}} บาท</p>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"deleteModal.hide()\">ยกเลิก</button>\n        <button type=\"button\" class=\"btn btn-success btn-square\" (click)=\"deleteItem()\">แน่ใจ</button>\n      </div>\n    </div>\n  </div>\n</div>\n\n<template #customItemTemplate let-model=\"item\" let-index=\"index\">\n  <h5><strong>ชื่อ :: </strong> {{model.first_name}} {{model.last_name}}</h5>\n  <h5><strong>เบอร์ :: </strong> {{model.phone}}</h5>\n</template>"

/***/ }),

/***/ 1240:
/***/ (function(module, exports) {

module.exports = "<!-- title -->\n<div id=\"title-breadcrumb-option-demo\" class=\"page-title-breadcrumb\">\n  <div class=\"page-header pull-left\">\n    <div class=\"page-title\">{{Heading}}</div>\n  </div>\n  <ol class=\"breadcrumb page-breadcrumb\">\n    <li>\n      <i class=\"fa fa-home\"></i>&nbsp;\n      <a [routerLink]=\"['/dashboard']\">Home</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n      <a [routerLink]=\"['/product/manage']\">สินค้า</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n      <a [routerLink]=\"['/product/accept/list']\">การโอนสินค้า</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n    </li>\n    <li class=\"active\">{{Heading}}</li>\n  </ol>\n  <div class=\"clearfix\"></div>\n</div>\n\n<!-- content -->\n<div class=\"page-content\" id=\"addTransferProduct\">\n  <ul id=\"addTransferProductTab\" class=\"nav nav-tabs ul-edit responsive\">\n    <li class=\"active\"><a href=\"#add\" data-toggle=\"tab\">{{Heading}}</a></li>\n  </ul>\n  <div id=\"addTransferProductTabContent\" class=\"tab-content\">\n    <app-loading *ngIf=\"isLoading\"></app-loading>\n    <div id=\"add\" class=\"tab-pane fade in active\">\n      <form [formGroup]=\"transferProductForm\" class=\"form-horizontal\">\n        <div class=\"form-body pal\">\n          <div class=\"form-group\">\n            <label for=\"productSelected\" class=\"col-md-3 control-label\">สินค้า<span class='require'>*</span></label>\n            <div class=\"col-md-6\" id=\"addCustomerInput\">\n              <input name=\"productSelected\" placeholder=\"กรุณากรอก barcode\" [(ngModel)]=\"productSelected.name\" [typeahead]=\"products\" [typeaheadItemTemplate]=\"customItemProductTemplate\"\n                [typeaheadOptionField]=\"'code'\" (typeaheadNoResults)=\"changeTypeaheadNoResults('product',$event)\" (typeaheadOnSelect)=\"typeaheadOnSelect('product',$event)\"\n                class=\"form-control\" formControlName=\"product_id\" />\n              <div class=\"no-data\" *ngIf=\"typeaheadNoResults.product\">\n                <i class=\"fa fa-remove\"></i> ไม่พบสินค้า\n              </div>\n              <p class=\"text-danger\" *ngIf=\"product_id.hasError('required') && product_id.dirty\">กรุณากรอก barcode สินค้า</p>\n            </div>\n          </div>\n          <div *ngIf=\"productSelected.code\">\n            <div class=\"form-group\">\n              <label for=\"amount\" class=\"col-md-3 control-label\">Barcode</label>\n              <div class=\"col-md-6\">\n                <div class=\"input-icon right\">\n                  <input type=\"text\" class=\"form-control\" disabled [(ngModel)]=\"productSelected.code\" [ngModelOptions]=\"{standalone: true}\"\n                  />\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <label for=\"amount\" class=\"col-md-3 control-label\">ชื่อสินค้า</label>\n              <div class=\"col-md-6\">\n                <div class=\"input-icon right\">\n                  <input type=\"text\" class=\"form-control\" disabled [(ngModel)]=\"productSelected.name\" [ngModelOptions]=\"{standalone: true}\"\n                  />\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <label for=\"amount\" class=\"col-md-3 control-label\">ต้นทุนสินค้า</label>\n              <div class=\"col-md-6\">\n                <div class=\"input-icon right\">\n                  <input type=\"text\" class=\"form-control\" disabled [(ngModel)]=\"productSelected.cost\" [ngModelOptions]=\"{standalone: true}\"\n                  />\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <label for=\"amount\" class=\"col-md-3 control-label\">ราคาขาย</label>\n              <div class=\"col-md-6\">\n                <div class=\"input-icon right\">\n                  <input type=\"text\" class=\"form-control\" disabled [(ngModel)]=\"productSelected.sell_price\" [ngModelOptions]=\"{standalone: true}\"\n                  />\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\" *ngIf=\"productSelected.detail\">\n              <label for=\"amount\" class=\"col-md-3 control-label\">รายละเอียด</label>\n              <div class=\"col-md-6\">\n                <div class=\"input-icon right\">\n                  <textarea type=\"text\" class=\"form-control\" disabled [(ngModel)]=\"productSelected.detail\" [ngModelOptions]=\"{standalone: true}\">\n                  </textarea>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\" *ngIf=\"productSelected.remark\">\n              <label for=\"amount\" class=\"col-md-3 control-label\">หมายเหตุ</label>\n              <div class=\"col-md-6\">\n                <div class=\"input-icon right\">\n                  <textarea type=\"text\" class=\"form-control\" disabled [(ngModel)]=\"productSelected.remark\" [ngModelOptions]=\"{standalone: true}\">\n                  </textarea>\n                </div>\n              </div>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label for=\"amount\" class=\"col-md-3 control-label\">จำนวน<span class='require'>*</span></label>\n            <div class=\"col-md-6\">\n              <div class=\"input-icon right\">\n                <input type=\"text\" id=\"amount\" name=\"amount\" class=\"form-control\" formControlName=\"amount\" />\n                <p class=\"text-danger\" *ngIf=\"amount.hasError('required') && amount.dirty\">กรุณากรอกจำนวน</p>\n              </div>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label class=\"col-md-3 control-label\">เลือกร้านค้า</label>\n            <div class=\"col-md-6\" id=\"addCustomerInput\">\n              <select [(ngModel)]=\"brands_select\" class=\"selectCenter\" name=\"brands\" formControlName=\"shop_destination_id\" style=\"width:100%\">\n                  <option value=\"\">อื่นๆ</option>\n                  <option *ngFor=\"let shop of shopDestinations\" [value]=\"shop.id\">{{shop.name}}</option>\n              </select>\n              <!--<input name=\"shop_destination_id\" placeholder=\"กรอกชื่อร้านค้า\" [(ngModel)]=\"shopDestinationSelected.name\" [typeahead]=\"shopDestinations\"\n                [typeaheadItemTemplate]=\"customItemshopDestinationTemplate\" [typeaheadOptionField]=\"'name'\" (typeaheadNoResults)=\"changeTypeaheadNoResults('shopDestination',$event)\"\n                (typeaheadOnSelect)=\"typeaheadOnSelect('shopDestination',$event)\" class=\"form-control\" formControlName=\"shop_destination_id\"\n              />\n              <div class=\"no-data\" *ngIf=\"typeaheadNoResults.shopDestination\">\n                <i class=\"fa fa-remove\"></i> ไม่พบข้อมูลร้านค้า\n              </div>-->\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label for=\"remark\" class=\"col-md-3 control-label\">หมายเหตุ</label>\n            <div class=\"col-md-6\">\n              <div class=\"input-icon right\">\n                <textarea id=\"remark\" rows=\"3\" class=\"form-control\" name=\"remark\" formControlName=\"remark\" [(ngModel)]=\"data.remark\"></textarea>\n              </div>\n            </div>\n          </div>\n          <div class=\"form-actions\">\n            <div class=\"col-md-6 col-md-offset-3\">\n              <button type=\"button\" class=\"btn btn-success btn-outlined\" [disabled]=\"!transferProductForm.valid\" (click)=\"sendForm()\">บันทึก</button>              &nbsp;\n              <button type=\"reset\" class=\"btn btn-danger btn-outlined\">ยกเลิก</button>\n            </div>\n          </div>\n        </div>\n      </form>\n    </div>\n  </div>\n</div>\n\n<template #customItemProductTemplate let-model=\"item\" let-index=\"index\">\n  <div class=\"nameAutocomplete\">\n    <h5><strong>barcode :: </strong> {{model.code}}</h5>\n    <h5><strong>ชื่อสินค้า :: </strong> {{model.name}}</h5>\n    <h5><strong>ประเภท :: </strong> {{model.product_type.name}}</h5>\n  </div>\n</template>"

/***/ }),

/***/ 1241:
/***/ (function(module, exports) {

module.exports = "<!-- title -->\n<div id=\"title-breadcrumb-option-demo\" class=\"page-title-breadcrumb\">\n  <div class=\"page-header pull-left\">\n    <div class=\"page-title\">{{Heading}}</div>\n  </div>\n  <ol class=\"breadcrumb page-breadcrumb\">\n    <li>\n      <i class=\"fa fa-home\"></i>&nbsp;\n      <a [routerLink]=\"['/dashboard']\">Home</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n      <a [routerLink]=\"['/product/manage']\">สินค้า</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n    </li>\n    <li class=\"active\">{{Heading}}</li>\n  </ol>\n  <div class=\"clearfix\"></div>\n</div>\n\n<!-- content -->\n<div class=\"page-content\" id=\"transferProduct\">\n  <ul id=\"transferProductTab\" class=\"nav nav-tabs ul-edit responsive\">\n    <li class=\"active\"><a href=\"#listTransferProduct\" data-toggle=\"tab\">จัดการ{{Heading}}</a></li>\n  </ul>\n  <div id=\"transferProductTabContent\" class=\"tab-content\">\n    <app-loading *ngIf=\"isLoading\"></app-loading>\n    <div id=\"listTransferProduct\" class=\"tab-pane fade in active\">\n      <div class=\"row dateSelect\">\n        <label class=\"control-label\">เลือกช่วงวัน</label>\n        <div>\n          <my-date-range-picker [options]=\"myDateRangePickerOptions\" (dateRangeChanged)=\"onDateRangeChanged($event)\" [(ngModel)]=\"dateRange\"></my-date-range-picker>\n        </div>\n      </div>\n      <div class=\"row\" *ngIf=\"!isLoading\">\n        <div class=\"col-lg-12 datatable-simple\">\n          <table datatable class=\"table table-bordered\" [dtOptions]=\"dtOptions\" *ngIf=\"data\">\n            <thead>\n              <tr>\n                <th>#</th>\n                <th>วันที่</th>\n                <th>Barcode</th>\n                <th>สินค้า</th>\n                <th>จำนวน</th>\n                <th>ปลายทาง</th>\n                <th>หมายเหตุ</th>\n                <th>Actions</th>\n              </tr>\n            </thead>\n            <tbody>\n              <tr *ngFor=\"let item of data; let i = index\">\n                <td>{{i + 1}}</td>\n                <td>{{item.date}}</td>\n                <td>{{item.product.code}}</td>\n                <td>{{item.product.name}}</td>\n                <td>{{item.amount}}</td>\n                <td>{{item.shop_destination ? item.shop_destination.name : '-'}}</td>\n                <td class=\"remark\">\n                  <p>{{item.remark}}</p>\n                </td>\n                <td>\n                  <div class=\"actionsBtn\">\n                    <a class=\"btn btn-info btn-outlined\" (click)=\"openEditModal(item.id)\"><i class=\"fa fa-edit\"></i></a>\n                    <a class=\"btn btn-danger btn-outlined\" (click)=\"openDeleteModal(item.id)\"><i class=\"fa fa-trash\"></i></a>\n                  </div>\n                </td>\n              </tr>\n            </tbody>\n          </table>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n<div class=\"modal fade\" bsModal #editModal=\"bs-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"mySmallModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-lg modalEditProduct\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title pull-left\">แก้ไข{{Heading}}</h4>\n        <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"editModal.hide()\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n\n      <form [formGroup]=\"transferProductForm\" class=\"form-horizontal\">\n        <div class=\"modal-body\" *ngIf=\"selectedItem\">\n          <div class=\"form-body pal\">\n            <div class=\"form-group\">\n              <label for=\"productSelected\" class=\"col-md-3 control-label\">สินค้า<span class='require'>*</span></label>\n              <div class=\"col-md-6\" id=\"addCustomerInput\">\n                <input name=\"productSelected\" placeholder=\"กรุณากรอก barcode\" [(ngModel)]=\"productSelected.name\" [typeahead]=\"products\" [typeaheadItemTemplate]=\"customItemProductTemplate\"\n                  [typeaheadOptionField]=\"'code'\" (typeaheadNoResults)=\"changeTypeaheadNoResults('product',$event)\" (typeaheadOnSelect)=\"typeaheadOnSelect('product',$event)\"\n                  class=\"form-control\" formControlName=\"product_id\" />\n                <div class=\"no-data\" *ngIf=\"typeaheadNoResults.product\">\n                  <i class=\"fa fa-remove\"></i> ไม่พบสินค้า\n                </div>\n                <p class=\"text-danger\" *ngIf=\"product_id.hasError('required') && product_id.dirty\">กรุณากรอก barcode สินค้า</p>\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <label for=\"amount\" class=\"col-md-3 control-label\">จำนวน<span class='require'>*</span></label>\n              <div class=\"col-md-6\">\n                <div class=\"input-icon right\">\n                  <input type=\"text\" id=\"amount\" name=\"amount\" class=\"form-control\" formControlName=\"amount\" [(ngModel)]=\"selectedItem.amount\"\n                  />\n                  <p class=\"text-danger\" *ngIf=\"amount.hasError('required') && amount.dirty\">กรุณากรอกจำนวน</p>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <label class=\"col-md-3 control-label\">เลือกร้านค้า</label>\n              <div class=\"col-md-6\" id=\"addCustomerInput\">\n                <select [(ngModel)]=\"selectedItem.shop_destination_id\" class=\"selectCenter\" name=\"brands\" style=\"width:100%\" [ngModelOptions]=\"{standalone: true}\">\n                    <option *ngFor=\"let shop of shopDestinations\" [value]=\"shop.id\">{{shop.name}}</option>\n                </select>\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <label for=\"remark\" class=\"col-md-3 control-label\">หมายเหตุ</label>\n              <div class=\"col-md-6\">\n                <div class=\"input-icon right\">\n                  <textarea id=\"remark\" rows=\"3\" class=\"form-control\" name=\"remark\" formControlName=\"remark\" [(ngModel)]=\"selectedItem.remark\"></textarea>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"editModal.hide()\">ยกเลิก</button>\n          <button type=\"button\" class=\"btn btn-success btn-square\" (click)=\"saveChange()\" [disabled]=\"!transferProductForm.valid\">บันทึก</button>\n        </div>\n      </form>\n    </div>\n  </div>\n</div>\n\n<div class=\"modal fade\" bsModal #deleteModal=\"bs-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"mySmallModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-sm modalEditProduct\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title pull-left\">ยืนยันการลบ{{Heading}}</h4>\n        <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"deleteModal.hide()\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\" *ngIf=\"selectedItem\">\n        <p>คุณแน่ใจที่ต้องการจะลบ \"{{selectedItem.product.name}}\" </p>\n        <p>จำนวน {{selectedItem.amount}} ชิ้น</p>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"deleteModal.hide()\">ยกเลิก</button>\n        <button type=\"button\" class=\"btn btn-success btn-square\" (click)=\"deleteItem()\">แน่ใจ</button>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ 1242:
/***/ (function(module, exports) {

module.exports = "<!-- title -->\n<div id=\"title-breadcrumb-option-demo\" class=\"page-title-breadcrumb\">\n  <div class=\"page-header pull-left\">\n    <div class=\"page-title\">{{Heading}}</div>\n  </div>\n  <ol class=\"breadcrumb page-breadcrumb\">\n    <li>\n      <i class=\"fa fa-home\"></i>&nbsp;\n      <a [routerLink]=\"['/dashboard']\">Home</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n    </li>\n    <li class=\"active\">{{Heading}}</li>\n  </ol>\n  <div class=\"btn btn-blue reportrange hide\">\n    <i class=\"fa fa-calendar\"></i>&nbsp;\n    <span></span>&nbsp;report&nbsp;\n    <i class=\"fa fa-angle-down\"></i>\n    <input type=\"hidden\" name=\"datestart\" />\n    <input type=\"hidden\" name=\"endstart\" />\n  </div>\n  <div class=\"clearfix\"></div>\n</div>\n\n<!-- content -->\n<div class=\"page-content\" id=\"report\">\n  <ul id=\"reportTab\" class=\"nav nav-tabs ul-edit responsive\">\n    <li class=\"active\"><a href=\"#reportDay\" data-toggle=\"tab\">รายงานทุกร้านค้า</a></li>\n  </ul>\n  <div id=\"reportTabContent\" class=\"tab-content\">\n    <app-loading *ngIf=\"isLoading\"></app-loading>\n    <div id=\"manageProductType\" class=\"tab-pane fade in active\">\n      <div class=\"row btnGroupSelect\">\n        <div class=\"btn-group\">\n          <button type=\"button\" class=\"btn btn-grey btn-outlined\" (click)=\"setDataToGet('today')\">วันนี้</button>\n          <button type=\"button\" class=\"btn btn-grey btn-outlined\" (click)=\"setDataToGet('yesterday')\">เมื่อวานนี้</button>\n          <button type=\"button\" class=\"btn btn-grey btn-outlined\" (click)=\"setDataToGet('threeDayAgo')\">ย้อนหลัง 3 วัน</button>\n          <button type=\"button\" class=\"btn btn-grey btn-outlined\" (click)=\"setDataToGet('fiveDayAgo')\">ย้อนหลัง 5 วัน</button>\n          <button type=\"button\" class=\"btn btn-grey btn-outlined\" (click)=\"setDataToGet('sevenDayAgo')\">ย้อนหลัง 7 วัน</button>\n          <button type=\"button\" class=\"btn btn-grey btn-outlined\" (click)=\"setDataToGet('thisMonth')\">เดือนนี้</button>\n          <button type=\"button\" class=\"btn btn-grey btn-outlined\" (click)=\"setDataToGet('lastMonth')\">เดือนที่แล้ว</button>\n          <button type=\"button\" class=\"btn btn-grey btn-outlined\" (click)=\"setDataToGet('twoMonthAgo')\">ย้อนหลัง 2 เดือน</button>\n        </div>\n      </div>\n      <div class=\"row form-horizontal\">\n        <label class=\"col-md-4 control-label\">เลือกช่วงวัน</label>\n        <div class=\"col-md-4\">\n          <my-date-range-picker [options]=\"myDateRangePickerOptions\" (dateRangeChanged)=\"onDateRangeChanged($event)\" [(ngModel)]=\"date_form\"></my-date-range-picker>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-lg-12 datatable-simple\">\n          <table datatable class=\"table table-bordered\" [dtOptions]=\"dtOptions\" *ngIf=\"data\">\n            <thead>\n              <tr>\n                <th>#</th>\n                <th>ร้านค้า</th>\n                <th>จำนวนการขาย</th>\n                <th>ขายหน้าร้าน</th>\n                <th>ขายส่ง</th>\n                <th>หัก</th>\n                <th>คงเหลือ</th>\n                <th>Actions</th>\n              </tr>\n            </thead>\n            <tbody>\n              <tr *ngFor=\"let item of data; let i = index;\">\n                <td>{{i + 1}}</td>\n                <td>{{item.name}}</td>\n                <td>{{item.summary.count_sell}}</td>\n                <td>{{item.summary.sum_retail_price}}</td>\n                <td>{{item.summary.sum_wholesale_price}}</td>\n                <td>{{item.summary.sum_general_expense_price}}</td>\n                <td>{{item.summary.balance}}</td>\n                <td>\n                  <div class=\"actionsBtn\">\n                    <a class=\"btn btn-info btn-outlined\" (click)=\"goToDetail(item)\"><i class=\"fa fa-edit\"></i></a>\n                  </div>\n                </td>\n              </tr>\n            </tbody>\n          </table>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ 1243:
/***/ (function(module, exports) {

module.exports = "<div>\n  <app-loading *ngIf=\"isLoading\"></app-loading>\n  <div class=\"row\">\n    <div class=\"col-md-6\">\n      <div class=\"portlet box\">\n        <div class=\"portlet-header\">\n          <div class=\"caption\">เลือกช่วงวัน</div>\n        </div>\n        <div class=\"portlet-body\">\n          <div class=\"row btnGroupSelect\">\n            <div class=\"btn-group\">\n              <button type=\"button\" class=\"btn btn-grey btn-outlined\" (click)=\"getReport('today')\">วันนี้</button>\n              <button type=\"button\" class=\"btn btn-grey btn-outlined\" (click)=\"getReport('yesterday')\">เมื่อวานนี้</button>\n              <button type=\"button\" class=\"btn btn-grey btn-outlined\" (click)=\"getReport('threeDayAgo')\">ย้อนหลัง 3 วัน</button>\n              <button type=\"button\" class=\"btn btn-grey btn-outlined\" (click)=\"getReport('fiveDayAgo')\">ย้อนหลัง 5 วัน</button>\n              <button type=\"button\" class=\"btn btn-grey btn-outlined\" (click)=\"getReport('sevenDayAgo')\">ย้อนหลัง 7 วัน</button>\n            </div>\n          </div>\n          <!--<div class=\"row\">\n            <form class=\"form-horizontal\" [formGroup]=\"reportDateForm\">\n              <div class=\"form-group\">\n                <label for=\"\" class=\"col-md-offset-3 col-md-2 control-label\">วันที่รับซ่อม<span class='require'>*</span></label>\n                <div class=\"col-md-4\">\n                  <div class=\"input-group\">\n                    <input type=\"text\" data-date-format=\"yyyy-mm-dd\" placeholder=\"yyyy-mm-dd\" class=\"datepicker-default form-control\" ngbDatepicker\n                      #d=\"ngbDatepicker\" (click)=\"d.toggle()\" formControlName=\"date_start_form\" [(ngModel)]=\"date_start\" />\n                    <div class=\"input-group-addon\" (click)=\"d.toggle()\"><i class=\"fa fa-calendar\"></i></div>\n                  </div>\n                </div>\n              </div>\n              <div class=\"form-group\">\n                <label for=\"\" class=\"col-md-offset-3 col-md-2 control-label\">วันที่รับซ่อม<span class='require'>*</span></label>\n                <div class=\"col-md-4\">\n                  <div class=\"input-group\">\n                    <input type=\"text\" data-date-format=\"yyyy-mm-dd\" placeholder=\"yyyy-mm-dd\" class=\"datepicker-default form-control\" ngbDatepicker\n                      #d1=\"ngbDatepicker\" (click)=\"d1.toggle()\" formControlName=\"date_end_form\" [(ngModel)]=\"date_end\" />\n                    <div class=\"input-group-addon\" (click)=\"d1.toggle()\"><i class=\"fa fa-calendar\"></i></div>\n                  </div>\n                </div>\n              </div>\n              <div class=\"form-group\">\n                <div class=\"col-md-4 col-md-offset-4 btnAction\">\n                  <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"reportDateForm.reset\">ล้าง</button>\n                  <button type=\"submit\" class=\"btn btn-success btn-outlined btn-square\" (click)=\"getReport('selectDate')\">ดูรายงาน</button>\n                </div>\n              </div>\n            </form>\n          </div>-->\n        </div>\n      </div>\n      <div class=\"portlet box\">\n        <div class=\"portlet-header\">\n          <div class=\"caption\">สรุปยอดรายการ</div>\n        </div>\n        <div class=\"portlet-body totalCost\">\n          <div class=\"col-md-12 detail\">\n            <div class=\"col-md-12 totalCost\">\n              <table *ngIf=\"data\">\n                <tr>\n                  <td class=\"title\">\n                    <h4>รายการขายได้</h4>\n                  </td>\n                  <td>{{data.count_sell}}</td>\n                  <td>\n                    <h4>รายการ</h4>\n                  </td>\n                </tr>\n                <tr>\n                  <td class=\"title\">\n                    <h4>รวมรายการขายหน้าร้าน</h4>\n                  </td>\n                  <td>{{data.sum_retail_price}}</td>\n                  <td>\n                    <h4>บาท</h4>\n                  </td>\n                </tr>\n                <tr>\n                  <td class=\"title\">\n                    <h4>รวมรายการขายส่ง</h4>\n                  </td>\n                  <td>{{data.sum_wholesale_price}}</td>\n                  <td>\n                    <h4>บาท</h4>\n                  </td>\n                </tr>\n                <tr>\n                  <td class=\"title\">\n                    <h4>รวมรายการขายทั้งหมด</h4>\n                  </td>\n                  <td>{{data.sum_price_sell}}</td>\n                  <td>\n                    <h4>บาท</h4>\n                  </td>\n                </tr>\n                <tr>\n                  <td class=\"title\">\n                    <h4>รายการหัก</h4>\n                  </td>\n                  <td>{{data.sum_general_expense_price}}</td>\n                  <td>\n                    <h4>บาท</h4>\n                  </td>\n                </tr>\n                <tr>\n                  <td class=\"title\">\n                    <h3>ยอดคงเหลือ</h3>\n                  </td>\n                  <td>\n                    <h3>{{data.balance}}</h3>\n                  </td>\n                  <td>\n                    <h3>บาท</h3>\n                  </td>\n                </tr>\n              </table>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"col-md-6\">\n      <div class=\"portlet box\">\n        <div class=\"portlet-header\">\n          <div class=\"caption\">รายการหัก</div>\n        </div>\n        <div class=\"portlet-body general_expenses\">\n          <div class=\"row\">\n            <div class=\"col-md-12 detail\">\n              <table class=\"table table-hover\" *ngIf=\"data\">\n                <thead>\n                  <tr>\n                    <th>#</th>\n                    <th>รายการ</th>\n                    <th>จำนวน</th>\n                  </tr>\n                </thead>\n                <tbody>\n                  <tr *ngFor=\"let item of data.general_expenses; let i = index;\">\n                    <td>{{i + 1}}</td>\n                    <td>{{item.detail}}</td>\n                    <td>{{item.amount}}</td>\n                  </tr>\n                </tbody>\n              </table>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"row\">\n    <div class=\"col-md-6 general_expenses\">\n      <div class=\"portlet box \">\n        <div class=\"portlet-header\">\n          <div class=\"caption\">รายการขายหน้าร้าน</div>\n        </div>\n        <div class=\"portlet-body general_expenses\">\n          <div class=\"row\">\n            <div class=\"col-md-12 detail\">\n              <table class=\"table table-hover\" *ngIf=\"data\">\n                <thead>\n                  <tr>\n                    <th>#</th>\n                    <th>สินค้า</th>\n                    <th>จำนวน</th>\n                  </tr>\n                </thead>\n                <tbody>\n                  <tr *ngFor=\"let item of data.product_transection_retails; let i = index;\">\n                    <td>{{i + 1}}</td>\n                    <td>{{item.product_id}}</td>\n                    <td>{{item.amount}}</td>\n                  </tr>\n                </tbody>\n              </table>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"col-md-6 general_expenses\">\n      <div class=\"portlet box \">\n        <div class=\"portlet-header\">\n          <div class=\"caption\">รายการขายส่ง</div>\n        </div>\n        <div class=\"portlet-body general_expenses\">\n          <div class=\"row\">\n            <div class=\"col-md-12 detail\">\n              <table class=\"table table-hover\" *ngIf=\"data\">\n                <thead>\n                  <tr>\n                    <th>#</th>\n                    <th>สินค้า</th>\n                    <th>จำนวน</th>\n                  </tr>\n                </thead>\n                <tbody>\n                  <tr *ngFor=\"let item of data.product_transection_wholesales; let i = index;\">\n                    <td>{{i + 1}}</td>\n                    <td>{{item.product_id}}</td>\n                    <td>{{item.amount}}</td>\n                  </tr>\n                </tbody>\n                <!--<tfoot>\n                  <tr>\n                    <td colspan=\"2\">\n                      ราคาขายส่งรวม\n                    </td>\n                    <td>\n                      {{data.sum_wholesale_price}}\n                    </td>\n                  </tr>\n                </tfoot>-->\n              </table>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ 1244:
/***/ (function(module, exports) {

module.exports = "<div>\n  <app-loading *ngIf=\"isLoading\"></app-loading>\n  <div class=\"row\">\n    <div class=\"col-md-6\">\n      <div class=\"portlet box\">\n        <div class=\"portlet-header\">\n          <div class=\"caption\">เลือกช่วงเดือน</div>\n        </div>\n        <div class=\"portlet-body\">\n          <div class=\"row btnGroupSelect\">\n            <div class=\"btn-group\">\n              <button type=\"button\" class=\"btn btn-grey btn-outlined\" (click)=\"getReport('thisMonth')\">เดือนนี้</button>\n              <button type=\"button\" class=\"btn btn-grey btn-outlined\" (click)=\"getReport('lastMonth')\">เดือนที่แล้ว</button>\n              <button type=\"button\" class=\"btn btn-grey btn-outlined\" (click)=\"getReport('twoMonthAgo')\">ย้อนหลัง 2 เดือน</button>\n            </div>\n          </div>\n          <!--<div class=\"row\">\n            <form class=\"form-horizontal\" [formGroup]=\"reportDateForm\">\n              <div class=\"form-group\">\n                <label for=\"\" class=\"col-md-offset-3 col-md-2 control-label\">วันที่รับซ่อม<span class='require'>*</span></label>\n                <div class=\"col-md-4\">\n                  <div class=\"input-group\">\n                    <input type=\"text\" data-date-format=\"yyyy-mm-dd\" placeholder=\"yyyy-mm-dd\" class=\"datepicker-default form-control\" ngbDatepicker\n                      #d=\"ngbDatepicker\" (click)=\"d.toggle()\" formControlName=\"date_start_form\" [(ngModel)]=\"date_start\" />\n                    <div class=\"input-group-addon\" (click)=\"d.toggle()\"><i class=\"fa fa-calendar\"></i></div>\n                  </div>\n                </div>\n              </div>\n              <div class=\"form-group\">\n                <label for=\"\" class=\"col-md-offset-3 col-md-2 control-label\">วันที่รับซ่อม<span class='require'>*</span></label>\n                <div class=\"col-md-4\">\n                  <div class=\"input-group\">\n                    <input type=\"text\" data-date-format=\"yyyy-mm-dd\" placeholder=\"yyyy-mm-dd\" class=\"datepicker-default form-control\" ngbDatepicker\n                      #d1=\"ngbDatepicker\" (click)=\"d1.toggle()\" formControlName=\"date_end_form\" [(ngModel)]=\"date_end\" />\n                    <div class=\"input-group-addon\" (click)=\"d1.toggle()\"><i class=\"fa fa-calendar\"></i></div>\n                  </div>\n                </div>\n              </div>\n              <div class=\"form-group\">\n                <div class=\"col-md-4 col-md-offset-4 btnAction\">\n                  <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"reportDateForm.reset\">ล้าง</button>\n                  <button type=\"submit\" class=\"btn btn-success btn-outlined btn-square\" (click)=\"getReport('selectDate')\">ดูรายงาน</button>\n                </div>\n              </div>\n            </form>\n          </div>-->\n        </div>\n      </div>\n      <div class=\"portlet box\">\n        <div class=\"portlet-header\">\n          <div class=\"caption\">สรุปยอดรายการ</div>\n        </div>\n        <div class=\"portlet-body totalCost\">\n          <div class=\"col-md-12 detail\">\n            <div class=\"col-md-12 totalCost\">\n              <table *ngIf=\"data\">\n                <tr>\n                  <td class=\"title\">\n                    <h4>รายการขายได้</h4>\n                  </td>\n                  <td>{{data.count_sell}}</td>\n                  <td>\n                    <h4>รายการ</h4>\n                  </td>\n                </tr>\n                <tr>\n                  <td class=\"title\">\n                    <h4>รวมรายการขายหน้าร้าน</h4>\n                  </td>\n                  <td>{{data.sum_retail_price}}</td>\n                  <td>\n                    <h4>บาท</h4>\n                  </td>\n                </tr>\n                <tr>\n                  <td class=\"title\">\n                    <h4>รวมรายการขายส่ง</h4>\n                  </td>\n                  <td>{{data.sum_wholesale_price}}</td>\n                  <td>\n                    <h4>บาท</h4>\n                  </td>\n                </tr>\n                <tr>\n                  <td class=\"title\">\n                    <h4>รวมรายการขายทั้งหมด</h4>\n                  </td>\n                  <td>{{data.sum_price_sell}}</td>\n                  <td>\n                    <h4>บาท</h4>\n                  </td>\n                </tr>\n                <tr>\n                  <td class=\"title\">\n                    <h4>รายการหัก</h4>\n                  </td>\n                  <td>{{data.sum_general_expense_price}}</td>\n                  <td>\n                    <h4>บาท</h4>\n                  </td>\n                </tr>\n                <tr>\n                  <td class=\"title\">\n                    <h3>ยอดคงเหลือ</h3>\n                  </td>\n                  <td>\n                    <h3>{{data.balance}}</h3>\n                  </td>\n                  <td>\n                    <h3>บาท</h3>\n                  </td>\n                </tr>\n              </table>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"col-md-6\">\n      <div class=\"portlet box\">\n        <div class=\"portlet-header\">\n          <div class=\"caption\">รายการหัก</div>\n        </div>\n        <div class=\"portlet-body general_expenses\">\n          <div class=\"row\">\n            <div class=\"col-md-12 detail\">\n              <table class=\"table table-hover\" *ngIf=\"data\">\n                <thead>\n                  <tr>\n                    <th>#</th>\n                    <th>รายการ</th>\n                    <th>จำนวน</th>\n                  </tr>\n                </thead>\n                <tbody>\n                  <tr *ngFor=\"let item of data.general_expenses; let i = index;\">\n                    <td>{{i + 1}}</td>\n                    <td>{{item.detail}}</td>\n                    <td>{{item.amount}}</td>\n                  </tr>\n                </tbody>\n              </table>\n              <p class=\"noContent\" *ngIf=\"data.product_transection_wholesales.length === 0\">ไม่มีข้อมูล</p>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"row\">\n    <div class=\"col-md-6 general_expenses\">\n      <div class=\"portlet box \">\n        <div class=\"portlet-header\">\n          <div class=\"caption\">รายการขายหน้าร้าน</div>\n        </div>\n        <div class=\"portlet-body general_expenses\">\n          <div class=\"row\">\n            <div class=\"col-md-12 detail\">\n              <table class=\"table table-hover\" *ngIf=\"data\">\n                <thead>\n                  <tr>\n                    <th>#</th>\n                    <th>วันที่</th>\n                    <th>สินค้า</th>\n                    <th>จำนวน</th>\n                  </tr>\n                </thead>\n                <tbody>\n                  <tr *ngFor=\"let item of data.product_transection_retails; let i = index;\">\n                    <td>{{i + 1}}</td>\n                    <td>{{item.created_at | date:\"yyyy-MM-dd\"}}</td>\n                    <td>{{item.product_id}}</td>\n                    <td>{{item.sell_price}}</td>\n                  </tr>\n                </tbody>\n              </table>\n              <p class=\"noContent\" *ngIf=\"data.product_transection_wholesales.length === 0\">ไม่มีข้อมูล</p>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"col-md-6 general_expenses\">\n      <div class=\"portlet box \">\n        <div class=\"portlet-header\">\n          <div class=\"caption\">รายการขายส่ง</div>\n        </div>\n        <div class=\"portlet-body general_expenses\">\n          <div class=\"row\">\n            <div class=\"col-md-12 detail\">\n              <table class=\"table table-hover\" *ngIf=\"data\">\n                <thead>\n                  <tr>\n                    <th>#</th>\n                    <th>วันที่</th>\n                    <th>สินค้า</th>\n                    <th>จำนวน</th>\n                  </tr>\n                </thead>\n                <tbody>\n                  <tr *ngFor=\"let item of data.product_transection_wholesales; let i = index;\">\n                    <td>{{i + 1}}</td>\n                    <td>{{item.created_at | date:\"yyyy-MM-dd\"}}</td>\n                    <td>{{item.product_id}}</td>\n                    <td>{{item.sell_price}}</td>\n                  </tr>\n                </tbody>\n                <!--<tfoot>\n                  <tr>\n                    <td colspan=\"2\">\n                      ราคาขายส่งรวม\n                    </td>\n                    <td>\n                      {{data.sum_wholesale_price}}\n                    </td>\n                  </tr>\n                </tfoot>-->\n              </table>\n              <p class=\"noContent\" *ngIf=\"data.product_transection_wholesales.length === 0\">ไม่มีข้อมูล</p>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ 1245:
/***/ (function(module, exports) {

module.exports = "<!-- title -->\n<div id=\"title-breadcrumb-option-demo\" class=\"page-title-breadcrumb\">\n  <div class=\"page-header pull-left\">\n    <div class=\"page-title\" *ngIf=\"!shop_name\">{{Heading}}</div>\n    <div class=\"page-title\" *ngIf=\"shop_name\">{{Heading}}ประจำร้าน {{shop_name}}</div>\n  </div>\n  <ol class=\"breadcrumb page-breadcrumb\">\n    <li>\n      <i class=\"fa fa-home\"></i>&nbsp;\n      <a [routerLink]=\"['/dashboard']\">Home</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n    </li>\n    <li class=\"active\">{{Heading}}</li>\n  </ol>\n  <div class=\"btn btn-blue reportrange hide\">\n    <i class=\"fa fa-calendar\"></i>&nbsp;\n    <span></span>&nbsp;report&nbsp;\n    <i class=\"fa fa-angle-down\"></i>\n    <input type=\"hidden\" name=\"datestart\" />\n    <input type=\"hidden\" name=\"endstart\" />\n  </div>\n  <div class=\"clearfix\"></div>\n</div>\n\n<!-- content -->\n<div class=\"page-content\" id=\"report\">\n  <ul id=\"reportTab\" class=\"nav nav-tabs ul-edit responsive\">\n    <li class=\"active\"><a href=\"#reportDay\" data-toggle=\"tab\">รายวัน</a></li>\n    <li (click)=\"getReportMonth('thisMonth')\"><a href=\"#reportMonth\" data-toggle=\"tab\">รายเดือน</a></li>\n  </ul>\n  <div id=\"reportTabContent\" class=\"tab-content\">\n    <div id=\"reportDay\" class=\"tab-pane fade in active\">\n      <div>\n        <app-loading *ngIf=\"isLoading\"></app-loading>\n        <div class=\"row\">\n          <div class=\"col-md-6\">\n            <div class=\"portlet box\">\n              <div class=\"portlet-header\">\n                <div class=\"caption\">เลือกช่วงวัน</div>\n              </div>\n              <div class=\"portlet-body\">\n                <div class=\"row btnGroupSelect\">\n                  <div class=\"btn-group\">\n                    <button type=\"button\" class=\"btn btn-grey btn-outlined\" (click)=\"getReportDay('today')\">วันนี้</button>\n                    <button type=\"button\" class=\"btn btn-grey btn-outlined\" (click)=\"getReportDay('yesterday')\">เมื่อวานนี้</button>\n                    <button type=\"button\" class=\"btn btn-grey btn-outlined\" (click)=\"getReportDay('ago',7)\">ย้อนหลัง 7 วัน</button>\n                    <button type=\"button\" class=\"btn btn-grey btn-outlined\" (click)=\"getReportDay('ago',15)\">ย้อนหลัง 15 วัน</button>\n                    <button type=\"button\" class=\"btn btn-grey btn-outlined\" (click)=\"getReportDay('ago',30)\">ย้อนหลัง 30 วัน</button>\n                  </div>\n                </div>\n                <div class=\"row form-horizontal\">\n                  <label class=\"col-md-4 control-label\">เลือกช่วงวัน</label>\n                  <div class=\"col-md-4\">\n                    <my-date-range-picker [options]=\"myDateRangePickerOptions\" (dateRangeChanged)=\"onDateRangeChanged($event)\" [(ngModel)]=\"date_form\"></my-date-range-picker>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"portlet box\">\n              <div class=\"portlet-header\">\n                <div class=\"caption\">สรุปยอดรายการ</div>\n              </div>\n              <div class=\"portlet-body totalCost\">\n                <div class=\"detail\">\n                  <table *ngIf=\"dataDay\">\n                    <tr>\n                      <td class=\"title\">\n                        <h4>รายการขายได้</h4>\n                      </td>\n                      <td>{{dataDay.count_sell || 0}}</td>\n                      <td>\n                        <h4>รายการ</h4>\n                      </td>\n                    </tr>\n                    <tr>\n                      <td class=\"title\">\n                        <h4>รวมรายการขายหน้าร้าน</h4>\n                      </td>\n                      <td>{{dataDay.sum_retail_price || 0}}</td>\n                      <td>\n                        <h4>บาท</h4>\n                      </td>\n                    </tr>\n                    <tr>\n                      <td class=\"title\">\n                        <h4>รวมรายการขายส่ง</h4>\n                      </td>\n                      <td>{{dataDay.sum_wholesale_price || 0}}</td>\n                      <td>\n                        <h4>บาท</h4>\n                      </td>\n                    </tr>\n                    <tr>\n                      <td class=\"title\">\n                        <h4>รวมรายการขายทั้งหมด</h4>\n                      </td>\n                      <td>{{dataDay.sum_price_sell || 0}}</td>\n                      <td>\n                        <h4>บาท</h4>\n                      </td>\n                    </tr>\n                    <tr>\n                      <td class=\"title\">\n                        <h4>รายการหัก</h4>\n                      </td>\n                      <td>{{dataDay.sum_general_expense_price || 0}}</td>\n                      <td>\n                        <h4>บาท</h4>\n                      </td>\n                    </tr>\n                    <tr>\n                      <td class=\"title\">\n                        <h3>ยอดคงเหลือ</h3>\n                      </td>\n                      <td>\n                        <h3>{{dataDay.balance || 0}}</h3>\n                      </td>\n                      <td>\n                        <h3>บาท</h3>\n                      </td>\n                    </tr>\n                  </table>\n                </div>\n              </div>\n            </div>\n          </div>\n          <div class=\"col-md-6\">\n            <div class=\"portlet box\">\n              <div class=\"portlet-header\">\n                <div class=\"caption\">รายการหัก</div>\n              </div>\n              <div class=\"portlet-body general_expenses\">\n                <div class=\"row\" *ngIf=\"!isLoading\">\n                  <div class=\"col-md-12 detail datatable-simple\" *ngIf=\"dataDay\">\n                    <table datatable class=\"table table-hover\" [dtOptions]=\"dtOptions\" *ngIf=\"dataDay\">\n                      <thead>\n                        <tr>\n                          <th>#</th>\n                          <th>วันที่</th>\n                          <th>รายการ</th>\n                          <th>จำนวน</th>\n                        </tr>\n                      </thead>\n                      <tbody>\n                        <tr *ngFor=\"let item of dataDay.general_expenses; let i = index;\">\n                          <td>{{i + 1}}</td>\n                          <td>{{item.created_at | date:\"yyyy-MM-dd\"}}</td>\n                          <td>{{item.detail}}</td>\n                          <td>{{item.amount}}</td>\n                        </tr>\n                      </tbody>\n                      <tfoot>\n                        <tr>\n                          <td colspan=\"3\" class=\"sum_title\">\n                            รายการหักรวม\n                          </td>\n                          <td class=\"sum_value\">\n                            {{dataDay.sum_general_expense_price}}\n                          </td>\n                        </tr>\n                      </tfoot>\n                    </table>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n\n        <div class=\"row\">\n          <div class=\"col-md-6 general_expenses\">\n            <div class=\"portlet box \">\n              <div class=\"portlet-header\">\n                <div class=\"caption\">รายการขายหน้าร้าน</div>\n              </div>\n              <div class=\"portlet-body general_expenses\">\n                <div class=\"row\" *ngIf=\"!isLoading\">\n                  <div class=\"col-md-12 detail datatable-simple\" *ngIf=\"dataDay\">\n                    <table datatable class=\"table table-hover\" [dtOptions]=\"dtOptions\" *ngIf=\"dataDay\">\n                      <thead>\n                        <tr>\n                          <th>#</th>\n                          <th>วันที่</th>\n                          <th>สินค้า</th>\n                          <th>จำนวน</th>\n                        </tr>\n                      </thead>\n                      <tbody>\n                        <tr *ngFor=\"let item of dataDay.product_transection_retails; let i = index;\">\n                          <td>{{i + 1}}</td>\n                          <td>{{item.created_at | date:\"yyyy-MM-dd\"}}</td>\n                          <td>{{item.product.name}}</td>\n                          <td>{{item.sell_price}} ({{item.amount}})</td>\n                        </tr>\n                      </tbody>\n                      <tfoot>\n                        <tr>\n                          <td colspan=\"3\" class=\"sum_title\">\n                            ราคาขายหน้าร้านรวม\n                          </td>\n                          <td class=\"sum_value\">\n                            {{dataDay.sum_retail_price}}\n                          </td>\n                        </tr>\n                      </tfoot>\n                    </table>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n          <div class=\"col-md-6 general_expenses\">\n            <div class=\"portlet box \">\n              <div class=\"portlet-header\">\n                <div class=\"caption\">รายการขายส่ง</div>\n              </div>\n              <div class=\"portlet-body general_expenses\">\n                <div class=\"row\" *ngIf=\"!isLoading\">\n                  <div class=\"col-md-12 detail datatable-simple\" *ngIf=\"dataDay\">\n                    <table datatable class=\"table table-hover\" [dtOptions]=\"dtOptions\" *ngIf=\"dataDay\">\n                      <thead>\n                        <tr>\n                          <th>#</th>\n                          <th>วันที่</th>\n                          <th>สินค้า</th>\n                          <th>จำนวน</th>\n                        </tr>\n                      </thead>\n                      <tbody>\n                        <tr *ngFor=\"let item of dataDay.product_transection_wholesales; let i = index;\">\n                          <td>{{i + 1}}</td>\n                          <td>{{item.created_at | date:\"yyyy-MM-dd\"}}</td>\n                          <td>{{item.product.name}}</td>\n                          <td>{{item.sell_price}} ({{item.amount}})</td>\n                        </tr>\n                      </tbody>\n                      <tfoot>\n                        <tr>\n                          <td colspan=\"3\" class=\"sum_title\">\n                            ราคาขายส่งรวม\n                          </td>\n                          <td class=\"sum_value\">\n                            {{dataDay.sum_wholesale_price}}\n                          </td>\n                        </tr>\n                      </tfoot>\n                    </table>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div id=\"reportMonth\" class=\"tab-pane fade\">\n      <div>\n        <app-loading *ngIf=\"isLoading\"></app-loading>\n        <div class=\"row\">\n          <div class=\"col-md-6\">\n            <div class=\"portlet box\">\n              <div class=\"portlet-header\">\n                <div class=\"caption\">เลือกช่วงเดือน</div>\n              </div>\n              <div class=\"portlet-body\">\n                <div class=\"row btnGroupSelect\">\n                  <div class=\"btn-group\">\n                    <button type=\"button\" class=\"btn btn-grey btn-outlined\" (click)=\"getReportMonth('thisMonth')\">เดือนนี้</button>\n                    <button type=\"button\" class=\"btn btn-grey btn-outlined\" (click)=\"getReportMonth('lastMonth')\">เดือนที่แล้ว</button>\n                    <button type=\"button\" class=\"btn btn-grey btn-outlined\" (click)=\"getReportMonth('twoMonthAgo')\">ย้อนหลัง 2 เดือน</button>\n                  </div>\n                </div>\n                <div class=\"selectMonth\">\n                  <div class=\"form-horizontal\">\n                    <div class=\"row\">\n                      <label class=\"col-md-4 control-label\">เริ่มเดือน</label>\n                      <month-picker class=\"col-md-4\" (change)=\"onMonthChange($event.target.value, 'start')\"></month-picker>\n                      <year-picker class=\"col-md-3\" (change)=\"onYearChange($event.target.value, 'start')\"></year-picker>\n                    </div>\n                    <div class=\"row\">\n                      <label class=\"col-md-4 control-label\">ถึงเดือน</label>\n                      <month-picker class=\"col-md-4\" (change)=\"onMonthChange($event.target.value, 'end')\"></month-picker>\n                      <year-picker class=\"col-md-3\" (change)=\"onYearChange($event.target.value, 'end')\"></year-picker>\n                    </div>\n                    <div class=\"btnAction\">\n                      <button type=\"button\" class=\"btn btn-success btn-outlined\" (click)=\"setMonthSelect()\">ค้นหา</button>\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"portlet box\">\n              <div class=\"portlet-header\">\n                <div class=\"caption\">สรุปยอดรายการ</div>\n              </div>\n              <div class=\"portlet-body totalCost\">\n                <div class=\"col-md-12 detail\">\n                  <div class=\"totalCost\">\n                    <div *ngIf=\"dataMonth\">\n                      <table>\n                        <tr>\n                          <td class=\"title\">\n                            <h4>รายการขายได้</h4>\n                          </td>\n                          <td>{{dataMonth.count_sell}}</td>\n                          <td>\n                            <h4>รายการ</h4>\n                          </td>\n                        </tr>\n                        <tr>\n                          <td class=\"title\">\n                            <h4>รวมรายการขายหน้าร้าน</h4>\n                          </td>\n                          <td>{{dataMonth.sum_retail_price}}</td>\n                          <td>\n                            <h4>บาท</h4>\n                          </td>\n                        </tr>\n                        <tr>\n                          <td class=\"title\">\n                            <h4>รวมรายการขายส่ง</h4>\n                          </td>\n                          <td>{{dataMonth.sum_wholesale_price}}</td>\n                          <td>\n                            <h4>บาท</h4>\n                          </td>\n                        </tr>\n                        <tr>\n                          <td class=\"title\">\n                            <h4>รวมรายการขายทั้งหมด</h4>\n                          </td>\n                          <td>{{dataMonth.sum_price_sell}}</td>\n                          <td>\n                            <h4>บาท</h4>\n                          </td>\n                        </tr>\n                        <tr>\n                          <td class=\"title\">\n                            <h4>รายการหัก</h4>\n                          </td>\n                          <td>{{dataMonth.sum_general_expense_price}}</td>\n                          <td>\n                            <h4>บาท</h4>\n                          </td>\n                        </tr>\n                        <tr>\n                          <td class=\"title\">\n                            <h3>ยอดคงเหลือ</h3>\n                          </td>\n                          <td>\n                            <h3>{{dataMonth.balance}}</h3>\n                          </td>\n                          <td>\n                            <h3>บาท</h3>\n                          </td>\n                        </tr>\n                      </table>\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n          <div class=\"col-md-6\">\n            <div class=\"portlet box\">\n              <div class=\"portlet-header\">\n                <div class=\"caption\">รายการหัก</div>\n              </div>\n              <div class=\"portlet-body general_expenses\">\n                <div class=\"row\" *ngIf=\"!isLoading\">\n                  <div class=\"col-md-12 detail datatable-simple\" *ngIf=\"dataMonth\">\n                    <table datatable class=\"table table-hover\" [dtOptions]=\"dtOptions\" *ngIf=\"dataMonth\">\n                      <thead>\n                        <tr>\n                          <th>#</th>\n                          <th>วันที่</th>\n                          <th>รายการ</th>\n                          <th>จำนวน</th>\n                        </tr>\n                      </thead>\n                      <tbody>\n                        <tr *ngFor=\"let item of dataMonth.general_expenses; let i = index;\">\n                          <td>{{i + 1}}</td>\n                          <td>{{item.created_at | date:\"yyyy-MM-dd\"}}</td>\n                          <td>{{item.detail}}</td>\n                          <td>{{item.amount}}</td>\n                        </tr>\n                      </tbody>\n                      <tfoot>\n                        <tr>\n                          <td colspan=\"3\" class=\"sum_title\">\n                            รายการหักรวม\n                          </td>\n                          <td class=\"sum_value\">\n                            {{dataMonth.sum_general_expense_price}}\n                          </td>\n                        </tr>\n                      </tfoot>\n                    </table>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n\n        <div class=\"row\">\n          <div class=\"col-md-6 general_expenses\">\n            <div class=\"portlet box \">\n              <div class=\"portlet-header\">\n                <div class=\"caption\">รายการขายหน้าร้าน</div>\n              </div>\n              <div class=\"portlet-body general_expenses\">\n                <div class=\"row\" *ngIf=\"!isLoading\">\n                  <div class=\"col-md-12 detail datatable-simple\" *ngIf=\"dataMonth\">\n                    <table datatable class=\"table table-hover\" [dtOptions]=\"dtOptions\" *ngIf=\"dataMonth\">\n                      <thead>\n                        <tr>\n                          <th>#</th>\n                          <th>วันที่</th>\n                          <th>สินค้า</th>\n                          <th>จำนวน</th>\n                        </tr>\n                      </thead>\n                      <tbody>\n                        <tr *ngFor=\"let item of dataMonth.product_transection_retails; let i = index;\">\n                          <td>{{i + 1}}</td>\n                          <td>{{item.created_at | date:\"yyyy-MM-dd\"}}</td>\n                          <td>{{item.product.name}}</td>\n                          <td>{{item.sell_price}} ({{item.amount}})</td>\n                        </tr>\n                      </tbody>\n                      <tfoot>\n                        <tr>\n                          <td colspan=\"3\" class=\"sum_title\">\n                            ราคาขายหน้าร้านรวม\n                          </td>\n                          <td class=\"sum_value\">\n                            {{dataMonth.sum_retail_price}}\n                          </td>\n                        </tr>\n                      </tfoot>\n                    </table>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n          <div class=\"col-md-6 general_expenses\">\n            <div class=\"portlet box \">\n              <div class=\"portlet-header\">\n                <div class=\"caption\">รายการขายส่ง</div>\n              </div>\n              <div class=\"portlet-body general_expenses\">\n                <div class=\"row\" *ngIf=\"!isLoading\">\n                  <div class=\"col-md-12 detail datatable-simple\" *ngIf=\"dataMonth\">\n                    <table datatable class=\"table table-hover\" [dtOptions]=\"dtOptions\" *ngIf=\"dataMonth\">\n                      <thead>\n                        <tr>\n                          <th>#</th>\n                          <th>วันที่</th>\n                          <th>สินค้า</th>\n                          <th>จำนวน</th>\n                        </tr>\n                      </thead>\n                      <tbody>\n                        <tr *ngFor=\"let item of dataMonth.product_transection_wholesales; let i = index;\">\n                          <td>{{i + 1}}</td>\n                          <td>{{item.created_at | date:\"yyyy-MM-dd\"}}</td>\n                          <td>{{item.product.name}}</td>\n                          <td>{{item.sell_price}} ({{item.amount}})</td>\n                        </tr>\n                      </tbody>\n                      <tfoot *ngIf=\"dataMonth\">\n                        <tr>\n                          <td colspan=\"3\" class=\"sum_title\">\n                            ราคาขายส่งรวม\n                          </td>\n                          <td class=\"sum_value\">\n                            {{dataMonth.sum_wholesale_price}}\n                          </td>\n                        </tr>\n                      </tfoot>\n                    </table>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ 1246:
/***/ (function(module, exports) {

module.exports = "<nav id=\"sidebar\" role=\"navigation\" class=\"navbar-default navbar-static-side\" [ngClass]=\"{'hideMenu': isHideMenu}\">\n  <div class=\"sidebar-collapse menu-scroll\">\n    <ul id=\"side-menu\" class=\"nav\" aria-multiselectable=\"true\" role=\"tablist\">\n      <li></li>\n      <!--<li routerLinkActive=\"active\">\n        <a [routerLink]=\"['/dashboard']\">\n          <i class=\"fa fa-tachometer fa-fw\">\n            <div class=\"icon-bg bg-orange\"></div>\n          </i>\n          <span class=\"menu-title\">Dashboard</span>\n        </a>\n      </li>-->\n      <li role=\"tab\" id=\"headingSellProduct\" routerLinkActive=\"active\" *ngIf=\"permission\" >\n        <a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#side-menu\" href=\"#collapseSellProduct\" aria-expanded=\"true\" aria-controls=\"collapseSellProduct\"\n          (click)=\"checkCollapsed('sell')\">\n          <i class=\"fa fa-shopping-basket fa-fw\">\n            <div class=\"icon-bg bg-pink\"></div>\n          </i>\n          <span class=\"menu-title\">การขายสินค้า</span>\n          <span class=\"fa icon-collapse fa-angle-down\"></span>\n          <!--<span class=\"label label-yellow\">v3.0</span>-->\n          </a>\n          <ul class=\"nav nav-second-level collapse out\" routerLinkActive=\"in\" id=\"collapseSellProduct\" role=\"tabpanel\" aria-labelledby=\"headingSellProduct\">\n            <li routerLinkActive=\"active\">\n              <a [routerLink]=\"['/product/sell/list']\">\n                <i class=\"fa fa-cogs\"></i>\n                <span class=\"submenu-title\">แสดงรายการขายสินค้า</span>\n              </a>\n            </li>\n            <li routerLinkActive=\"active\">\n              <a [routerLink]=\"['/product/sell/add']\">\n                <i class=\"fa fa-plus-circle\"></i>\n                <span class=\"submenu-title\">เพิ่มการขายสินค้า</span>\n              </a>\n            </li>\n          </ul>\n      </li>\n      <li role=\"tab\" id=\"headingTransferProduct\" routerLinkActive=\"active\" *ngIf=\"permission\" >\n        <a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#side-menu\" href=\"#collapseTransferProduct\" aria-expanded=\"true\"\n          aria-controls=\"collapseTransferProduct\" (click)=\"checkCollapsed('transfer')\">\n          <i class=\"fa fa-external-link fa-fw\">\n            <div class=\"icon-bg bg-pink\"></div>\n          </i>\n          <span class=\"menu-title\">การโอนสินค้า</span>\n          <span class=\"fa icon-collapse fa-angle-down\"></span>\n          </a>\n          <ul class=\"nav nav-second-level collapse out\" routerLinkActive=\"in\" id=\"collapseTransferProduct\" role=\"tabpanel\" aria-labelledby=\"headingTransferProduct\">\n            <li routerLinkActive=\"active\">\n              <a [routerLink]=\"['/product/transfer/list']\">\n                <i class=\"fa fa-cogs\"></i>\n                <span class=\"submenu-title\">แสดงรายการโอนสินค้า</span>\n              </a>\n            </li>\n            <li routerLinkActive=\"active\">\n              <a [routerLink]=\"['/product/transfer/add']\">\n                <i class=\"fa fa-plus-circle\"></i>\n                <span class=\"submenu-title\">เพิ่มการโอนสินค้า</span>\n              </a>\n            </li>\n          </ul>\n      </li>\n      <li role=\"tab\" id=\"headingChange\" routerLinkActive=\"active\" *ngIf=\"permission\" >\n        <a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#side-menu\" href=\"#collapseChange\" aria-expanded=\"false\" aria-controls=\"collapseChange\"\n          (click)=\"checkCollapsed('change')\">\n          <i class=\"fa fa-exchange fa-fw\">\n            <div class=\"icon-bg bg-pink\"></div>\n          </i>\n          <span class=\"menu-title\">การเปลี่ยนสินค้า</span>\n          <span class=\"fa icon-collapse fa-angle-down\"></span>\n          <!--<span class=\"label label-yellow\">v3.0</span>-->\n          </a>\n          <ul class=\"nav nav-second-level collapse out\" routerLinkActive=\"in\" id=\"collapseChange\" role=\"tabpanel\" aria-labelledby=\"headingChange\">\n            <li routerLinkActive=\"active\">\n              <a [routerLink]=\"['/claim/change/list']\">\n                <i class=\"fa fa-cogs\"></i>\n                <span class=\"submenu-title\">แสดงรายการเปลี่ยนสินค้า</span>\n              </a>\n            </li>\n            <li routerLinkActive=\"active\">\n              <a [routerLink]=\"['/claim/change/add']\">\n                <i class=\"fa fa-plus-circle\"></i>\n                <span class=\"submenu-title\">เพิ่มรายการเปลี่ยนสินค้า</span>\n              </a>\n            </li>\n          </ul>\n      </li>\n      <li role=\"tab\" id=\"headingFix\" routerLinkActive=\"active\" *ngIf=\"permission\" >\n        <a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#side-menu\" href=\"#collapseFix\" aria-expanded=\"false\" aria-controls=\"collapseFix\"\n          (click)=\"checkCollapsed('fix')\">\n          <i class=\"fa fa-wrench fa-fw\">\n            <div class=\"icon-bg bg-pink\"></div>\n          </i>\n          <span class=\"menu-title\">การซ่อมสินค้า</span>\n          <span class=\"fa icon-collapse fa-angle-down\"></span>\n          <!--<span class=\"label label-yellow\">v3.0</span>-->\n          </a>\n          <ul class=\"nav nav-second-level collapse out\" routerLinkActive=\"in\" id=\"collapseFix\" role=\"tabpanel\" aria-labelledby=\"headingFix\">\n            <li routerLinkActive=\"active\">\n              <a [routerLink]=\"['/claim/fix/list']\">\n                <i class=\"fa fa-cogs\"></i>\n                <span class=\"submenu-title\">แสดงรายการซ่อมสินค้า</span>\n              </a>\n            </li>\n            <li routerLinkActive=\"active\">\n              <a [routerLink]=\"['/claim/fix/add']\">\n                <i class=\"fa fa-plus-circle\"></i>\n                <span class=\"submenu-title\">เพิ่มรายการซ่อมสินค้า</span>\n              </a>\n            </li>\n          </ul>\n      </li>\n      <li role=\"tab\" id=\"headingAcceptProduct\" routerLinkActive=\"active\" *ngIf=\"permission\" >\n        <a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#side-menu\" href=\"#collapseAcceptProduct\" aria-expanded=\"false\"\n          aria-controls=\"collapseAcceptProduct\" (click)=\"checkCollapsed('accept')\">\n          <i class=\"fa fa-sign-in fa-fw\">\n            <div class=\"icon-bg bg-pink\"></div>\n          </i>\n          <span class=\"menu-title\">การนำเข้าสินค้า</span>\n          <span class=\"fa icon-collapse fa-angle-down\"></span>\n          <!--<span class=\"label label-yellow\">v3.0</span>-->\n          </a>\n          <ul class=\"nav nav-second-level collapse out\" routerLinkActive=\"in\" id=\"collapseAcceptProduct\" role=\"tabpanel\" aria-labelledby=\"headingAcceptProduct\">\n            <li routerLinkActive=\"active\">\n              <a [routerLink]=\"['/product/accept/list']\">\n                <i class=\"fa fa-cogs\"></i>\n                <span class=\"submenu-title\">แสดงรายการนำเข้าสินค้า</span>\n              </a>\n            </li>\n            <li routerLinkActive=\"active\">\n              <a [routerLink]=\"['/product/accept/add']\">\n                <i class=\"fa fa-plus-circle\"></i>\n                <span class=\"submenu-title\">เพิ่มรายการนำเข้าสินค้า</span>\n              </a>\n            </li>\n          </ul>\n      </li>\n      <li role=\"tab\" id=\"headingProduct\" routerLinkActive=\"active\">\n        <a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#side-menu\" href=\"#collapseProduct\" aria-expanded=\"true\" aria-controls=\"collapseProduct\"\n          (click)=\"checkCollapsed('product')\">\n          <i class=\"fa fa-cube fa-fw\">\n            <div class=\"icon-bg bg-pink\"></div>\n          </i>\n          <span class=\"menu-title\">สินค้า</span>\n          <span class=\"fa icon-collapse fa-angle-down\"></span>\n          </a>\n          <ul class=\"nav nav-second-level collapse out\" routerLinkActive=\"in\" id=\"collapseProduct\" role=\"tabpanel\" aria-labelledby=\"headingProduct\">\n            <li routerLinkActive=\"active\">\n              <a [routerLink]=\"['/product/manage/list']\">\n                <i class=\"fa fa-cogs\"></i>\n                <span class=\"submenu-title\">แสดงรายการสินค้า</span>\n              </a>\n            </li>\n            <li routerLinkActive=\"active\">\n              <a [routerLink]=\"['/product/manage/add']\">\n                <i class=\"fa fa-plus-circle \"></i>\n                <span class=\"submenu-title\">เพิ่มสินค้า</span>\n              </a>\n            </li>\n          </ul>\n      </li>\n      <li role=\"tab\" id=\"headingTypeProduct\" routerLinkActive=\"active\">\n        <a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#side-menu\" href=\"#collapseTypeProduct\" aria-expanded=\"true\" aria-controls=\"collapseTypeProduct\"\n          (click)=\"checkCollapsed('type')\">\n          <i class=\"fa fa-archive fa-fw\">\n            <div class=\"icon-bg bg-pink\"></div>\n          </i>\n          <span class=\"menu-title\">ประเภทสินค้า</span>\n          <span class=\"fa icon-collapse fa-angle-down\"></span>\n          <!--<span class=\"label label-yellow\">v3.0</span>-->\n          </a>\n          <ul class=\"nav nav-second-level collapse out\" routerLinkActive=\"in\" id=\"collapseTypeProduct\" role=\"tabpanel\" aria-labelledby=\"headingTypeProduct\">\n            <li routerLinkActive=\"active\">\n              <a [routerLink]=\"['/product/type/list']\">\n                <i class=\"fa fa-cogs\"></i>\n                <span class=\"submenu-title\">แสดงรายการประเภทสินค้า</span>\n              </a>\n            </li>\n            <li routerLinkActive=\"active\">\n              <a [routerLink]=\"['/product/type/add']\">\n                <i class=\"fa fa-plus-circle\"></i>\n                <span class=\"submenu-title\">เพิ่มประเภทสินค้า</span>\n              </a>\n            </li>\n          </ul>\n      </li>\n      <li role=\"tab\" id=\"headingStore\" routerLinkActive=\"active\" *ngIf=\"!permission\">\n        <a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#side-menu\" href=\"#collapseStore\" aria-expanded=\"true\" aria-controls=\"collapseStore\"\n          (click)=\"checkCollapsed('store')\">\n          <i class=\"fa fa-cube fa-fw\">\n            <div class=\"icon-bg bg-pink\"></div>\n          </i>\n          <span class=\"menu-title\">ร้านค้า</span>\n          <span class=\"fa icon-collapse fa-angle-down\"></span>\n          </a>\n          <ul class=\"nav nav-second-level collapse out\" routerLinkActive=\"in\" id=\"collapseStore\" role=\"tabpanel\" aria-labelledby=\"headingStore\">\n            <li routerLinkActive=\"active\">\n              <a [routerLink]=\"['/store/list']\">\n                <i class=\"fa fa-cogs\"></i>\n                <span class=\"submenu-title\">แสดงรายการร้านค้า</span>\n              </a>\n            </li>\n            <li routerLinkActive=\"active\">\n              <a [routerLink]=\"['/store/add']\">\n                <i class=\"fa fa-plus-circle\"></i>\n                <span class=\"submenu-title\">เพิ่มร้านค้า</span>\n              </a>\n            </li>\n          </ul>\n      </li>\n      <li role=\"tab\" id=\"headingCustomer\" routerLinkActive=\"active\">\n        <a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#side-menu\" href=\"#collapseCustomer\" aria-expanded=\"true\" aria-controls=\"collapseCustomer\"\n          (click)=\"checkCollapsed('customer')\">\n          <i class=\"fa fa-users fa-fw\">\n            <div class=\"icon-bg bg-pink\"></div>\n          </i>\n          <span class=\"menu-title\">ลูกค้า</span>\n          <span class=\"fa icon-collapse fa-angle-down\"></span>\n          <!--<span class=\"label label-yellow\">v3.0</span>-->\n          </a>\n          <ul class=\"nav nav-second-level collapse out\" routerLinkActive=\"in\" id=\"collapseCustomer\" role=\"tabpanel\" aria-labelledby=\"headingCustomer\">\n            <li routerLinkActive=\"active\">\n              <a [routerLink]=\"['/customer/list']\">\n                <i class=\"fa fa-cogs\"></i>\n                <span class=\"submenu-title\">แสดงรายการลูกค้า</span>\n              </a>\n            </li>\n            <li routerLinkActive=\"active\">\n              <a [routerLink]=\"['/customer/add']\">\n                <i class=\"fa fa-plus-circle\"></i>\n                <span class=\"submenu-title\">เพิ่มลูกค้า</span>\n              </a>\n            </li>\n          </ul>\n      </li>\n      <li role=\"tab\" id=\"headingExpense\" routerLinkActive=\"active\" *ngIf=\"permission\">\n        <a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#side-menu\" href=\"#collapseExpense\" aria-expanded=\"true\" aria-controls=\"collapseExpense\"\n          (click)=\"checkCollapsed('expense')\">\n          <i class=\"fa fa-money fa-fw\">\n            <div class=\"icon-bg bg-pink\"></div>\n          </i>\n          <span class=\"menu-title\">รายจ่ายจิปาถะ</span>\n          <span class=\"fa icon-collapse fa-angle-down\"></span>\n          <!--<span class=\"label label-yellow\">v3.0</span>-->\n          </a>\n          <ul class=\"nav nav-second-level collapse out\" routerLinkActive=\"in\" id=\"collapseExpense\" role=\"tabpanel\" aria-labelledby=\"headingExpense\">\n            <li routerLinkActive=\"active\">\n              <a [routerLink]=\"['/expense/list']\">\n                <i class=\"fa fa-cogs\"></i>\n                <span class=\"submenu-title\">แสดงรายการรายจ่าย</span>\n              </a>\n            </li>\n            <li routerLinkActive=\"active\">\n              <a [routerLink]=\"['/expense/add']\">\n                <i class=\"fa fa-plus-circle\"></i>\n                <span class=\"submenu-title\">เพิ่มรายจ่าย</span>\n              </a>\n            </li>\n          </ul>\n      </li>\n      <li role=\"tab\" id=\"headingReport\" routerLinkActive=\"active\">\n        <a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#side-menu\" href=\"#collapseReport\" aria-expanded=\"true\" aria-controls=\"collapseReport\"\n          (click)=\"checkCollapsed('report')\">\n          <i class=\"fa fa-file-text fa-fw\">\n            <div class=\"icon-bg bg-pink\"></div>\n          </i>\n          <span class=\"menu-title\">รายงาน</span>\n          <span class=\"fa icon-collapse fa-angle-down\"></span>\n          </a>\n          <ul class=\"nav nav-second-level collapse out\" routerLinkActive=\"in\" id=\"collapseReport\" role=\"tabpanel\" aria-labelledby=\"headingReport\">\n            <li routerLinkActive=\"active\" *ngIf=\"permission\">\n              <a [routerLink]=\"['/report/list']\">\n                <i class=\"fa fa-cogs\"></i>\n                <span class=\"submenu-title\">รายงานประจำร้าน</span>\n              </a>\n            </li>\n            <li routerLinkActive=\"active\" *ngIf=\"!permission\">\n              <a [routerLink]=\"['/report/all/list']\">\n                <i class=\"fa fa-plus-circle\"></i>\n                <span class=\"submenu-title\">รายงานทั้งหมด</span>\n              </a>\n            </li>\n          </ul>\n      </li>\n      <li role=\"tab\" id=\"headingUser\" routerLinkActive=\"active\">\n        <a class=\"collapsed\" data-toggle=\"collapse\" data-parent=\"#side-menu\" href=\"#collapseUser\" aria-expanded=\"true\" aria-controls=\"collapseUser\"\n          (click)=\"checkCollapsed('user')\">\n          <i class=\"fa fa-user fa-fw\">\n            <div class=\"icon-bg bg-pink\"></div>\n          </i>\n          <span class=\"menu-title\">ผู้ใช้</span>\n          <span class=\"fa icon-collapse fa-angle-down\"></span>\n          </a>\n          <ul class=\"nav nav-second-level collapse out\" routerLinkActive=\"in\" id=\"collapseUser\" role=\"tabpanel\" aria-labelledby=\"headingUser\">\n            <li routerLinkActive=\"active\">\n              <a [routerLink]=\"['/user/list']\">\n                <i class=\"fa fa-cogs\"></i>\n                <span class=\"submenu-title\">แสดงรายการผู้ใช้</span>\n              </a>\n            </li>\n            <li routerLinkActive=\"active\">\n              <a [routerLink]=\"['/user/add']\">\n                <i class=\"fa fa-plus-circle\"></i>\n                <span class=\"submenu-title\">เพิ่มผู้ใช้</span>\n              </a>\n            </li>\n          </ul>\n      </li>\n    </ul>\n  </div>\n</nav>"

/***/ }),

/***/ 1247:
/***/ (function(module, exports) {

module.exports = "<!-- title -->\n<div id=\"title-breadcrumb-option-demo\" class=\"page-title-breadcrumb\">\n  <div class=\"page-header pull-left\">\n    <div class=\"page-title\">{{Heading}}</div>\n  </div>\n  <ol class=\"breadcrumb page-breadcrumb\">\n    <li>\n      <i class=\"fa fa-home\"></i>&nbsp;\n      <a [routerLink]=\"['/dashboard']\">Home</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n    </li>\n    <li class=\"active\">{{Heading}}</li>\n  </ol>\n  <div class=\"clearfix\"></div>\n</div>\n\n<!-- content -->\n<div class=\"page-content\" id=\"store\">\n  <ul id=\"storeTab\" class=\"nav nav-tabs ul-edit responsive\">\n    <li class=\"active\"><a href=\"#addStore\" data-toggle=\"tab\">เพิ่มร้านค้า</a></li>\n  </ul>\n\n  <div id=\"storeTabContent\" class=\"tab-content\">\n    <app-loading *ngIf=\"isLoading\"></app-loading>\n    <div id=\"addStore\" class=\"tab-pane fade in active\">\n      <form [formGroup]=\"storeForm\" class=\"form-horizontal\" (ngSubmit)=\"sendForm()\">\n        <div class=\"form-body pal\">\n          <div class=\"form-group\">\n            <label for=\"name\" class=\"col-md-3 control-label\">ชื่อร้าน<span class='require'>*</span></label>\n            <div class=\"col-md-5\">\n              <div class=\"input-icon right\">\n                <input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" formControlName=\"name\" [(ngModel)]=\"data.name\"\n                />\n                <p class=\"text-danger\" *ngIf=\"name.hasError('required') && name.dirty\">กรุณากรอกชื่อร้าน</p>\n              </div>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label for=\"tel\" class=\"col-md-3 control-label\">เบอร์ติดต่อ<span class='require'>*</span></label>\n            <div class=\"col-md-5\">\n              <div class=\"input-icon right\">\n                <input type=\"tel\" id=\"tel\" name=\"tel\" class=\"form-control\" formControlName=\"tel\" [(ngModel)]=\"data.tel\"\n                />\n                <p class=\"text-danger\" *ngIf=\"tel.hasError('required') && tel.dirty\">กรุณากรอกเบอร์ติดต่อ</p>\n              </div>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <label for=\"address\" class=\"col-md-3 control-label\">ที่อยู่<span class='require'>*</span></label>\n            <div class=\"col-md-5\">\n              <div class=\"input-icon right\">\n                <textarea id=\"address\" rows=\"3\" class=\"form-control\" name=\"address\" formControlName=\"address\" [(ngModel)]=\"data.address\"></textarea>\n                <p class=\"text-danger\" *ngIf=\"address.hasError('required') && address.dirty\">กรุณากรอกที่อยู่</p>\n              </div>\n            </div>\n          </div>\n          <div class=\"form-actions\">\n            <div class=\"col-md-5 col-md-offset-3\">\n              <button type=\"submit\" class=\"btn btn-success btn-outlined\" [disabled]=\"!storeForm.valid\">บันทึก</button> &nbsp;\n              <button type=\"reset\" class=\"btn btn-danger btn-outlined\">ยกเลิก</button>\n            </div>\n          </div>\n        </div>\n      </form>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ 1248:
/***/ (function(module, exports) {

module.exports = "<!-- title -->\n<div id=\"title-breadcrumb-option-demo\" class=\"page-title-breadcrumb\">\n  <div class=\"page-header pull-left\">\n    <div class=\"page-title\">{{Heading}}</div>\n  </div>\n  <ol class=\"breadcrumb page-breadcrumb\">\n    <li>\n      <i class=\"fa fa-home\"></i>&nbsp;\n      <a [routerLink]=\"['/dashboard']\">Home</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n    </li>\n    <li class=\"active\">{{Heading}}</li>\n  </ol>\n  <div class=\"btn btn-blue reportrange hide\">\n    <i class=\"fa fa-calendar\"></i>&nbsp;\n    <span></span>&nbsp;report&nbsp;\n    <i class=\"fa fa-angle-down\"></i>\n    <input type=\"hidden\" name=\"datestart\" />\n    <input type=\"hidden\" name=\"endstart\" />\n  </div>\n  <div class=\"clearfix\"></div>\n</div>\n\n<!-- content -->\n<div class=\"page-content\" id=\"store\">\n  <ul id=\"storeTab\" class=\"nav nav-tabs ul-edit responsive\">\n    <li class=\"active\"><a href=\"#manageStore\" data-toggle=\"tab\">จัดการร้านค้า</a></li>\n  </ul>\n  <div id=\"storeTabContent\" class=\"tab-content\">\n    <app-loading *ngIf=\"isLoading\"></app-loading>\n    <div id=\"manageStore\" class=\"tab-pane fade in active\">\n      <div class=\"row\">\n        <div class=\"col-lg-12 datatable-simple\" [ngClass]=\"{'tableWhenHideMenu': !isHideMenu}\">\n          <table datatable class=\"table table-bordered\" [dtOptions]=\"dtOptions\" *ngIf=\"data\">\n            <thead>\n              <tr>\n                <th>ID</th>\n                <th>ชื่อร้านค้า</th>\n                <th>เบอร์</th>\n                <th>ที่อยู่</th>\n                <th>Actions</th>\n              </tr>\n            </thead>\n            <tbody>\n              <tr *ngFor=\"let item of data\">\n                <td>{{item.id}}</td>\n                <td>{{item.name}}</td>\n                <td>{{item.tel}}</td>\n                <td>{{item.address}}</td>\n                <td>\n                  <div class=\"actionsBtn\">\n                    <a class=\"btn btn-info btn-outlined\" (click)=\"openEditModal(item.id)\"><i class=\"fa fa-edit\"></i></a>\n                    <a class=\"btn btn-danger btn-outlined\" (click)=\"openDeleteModal(item.id)\"><i class=\"fa fa-trash\"></i></a>\n                  </div>\n                </td>\n              </tr>\n            </tbody>\n          </table>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n\n<div class=\"modal fade\" bsModal #editModal=\"bs-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"mySmallModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-lg modalEditProduct\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title pull-left\">แก้ไข{{Heading}}</h4>\n        <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"editModal.hide()\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <form [formGroup]=\"storeForm\" class=\"form-horizontal\" (ngSubmit)=\"saveChange()\">\n        <div class=\"modal-body\" *ngIf=\"selectedItem\">\n          <div class=\"form-body pal\">\n            <div class=\"form-group\">\n              <label for=\"name\" class=\"col-md-3 control-label\">ชื่อร้าน<span class='require'>*</span></label>\n              <div class=\"col-md-5\">\n                <div class=\"input-icon right\">\n                  <input type=\"text\" id=\"name\" name=\"name\" class=\"form-control\" formControlName=\"name\" [(ngModel)]=\"selectedItem.name\" />\n                  <p class=\"text-danger\" *ngIf=\"name.hasError('required') && name.dirty\">กรุณากรอกชื่อร้าน</p>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <label for=\"tel\" class=\"col-md-3 control-label\">เบอร์ติดต่อ<span class='require'>*</span></label>\n              <div class=\"col-md-5\">\n                <div class=\"input-icon right\">\n                  <input type=\"tel\" id=\"tel\" name=\"tel\" class=\"form-control\" formControlName=\"tel\" [(ngModel)]=\"selectedItem.tel\" />\n                  <p class=\"text-danger\" *ngIf=\"tel.hasError('required') && tel.dirty\">กรุณากรอกเบอร์ติดต่อ</p>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <label for=\"address\" class=\"col-md-3 control-label\">ที่อยู่<span class='require'>*</span></label>\n              <div class=\"col-md-5\">\n                <div class=\"input-icon right\">\n                  <textarea id=\"address\" rows=\"3\" class=\"form-control\" name=\"address\" formControlName=\"address\" [(ngModel)]=\"selectedItem.address\"></textarea>\n                  <p class=\"text-danger\" *ngIf=\"address.hasError('required') && address.dirty\">กรุณากรอกที่อยู่</p>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"editModal.hide()\">ยกเลิก</button>\n          <button type=\"submit\" class=\"btn btn-success btn-square\" [disabled]=\"!storeForm.valid\">บันทึก</button>\n        </div>\n      </form>\n    </div>\n  </div>\n</div>\n\n<div class=\"modal fade\" bsModal #deleteModal=\"bs-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"mySmallModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-sm modalEditProduct\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title pull-left\">ยืนยันการลบ{{Heading}}</h4>\n        <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"deleteModal.hide()\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\" *ngIf=\"selectedItem\">\n        <p>คุณแน่ใจที่ต้องการจะลบ \"{{selectedItem.name}}\"</p>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"deleteModal.hide()\">ยกเลิก</button>\n        <button type=\"button\" class=\"btn btn-success btn-square\" (click)=\"deleteItem()\">แน่ใจ</button>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ 1249:
/***/ (function(module, exports) {

module.exports = "<!-- title -->\n<div id=\"title-breadcrumb-option-demo\" class=\"page-title-breadcrumb\">\n  <div class=\"page-header pull-left\">\n    <div class=\"page-title\">{{Heading}}</div>\n  </div>\n  <ol class=\"breadcrumb page-breadcrumb\">\n    <li>\n      <i class=\"fa fa-home\"></i>&nbsp;\n      <a [routerLink]=\"['/dashboard']\">Home</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n      <a [routerLink]=\"['/product/type/list']\">{{Heading}}</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n    </li>\n    <li class=\"active\">เพิ่ม{{Heading}}</li>\n  </ol>\n</div>\n\n<!-- content -->\n<div class=\"page-content\" id=\"userPermission\">\n  <ul id=\"userPermissionTab\" class=\"nav nav-tabs ul-edit responsive\">\n    <li class=\"active\"><a href=\"#addUserPermission\" data-toggle=\"tab\">เพิ่ม{{Heading}}</a></li>\n  </ul>\n  <div id=\"userPermissionTabContent\" class=\"tab-content\">\n    <app-loading *ngIf=\"isLoading\"></app-loading>\n    <div id=\"addUserPermission\" class=\"tab-pane  fade in active\">\n      <form [formGroup]=\"userPermissionForm\" class=\"form-horizontal\" (ngSubmit)=\"sendForm()\">\n        <div class=\"form-body pal\">\n          <div class=\"form-body pal\">\n            <div class=\"form-group\">\n              <label for=\"username\" class=\"col-md-3 control-label\">ชื่อ{{Heading}}<span class='require'>*</span></label>\n              <div class=\"col-md-8\">\n                <div>\n                  <input type=\"text\" id=\"username\" name=\"username\" formControlName=\"username\" class=\"form-control\" />\n                  <p class=\"text-danger\" *ngIf=\"username.hasError('required') && username.dirty\">กรุณากรอกชื่อผู้ใช้</p>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <label for=\"password\" class=\"col-md-3 control-label\">รหัสผ่าน<span class='require'>*</span></label>\n              <div class=\"col-md-8\">\n                <div>\n                  <input type=\"password\" id=\"password\" name=\"password\" formControlName=\"password\" type=\"password\" class=\"form-control\" />\n                  <p class=\"text-danger\" *ngIf=\"password.hasError('required') && password.dirty\">กรุณากรอกรหัสผ่าน</p>\n                  <p class=\"text-danger\" *ngIf=\"password.errors?.rangeLength && password.dirty\">รหัสผ่านต้องมีมากกว่า 6 ตัวอักษร</p>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <label for=\"repassword\" class=\"col-md-3 control-label\">ยืนยันรหัสผ่าน<span class='require'>*</span></label>\n              <div class=\"col-md-8\">\n                <div>\n                  <input type=\"password\" id=\"repassword\" name=\"repassword\" formControlName=\"repassword\" type=\"password\" class=\"form-control\"\n                  />\n                  <p class=\"text-danger\" *ngIf=\"repassword.hasError('required') && repassword.dirty\">กรุณายืนยันรหัสผ่าน</p>\n                  <p class=\"text-danger\" *ngIf=\"repassword.errors?.equalTo && repassword.dirty\">รหัสผ่านไม่ถูกต้อง</p>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <label for=\"name\" class=\"col-md-3 control-label\">ชื่อ<span class='require'>*</span></label>\n              <div class=\"col-md-8\">\n                <div>\n                  <input type=\"text\" id=\"name\" name=\"name\" formControlName=\"name\" class=\"form-control\" />\n                  <p class=\"text-danger\" *ngIf=\"name.hasError('required') && name.dirty\">กรุณากรอกชื่อ</p>\n                </div>\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <label class=\"col-md-3 control-label\">สาขา</label>\n              <div class=\"col-md-7 selectStore\">\n                <ng-select name=\"shop_id\" formControlName=\"shop_id\" [options]=\"shop_data\" [multiple]=\"false\" highlightColor=\"#d94e37\" highlightTextColor=\"#ffffff\"\n                  *ngIf=\"shops\" notFoundMsg=\"ไม่พบร้านค้า\">\n                </ng-select>\n              </div>\n              <div class=\"col-md-1 addStoreBtn\">\n                <button class=\"btn col-md-1 btn-success btn-outlined\" routerLink=\"/store/add\" type=\"button\">เพิ่มร้าน</button>\n              </div>\n            </div>\n            <div class=\"form-group\">\n              <label for=\"detail\" class=\"col-md-3 control-label\">รายละเอียด</label>\n              <div class=\"col-md-8\">\n                <div>\n                  <textarea id=\"detail\" rows=\"3\" class=\"form-control\" name=\"detail\" formControlName=\"detail\"></textarea>\n                </div>\n              </div>\n            </div>\n          </div>\n          <div class=\"form-actions\">\n            <div class=\"col-md-12\">\n              <button type=\"submit\" class=\"btn btn-success btn-outlined\" [disabled]=\"!userPermissionForm.valid\">บันทึก</button>              &nbsp;\n              <button type=\"reset\" class=\"btn btn-danger btn-outlined\">ล้าง</button>\n            </div>\n          </div>\n        </div>\n      </form>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ 1250:
/***/ (function(module, exports) {

module.exports = "<!-- title -->\n<div id=\"title-breadcrumb-option-demo\" class=\"page-title-breadcrumb\">\n  <div class=\"page-header pull-left\">\n    <div class=\"page-title\">{{Heading}}</div>\n  </div>\n  <ol class=\"breadcrumb page-breadcrumb\">\n    <li>\n      <i class=\"fa fa-home\"></i>&nbsp;\n      <a [routerLink]=\"['/dashboard']\">Home</a>&nbsp;&nbsp;\n      <i class=\"fa fa-angle-right\"></i>&nbsp;&nbsp;\n    </li>\n    <li class=\"active\">{{Heading}}</li>\n  </ol>\n  <div class=\"btn btn-blue reportrange hide\">\n    <i class=\"fa fa-calendar\"></i>&nbsp;\n    <span></span>&nbsp;report&nbsp;\n    <i class=\"fa fa-angle-down\"></i>\n    <input type=\"hidden\" name=\"datestart\" />\n    <input type=\"hidden\" name=\"endstart\" />\n  </div>\n  <div class=\"clearfix\"></div>\n</div>\n\n<!-- content -->\n<div class=\"page-content\" id=\"userPermistion\">\n  <ul id=\"userPermistionTab\" class=\"nav nav-tabs ul-edit responsive\">\n    <li class=\"active\"><a href=\"#manageUserPermistion\" data-toggle=\"tab\">จัดการ{{Heading}}</a></li>\n  </ul>\n  <div id=\"userPermistionTabContent\" class=\"tab-content\">\n    <app-loading *ngIf=\"isLoading\"></app-loading>\n    <div id=\"manageUserPermistion\" class=\"tab-pane fade in active\">\n      <div class=\"row\" *ngIf=\"!isLoading\">\n        <div class=\"col-lg-12 datatable-simple\">\n          <table datatable class=\"table table-bordered\" [dtOptions]=\"dtOptions\" *ngIf=\"data\">\n            <thead>\n              <tr>\n                <th>#</th>\n                <th>ชื่อผู้ใช้</th>\n                <th>ชื่อ</th>\n                <th>ร้านค้า</th>\n                <th>รายละเอียด</th>\n                <th>Actions</th>\n              </tr>\n            </thead>\n            <tbody>\n              <tr *ngFor=\"let item of data; let i = index\">\n                <td>{{i+1}}</td>\n                <td>{{item.username}}</td>\n                <td>{{item.name}}</td>\n                <td>{{item.shop && item.shop.name}}</td>\n                <td>{{item.detail}}</td>\n                <td>\n                  <div class=\"actionsBtn\">\n                    <a class=\"btn btn-info btn-outlined\" (click)=\"openEditModal(item)\"><i class=\"fa fa-edit\"></i></a>\n                    <a class=\"btn btn-danger btn-outlined\" (click)=\"openDeleteModal(item)\"><i class=\"fa fa-trash\"></i></a>\n                  </div>\n                </td>\n              </tr>\n            </tbody>\n          </table>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n\n<div class=\"modal fade\" bsModal #editModal=\"bs-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"mySmallModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-lg modalEditProduct\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title pull-left\">แก้ไข{{Heading}}</h4>\n        <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"editModal.hide()\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <!--<form [formGroup]=\"userPermissionForm\" class=\"form-horizontal\">-->\n      <form [formGroup]=\"userPermissionForm\" class=\"form-horizontal\" (ngSubmit)=\"saveChange()\">\n        <div class=\"modal-body\" *ngIf=\"selectedItem\">\n          <div class=\"form-body pal\">\n            <div class=\"form-body pal\">\n              <div class=\"form-group\">\n                <label for=\"username\" class=\"col-md-3 control-label\">ชื่อผู้ใช้<span class='require'>*</span></label>\n                <div class=\"col-md-8 radioButtonCustom\">\n                  <div>\n                    <input type=\"text\" id=\"username\" name=\"username\" formControlName=\"username\" class=\"form-control\" [(ngModel)]=\"selectedItem.username\"\n                      disabled/>\n                    <p class=\"text-danger\" *ngIf=\"username.hasError('required') && username.dirty\">กรุณากรอกชื่อผู้ใช้</p>\n                  </div>\n                  <div>\n                    <label class=\"btn active\">\n                      <input type=\"checkbox\" name=\"changePass\" formControlName=\"changePass\" [(ngModel)]=\"changePassword\">\n                      <i class=\"fa fa-square-o notCheck\"></i>\n                      <i class=\"fa fa-check-square selected\"></i>\n                      <span>เปลี่ยนรหัสผ่าน</span>\n                    </label>\n                  </div>\n                </div>\n              </div>\n              <div *ngIf=\"changePassword\">\n                <div class=\"form-group\">\n                  <label for=\"password\" class=\"col-md-3 control-label\">รหัสผ่าน<span class='require'>*</span></label>\n                  <div class=\"col-md-8\">\n                    <div>\n                      <input type=\"password\" id=\"password\" name=\"password\" formControlName=\"password\" type=\"password\" class=\"form-control\" [(ngModel)]=\"selectedItem.password\"\n                      />\n                      <p class=\"text-danger\" *ngIf=\"password.errors?.rangeLength && password.dirty\">รหัสผ่านต้องมีมากกว่า 6 ตัวอักษร</p>\n                    </div>\n                  </div>\n                </div>\n                <div class=\"form-group\">\n                  <label for=\"repassword\" class=\"col-md-3 control-label\">ยืนยันรหัสผ่าน<span class='require'>*</span></label>\n                  <div class=\"col-md-8\">\n                    <div>\n                      <input type=\"password\" id=\"repassword\" name=\"repassword\" formControlName=\"repassword\" type=\"password\" class=\"form-control\"\n                        [(ngModel)]=\"selectedItem.repassword\" />\n                      <p class=\"text-danger\" *ngIf=\"repassword.errors?.equalTo && repassword.dirty\">รหัสผ่านไม่ถูกต้อง</p>\n                    </div>\n                  </div>\n                </div>\n              </div>\n              <div class=\"form-group\">\n                <label for=\"name\" class=\"col-md-3 control-label\">ชื่อ<span class='require'>*</span></label>\n                <div class=\"col-md-8\">\n                  <div>\n                    <input type=\"text\" id=\"name\" name=\"name\" formControlName=\"name\" class=\"form-control\" [(ngModel)]=\"selectedItem.name\" />\n                    <p class=\"text-danger\" *ngIf=\"name.hasError('required') && name.dirty\">กรุณากรอกชื่อ</p>\n                  </div>\n                </div>\n              </div>\n              <div class=\"form-group\">\n                <label class=\"col-md-3 control-label\">สาขา<span class='require'>*</span></label>\n                <div class=\"col-md-8\">\n                  <select [(ngModel)]=\"selectedItem.shop_id\" name=\"Select name\" class=\"selection\" [ngModelOptions]=\"{standalone: true}\"> \n                    <option *ngFor=\"let item of shop_data\"  [attr.value]=\"item.value\" [attr.selected]=\"item.value == selectedItem.shop_id ? true : null\">{{item.label}}</option>\n                  </select>\n                  <p class=\"text-danger\" *ngIf=\"selectedItem.shop === ''\">กรุณาเลือกสาขา</p>\n                </div>\n              </div>\n              <div class=\"form-group\">\n                <label for=\"detail\" class=\"col-md-3 control-label\">รายละเอียด<span class='require'>*</span></label>\n                <div class=\"col-md-8\">\n                  <div>\n                    <textarea id=\"detail\" rows=\"3\" class=\"form-control\" name=\"detail\" formControlName=\"detail\" [(ngModel)]=\"selectedItem.detail\"></textarea>\n                    <p class=\"text-danger\" *ngIf=\"detail.hasError('required') && detail.dirty\">กรุณารายละเอียด</p>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n        <div class=\"modal-footer\">\n          <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"editModal.hide()\">ยกเลิก</button>\n          <button type=\"submit\" class=\"btn btn-success btn-square\" [disabled]=\"!userPermissionForm.valid\">บันทึก</button>\n        </div>\n      </form>\n    </div>\n  </div>\n</div>\n\n<div class=\"modal fade\" bsModal #deleteModal=\"bs-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"mySmallModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-sm modalEditProduct\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title pull-left\">ยืนยันการลบ{{Heading}}</h4>\n        <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"deleteModal.hide()\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\" *ngIf=\"selectedItem\">\n        <p>คุณแน่ใจที่ต้องการจะลบ \"{{selectedItem.name}}\"</p>\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-danger btn-outlined btn-square\" (click)=\"deleteModal.hide()\">ยกเลิก</button>\n        <button type=\"button\" class=\"btn btn-success btn-square\" (click)=\"deleteItem()\">แน่ใจ</button>\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ 131:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HideSidebarService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

let HideSidebarService = class HideSidebarService extends __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] {
    constructor() {
        super();
    }
    initHideSidebar() {
        this.isHideSidebar = true;
        console.log('init hide Sidebar :: ', this.isHideSidebar);
        return this.isHideSidebar;
    }
    setHideSidebar() {
        this.isHideSidebar = !this.isHideSidebar;
        console.log('set hide Sidebar :: ', this.isHideSidebar);
        return this.isHideSidebar;
    }
    getHideSidebar() {
        return this.isHideSidebar;
    }
};
HideSidebarService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(), 
    __metadata('design:paramtypes', [])
], HideSidebarService);
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/hide-sidebar.service.js.map

/***/ }),

/***/ 1317:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(704);


/***/ }),

/***/ 32:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HideMenuService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

let HideMenuService = class HideMenuService extends __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"] {
    constructor() {
        super();
    }
    initHideMenu() {
        this.isShowMenu = true;
        console.log('init hide menu :: ', this.isShowMenu);
        return this.isShowMenu;
    }
    setHideMenu() {
        this.isShowMenu = !this.isShowMenu;
        console.log('set hide menu :: ', this.isShowMenu);
        return this.isShowMenu;
    }
    getHideMenu() {
        return this.isShowMenu;
    }
};
HideMenuService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(), 
    __metadata('design:paramtypes', [])
], HideMenuService);
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/hide-menu.service.js.map

/***/ }),

/***/ 4:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__(1257);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_throw__ = __webpack_require__(1256);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_throw___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_observable_throw__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_toPromise__ = __webpack_require__(1261);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_toPromise__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AllServiceService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};







let AllServiceService = class AllServiceService {
    constructor(http, router) {
        this.http = http;
        this.router = router;
        this.req_params = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* URLSearchParams */]();
        // public baseUrl: string = 'http://128.199.118.178/stock/public/index.php/';
        this.baseUrl = 'http://jipataphone.com/service/public/index.php/';
    }
    post(path, data) {
        return __awaiter(this, void 0, void 0, function* () {
            this.setHeader(null);
            let url = this.baseUrl + path;
            // data.user_id = await this.getLocal('user').shop_id;
            console.log('this.options', this.options);
            const response = yield this.http.post(url, data, this.options).toPromise();
            return response.json();
        });
    }
    postWithId(path, id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            this.setHeader(null);
            let url = this.baseUrl + path + '/' + id;
            // data.user_id = await this.getLocal('user').shop_id;
            const response = yield this.http.post(url, data, this.options).toPromise();
            return response.json();
        });
    }
    get(path, params) {
        return __awaiter(this, void 0, void 0, function* () {
            this.setHeader(params);
            let url = this.baseUrl + path;
            const response = yield this.http.get(url, this.options).toPromise();
            return response.json();
        });
    }
    getWithId(path, id, params) {
        return __awaiter(this, void 0, void 0, function* () {
            this.setHeader(params);
            let url = this.baseUrl + path + '/' + id;
            const response = yield this.http.get(url, this.options).toPromise();
            return response.json();
        });
    }
    put(path, data) {
        return __awaiter(this, void 0, void 0, function* () {
            this.setHeader(null);
            let url = this.baseUrl + path;
            // const response = await this.http.put(url, data, this.options).toPromise();
            this.headers.append('X-HTTP-Method-Override', 'PUT');
            this.options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* RequestOptions */]({ headers: this.headers, search: this.req_params });
            const response = yield this.http.post(url, data, this.options).toPromise();
            return response.json();
        });
    }
    putWithId(path, id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            this.setHeader(null);
            let url = this.baseUrl + path + '/' + id;
            // data.user_id = await this.getLocal('user').shop_id;
            this.headers.append('X-HTTP-Method-Override', 'PUT');
            this.options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* RequestOptions */]({ headers: this.headers, search: this.req_params });
            const response = yield this.http.post(url, data, this.options).toPromise();
            return response.json();
        });
    }
    delete(path, params) {
        return __awaiter(this, void 0, void 0, function* () {
            this.setHeader(null);
            let url = this.baseUrl + path;
            this.headers.append('X-HTTP-Method-Override', 'DELETE');
            this.options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* RequestOptions */]({ headers: this.headers, search: this.req_params });
            const response = yield this.http.post(url, null, this.options).toPromise();
            return response.json();
        });
    }
    deleteWithId(path, id, params) {
        return __awaiter(this, void 0, void 0, function* () {
            this.setHeader(null);
            let url = this.baseUrl + path + '/' + id;
            this.headers.append('X-HTTP-Method-Override', 'DELETE');
            this.options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* RequestOptions */]({ headers: this.headers, search: this.req_params });
            const response = yield this.http.post(url, null, this.options).toPromise();
            return response.json();
        });
    }
    setToken(value) {
        localStorage.setItem('token', value);
    }
    getToken() {
        let token = localStorage.getItem('token');
        return token;
    }
    setLocal(name, value) {
        localStorage.setItem(name, JSON.stringify(value));
    }
    getLocal(name) {
        let result = localStorage.getItem(name);
        return result ? JSON.parse(result) : null;
    }
    refreshToken() {
        return __awaiter(this, void 0, void 0, function* () {
            let token = yield this.get('auth/refreshToken');
            console.log('token', token.data.newToken);
            this.setToken(token.data.newToken);
        });
    }
    checkLogin() {
        if (this.getToken === null) {
            this.router.navigate(['/login']);
        }
        else {
            return true;
        }
    }
    setHeader(params) {
        let authToken = 'Bearer ' + this.getToken();
        this.headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* Headers */]({
            'Accept': 'application/json'
        });
        this.headers.append('Authorization', authToken);
        // let req_params: URLSearchParams = new URLSearchParams();
        if (params) {
            let keys = Object.keys(params);
            for (let i = 0; i < keys.length; i++) {
                this.req_params.set(keys[i], params[keys[i]]);
            }
        }
        this.options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* RequestOptions */]({ headers: this.headers, search: this.req_params });
    }
};
AllServiceService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(), 
    __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["e" /* Http */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__angular_http__["e" /* Http */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_router__["b" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_router__["b" /* Router */]) === 'function' && _b) || Object])
], AllServiceService);
var _a, _b;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/all-service.service.js.map

/***/ }),

/***/ 451:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__shared_hide_sidebar_service__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__service_all_service_service__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_hide_menu_service__ = __webpack_require__(32);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





let AppComponent = class AppComponent {
    constructor(hideMenuService, hideSidebarService, router, allService) {
        this.hideMenuService = hideMenuService;
        this.hideSidebarService = hideSidebarService;
        this.router = router;
        this.allService = allService;
        this.title = 'app works!';
        if (this.allService.getToken()) {
            console.log('have token', allService.getToken());
            this.isHideSidebar = true;
        }
        else {
            console.log('not have token', allService.getToken());
            this.isHideSidebar = false;
            router.navigate(['/login']);
        }
        router.events.subscribe((url) => {
            this.rootUrl = url.url;
            if (this.rootUrl === '/login') {
                this.isHideSidebar = false;
            }
            else {
                this.isHideSidebar = true;
            }
        });
        hideSidebarService.subscribe({
            next: isHideSidebar => {
                this.isHideSidebar = isHideSidebar;
                console.log('isHideMenu', this.isHideMenu);
                return this.isHideMenu;
            }
        });
        hideMenuService.subscribe({
            next: isHideMenu => {
                this.isHideMenu = isHideMenu;
                console.log('isHideMenu', this.isHideMenu);
                return this.isHideMenu;
            }
        });
    }
};
AppComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
        selector: 'app-root',
        template: __webpack_require__(1212),
        styles: [__webpack_require__(1171)]
    }), 
    __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_4__shared_hide_menu_service__["a" /* HideMenuService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4__shared_hide_menu_service__["a" /* HideMenuService */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__shared_hide_sidebar_service__["a" /* HideSidebarService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__shared_hide_sidebar_service__["a" /* HideSidebarService */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__service_all_service_service__["a" /* AllServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__service_all_service_service__["a" /* AllServiceService */]) === 'function' && _d) || Object])
], AppComponent);
var _a, _b, _c, _d;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/app.component.js.map

/***/ }),

/***/ 452:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__service_all_service_service__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_hide_menu_service__ = __webpack_require__(32);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SidebarComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};




let SidebarComponent = class SidebarComponent {
    constructor(_router, hideMenuService, allService) {
        this._router = _router;
        this.hideMenuService = hideMenuService;
        this.allService = allService;
        this.isCollapsed = {
            'store': false,
            'product': false,
            'accept': false,
            'transfer': false,
            'sell': false,
            'change': false,
            'fix': false,
            'type': false,
            'customer': false,
            'expense': false,
            'report': false,
            'user': false
        };
        this.isSelected = false;
        this.router = _router;
        hideMenuService.subscribe({
            next: isHideMenu => {
                this.isHideMenu = isHideMenu;
                return this.isHideMenu;
            }
        });
    }
    ngOnInit() {
        this.init();
    }
    init() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.checkPermission();
        });
    }
    checkPermission() {
        return __awaiter(this, void 0, void 0, function* () {
            let user = yield this.allService.getLocal('user');
            this.permission = user.shop_id !== null ? true : false;
        });
    }
    checkCollapsed(tab) {
        for (var key in this.isCollapsed) {
            if (this.isCollapsed.hasOwnProperty(tab) && key === tab) {
                this.isCollapsed[key] = !this.isCollapsed[key];
            }
            else {
                this.isCollapsed[key] = false;
            }
        }
        return this.isCollapsed;
    }
};
SidebarComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
        selector: 'app-sidebar',
        template: __webpack_require__(1246),
        styles: [__webpack_require__(1205)]
    }), 
    __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__shared_hide_menu_service__["a" /* HideMenuService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__shared_hide_menu_service__["a" /* HideMenuService */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__service_all_service_service__["a" /* AllServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__service_all_service_service__["a" /* AllServiceService */]) === 'function' && _c) || Object])
], SidebarComponent);
var _a, _b, _c;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/sidebar.component.js.map

/***/ }),

/***/ 703:
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = 703;


/***/ }),

/***/ 704:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__polyfills_ts__ = __webpack_require__(889);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__(809);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__(888);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app___ = __webpack_require__(861);





if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["enableProdMode"])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_4__app___["a" /* AppModule */]);
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/main.js.map

/***/ }),

/***/ 843:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(127);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ng_bootstrap_ng_bootstrap__ = __webpack_require__(841);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ng2_bootstrap__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ng2_bootstrap_component_loader__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ng2_charts_ng2_charts__ = __webpack_require__(1119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ng2_charts_ng2_charts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_ng2_charts_ng2_charts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ng2_validation__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ng2_validation___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_ng2_validation__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_ng2_toastr_ng2_toastr__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_ng2_toastr_ng2_toastr__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_angular2_select__ = __webpack_require__(902);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_angular2_select___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_angular2_select__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_angular_datatables__ = __webpack_require__(890);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_mydatepicker__ = __webpack_require__(1091);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_mydaterangepicker__ = __webpack_require__(1096);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_mydaterangepicker___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_14_mydaterangepicker__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__shared_hide_menu_service__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_ng2_bootstrap_positioning__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17_ng2_bootstrap_progressbar__ = __webpack_require__(641);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__auth_guard_service__ = __webpack_require__(844);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__user_guard_service__ = __webpack_require__(885);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__service_all_service_service__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__service_toast_service__ = __webpack_require__(881);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__app_component__ = __webpack_require__(451);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__header_header_component__ = __webpack_require__(859);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__sidebar_sidebar_component__ = __webpack_require__(452);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__store_store_component__ = __webpack_require__(884);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__product_manage_product_manage_product_component__ = __webpack_require__(871);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__product_accept_product_accept_product_component__ = __webpack_require__(867);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__product_sell_product_sell_product_component__ = __webpack_require__(874);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__product_transfer_product_transfer_product_component__ = __webpack_require__(876);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__customer_customer_component__ = __webpack_require__(854);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__product_type_product_type_component__ = __webpack_require__(866);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__report_report_component__ = __webpack_require__(880);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__product_info_product_info_product_component__ = __webpack_require__(869);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__product_manage_product_add_product_component__ = __webpack_require__(870);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__product_accept_product_add_accept_product_component__ = __webpack_require__(868);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__product_sell_product_add_sell_product_component__ = __webpack_require__(873);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__product_transfer_product_add_transfer_product_component__ = __webpack_require__(875);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__store_add_store_component__ = __webpack_require__(883);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__product_type_product_type_add_component__ = __webpack_require__(865);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__customer_customer_add_component__ = __webpack_require__(853);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__expense_expense_component__ = __webpack_require__(858);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__expense_expense_add_component__ = __webpack_require__(857);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__report_report_all_component__ = __webpack_require__(877);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__report_report_day_component__ = __webpack_require__(878);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__report_report_month_component__ = __webpack_require__(879);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__login_login_component__ = __webpack_require__(863);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__user_permission_user_permission_component__ = __webpack_require__(887);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__home_home_component__ = __webpack_require__(860);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__user_permission_user_add_component__ = __webpack_require__(886);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__claim_fix_fix_manage_component__ = __webpack_require__(851);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51__claim_fix_fix_info_component__ = __webpack_require__(850);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_52__claim_fix_fix_add_component__ = __webpack_require__(849);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_53__claim_fix_fix_print_component__ = __webpack_require__(852);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_54__claim_change_change_manage_component__ = __webpack_require__(847);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_55__claim_change_change_info_component__ = __webpack_require__(846);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_56__claim_change_change_add_component__ = __webpack_require__(845);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_57__claim_change_change_print_component__ = __webpack_require__(848);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_58__product_print_barcode_print_barcode_component__ = __webpack_require__(872);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_59__shared_array_number_pipe__ = __webpack_require__(882);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_60__loading_loading_component__ = __webpack_require__(862);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_61__pagination_pagination_component__ = __webpack_require__(864);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_62__shared_hide_sidebar_service__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_63__date_month_picker_component__ = __webpack_require__(855);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_64__date_year_picker_component__ = __webpack_require__(856);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

































































let AppModule = class AppModule {
};
AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_22__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_23__header_header_component__["a" /* HeaderComponent */],
            __WEBPACK_IMPORTED_MODULE_24__sidebar_sidebar_component__["a" /* SidebarComponent */],
            __WEBPACK_IMPORTED_MODULE_25__store_store_component__["a" /* StoreComponent */],
            __WEBPACK_IMPORTED_MODULE_26__product_manage_product_manage_product_component__["a" /* ManageProductComponent */],
            __WEBPACK_IMPORTED_MODULE_27__product_accept_product_accept_product_component__["a" /* AcceptProductComponent */],
            __WEBPACK_IMPORTED_MODULE_28__product_sell_product_sell_product_component__["a" /* SellProductComponent */],
            __WEBPACK_IMPORTED_MODULE_29__product_transfer_product_transfer_product_component__["a" /* TransferProductComponent */],
            __WEBPACK_IMPORTED_MODULE_30__customer_customer_component__["a" /* CustomerComponent */],
            __WEBPACK_IMPORTED_MODULE_31__product_type_product_type_component__["a" /* ProductTypeComponent */],
            __WEBPACK_IMPORTED_MODULE_32__report_report_component__["a" /* ReportComponent */],
            __WEBPACK_IMPORTED_MODULE_33__product_info_product_info_product_component__["a" /* InfoProductComponent */],
            __WEBPACK_IMPORTED_MODULE_34__product_manage_product_add_product_component__["a" /* AddProductComponent */],
            __WEBPACK_IMPORTED_MODULE_35__product_accept_product_add_accept_product_component__["a" /* AddAcceptProductComponent */],
            __WEBPACK_IMPORTED_MODULE_36__product_sell_product_add_sell_product_component__["a" /* AddSellProductComponent */],
            __WEBPACK_IMPORTED_MODULE_37__product_transfer_product_add_transfer_product_component__["a" /* AddTransferProductComponent */],
            __WEBPACK_IMPORTED_MODULE_38__store_add_store_component__["a" /* AddStoreComponent */],
            __WEBPACK_IMPORTED_MODULE_39__product_type_product_type_add_component__["a" /* ProductTypeAddComponent */],
            __WEBPACK_IMPORTED_MODULE_40__customer_customer_add_component__["a" /* CustomerAddComponent */],
            __WEBPACK_IMPORTED_MODULE_41__expense_expense_component__["a" /* ExpenseComponent */],
            __WEBPACK_IMPORTED_MODULE_42__expense_expense_add_component__["a" /* ExpenseAddComponent */],
            __WEBPACK_IMPORTED_MODULE_43__report_report_all_component__["a" /* ReportAllComponent */],
            __WEBPACK_IMPORTED_MODULE_44__report_report_day_component__["a" /* ReportDayComponent */],
            __WEBPACK_IMPORTED_MODULE_45__report_report_month_component__["a" /* ReportMonthComponent */],
            __WEBPACK_IMPORTED_MODULE_46__login_login_component__["a" /* LoginComponent */],
            __WEBPACK_IMPORTED_MODULE_47__user_permission_user_permission_component__["a" /* UserPermissionComponent */],
            __WEBPACK_IMPORTED_MODULE_48__home_home_component__["a" /* HomeComponent */],
            __WEBPACK_IMPORTED_MODULE_49__user_permission_user_add_component__["a" /* UserAddComponent */],
            __WEBPACK_IMPORTED_MODULE_50__claim_fix_fix_manage_component__["a" /* FixManageComponent */],
            __WEBPACK_IMPORTED_MODULE_51__claim_fix_fix_info_component__["a" /* FixInfoComponent */],
            __WEBPACK_IMPORTED_MODULE_52__claim_fix_fix_add_component__["a" /* FixAddComponent */],
            __WEBPACK_IMPORTED_MODULE_53__claim_fix_fix_print_component__["a" /* FixPrintComponent */],
            __WEBPACK_IMPORTED_MODULE_54__claim_change_change_manage_component__["a" /* ChangeManageComponent */],
            __WEBPACK_IMPORTED_MODULE_55__claim_change_change_info_component__["a" /* ChangeInfoComponent */],
            __WEBPACK_IMPORTED_MODULE_56__claim_change_change_add_component__["a" /* ChangeAddComponent */],
            __WEBPACK_IMPORTED_MODULE_57__claim_change_change_print_component__["a" /* ChangePrintComponent */],
            __WEBPACK_IMPORTED_MODULE_58__product_print_barcode_print_barcode_component__["a" /* PrintBarcodeComponent */],
            __WEBPACK_IMPORTED_MODULE_59__shared_array_number_pipe__["a" /* ArrayNumberPipe */],
            __WEBPACK_IMPORTED_MODULE_60__loading_loading_component__["a" /* LoadingComponent */],
            __WEBPACK_IMPORTED_MODULE_61__pagination_pagination_component__["a" /* PaginationComponent */],
            __WEBPACK_IMPORTED_MODULE_63__date_month_picker_component__["a" /* MonthPicker */],
            __WEBPACK_IMPORTED_MODULE_64__date_year_picker_component__["a" /* YearPicker */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["BrowserModule"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormsModule"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["ReactiveFormsModule"],
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_5__ng_bootstrap_ng_bootstrap__["a" /* NgbModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_6_ng2_bootstrap__["a" /* PaginationModule */],
            __WEBPACK_IMPORTED_MODULE_6_ng2_bootstrap__["b" /* TypeaheadModule */],
            __WEBPACK_IMPORTED_MODULE_6_ng2_bootstrap__["c" /* ProgressbarModule */],
            __WEBPACK_IMPORTED_MODULE_6_ng2_bootstrap__["d" /* CollapseModule */],
            __WEBPACK_IMPORTED_MODULE_6_ng2_bootstrap__["e" /* ModalModule */],
            __WEBPACK_IMPORTED_MODULE_8_ng2_charts_ng2_charts__["ChartsModule"],
            __WEBPACK_IMPORTED_MODULE_9_ng2_validation__["CustomFormsModule"],
            __WEBPACK_IMPORTED_MODULE_10_ng2_toastr_ng2_toastr__["ToastModule"].forRoot(),
            __WEBPACK_IMPORTED_MODULE_11_angular2_select__["SelectModule"],
            __WEBPACK_IMPORTED_MODULE_12_angular_datatables__["a" /* DataTablesModule */],
            __WEBPACK_IMPORTED_MODULE_13_mydatepicker__["MyDatePickerModule"],
            __WEBPACK_IMPORTED_MODULE_14_mydaterangepicker__["MyDateRangePickerModule"],
            __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* RouterModule */].forRoot([
                { path: 'login', component: __WEBPACK_IMPORTED_MODULE_46__login_login_component__["a" /* LoginComponent */] },
                {
                    path: '',
                    canActivate: [__WEBPACK_IMPORTED_MODULE_18__auth_guard_service__["a" /* AuthGuardService */]],
                    children: [
                        {
                            path: 'user',
                            children: [
                                { path: 'list', component: __WEBPACK_IMPORTED_MODULE_47__user_permission_user_permission_component__["a" /* UserPermissionComponent */] },
                                { path: 'add', component: __WEBPACK_IMPORTED_MODULE_49__user_permission_user_add_component__["a" /* UserAddComponent */] }
                            ]
                        },
                        {
                            path: 'store',
                            children: [
                                { path: 'list', component: __WEBPACK_IMPORTED_MODULE_25__store_store_component__["a" /* StoreComponent */] },
                                { path: 'add', component: __WEBPACK_IMPORTED_MODULE_38__store_add_store_component__["a" /* AddStoreComponent */] }
                            ]
                        },
                        {
                            path: 'product',
                            children: [
                                { path: 'manage/list', component: __WEBPACK_IMPORTED_MODULE_26__product_manage_product_manage_product_component__["a" /* ManageProductComponent */] },
                                { path: 'manage/list/info/:id', component: __WEBPACK_IMPORTED_MODULE_33__product_info_product_info_product_component__["a" /* InfoProductComponent */] },
                                { path: 'manage/add', component: __WEBPACK_IMPORTED_MODULE_34__product_manage_product_add_product_component__["a" /* AddProductComponent */] },
                                { path: 'type/list', component: __WEBPACK_IMPORTED_MODULE_31__product_type_product_type_component__["a" /* ProductTypeComponent */] },
                                { path: 'type/add', component: __WEBPACK_IMPORTED_MODULE_39__product_type_product_type_add_component__["a" /* ProductTypeAddComponent */] },
                                { path: 'accept/list', component: __WEBPACK_IMPORTED_MODULE_27__product_accept_product_accept_product_component__["a" /* AcceptProductComponent */] },
                                { path: 'accept/add', component: __WEBPACK_IMPORTED_MODULE_35__product_accept_product_add_accept_product_component__["a" /* AddAcceptProductComponent */] },
                                { path: 'transfer/list', component: __WEBPACK_IMPORTED_MODULE_29__product_transfer_product_transfer_product_component__["a" /* TransferProductComponent */] },
                                { path: 'transfer/add', component: __WEBPACK_IMPORTED_MODULE_37__product_transfer_product_add_transfer_product_component__["a" /* AddTransferProductComponent */] },
                                { path: 'sell/list', component: __WEBPACK_IMPORTED_MODULE_28__product_sell_product_sell_product_component__["a" /* SellProductComponent */] },
                                { path: 'sell/add', component: __WEBPACK_IMPORTED_MODULE_36__product_sell_product_add_sell_product_component__["a" /* AddSellProductComponent */] },
                                { path: 'print', component: __WEBPACK_IMPORTED_MODULE_58__product_print_barcode_print_barcode_component__["a" /* PrintBarcodeComponent */], data: [{ 'dataSelect': 'lass' }] }
                            ]
                        },
                        {
                            path: 'customer',
                            children: [
                                { path: 'list', component: __WEBPACK_IMPORTED_MODULE_30__customer_customer_component__["a" /* CustomerComponent */] },
                                { path: 'add', component: __WEBPACK_IMPORTED_MODULE_40__customer_customer_add_component__["a" /* CustomerAddComponent */] }
                            ]
                        },
                        {
                            path: 'report',
                            children: [
                                { path: 'list', component: __WEBPACK_IMPORTED_MODULE_32__report_report_component__["a" /* ReportComponent */] },
                                { path: 'all/list', component: __WEBPACK_IMPORTED_MODULE_43__report_report_all_component__["a" /* ReportAllComponent */] },
                                { path: 'all/list/info', component: __WEBPACK_IMPORTED_MODULE_32__report_report_component__["a" /* ReportComponent */] },
                            ]
                        },
                        {
                            path: 'claim',
                            children: [
                                {
                                    path: 'fix',
                                    children: [
                                        {
                                            path: 'list', children: [
                                                { path: '', component: __WEBPACK_IMPORTED_MODULE_50__claim_fix_fix_manage_component__["a" /* FixManageComponent */] },
                                                {
                                                    path: 'info/:id',
                                                    children: [
                                                        { path: '', component: __WEBPACK_IMPORTED_MODULE_51__claim_fix_fix_info_component__["a" /* FixInfoComponent */] },
                                                        { path: 'print', component: __WEBPACK_IMPORTED_MODULE_53__claim_fix_fix_print_component__["a" /* FixPrintComponent */] }
                                                    ]
                                                }
                                            ]
                                        },
                                        { path: 'add', component: __WEBPACK_IMPORTED_MODULE_52__claim_fix_fix_add_component__["a" /* FixAddComponent */] }
                                    ]
                                },
                                {
                                    path: 'change',
                                    children: [
                                        {
                                            path: 'list', children: [
                                                { 'path': '', component: __WEBPACK_IMPORTED_MODULE_54__claim_change_change_manage_component__["a" /* ChangeManageComponent */] },
                                                {
                                                    path: 'info/:id',
                                                    children: [
                                                        { path: '', component: __WEBPACK_IMPORTED_MODULE_55__claim_change_change_info_component__["a" /* ChangeInfoComponent */] },
                                                        { path: 'print', component: __WEBPACK_IMPORTED_MODULE_57__claim_change_change_print_component__["a" /* ChangePrintComponent */] }
                                                    ]
                                                }
                                            ]
                                        },
                                        { path: 'add', component: __WEBPACK_IMPORTED_MODULE_56__claim_change_change_add_component__["a" /* ChangeAddComponent */] }
                                    ]
                                }
                            ]
                        },
                        {
                            path: 'expense',
                            children: [
                                { path: 'list', component: __WEBPACK_IMPORTED_MODULE_41__expense_expense_component__["a" /* ExpenseComponent */] },
                                { path: 'add', component: __WEBPACK_IMPORTED_MODULE_42__expense_expense_add_component__["a" /* ExpenseAddComponent */] }
                            ]
                        },
                    ]
                },
                // { path: '404', component: NotFoundComponent },
                { path: '', redirectTo: '/report', pathMatch: 'full' },
            ], { useHash: true })
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_18__auth_guard_service__["a" /* AuthGuardService */],
            __WEBPACK_IMPORTED_MODULE_19__user_guard_service__["a" /* UserGuardService */],
            __WEBPACK_IMPORTED_MODULE_20__service_all_service_service__["a" /* AllServiceService */],
            __WEBPACK_IMPORTED_MODULE_15__shared_hide_menu_service__["a" /* HideMenuService */],
            __WEBPACK_IMPORTED_MODULE_62__shared_hide_sidebar_service__["a" /* HideSidebarService */],
            __WEBPACK_IMPORTED_MODULE_24__sidebar_sidebar_component__["a" /* SidebarComponent */],
            __WEBPACK_IMPORTED_MODULE_7_ng2_bootstrap_component_loader__["a" /* ComponentLoaderFactory */],
            __WEBPACK_IMPORTED_MODULE_16_ng2_bootstrap_positioning__["a" /* PositioningService */],
            __WEBPACK_IMPORTED_MODULE_17_ng2_bootstrap_progressbar__["a" /* ProgressbarConfig */],
            __WEBPACK_IMPORTED_MODULE_21__service_toast_service__["a" /* ToastService */]
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_22__app_component__["a" /* AppComponent */]]
    }), 
    __metadata('design:paramtypes', [])
], AppModule);
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/app.module.js.map

/***/ }),

/***/ 844:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__shared_hide_sidebar_service__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service_all_service_service__ = __webpack_require__(4);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthGuardService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




let AuthGuardService = class AuthGuardService {
    constructor(router, allService, hideSidebarService) {
        this.router = router;
        this.allService = allService;
        this.hideSidebarService = hideSidebarService;
    }
    canActivate() {
        if (this.allService.getToken()) {
            this.isHideSidebar = false;
            return true;
        }
        else {
            this.isHideSidebar = true;
            this.router.navigate(['/login']);
        }
        this.hideSidebarService.next(this.isHideSidebar);
    }
};
AuthGuardService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(), 
    __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__service_all_service_service__["a" /* AllServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__service_all_service_service__["a" /* AllServiceService */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__shared_hide_sidebar_service__["a" /* HideSidebarService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__shared_hide_sidebar_service__["a" /* HideSidebarService */]) === 'function' && _c) || Object])
], AuthGuardService);
var _a, _b, _c;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/auth-guard.service.js.map

/***/ }),

/***/ 845:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_bootstrap__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_debounceTime__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_debounceTime___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_debounceTime__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_distinctUntilChanged__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_distinctUntilChanged___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_distinctUntilChanged__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_add_observable_of__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_add_observable_of___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_rxjs_add_observable_of__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_moment__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChangeAddComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};











let ChangeAddComponent = class ChangeAddComponent {
    constructor(fb, allService, toastr, vcr, router) {
        this.fb = fb;
        this.allService = allService;
        this.toastr = toastr;
        this.vcr = vcr;
        this.router = router;
        this.Heading = 'เพิ่มรายการเปลี่ยนสินค้า';
        this.isLoading = false;
        //Dynamic Form
        this.brokens = [{ 'message': '' }];
        this.asyncSelected = '';
        this.typeaheadLoading = false;
        //date
        this.selDate = { year: 0, month: 0, day: 0 };
        this.myDatePickerOptions = {
            // other options...
            dateFormat: 'dd/mm/yyyy',
            firstDayOfWeek: 'su',
            selectionTxtFontSize: '16px',
            editableDateField: false,
            openSelectorOnInputClick: true
        };
        this.toastr.setRootViewContainerRef(vcr);
        //form wizard
        this.formName = [
            {
                'name': 'ข้อมูลทั่วไป',
                'path': '#tab-1-info',
                'icon': 'fa-info',
                'desc': ''
            },
            {
                'name': 'สินค้า',
                'path': '#tab-2-product-broken',
                'icon': 'fa-cube',
                'desc': ''
            },
            {
                'name': 'อาการ',
                'path': '#tab-3-detail',
                'icon': 'fa-exclamation-triangle',
                'desc': ''
            },
            {
                'name': 'ข้อมูลพนักงาน',
                'path': '#tab-4-employee',
                'icon': 'fa-user',
                'desc': ''
            }
        ];
        this.isFirstTab = true;
        this.isLastTab = false;
        this.selectedTab = 1;
        //Progressbar
        this.progress = 20;
        //data
        this.customerSelected = {
            item: {
                'name': '',
                'tel': ''
            }
        };
        this.product_source = {
            code: '',
            name: ''
        };
        this.product_target = {
            code: '',
            name: ''
        };
        this.typeaheadNoResults = {
            'customer': false,
            'product_source': false,
            'product_target': false
        };
        //FormControl
        this.tel = fb.control('', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].required]));
        this.exchange_date = fb.control('', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].required]));
        this.source_product_id = fb.control('', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].required]));
        this.target_product_id = fb.control('', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].required]));
        this.infoForm = fb.group({
            'tel': this.tel,
            'customer_id': this.customer_id,
            'exchange_date': this.exchange_date,
        });
        this.productForm = fb.group({
            'source_product_id': this.source_product_id,
            'target_product_id': this.target_product_id,
            'shop_id': this.shop_id,
        });
        this.causeForm = fb.group({
            'cause_detail': this.cause_detail,
            'remark': this.remark,
        });
        this.employeeForm = fb.group({
            'staff_name': this.staff_name,
        });
        //validate FormBuilder Modal add cistomer
        this.name_customer = fb.control('', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].required]));
        this.tel_customer = fb.control('', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].required]));
        this.customerForm = fb.group({
            'name_customer': this.name_customer,
            'tel_customer': this.tel_customer
        });
    }
    ngOnInit() {
        this.setInit();
    }
    setInit() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = true;
            yield this.getCustomerData();
            yield this.getProductData();
            yield this.setCurrentDate();
            this.isLoading = false;
        });
    }
    //--------------------------------------- get Customer Data ---------------------------------------//
    getCustomerData() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let res_customer = yield this.allService.get('customer');
                console.log('res_customer', res_customer);
                this.customers = res_customer.data;
            }
            catch (error) {
                console.log('error get customer', error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.getCustomerData();
                }
                else {
                    this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
                }
            }
        });
    }
    //--------------------------------------- get Product Data ---------------------------------------//
    getProductData() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let res_product = yield this.allService.get('product');
                console.log('res_product', res_product);
                this.products = res_product.data;
            }
            catch (error) {
                console.log('error get product', error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.getProductData();
                }
                else {
                    this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
                }
            }
        });
    }
    //--------------------------------------- Date ---------------------------------------//
    setCurrentDate() {
        console.log('current date', __WEBPACK_IMPORTED_MODULE_10_moment__().format());
        this.date = {
            'year': parseInt(__WEBPACK_IMPORTED_MODULE_10_moment__().format('YYYY')),
            'month': parseInt(__WEBPACK_IMPORTED_MODULE_10_moment__().format('MM')),
            'day': parseInt(__WEBPACK_IMPORTED_MODULE_10_moment__().format('DD')),
        };
        this.selDate = this.date;
    }
    setFormatDate(data) {
        if (typeof data === 'object') {
            return data.year + '-' + data.month + '-' + data.day;
        }
        else if (typeof data === 'string') {
            return data;
        }
    }
    onDateChanged(event) {
        console.log('IMyDateModel', event);
        this.date = event.date;
        console.log('IMyDateModel', this.date);
        console.log('FormControl', this.infoForm.value.exchange_date);
    }
    //--------------------------------------- Progressbar ---------------------------------------//
    setProgress(status) {
        if (status === 1) {
            this.isFirstTab = true;
            this.isLastTab = false;
        }
        else if (status === 4) {
            this.isFirstTab = false;
            this.isLastTab = true;
        }
        else {
            this.isFirstTab = false;
            this.isLastTab = false;
        }
        return this.progress = status * 25;
    }
    //--------------------------------------- TAB ---------------------------------------//
    setTab(tab, action) {
        if (action === 'previous') {
            console.log('previous');
            this.selectedTab = this.selectedTab - 1;
            this.setProgress(this.selectedTab);
        }
        else if (action === 'next') {
            console.log('next');
            this.selectedTab = this.selectedTab + 1;
            this.setProgress(this.selectedTab);
        }
        else {
            this.selectedTab = tab;
            this.setProgress(this.selectedTab);
        }
    }
    //--------------------------------------- Typeahead ---------------------------------------//
    changeTypeaheadLoading(e) {
        this.typeaheadLoading = e;
    }
    changeTypeaheadNoResults(status, e) {
        switch (status) {
            case 0:
                this.typeaheadNoResults.customer = e;
                break;
            case 1:
                this.typeaheadNoResults.product_source = e;
                break;
            case 2:
                this.typeaheadNoResults.product_target = e;
                break;
        }
    }
    typeaheadOnSelect(status, e) {
        switch (status) {
            case 0:
                this.customerSelected = e.item;
                console.log('Selected value: ', e);
                break;
            case 1:
                this.product_source = e.item;
                console.log('Selected value:', e);
                break;
            case 2:
                this.product_target = e.item;
                console.log('Selected value:', e);
                break;
        }
    }
    //--------------------------------------- Modal ---------------------------------------//
    openAddCustomerModal() {
        this.editModal.show();
    }
    saveCustomer() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('save data', this.customerForm.value);
            this.isLoading = true;
            let customer_data = {
                'name': this.customerForm.value.name_customer,
                'tel': this.customerForm.value.tel_customer,
            };
            try {
                let res_edit_customer = yield this.allService.post('customer', customer_data);
                this.showSuccess('แก้ไขข้อมูลเรียบร้อย');
                this.customerForm.reset();
                yield this.getCustomerData();
                this.isLoading = false;
            }
            catch (error) {
                console.log('error add customer', error);
                if (error.status === 500) {
                    this.showError('ไม่สามารถเพิ่มลูกค้าได้ กรุณาลองใหม่อีกครั้ง');
                }
                else if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.saveCustomer();
                }
                else {
                    this.showError('กรุณาลองใหม่อีกครั้ง');
                    this.isLoading = false;
                }
            }
            this.editModal.hide();
        });
    }
    // --------------------------------------- Save Data ---------------------------------------//
    saveData() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = true;
            console.log('save data infoForm', this.infoForm.value);
            console.log('save data productForm', this.productForm.value);
            console.log('save data causeForm', this.causeForm.value);
            console.log('save data brokens', this.brokens);
            console.log('save data employeeForm', this.employeeForm.value);
            console.log('localstorage', this.allService.getLocal('user'));
            let user = this.allService.getLocal('user');
            let changeData = {
                tel: this.infoForm.value.tel,
                customer_id: this.customerSelected.id,
                exchange_date: this.setFormatDate(this.date.date),
                source_product_id: this.product_source.id,
                target_product_id: this.product_target.id,
                shop_id: user.shop_id,
                user_id: user.id,
                cause_detail: JSON.stringify(this.brokens),
                remark: this.causeForm.value.remark,
                staff_name: this.employeeForm.value.staff_name,
            };
            console.log('changeData', changeData);
            try {
                let res_change_product = yield this.allService.post('exchangeTransection', changeData);
                this.showSuccess('เพิ่มข้อมูลเรียบร้อย');
                this.infoForm.reset;
                this.productForm.reset;
                this.causeForm.reset;
                this.employeeForm.reset;
                console.log('res_change_product', res_change_product);
                this.showDetail(res_change_product.data.id);
                this.isLoading = false;
            }
            catch (error) {
                console.log('error add customer', error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.saveData();
                }
                else if (error.status === 500) {
                    this.showError('ไม่สามารถเพิ่มได้ กรุณาลองใหม่อีกครั้ง');
                    this.isLoading = false;
                }
                else {
                    this.showError('กรุณาลองใหม่อีกครั้ง');
                    this.isLoading = false;
                }
            }
        });
    }
    // --------------------------------------- Toast ---------------------------------------//
    showSuccess(message) {
        this.toastr.success(message, 'สำเร็จ!');
    }
    showError(message) {
        this.toastr.error(message, 'เกิดข้อผิดพลาด');
    }
    //--------------------------------------- Dynamic Form ---------------------------------------//
    addBrokens() {
        this.brokens.push({ 'message': '' });
    }
    deleteBrokens(index) {
        if (this.brokens.length > 1) {
            this.brokens.splice(index, 1);
        }
        else {
            this.brokens = [{ 'message': '' }];
        }
    }
    //--------------------------------------- Detail ---------------------------------------//
    showDetail(id) {
        console.log('id', id);
        this.router.navigate(['claim/fix/list/info', id]);
    }
};
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3__angular_core__["ViewChild"])('editModal'), 
    __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_4_ng2_bootstrap__["f" /* ModalDirective */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4_ng2_bootstrap__["f" /* ModalDirective */]) === 'function' && _a) || Object)
], ChangeAddComponent.prototype, "editModal", void 0);
ChangeAddComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3__angular_core__["Component"])({
        selector: 'app-change-add',
        template: __webpack_require__(1213),
        styles: [__webpack_require__(1172)]
    }), 
    __metadata('design:paramtypes', [(typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_5__angular_forms__["FormBuilder"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_5__angular_forms__["FormBuilder"]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__["a" /* AllServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__["a" /* AllServiceService */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr__["ToastsManager"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr__["ToastsManager"]) === 'function' && _d) || Object, (typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__angular_core__["ViewContainerRef"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__angular_core__["ViewContainerRef"]) === 'function' && _e) || Object, (typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_0__angular_router__["b" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_router__["b" /* Router */]) === 'function' && _f) || Object])
], ChangeAddComponent);
var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/change-add.component.js.map

/***/ }),

/***/ 846:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ng2_toastr_ng2_toastr__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_ng2_toastr_ng2_toastr__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__service_all_service_service__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_bootstrap__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_Observable__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_map__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_debounceTime__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_debounceTime___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_debounceTime__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_add_operator_distinctUntilChanged__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_add_operator_distinctUntilChanged___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_rxjs_add_operator_distinctUntilChanged__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_rxjs_add_observable_of__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_rxjs_add_observable_of___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_rxjs_add_observable_of__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChangeInfoComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};











let ChangeInfoComponent = class ChangeInfoComponent {
    constructor(router, route, allService, vcr, toastr) {
        this.router = router;
        this.route = route;
        this.allService = allService;
        this.vcr = vcr;
        this.toastr = toastr;
        this.Heading = 'ข้อมูลการเปลี่ยนสินค้า';
        this.statusText = [
            {
                'id': '1',
                'message': 'รับเรื่อง',
                'class': 'text-blue'
            },
            {
                'id': '2',
                'message': 'กำลังเปลี่ยน',
                'class': 'text-yellow'
            },
            {
                'id': '3',
                'message': 'สำเร็จ',
                'class': 'text-green'
            },
            {
                'id': '4',
                'message': 'ข้อผิดพลาด',
                'class': 'text-red'
            }
        ];
        this.productSelected = '';
        this.asyncSelected = '';
        this.typeaheadLoading = false;
        this.typeaheadNoResults = false;
        this.toastr.setRootViewContainerRef(vcr);
        router.events.subscribe((url) => {
            this.rootUrl = url;
        });
    }
    ngOnInit() {
        this.subGetParams = this.route.params.subscribe(params => {
            this.change_id = params['id'];
        });
        console.log('this.change_id', this.change_id);
        this.setInitEdit();
        this.setInitData();
        this.getAllData();
    }
    setInitData() {
        this.data = {
            'code': '',
            'exchange_date': '',
            'cause_detail': '',
            'broken': [],
            'customer_id': '',
            'HideMenuServiceremark': '',
            'shop_id': '',
            'source_product_id': '',
            'source_product': {},
            'staff_name': '',
            'target_product_id': '',
            'target_product': {},
            'tel': '',
            'user_id': '',
            'status': ''
        };
    }
    setInitEdit() {
        this.edit = {
            'info': false,
            'status': false,
            'source_product': false,
            'target_product': false,
            'broken': false,
            'remark': false
        };
        this.customerSelected = {
            'name': '',
            'id': '',
            'tel': ''
        };
    }
    cancleEdit() {
        console.log('data_original', this.data_original);
        this.data = Object.assign({}, this.data_original);
        this.setInitEdit();
    }
    //--------------------------------------- get data ---------------------------------------//
    getAllData() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = true;
            yield this.loadDataToEdit();
            yield this.loadClaimData();
            this.isLoading = false;
        });
    }
    loadClaimData() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let resp_exchange = yield this.allService.getWithId('exchangeTransection', this.change_id);
                console.log('exchangeTransection', resp_exchange);
                this.data_original = resp_exchange.data;
                this.data_original.broken = JSON.parse(resp_exchange.data.cause_detail);
                this.data_original.exchange_date = this.setFormatDate(resp_exchange.data.exchange_date);
                this.customerSelected = this.data.customer;
                this.data_original.dateFormat = this.setObjectDate(this.data_original.exchange_date);
                this.data = Object.assign({}, this.data_original);
                console.log('this.data', this.data);
            }
            catch (error) {
                console.log('error get exchange', error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.loadClaimData();
                }
                else {
                    this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
                }
            }
        });
    }
    loadDataToEdit() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let resp_customer = yield this.allService.get('customer');
                let resp_product = yield this.allService.get('product');
                this.customers = resp_customer.data;
            }
            catch (error) {
                console.log('error get loadDataToEdit', error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.loadDataToEdit();
                }
                else {
                    this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
                }
            }
        });
    }
    //--------------------------------------- Date Format ---------------------------------------//
    setFormatDate(data) {
        if (typeof data === 'object') {
            return data.year + '-' + data.month + '-' + data.day;
        }
        else if (typeof data === 'string') {
            return data;
        }
    }
    setObjectDate(data) {
        let date = {
            'year': parseInt(__WEBPACK_IMPORTED_MODULE_5_moment__(data).format('YYYY')),
            'month': parseInt(__WEBPACK_IMPORTED_MODULE_5_moment__(data).format('MM')),
            'day': parseInt(__WEBPACK_IMPORTED_MODULE_5_moment__(data).format('DD')),
        };
        console.log('date', date);
        return date;
    }
    //--------------------------------------- Dynamic Form ---------------------------------------//
    openPagePrint() {
        let gotourl = this.rootUrl.url + '/print';
        console.log('openPagePrint', this.data);
        // this.router.navigate([gotourl], { relativeTo: this.route });
        let navigationExtras;
        navigationExtras = {
            queryParams: {
                "id": this.change_id
            }
        };
        window.open('/#' + gotourl + '?id=' + this.change_id, "_blank");
        // this.router.navigate([gotourl], navigationExtras);
    }
    //--------------------------------------- Dynamic Form ---------------------------------------//
    addBrokens() {
        this.data.broken.push({ 'message': '' });
    }
    //--------------------------------------- Typeahead ---------------------------------------//
    getStatesAsObservable(token) {
        let query = new RegExp(token, 'ig');
        return __WEBPACK_IMPORTED_MODULE_6_rxjs_Observable__["Observable"].of(this.customers.filter((state) => {
            return query.test(state.name);
        }));
    }
    changeTypeaheadLoading(e) {
        this.typeaheadLoading = e;
    }
    changeTypeaheadNoResults(e) {
        this.typeaheadNoResults = e;
    }
    typeaheadOnSelect(status, e) {
        switch (status) {
            case 0:
                this.customerSelected = e.item;
                console.log('Selected value: ', e);
                break;
            case 1:
                this.productSelected = e.item;
                console.log('Selected value: ', e);
                break;
        }
    }
    //--------------------------------------- Edit Data ---------------------------------------//
    editData() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = true;
            try {
                if (this.customerSelected) {
                    this.data.customer_id = this.customerSelected.id;
                }
                console.log('this.data edit', this.data);
                this.data.cause_detail = JSON.stringify(this.data.broken);
                let resp_edit_data = yield this.allService.putWithId('exchangeTransection', this.data.id, this.data);
                console.log('resp_edit_data', resp_edit_data);
                this.setInitEdit();
                this.getAllData();
            }
            catch (error) {
                console.log('error edit data', error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.editData();
                }
                else {
                    this.showError('ไม่สามารถแก้ไขข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
                }
            }
        });
    }
    //--------------------------------------- Open Modal ---------------------------------------//
    openDeleteModal(item) {
        this.deleteModal.show();
    }
    deleteItem() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = true;
            try {
                let resp_delete_exchange = yield this.allService.deleteWithId('exchangeTransection', this.data.id);
                console.log('Delete data user : ', resp_delete_exchange.data);
                this.showSuccess('ลบข้อมูลเรียบร้อย');
                this.isLoading = false;
                this.router.navigate(['/claim/change/list']);
            }
            catch (error) {
                console.log("Error delete data user!", error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.deleteItem();
                }
                else {
                    this.showError('ไม่สามารถลบข้อมูลได้');
                    this.isLoading = false;
                }
            }
            this.deleteModal.hide();
        });
    }
    // --------------------------------------- Toast ---------------------------------------//
    showSuccess(message) {
        this.toastr.success(message, 'สำเร็จ!');
    }
    showError(message) {
        this.toastr.error(message, 'เกิดข้อผิดพลาด');
    }
};
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["ViewChild"])('deleteModal'), 
    __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_4_ng2_bootstrap__["f" /* ModalDirective */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4_ng2_bootstrap__["f" /* ModalDirective */]) === 'function' && _a) || Object)
], ChangeInfoComponent.prototype, "deleteModal", void 0);
ChangeInfoComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
        selector: 'app-change-info',
        template: __webpack_require__(1214),
        styles: [__webpack_require__(1173)]
    }), 
    __metadata('design:paramtypes', [(typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* ActivatedRoute */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* ActivatedRoute */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__service_all_service_service__["a" /* AllServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__service_all_service_service__["a" /* AllServiceService */]) === 'function' && _d) || Object, (typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2__angular_core__["ViewContainerRef"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__angular_core__["ViewContainerRef"]) === 'function' && _e) || Object, (typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_0_ng2_toastr_ng2_toastr__["ToastsManager"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0_ng2_toastr_ng2_toastr__["ToastsManager"]) === 'function' && _f) || Object])
], ChangeInfoComponent);
var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/change-info.component.js.map

/***/ }),

/***/ 847:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_bootstrap__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_all_service_service__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_toastr_ng2_toastr__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_ng2_toastr_ng2_toastr__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_moment__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChangeManageComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};







let ChangeManageComponent = class ChangeManageComponent {
    constructor(fb, allService, toastr, vcr, router) {
        this.fb = fb;
        this.allService = allService;
        this.toastr = toastr;
        this.vcr = vcr;
        this.router = router;
        this.Heading = 'การเปลี่ยนสินค้า';
        this.isLoading = false;
        //data
        this.data = null;
        //table
        this.dtOptions = {};
        this.myDateRangePickerOptions = {
            dateFormat: 'dd/mm/yyyy',
            firstDayOfWeek: 'su'
        };
        this.dateRange = {
            beginDate: { year: '', month: '', day: '' },
            endDate: { year: '', month: '', day: '' }
        };
        this.toastr.setRootViewContainerRef(vcr);
    }
    ngOnInit() {
        this.dtOptions = {
            displayLength: 10,
            paginationType: 'simple_numbers',
            language: {
                info: 'แสดง _START_ ถึง _END_ จาก _TOTAL_',
                paginate: {
                    previous: '<',
                    next: '>',
                },
                search: 'ค้นหา',
                emptyTable: 'ไม่มีข้อมูล',
                infoEmpty: 'แสดง 0 ถึง 0 จาก 0',
                zeroRecords: 'ไม่มีข้อมูล',
                lengthMenu: 'แสดง _MENU_ แถว'
            }
        };
        this.initData();
        this.loadData();
    }
    initData() {
        this.dateRange = {
            beginDate: { year: __WEBPACK_IMPORTED_MODULE_6_moment__().year(), month: __WEBPACK_IMPORTED_MODULE_6_moment__().month() + 1, day: __WEBPACK_IMPORTED_MODULE_6_moment__().date() },
            endDate: { year: __WEBPACK_IMPORTED_MODULE_6_moment__().year(), month: __WEBPACK_IMPORTED_MODULE_6_moment__().month() + 1, day: __WEBPACK_IMPORTED_MODULE_6_moment__().date() }
        };
        this.dateSelect = {
            'date_from': this.setFormatDate(__WEBPACK_IMPORTED_MODULE_6_moment__().format('YYYY-MM-DD')),
            'date_to': this.setFormatDate(__WEBPACK_IMPORTED_MODULE_6_moment__().format('YYYY-MM-DD'))
        };
    }
    loadData() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = true;
            try {
                let resp_exchange = yield this.allService.get('exchangeTransection', this.dateSelect);
                console.log('exchangeTransection', resp_exchange);
                this.data = resp_exchange.data;
                this.isLoading = false;
            }
            catch (error) {
                console.log('error get exchange', error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.loadData();
                }
                else {
                    this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
                    this.isLoading = false;
                }
            }
        });
    }
    //--------------------------------------- Open Modal ---------------------------------------//
    openDeleteModal(item) {
        this.selectedItem = item;
        this.deleteModal.show();
    }
    deleteItem() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('delete data', this.selectedItem);
            this.isLoading = !this.isLoading;
            try {
                let resp_delete_exchange = yield this.allService.deleteWithId('exchangeTransection', this.selectedItem.id);
                console.log('Delete data user : ', resp_delete_exchange.data);
                this.showSuccess('ลบข้อมูลเรียบร้อย');
                this.loadData();
            }
            catch (error) {
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.deleteItem();
                }
                else {
                    this.showError('ไม่สามารถลบข้อมูลได้');
                    this.isLoading = false;
                }
            }
            this.deleteModal.hide();
        });
    }
    //--------------------------------------- Detail ---------------------------------------//
    showDetail(item) {
        console.log('item', item);
        this.router.navigate(['claim/change/list/info', item.id]);
    }
    // --------------------------------------- Toast ---------------------------------------//
    showSuccess(message) {
        this.toastr.success(message, 'สำเร็จ!');
    }
    showError(message) {
        this.toastr.error(message, 'เกิดข้อผิดพลาด');
    }
    // --------------------------------------- date ---------------------------------------//
    onDateRangeChanged(event) {
        this.dateSelect = {
            'date_from': this.setFormatDate(event.beginDate),
            'date_to': this.setFormatDate(event.endDate)
        };
        this.loadData();
    }
    setFormatDate(data) {
        if (typeof data === 'object') {
            return data.year + '-' + data.month + '-' + data.day;
        }
        else if (typeof data === 'string') {
            return data;
        }
    }
};
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["ViewChild"])('deleteModal'), 
    __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3_ng2_bootstrap__["f" /* ModalDirective */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3_ng2_bootstrap__["f" /* ModalDirective */]) === 'function' && _a) || Object)
], ChangeManageComponent.prototype, "deleteModal", void 0);
ChangeManageComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
        selector: 'app-change-manage',
        template: __webpack_require__(1215),
        styles: [__webpack_require__(1174)]
    }), 
    __metadata('design:paramtypes', [(typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4__service_all_service_service__["a" /* AllServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4__service_all_service_service__["a" /* AllServiceService */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_5_ng2_toastr_ng2_toastr__["ToastsManager"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_5_ng2_toastr_ng2_toastr__["ToastsManager"]) === 'function' && _d) || Object, (typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2__angular_core__["ViewContainerRef"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__angular_core__["ViewContainerRef"]) === 'function' && _e) || Object, (typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_0__angular_router__["b" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_router__["b" /* Router */]) === 'function' && _f) || Object])
], ChangeManageComponent);
var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/change-manage.component.js.map

/***/ }),

/***/ 848:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ng2_toastr_ng2_toastr__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_ng2_toastr_ng2_toastr__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__service_all_service_service__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_moment__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChangePrintComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};





let ChangePrintComponent = class ChangePrintComponent {
    constructor(router, route, allService, toastr, vcr) {
        this.router = router;
        this.route = route;
        this.allService = allService;
        this.toastr = toastr;
        this.vcr = vcr;
        this.Heading = "พิมพ์ข้อมูลใบเปลี่ยนสินค้า";
        this.toastr.setRootViewContainerRef(vcr);
        // router.events.subscribe((url: any) => {
        //   console.log('url', url.url);
        //   this.rootUrl = url.url.split('/');
        //   console.log('url.url.split()', this.rootUrl);
        //   if (parseInt(this.rootUrl[5])) {
        //     this.change_id = this.rootUrl[5];
        //     this.getData();
        //   }
        // });
        route.queryParams.subscribe(params => {
            this.change_id = params["id"];
            console.log(this.change_id);
            this.getData();
        });
    }
    ngOnInit() {
    }
    getData() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = true;
            try {
                let user = yield this.allService.getLocal('user');
                let resp_store = yield this.allService.getWithId('shop', user.shop_id);
                let resp_exchange = yield this.allService.getWithId('exchangeTransection', this.change_id);
                console.log('exchangeTransection', resp_exchange);
                this.store = resp_store.data;
                this.data = resp_exchange.data;
                this.data.exchange_date = this.setFormatDate(resp_exchange.data.exchange_date);
                this.data.status = '1';
                this.data.broken = JSON.parse(resp_exchange.data.cause_detail);
                // this.customerSelected = this.data.customer_id;
                this.data.dateFormat = this.setObjectDate(this.data.exchange_date);
                console.log('this.data', this.data);
                this.data.source_product_block = this.makeImei(this.data.source_product.code);
                this.data.target_product_block = this.makeImei(this.data.target_product.code);
                this.isLoading = false;
            }
            catch (error) {
                console.log('error get exchange', error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.getData();
                }
                else {
                    this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
                    this.isLoading = false;
                }
            }
        });
    }
    makeImei(data) {
        let result = [];
        for (var i = 0; i < data.length; i++) {
            result.push(data.slice(i, i + 1));
        }
        return result;
    }
    //--------------------------------------- Date Format ---------------------------------------//
    setFormatDate(data) {
        if (typeof data === 'object') {
            return data.year + '-' + data.month + '-' + data.day;
        }
        else if (typeof data === 'string') {
            return data;
        }
    }
    setObjectDate(data) {
        let date = __WEBPACK_IMPORTED_MODULE_4_moment__(data);
        return {
            'day': date.format('DD'),
            'month': date.format('MM'),
            'year': date.format('YYYY')
        };
    }
    print() {
        let printContents, popupWin;
        printContents = document.getElementById('print-section').innerHTML;
        // printContents = this.printSection;
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open();
        popupWin.document.write(`
      <html>
        <head>
          <link href="https://fonts.googleapis.com/css?familyTrirong" rel="stylesheet">
          <link href="https://fonts.googleapis.com/css?family=Nunito+Sans" rel="stylesheet">
          <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

          <style>
            @page {
              size:  auto;   /* auto is the initial value */
              margin: 5mm;  /* this affects the margin in the printer settings */
            }
            html, body {
              font-family: 'Nunito Sans', 'Trirong', 'FontAwesome';
              height:98%;
              width:1000px;
              background: #FFF; 
              font-size: 16pt;
            }

            ol{
              margin-bottom:0px;
            }

            .panel-body {
              padding: 30px;
            }

            .panel-body .claimPrint-title {
              text-align: center;
            }

            .panel-body .claimPrint-title h2 {
              margin-top: 0;
              margin-bottom: 0;
              font-size: 25px;
            }

            .panel-body .claimPrint-title p {
              margin: 0;
            }

            .panel-body .logo {
              font-family: 'Oswald';
              font-weight: bold;
              margin-top: 0;
            }

            .panel-body hr {
              margin-bottom: 10px;
              margin-top: 0px;
              margin-left: -20px;
              margin-right: -20px;
            }

            .panel-body .table {
              table-layout: fixed;
              width: 100%;
              max-width: 100%;
              margin: 0px;
            }

            .panel-body .table td {
              border-top: none;
              padding: 8px;
              line-height: 1.42857143;
              vertical-align: top;
              font-size: 16px;
            }

            .panel-body .table td .header {
              margin: 0px;
              font-weight: 600;
            }

            .panel-body .table.table-claim-info {
              margin: 0px;
              margin-top: 18px;
            }

            .panel-body .table.table-claim-info tr td address {
              margin-bottom: 0px;
              font-style: normal;
              line-height: 1.42857143;
            }

            .panel-body .table.table-product tr td {
              padding: 8px 0px;
            }

            .panel-body .table.table-product tr td .imei {
              padding: 3px 6px;
              border: 1px solid #ddd;
              margin-right: 5px;
            }

            .panel-body .table.table-product tr td .nopadding {
              padding: 0px;
            }

            .panel-body .table.table-product tr .table-list p {
              margin: 10px 0px;
            }

            .panel-body .table.table-product tr .cost {
              padding-top: 30px;
              width: 20%;
            }

            .panel-body .table.table-product tr .cost p {
              margin: 10px 0px;
            }

            .panel-body .table.table-product tr .cost .textextra {
              word-wrap: break-word;
              overflow: hidden;
              text-overflow: ellipsis;
            }

            .panel-body .table.table-product tr .table-option thead tr td {
              text-align: center;
            }

            .panel-body .table.table-product tr .table-option tbody tr td {
              width: 33%;
              line-height: 25px;
            }

            .panel-body .table.table-product tr .table-option tbody tr .hasIcon {
              text-align: center;
            }

            .panel-body .table.table-product tr .table-option tbody tr .hasIcon .icon {
              font-size: 25px;
            }

            .panel-body .table.table-signature .signatureHeader {
              text-align: center;
              font-weight: bolder;
            }

            .panel-body .table.table-signature .signature {
              text-align: center;
              height: 15px;
            }

            .panel-body .table.table-signature .signature td {
              padding: 0px;
            }

            .panel-body .table.table-signature .signature .borderShow {
              border-bottom: 1px solid #ddd;
            }

            .panel-body .table.table-signature .signatureDate {
              text-align: center;
              height: 25px;
            }

            .panel-body .table.table-signature .signatureDate td {
              padding: 0px;
              padding-top: 10px;
              position: relative;
            }

            .panel-body .table.table-signature .signatureDate td p {
              margin: 0px;
              display: flex;
              align-items: center;
              justify-content: flex-end;
              vertical-align: text-bottom;
              position: absolute;
              right: 5px;
              bottom: -5px;
            }

            .panel-body .table.table-signature .signatureDate .borderShow {
              border-bottom: 1px solid #ddd;
            }

            .panel-body .table.table-signature .signatureSpace {
              height: 10px;
            }
          </style>
        </head>
      <body onload="window.print();window.close()">${printContents}</body>
      </html>`);
        setTimeout(function () {
            popupWin.document.close();
        }, 500);
    }
    // --------------------------------------- Toast ---------------------------------------//
    showSuccess(message) {
        this.toastr.success(message, 'สำเร็จ!');
    }
    showError(message) {
        this.toastr.error(message, 'เกิดข้อผิดพลาด');
    }
};
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["ViewChild"])('print-section'), 
    __metadata('design:type', Object)
], ChangePrintComponent.prototype, "printSection", void 0);
ChangePrintComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
        selector: 'app-change-print',
        template: __webpack_require__(1216),
        styles: [__webpack_require__(1175)]
    }), 
    __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* ActivatedRoute */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* ActivatedRoute */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__service_all_service_service__["a" /* AllServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__service_all_service_service__["a" /* AllServiceService */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0_ng2_toastr_ng2_toastr__["ToastsManager"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0_ng2_toastr_ng2_toastr__["ToastsManager"]) === 'function' && _d) || Object, (typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2__angular_core__["ViewContainerRef"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__angular_core__["ViewContainerRef"]) === 'function' && _e) || Object])
], ChangePrintComponent);
var _a, _b, _c, _d, _e;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/change-print.component.js.map

/***/ }),

/***/ 849:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_validation__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_validation___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_ng2_validation__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng2_toastr_ng2_toastr__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ng2_toastr_ng2_toastr__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service_all_service_service__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_bootstrap__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_forms__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_map__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_debounceTime__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_debounceTime___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_debounceTime__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_add_operator_distinctUntilChanged__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_add_operator_distinctUntilChanged___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_rxjs_add_operator_distinctUntilChanged__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_rxjs_add_observable_of__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_rxjs_add_observable_of___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_rxjs_add_observable_of__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_moment__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FixAddComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};












let FixAddComponent = class FixAddComponent {
    constructor(fb, allService, toastr, vcr, router) {
        this.fb = fb;
        this.allService = allService;
        this.toastr = toastr;
        this.vcr = vcr;
        this.router = router;
        this.Heading = 'เพิ่มรายการซ่อมสินค้า';
        //Checkbox
        this.selected = [];
        //Dynamic Form
        this.brokens = [{ 'message': '' }];
        this.fixs = [{ 'message': '' }];
        this.agreements = [
            { 'message': 'ทางร้านขอสงวนสิทธิ์ในความรับผิดชอบต่อความสูญหายของข้อมูลที่จัดเก็บไว้ในเครื่องรวมทั้งอุปกรณ์แต่งของเครื่องดังกล่าวด้วย' },
            { 'message': 'กรณีที่ไม่สามารถติดต่อเสนอราคาได้ภายใน 15 วัน นับตั้งแต่วันที่รับเครื่อง ทางร้านขอสงวนสิทธิ์ในการให้บริการและส่งเครื่องคืนให้ลูกค้า โดยสามารถรับคืนได้ที่สาขาที่เข้ารับบริการ' },
            { 'message': 'กรุณามารับเครื่องคืนภายใน 30 วัน นับจากวันที่ได้รับแจ้งให้มารับคืน มิฉะนั้นทางร้านถือว่าท่านสละสิทธิ์การรับเครื่องคืนและอุปกรณ์ และท่านยินดีให้กรรมสิทธิ์ทั้งหมดตกเป็นของทางร้าน โดยจะไม่ติดใจเรียกร้องใดๆ ทั้งทางแพ่งและทางอาญากับทางร้าน' },
            { 'message': 'ท่านได้ทำการตรวจสอบเครื่องซ่อมและได้ทดลองใช้โทรศัพท์ดังกล่าวแล้วเห็นว่าใช้งานได้ตามปกติและได้ตรวจสอบตำหนิในบริเวณต่างๆของตัวเครื่องไม่พบสิ่งผิดปกติใดๆ จึงยินดีรับเครื่องไปและท่านได้ตรวจสอบรายละเอียดสินค้าที่ส่งซ่อมพบว่าได้รับครบถ้วนตามรายการที่ระบุข้างต้นแล้ว' }
        ];
        this.asyncSelected = '';
        this.typeaheadLoading = false;
        //date
        this.selDate = { year: 0, month: 0, day: 0 };
        this.myDatePickerOptions = {
            // other options...
            dateFormat: 'dd/mm/yyyy',
            firstDayOfWeek: 'su',
            selectionTxtFontSize: '16px',
            editableDateField: false,
            openSelectorOnInputClick: true
        };
        this.toastr.setRootViewContainerRef(vcr);
        //form wizard
        this.formName = [
            {
                'name': 'ข้อมูลทั่วไป',
                'path': '#tab-1-info',
                'icon': 'fa-info',
                'desc': ''
            },
            {
                'name': 'สินค้า',
                'path': '#tab-2-product',
                'icon': 'fa-cube',
                'desc': ''
            },
            {
                'name': 'อาการ',
                'path': '#tab-3-detail',
                'icon': 'fa-exclamation-triangle',
                'desc': ''
            },
            {
                'name': 'ข้อมูลพนักงาน',
                'path': '#tab-4-employee',
                'icon': 'fa-user',
                'desc': ''
            }
        ];
        this.isFirstTab = true;
        this.isLastTab = false;
        this.selectedTab = 1;
        //data
        this.customerSelected = {
            item: {
                'name': '',
                'tel': ''
            }
        };
        this.productSelected = {
            code: '',
            name: ''
        };
        this.typeaheadNoResults = {
            'customer': false,
            'product': false
        };
        //Progressbar
        this.progress = 25;
        this.repair_date = fb.control('', __WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].required]));
        this.customer_id = fb.control('', __WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].required]));
        this.tel = fb.control('', __WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].required]));
        this.item_brand = fb.control('', __WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].required]));
        this.item_color = fb.control('', __WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].required]));
        this.item_blame = fb.control('', __WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].required]));
        this.item_generation = fb.control('', __WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].required]));
        this.item_imei = fb.control('', __WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].required]));
        this.item_has_sim = fb.control('1', __WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].required]));
        this.item_has_mem = fb.control('1', __WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].required]));
        this.item_has_battery = fb.control('1', __WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].required]));
        this.estimate_price = fb.control('0', __WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_1_ng2_validation__["CustomValidators"].number, __WEBPACK_IMPORTED_MODULE_1_ng2_validation__["CustomValidators"].min(0)]));
        this.repair_price = fb.control('0', __WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_1_ng2_validation__["CustomValidators"].number, __WEBPACK_IMPORTED_MODULE_1_ng2_validation__["CustomValidators"].min(0)]));
        this.infoForm = fb.group({
            'repair_date': this.repair_date,
            'customer_id': this.customer_id,
            'tel': this.tel
        });
        this.productForm = fb.group({
            'product_id': this.product_id,
            'item_brand': this.item_brand,
            'item_color': this.item_color,
            'item_blame': this.item_blame,
            'item_generation': this.item_generation,
            'item_imei': this.item_imei,
            'item_has_sim': this.item_has_sim,
            'item_has_mem': this.item_has_mem,
            'item_has_battery': this.item_has_battery
        });
        this.causeForm = fb.group({
            'cause_detail': this.cause_detail,
            'estimate_price': this.estimate_price,
            'cause_remark': this.cause_remark,
            'repair_detail': this.repair_detail,
            'repair_price': this.repair_price,
            'repair_remark': this.repair_remark,
            'agreement_detail': this.agreement_detail,
            'agreement_remark': this.agreement_remark,
        });
        this.employeeForm = fb.group({
            'staff_name': this.staff_name
        });
        //validate FormBuilder Modal add cistomer
        this.name_customer = fb.control('', __WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].required]));
        this.tel_customer = fb.control('', __WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_6__angular_forms__["Validators"].required]));
        this.customerForm = fb.group({
            'name_customer': this.name_customer,
            'tel_customer': this.tel_customer
        });
    }
    ngOnInit() {
        this.setInit();
    }
    setInit() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.getCustomerData();
            yield this.getProductData();
            yield this.setCurrentDate();
        });
    }
    //--------------------------------------- get Customer Data ---------------------------------------//
    getCustomerData() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = true;
            try {
                let res_customer = yield this.allService.get('customer');
                console.log('res_customer', res_customer);
                this.customers = res_customer.data;
                this.isLoading = false;
            }
            catch (error) {
                console.log('error get customer', error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.getCustomerData();
                }
                else {
                    this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
                    this.isLoading = false;
                }
            }
        });
    }
    //--------------------------------------- get Product Data ---------------------------------------//
    getProductData() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = true;
            try {
                let res_product = yield this.allService.get('product');
                console.log('res_product', res_product);
                this.products = res_product.data;
                this.isLoading = false;
            }
            catch (error) {
                console.log('error get product', error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.getProductData();
                }
                else {
                    this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
                    this.isLoading = false;
                }
            }
        });
    }
    //--------------------------------------- Progressbar ---------------------------------------//
    setProgress(status) {
        if (status === 1) {
            this.isFirstTab = true;
            this.isLastTab = false;
        }
        else if (status === 4) {
            this.isFirstTab = false;
            this.isLastTab = true;
        }
        else {
            this.isFirstTab = false;
            this.isLastTab = false;
        }
        return this.progress = status * 25;
    }
    //--------------------------------------- TAB ---------------------------------------//
    setTab(tab, action) {
        console.group('form');
        console.log('info form', this.infoForm.value);
        console.log('product form', this.productForm.value);
        console.log('cause form', this.causeForm.value);
        console.log('employee form', this.employeeForm.value);
        console.groupEnd();
        if (action === 'previous') {
            console.log('previous');
            this.selectedTab = this.selectedTab - 1;
            this.setProgress(this.selectedTab);
        }
        else if (action === 'next') {
            console.log('next');
            this.selectedTab = this.selectedTab + 1;
            this.setProgress(this.selectedTab);
        }
        else {
            this.selectedTab = tab;
            this.setProgress(this.selectedTab);
        }
    }
    //--------------------------------------- Date ---------------------------------------//
    setCurrentDate() {
        console.log('current date', __WEBPACK_IMPORTED_MODULE_11_moment__().format());
        this.date = {
            'year': parseInt(__WEBPACK_IMPORTED_MODULE_11_moment__().format('YYYY')),
            'month': parseInt(__WEBPACK_IMPORTED_MODULE_11_moment__().format('MM')),
            'day': parseInt(__WEBPACK_IMPORTED_MODULE_11_moment__().format('DD')),
        };
        this.selDate = this.date;
    }
    setFormatDate(data) {
        if (typeof data === 'object') {
            return data.year + '-' + data.month + '-' + data.day;
        }
        else if (typeof data === 'string') {
            return data;
        }
    }
    onDateChanged(event) {
        console.log('IMyDateModel', event);
        this.date = event.date;
        console.log('IMyDateModel', this.date);
        console.log('FormControl', this.infoForm.value.exchange_date);
    }
    //--------------------------------------- Typeahead ---------------------------------------//
    changeTypeaheadLoading(e) {
        this.typeaheadLoading = e;
    }
    changeTypeaheadNoResults(status, e) {
        switch (status) {
            case 0:
                this.typeaheadNoResults.customer = e;
                break;
            case 1:
                this.typeaheadNoResults.product = e;
                break;
        }
    }
    typeaheadOnSelect(status, e) {
        switch (status) {
            case 0:
                this.customerSelected = e.item;
                console.log('Selected value: ', e);
                break;
            case 1:
                this.productSelected = e.item;
                console.log('Selected value:', e);
                break;
        }
    }
    //--------------------------------------- Modal ---------------------------------------//
    openAddCustomerModal() {
        this.editModal.show();
    }
    saveCustomer() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('save data', this.customerForm.value);
            let customer_data = {
                'name': this.customerForm.value.name_customer,
                'tel': this.customerForm.value.tel_customer,
            };
            try {
                let res_edit_customer = yield this.allService.post('customer', customer_data);
                this.showSuccess('แก้ไขข้อมูลเรียบร้อย');
                console.log('res_edit_customer', res_edit_customer);
                this.customerForm.reset();
                this.getCustomerData();
                this.customerSelected = res_edit_customer.data;
                this.customerSelected.value = res_edit_customer.data.name;
                console.log('customerSelected', this.customerSelected);
            }
            catch (error) {
                console.log('error add customer', error);
                if (error.status === 500) {
                    this.showError('ไม่สามารถเพิ่มลูกค้าได้ กรุณาลองใหม่อีกครั้ง');
                }
                else if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.saveCustomer();
                }
                else {
                    this.showError('กรุณาลองใหม่อีกครั้ง');
                }
            }
            this.editModal.hide();
        });
    }
    //--------------------------------------- Dynamic Form ---------------------------------------//
    addBrokens() {
        this.brokens.push({ 'message': '' });
    }
    addFixs() {
        this.fixs.push({ 'message': '' });
    }
    addAgreements() {
        this.agreements.push({ 'message': '' });
    }
    deleteBrokens(index) {
        if (this.brokens.length > 1) {
            this.brokens.splice(index, 1);
        }
        else {
            this.brokens = [{ 'message': '' }];
        }
    }
    deleteFixs(index) {
        if (this.fixs.length > 1) {
            this.fixs.splice(index, 1);
        }
        else {
            this.fixs = [{ 'message': '' }];
        }
    }
    deleteAgreements(index) {
        if (this.agreements.length > 1) {
            this.agreements.splice(index, 1);
        }
        else {
            this.agreements = [{ 'message': '' }];
        }
    }
    // --------------------------------------- Toast ---------------------------------------//
    showSuccess(message) {
        this.toastr.success(message, 'สำเร็จ!');
    }
    showError(message) {
        this.toastr.error(message, 'เกิดข้อผิดพลาด');
    }
    // --------------------------------------- Save Data ---------------------------------------//
    saveData() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = true;
            console.log('save data infoForm', this.infoForm.value);
            console.log('save data productForm', this.productForm.value);
            console.log('save data causeForm', this.causeForm.value);
            console.log('save data brokens', this.brokens);
            console.log('save data fixs', this.fixs);
            console.log('save data agreements', this.agreements);
            console.log('save data employeeForm', this.employeeForm.value);
            let changeData = {
                product_id: this.productSelected.id,
                // code: this.infoForm.value.code,
                tel: this.infoForm.value.tel,
                customer_id: this.customerSelected.id,
                repair_date: this.setFormatDate(this.date.date),
                item_brand: this.productForm.value.item_brand,
                item_color: this.productForm.value.item_color,
                item_blame: this.productForm.value.item_blame,
                item_generation: this.productForm.value.item_generation,
                item_imei: this.productForm.value.item_imei,
                item_has_sim: this.productForm.value.item_has_sim.toString(),
                item_has_mem: this.productForm.value.item_has_mem.toString(),
                item_has_battery: this.productForm.value.item_has_battery.toString(),
                cause_detail: JSON.stringify(this.brokens),
                estimate_price: this.causeForm.value.estimate_price,
                cause_remark: this.causeForm.value.cause_remark,
                repair_detail: JSON.stringify(this.fixs),
                repair_price: this.causeForm.value.repair_price,
                repair_remark: this.causeForm.value.repair_remark,
                agreement_detail: JSON.stringify(this.agreements),
                agreement_remark: this.causeForm.value.agreement_remark,
                staff_name: this.employeeForm.value.staff_name
            };
            console.log('changeData', changeData);
            try {
                let res_change_product = yield this.allService.post('repairTransection', changeData);
                this.showSuccess('เพิ่มข้อมูลเรียบร้อย');
                this.infoForm.reset;
                this.productForm.reset;
                this.causeForm.reset;
                this.employeeForm.reset;
                console.log('res_change_product', res_change_product);
                this.showDetail(res_change_product.data.id);
                this.isLoading = false;
            }
            catch (error) {
                console.log('error add customer', error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.saveData();
                }
                else if (error.status === 500) {
                    this.showError(`ไม่สามารถเพิ่มได้ กรุณาลองใหม่อีกครั้ง ${JSON.parse(error._body).message}`);
                    this.isLoading = false;
                }
                else {
                    this.showError(`กรุณาลองใหม่อีกครั้ง ${JSON.parse(error._body).message}`);
                    this.isLoading = false;
                }
            }
        });
    }
    //--------------------------------------- Detail ---------------------------------------//
    showDetail(id) {
        console.log('id', id);
        this.router.navigate(['claim/fix/list/info', id]);
    }
};
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_4__angular_core__["ViewChild"])('editModal'), 
    __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_5_ng2_bootstrap__["f" /* ModalDirective */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_5_ng2_bootstrap__["f" /* ModalDirective */]) === 'function' && _a) || Object)
], FixAddComponent.prototype, "editModal", void 0);
FixAddComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_4__angular_core__["Component"])({
        selector: 'app-fix-add',
        template: __webpack_require__(1217),
        styles: [__webpack_require__(1176)]
    }), 
    __metadata('design:paramtypes', [(typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_6__angular_forms__["FormBuilder"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_6__angular_forms__["FormBuilder"]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__service_all_service_service__["a" /* AllServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__service_all_service_service__["a" /* AllServiceService */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2_ng2_toastr_ng2_toastr__["ToastsManager"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2_ng2_toastr_ng2_toastr__["ToastsManager"]) === 'function' && _d) || Object, (typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__angular_core__["ViewContainerRef"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4__angular_core__["ViewContainerRef"]) === 'function' && _e) || Object, (typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_0__angular_router__["b" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_router__["b" /* Router */]) === 'function' && _f) || Object])
], FixAddComponent);
var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/fix-add.component.js.map

/***/ }),

/***/ 850:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ng2_toastr_ng2_toastr__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_ng2_toastr_ng2_toastr__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__service_all_service_service__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_Observable__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_map__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_debounceTime__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_debounceTime___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_debounceTime__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_add_operator_distinctUntilChanged__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_add_operator_distinctUntilChanged___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_rxjs_add_operator_distinctUntilChanged__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_rxjs_add_observable_of__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_rxjs_add_observable_of___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_rxjs_add_observable_of__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FixInfoComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};











let FixInfoComponent = class FixInfoComponent {
    constructor(fb, router, route, allService, toastr, vcr) {
        this.fb = fb;
        this.router = router;
        this.route = route;
        this.allService = allService;
        this.toastr = toastr;
        this.vcr = vcr;
        this.Heading = 'ข้อมูลการซ่อมสินค้า';
        this.statusText = [
            {
                'id': '1',
                'message': 'รับเรื่อง',
                'class': 'text-blue'
            },
            {
                'id': '2',
                'message': 'กำลังซ่อม',
                'class': 'text-yellow'
            },
            {
                'id': '3',
                'message': 'สำเร็จ',
                'class': 'text-green'
            },
            {
                'id': '4',
                'message': 'ข้อผิดพลาด',
                'class': 'text-red'
            }
        ];
        this.productSelected = '';
        this.asyncSelected = '';
        this.typeaheadLoading = false;
        this.typeaheadNoResults = false;
        this.toastr.setRootViewContainerRef(vcr);
        router.events.subscribe((url) => {
            this.rootUrl = url;
        });
        this.setInitEdit();
        this.productHasItem = this.fb.group({
            'item_has_sim': this.item_has_sim,
            'item_has_mem': this.item_has_mem,
            'item_has_battery': this.item_has_battery,
        });
    }
    ngOnInit() {
        this.subGetParams = this.route.params.subscribe(params => {
            this.repair_id = params['id'];
        });
        console.log('this.repair_id', this.repair_id);
        this.setInitData();
        this.getAllData();
    }
    setInitData() {
        this.data = {
            'code': '',
            'repair_date': '',
            'cause_detail': '',
            'broken': [],
            'customer_id': '',
            'remark': '',
            'shop_id': '',
            'source_product_id': '',
            'source_product': {},
            'staff_name': '',
            'target_product_id': '',
            'target_product': {},
            'tel': '',
            'user_id': '',
            'status': ''
        };
    }
    setInitEdit() {
        this.edit = {
            'info': false,
            'broken': false,
            'fix': false,
            'status': false,
            'product': false,
            'agreement': false,
        };
        this.customerSelected = {
            'name': '',
            'id': '',
            'tel': ''
        };
    }
    setEdit(type) {
        this.edit[type] = !this.edit[type];
    }
    cancleEdit() {
        console.log('data_original', this.data_original);
        this.data = Object.assign({}, this.data_original);
        this.setInitEdit();
    }
    //--------------------------------------- get data ---------------------------------------//
    getAllData() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = true;
            yield this.loadDataToEdit();
            yield this.loadClaimData();
            this.isLoading = false;
        });
    }
    loadClaimData() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let resp_repair = yield this.allService.getWithId('repairTransection', this.repair_id);
                console.log('repairTransection', resp_repair);
                this.data_original = resp_repair.data;
                this.data_original.broken = JSON.parse(this.data_original.cause_detail);
                this.data_original.fix = JSON.parse(this.data_original.repair_detail);
                this.data_original.agreement = JSON.parse(this.data_original.agreement_detail);
                this.data_original.repair_date = this.setFormatDate(this.data_original.repair_date);
                this.data_original.dateFormat = this.setObjectDate(this.data_original.repair_date);
                this.customerSelected = this.data_original.customer;
                console.log('this.data_original', this.data_original.item_has_sim);
                this.data = Object.assign({}, this.data_original);
                console.log('this.data', this.data);
            }
            catch (error) {
                console.log('error get repair', error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.loadClaimData();
                }
                else {
                    this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
                    this.isLoading = false;
                }
            }
        });
    }
    loadDataToEdit() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let resp_customer = yield this.allService.get('customer');
                let resp_product = yield this.allService.get('product');
                this.customers = resp_customer.data;
            }
            catch (error) {
                console.log('error get loadDataToEdit', error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.loadDataToEdit();
                }
                else {
                    this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
                    this.isLoading = false;
                }
            }
        });
    }
    //--------------------------------------- Date Format ---------------------------------------//
    setFormatDate(data) {
        if (typeof data === 'object') {
            return data.year + '-' + data.month + '-' + data.day;
        }
        else if (typeof data === 'string') {
            return data;
        }
    }
    setObjectDate(data) {
        let date = {
            'year': parseInt(__WEBPACK_IMPORTED_MODULE_4_moment__(data).format('YYYY')),
            'month': parseInt(__WEBPACK_IMPORTED_MODULE_4_moment__(data).format('MM')),
            'day': parseInt(__WEBPACK_IMPORTED_MODULE_4_moment__(data).format('DD')),
        };
        console.log('date', date);
        return date;
    }
    //--------------------------------------- Dynamic Form ---------------------------------------//
    openPagePrint() {
        let gotourl = this.rootUrl.url + '/print';
        console.log('openPagePrint', this.data);
        // this.router.navigate([gotourl], { relativeTo: this.route });
        let navigationExtras;
        navigationExtras = {
            queryParams: {
                "id": this.repair_id
            }
        };
        window.open('/#' + gotourl + '?id=' + this.repair_id, "_blank");
        // this.router.navigate([gotourl], navigationExtras);
    }
    //--------------------------------------- Typeahead ---------------------------------------//
    getStatesAsObservable(token) {
        let query = new RegExp(token, 'ig');
        return __WEBPACK_IMPORTED_MODULE_6_rxjs_Observable__["Observable"].of(this.customers.filter((state) => {
            return query.test(state.name);
        }));
    }
    changeTypeaheadLoading(e) {
        this.typeaheadLoading = e;
    }
    changeTypeaheadNoResults(e) {
        this.typeaheadNoResults = e;
    }
    typeaheadOnSelect(status, e) {
        switch (status) {
            case 0:
                this.customerSelected = e.item;
                console.log('Selected value: ', e);
                break;
            case 1:
                this.productSelected = e.item;
                console.log('Selected value: ', e);
                break;
        }
    }
    //--------------------------------------- Edit Data ---------------------------------------//
    editData() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = true;
            try {
                if (this.customerSelected) {
                    this.data.customer_id = this.customerSelected.id;
                }
                this.data.cause_detail = JSON.stringify(this.data.broken);
                this.data.repair_detail = JSON.stringify(this.data.fix);
                this.data.agreement_detail = JSON.stringify(this.data.agreement);
                console.log('this.data edit', this.data);
                let resp_edit_data = yield this.allService.putWithId('repairTransection', this.data.id, this.data);
                console.log('resp_edit_data', resp_edit_data);
                this.setInitEdit();
                this.getAllData();
            }
            catch (error) {
                console.log('error edit data', error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.editData();
                }
            }
        });
    }
    //--------------------------------------- Dynamic Form ---------------------------------------//
    addBrokens() {
        this.data.broken.push({ 'message': '' });
    }
    addFixs() {
        this.data.fix.push({ 'message': '' });
    }
    addAgreements() {
        this.data.agreement.push({ 'message': '' });
    }
    // --------------------------------------- Toast ---------------------------------------//
    showSuccess(message) {
        this.toastr.success(message, 'สำเร็จ!');
    }
    showError(message) {
        this.toastr.error(message, 'เกิดข้อผิดพลาด');
    }
};
FixInfoComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
        selector: 'app-fix-info',
        template: __webpack_require__(1218),
        styles: [__webpack_require__(1177)]
    }), 
    __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_5__angular_forms__["FormBuilder"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_5__angular_forms__["FormBuilder"]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* ActivatedRoute */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* ActivatedRoute */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__service_all_service_service__["a" /* AllServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__service_all_service_service__["a" /* AllServiceService */]) === 'function' && _d) || Object, (typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_0_ng2_toastr_ng2_toastr__["ToastsManager"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0_ng2_toastr_ng2_toastr__["ToastsManager"]) === 'function' && _e) || Object, (typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_2__angular_core__["ViewContainerRef"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__angular_core__["ViewContainerRef"]) === 'function' && _f) || Object])
], FixInfoComponent);
var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/fix-info.component.js.map

/***/ }),

/***/ 851:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ng2_toastr_ng2_toastr__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_ng2_toastr_ng2_toastr__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_bootstrap__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_router__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_moment__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FixManageComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};







let FixManageComponent = class FixManageComponent {
    constructor(fb, allService, toastr, vcr, router) {
        this.fb = fb;
        this.allService = allService;
        this.toastr = toastr;
        this.vcr = vcr;
        this.router = router;
        this.Heading = 'การซ่อมสินค้า';
        this.isLoading = false;
        //data
        this.data = null;
        //table
        this.dtOptions = {};
        this.myDateRangePickerOptions = {
            dateFormat: 'dd/mm/yyyy',
            firstDayOfWeek: 'su'
        };
        this.dateRange = {
            beginDate: { year: '', month: '', day: '' },
            endDate: { year: '', month: '', day: '' }
        };
        this.toastr.setRootViewContainerRef(vcr);
        this.loadData();
    }
    ngOnInit() {
        this.dtOptions = {
            displayLength: 10,
            paginationType: 'simple_numbers',
            language: {
                info: 'แสดง _START_ ถึง _END_ จาก _TOTAL_',
                paginate: {
                    previous: '<',
                    next: '>',
                },
                search: 'ค้นหา',
                emptyTable: 'ไม่มีข้อมูล',
                infoEmpty: 'แสดง 0 ถึง 0 จาก 0',
                zeroRecords: 'ไม่มีข้อมูล',
                lengthMenu: 'แสดง _MENU_ แถว'
            }
        };
        this.initData();
        this.loadData();
    }
    initData() {
        this.dateRange = {
            beginDate: { year: __WEBPACK_IMPORTED_MODULE_6_moment__().year(), month: __WEBPACK_IMPORTED_MODULE_6_moment__().month() + 1, day: __WEBPACK_IMPORTED_MODULE_6_moment__().date() },
            endDate: { year: __WEBPACK_IMPORTED_MODULE_6_moment__().year(), month: __WEBPACK_IMPORTED_MODULE_6_moment__().month() + 1, day: __WEBPACK_IMPORTED_MODULE_6_moment__().date() }
        };
        this.dateSelect = {
            'date_from': this.setFormatDate(__WEBPACK_IMPORTED_MODULE_6_moment__().format('YYYY-MM-DD')),
            'date_to': this.setFormatDate(__WEBPACK_IMPORTED_MODULE_6_moment__().format('YYYY-MM-DD'))
        };
    }
    loadData() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = true;
            try {
                let resp_repair = yield this.allService.get('repairTransection', this.dateSelect);
                console.log('repairTransection', resp_repair);
                this.data = resp_repair.data;
                this.isLoading = false;
            }
            catch (error) {
                console.log('error get exchange', error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.loadData();
                }
                else {
                    this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
                    this.isLoading = false;
                }
            }
        });
    }
    //--------------------------------------- Open Modal ---------------------------------------//
    openDeleteModal(item) {
        this.selectedItem = item;
        this.deleteModal.show();
    }
    deleteItem() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('delete data', this.selectedItem);
            this.isLoading = true;
            try {
                let resp_delete_repair = yield this.allService.deleteWithId('repairTransection', this.selectedItem.id);
                console.log('Delete data user : ', resp_delete_repair.data);
                this.showSuccess('ลบข้อมูลเรียบร้อย');
                this.loadData();
            }
            catch (error) {
                console.log("Error delete data user!", error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.deleteItem();
                }
                else {
                    this.showError('ไม่สามารถลบข้อมูลได้');
                    this.isLoading = false;
                }
            }
            this.deleteModal.hide();
        });
    }
    //--------------------------------------- Detail ---------------------------------------//
    showDetail(item) {
        console.log('item', item);
        this.router.navigate(['claim/fix/list/info', item.id]);
    }
    // --------------------------------------- Toast ---------------------------------------//
    showSuccess(message) {
        this.toastr.success(message, 'สำเร็จ!');
    }
    showError(message) {
        this.toastr.error(message, 'เกิดข้อผิดพลาด');
    }
    // --------------------------------------- date ---------------------------------------//
    onDateRangeChanged(event) {
        this.dateSelect = {
            'date_from': this.setFormatDate(event.beginDate),
            'date_to': this.setFormatDate(event.endDate)
        };
        this.loadData();
    }
    setFormatDate(data) {
        if (typeof data === 'object') {
            return data.year + '-' + data.month + '-' + data.day;
        }
        else if (typeof data === 'string') {
            return data;
        }
    }
};
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3__angular_core__["ViewChild"])('deleteModal'), 
    __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_4_ng2_bootstrap__["f" /* ModalDirective */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4_ng2_bootstrap__["f" /* ModalDirective */]) === 'function' && _a) || Object)
], FixManageComponent.prototype, "deleteModal", void 0);
FixManageComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3__angular_core__["Component"])({
        selector: 'app-fix-manage',
        template: __webpack_require__(1219),
        styles: [__webpack_require__(1178)]
    }), 
    __metadata('design:paramtypes', [(typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__["a" /* AllServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__["a" /* AllServiceService */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0_ng2_toastr_ng2_toastr__["ToastsManager"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0_ng2_toastr_ng2_toastr__["ToastsManager"]) === 'function' && _d) || Object, (typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__angular_core__["ViewContainerRef"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__angular_core__["ViewContainerRef"]) === 'function' && _e) || Object, (typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_5__angular_router__["b" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_5__angular_router__["b" /* Router */]) === 'function' && _f) || Object])
], FixManageComponent);
var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/fix-manage.component.js.map

/***/ }),

/***/ 852:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ng2_toastr_ng2_toastr__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_ng2_toastr_ng2_toastr__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__service_all_service_service__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_moment__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FixPrintComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};





let FixPrintComponent = class FixPrintComponent {
    constructor(router, route, allService, toastr, vcr) {
        this.router = router;
        this.route = route;
        this.allService = allService;
        this.toastr = toastr;
        this.vcr = vcr;
        this.Heading = "พิมพ์ข้อมูลใบซ่อม";
        this.toastr.setRootViewContainerRef(vcr);
        route.queryParams.subscribe(params => {
            this.change_id = params["id"];
            console.log(this.change_id);
            this.getData();
        });
    }
    ngOnInit() {
    }
    getData() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = true;
            try {
                let user = yield this.allService.getLocal('user');
                console.log('user', user);
                let resp_store = yield this.allService.getWithId('shop', user.shop_id);
                let resp_repair = yield this.allService.getWithId('repairTransection', this.change_id);
                console.log('repairTransection', resp_repair);
                this.store = resp_store.data;
                this.data = resp_repair.data;
                this.data.cause = resp_repair.data.cause_detail ? JSON.parse(resp_repair.data.cause_detail) : '';
                this.data.agreement = resp_repair.data.agreement_detail ? JSON.parse(resp_repair.data.agreement_detail) : '';
                this.data.repair = resp_repair.data.repair_detail ? JSON.parse(resp_repair.data.repair_detail) : '';
                this.data.repair_date = this.setFormatDate(resp_repair.data.repair_date);
                // this.customerSelected = this.data.customer_id;
                this.data.dateFormat = this.setObjectDate(this.data.repair_date);
                console.log('this.data', this.data);
                console.log('this.store', this.store);
                this.data.product_block = this.makeImei(this.data.item_imei);
                this.isLoading = false;
            }
            catch (error) {
                console.log('error get repair', error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.getData();
                }
                else {
                    this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
                    this.isLoading = false;
                }
            }
        });
    }
    makeImei(data) {
        let result = [];
        for (var i = 0; i < data.length; i++) {
            result.push(data.slice(i, i + 1));
        }
        return result;
    }
    //--------------------------------------- Date Format ---------------------------------------//
    setFormatDate(data) {
        if (typeof data === 'object') {
            return data.year + '-' + data.month + '-' + data.day;
        }
        else if (typeof data === 'string') {
            return data;
        }
    }
    setObjectDate(data) {
        let date = __WEBPACK_IMPORTED_MODULE_4_moment__(data);
        return {
            'day': date.format('DD'),
            'month': date.format('MM'),
            'year': date.format('YYYY')
        };
    }
    print() {
        let printContents, popupWin;
        printContents = document.getElementById('print-section').innerHTML;
        // printContents = this.printSection;
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open();
        popupWin.document.write(`
      <html>
        <head>
          <link href="https://fonts.googleapis.com/css?familyTrirong" rel="stylesheet">
          <link href="https://fonts.googleapis.com/css?family=Nunito+Sans" rel="stylesheet">
          <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

          <style>
            @page {
              size:  auto;   /* auto is the initial value */
              margin: 10mm;  /* this affects the margin in the printer settings */
            }
            html, body {
              font-family: 'Nunito Sans', 'Trirong', 'FontAwesome';
              height:98%;
              width:1000px;
              background: #FFF; 
              font-size: 16pt;
            }

            ol{
              margin-bottom:0px;
            }

            .panel-body {
              padding: 30px;
            }

            .panel-body .claimPrint-title {
              text-align: center;
            }

            .panel-body .claimPrint-title h2 {
              margin-top: 0;
              margin-bottom: 0;
              font-size: 25px;
            }

            .panel-body .claimPrint-title p {
              margin: 0;
            }

            .panel-body .logo {
              font-family: 'Oswald';
              font-weight: bold;
              margin-top: 0;
            }

            .panel-body hr {
              margin-bottom: 10px;
              margin-top: 0px;
              margin-left: -20px;
              margin-right: -20px;
            }

            .panel-body .table {
              table-layout: fixed;
              width: 100%;
              max-width: 100%;
              margin: 0px;
            }

            .panel-body .table td {
              border-top: none;
              padding: 8px;
              line-height: 1.42857143;
              vertical-align: top;
              font-size: 16px;
            }

            .panel-body .table td .header {
              margin: 0px;
              font-weight: 600;
            }

            .panel-body .table.table-claim-info {
              margin: 0px;
              margin-top: 18px;
            }

            .panel-body .table.table-claim-info tr td address {
              margin-bottom: 0px;
              font-style: normal;
              line-height: 1.42857143;
            }

            .panel-body .table.table-product tr td {
              padding: 8px 0px;
            }

            .panel-body .table.table-product tr td .imei {
              padding: 3px 6px;
              border: 1px solid #ddd;
              margin-right: 5px;
            }

            .panel-body .table.table-product tr td .nopadding {
              padding: 0px;
            }

            .panel-body .table.table-product tr .table-list p {
              margin: 10px 0px;
            }

            .panel-body .table.table-product tr .cost {
              padding-top: 30px;
              width: 20%;
            }

            .panel-body .table.table-product tr .cost p {
              margin: 10px 0px;
            }

            .panel-body .table.table-product tr .cost .textextra {
              word-wrap: break-word;
              overflow: hidden;
              text-overflow: ellipsis;
            }

            .panel-body .table.table-product tr .table-option thead tr td {
              text-align: center;
            }

            .panel-body .table.table-product tr .table-option tbody tr td {
              width: 33%;
              line-height: 25px;
            }

            .panel-body .table.table-product tr .table-option tbody tr .hasIcon {
              text-align: center;
            }

            .panel-body .table.table-product tr .table-option tbody tr .hasIcon .icon {
              font-size: 25px;
            }

            .panel-body .table.table-signature .signatureHeader {
              text-align: center;
              font-weight: bolder;
            }

            .panel-body .table.table-signature .signature {
              text-align: center;
              height: 15px;
            }

            .panel-body .table.table-signature .signature td {
              padding: 0px;
            }

            .panel-body .table.table-signature .signature .borderShow {
              border-bottom: 1px solid #ddd;
            }

            .panel-body .table.table-signature .signatureDate {
              text-align: center;
              height: 25px;
            }

            .panel-body .table.table-signature .signatureDate td {
              padding: 0px;
              padding-top: 10px;
              position: relative;
            }

            .panel-body .table.table-signature .signatureDate td p {
              margin: 0px;
              display: flex;
              align-items: center;
              justify-content: flex-end;
              vertical-align: text-bottom;
              position: absolute;
              right: 5px;
              bottom: -5px;
            }

            .panel-body .table.table-signature .signatureDate .borderShow {
              border-bottom: 1px solid #ddd;
            }

            .panel-body .table.table-signature .signatureSpace {
              height: 10px;
            }

            .panel-body .table.table-product .borderDiv {
                border-bottom: 1px solid #eee;
            }
          </style>
        </head>
      <body onload="setTimeout(function () { window.print(); window.close(); }, 1000)">${printContents}</body>
      </html>`);
        setTimeout(function () {
            popupWin.document.close();
        }, 500);
    }
    // --------------------------------------- Toast ---------------------------------------//
    showSuccess(message) {
        this.toastr.success(message, 'สำเร็จ!');
    }
    showError(message) {
        this.toastr.error(message, 'เกิดข้อผิดพลาด');
    }
};
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["ViewChild"])('print-section'), 
    __metadata('design:type', Object)
], FixPrintComponent.prototype, "printSection", void 0);
FixPrintComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
        selector: 'app-fix-print',
        template: __webpack_require__(1220),
        styles: [__webpack_require__(1179)]
    }), 
    __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* ActivatedRoute */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* ActivatedRoute */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__service_all_service_service__["a" /* AllServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__service_all_service_service__["a" /* AllServiceService */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0_ng2_toastr_ng2_toastr__["ToastsManager"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0_ng2_toastr_ng2_toastr__["ToastsManager"]) === 'function' && _d) || Object, (typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2__angular_core__["ViewContainerRef"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__angular_core__["ViewContainerRef"]) === 'function' && _e) || Object])
], FixPrintComponent);
var _a, _b, _c, _d, _e;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/fix-print.component.js.map

/***/ }),

/***/ 853:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_toastr_ng2_toastr__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ng2_toastr_ng2_toastr__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CustomerAddComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};




let CustomerAddComponent = class CustomerAddComponent {
    constructor(fb, allService, toastr, vcr) {
        this.fb = fb;
        this.allService = allService;
        this.toastr = toastr;
        this.vcr = vcr;
        this.Heading = 'ลูกค้า';
        this.isLoading = false;
        this.toastr.setRootViewContainerRef(vcr);
        //validate FormBuilder
        this.name = fb.control('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required]));
        this.tel = fb.control('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required]));
        this.customerForm = fb.group({
            'name': this.name,
            'tel': this.tel
        });
    }
    ngOnInit() {
        this.setData();
    }
    setData() {
        this.data = {
            'name': '',
            'tel': ''
        };
    }
    sendForm() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('data : ', this.data);
            this.isLoading = true;
            try {
                let res_add_customer = yield this.allService.post('customer', this.customerForm.value);
                this.customerForm.reset();
                this.isLoading = false;
                this.showSuccess(`เพิ่ม ${res_add_customer.data.name} สำเร็จแล้ว`);
            }
            catch (error) {
                console.log("Error create store", error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.sendForm();
                }
                else {
                    this.showError('ไม่สามารถเพิ่มข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
                    this.isLoading = false;
                }
            }
        });
    }
    // --------------------------------------- Toast ---------------------------------------//
    showSuccess(message) {
        this.toastr.success(message, 'สำเร็จ!');
    }
    showError(message) {
        this.toastr.error(message, 'เกิดข้อผิดพลาด');
    }
};
CustomerAddComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-customer-add',
        template: __webpack_require__(1221),
        styles: [__webpack_require__(1180)]
    }), 
    __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__["a" /* AllServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__["a" /* AllServiceService */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3_ng2_toastr_ng2_toastr__["ToastsManager"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3_ng2_toastr_ng2_toastr__["ToastsManager"]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"]) === 'function' && _d) || Object])
], CustomerAddComponent);
var _a, _b, _c, _d;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/customer-add.component.js.map

/***/ }),

/***/ 854:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_hide_menu_service__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_all_service_service__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ng2_toastr_ng2_toastr__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_ng2_toastr_ng2_toastr__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CustomerComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};







let CustomerComponent = class CustomerComponent {
    constructor(router, fb, hideMenuService, allService, toastr, vcr) {
        this.router = router;
        this.fb = fb;
        this.hideMenuService = hideMenuService;
        this.allService = allService;
        this.toastr = toastr;
        this.vcr = vcr;
        this.Heading = 'ลูกค้า';
        this.isLoading = false;
        //data
        this.data = null;
        //table
        this.dtOptions = {};
        this.toastr.setRootViewContainerRef(vcr);
        this.getData();
        this.isHideMenu = true;
        hideMenuService.subscribe({
            next: isHideMenu => {
                this.isHideMenu = isHideMenu;
                return this.isHideMenu;
            }
        });
        //validate FormBuilder
        this.name = fb.control('', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]));
        this.tel = fb.control('', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]));
        this.customerForm = fb.group({
            'name': this.name,
            'tel': this.tel
        });
    }
    ngOnInit() {
        this.initTable();
    }
    initTable() {
        this.dtOptions = {
            displayLength: 10,
            bSort: false,
            paginationType: 'simple_numbers',
            language: {
                info: 'แสดง _START_ ถึง _END_ จาก _TOTAL_',
                paginate: {
                    previous: '<',
                    next: '>',
                },
                search: 'ค้นหา',
                emptyTable: 'ไม่มีข้อมูล',
                infoEmpty: 'แสดง 0 ถึง 0 จาก 0',
                zeroRecords: 'ไม่มีข้อมูล',
                lengthMenu: 'แสดง _MENU_ แถว'
            }
        };
    }
    getData() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = true;
            try {
                let res_customer = yield this.allService.get('customer');
                console.log('res_customer', res_customer);
                this.data = res_customer.data;
                this.isLoading = false;
            }
            catch (error) {
                console.log('error get customer', error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.getData();
                }
                else {
                    this.isLoading = false;
                    this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
                }
            }
        });
    }
    // --------------------------------------- Open Modal ---------------------------------------//
    openEditModal(id) {
        this.selectedItem = this.data.filter(customer => customer.id == id)[0];
        this.editModal.show();
    }
    openDeleteModal(id) {
        this.selectedItem = this.data.filter(customer => customer.id == id)[0];
        this.deleteModal.show();
    }
    saveChange() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('save data', this.customerForm.value);
            this.isLoading = true;
            try {
                let res_edit_customer = yield this.allService.putWithId('customer', this.selectedItem.id, this.customerForm.value);
                this.showSuccess('แก้ไขข้อมูลเรียบร้อย');
                this.getData();
            }
            catch (error) {
                if (error.status === 500) {
                    this.showError('ไม่พบลูกค้า กรุณาลองใหม่อีกครั้ง');
                    this.isLoading = false;
                }
                else if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.saveChange();
                }
                else {
                    this.showError('กรุณาลองใหม่อีกครั้ง');
                    this.isLoading = false;
                }
            }
            this.editModal.hide();
        });
    }
    deleteItem() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('delete data', this.selectedItem);
            this.isLoading = true;
            try {
                let res_delete_customer = yield this.allService.deleteWithId('customer', this.selectedItem.id);
                this.showSuccess('ลบข้อมูลเรียบร้อย');
                this.getData();
            }
            catch (error) {
                console.log("Error delete data customer!", error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.deleteItem();
                }
                else {
                    this.showError('ไม่สามารถลบข้อมูลได้');
                    this.isLoading = false;
                }
            }
            this.deleteModal.hide();
        });
    }
    // --------------------------------------- Toast ---------------------------------------//
    showSuccess(message) {
        this.toastr.success(message, 'สำเร็จ!');
    }
    showError(message) {
        this.toastr.error(message, 'เกิดข้อผิดพลาด');
    }
    onUserRowSelect(event) {
        console.log(event);
    }
};
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('editModal'), 
    __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["f" /* ModalDirective */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["f" /* ModalDirective */]) === 'function' && _a) || Object)
], CustomerComponent.prototype, "editModal", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('deleteModal'), 
    __metadata('design:type', (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["f" /* ModalDirective */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["f" /* ModalDirective */]) === 'function' && _b) || Object)
], CustomerComponent.prototype, "deleteModal", void 0);
CustomerComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-customer',
        template: __webpack_require__(1222),
        styles: [__webpack_require__(1181)]
    }), 
    __metadata('design:paramtypes', [(typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormBuilder"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormBuilder"]) === 'function' && _d) || Object, (typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__shared_hide_menu_service__["a" /* HideMenuService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__shared_hide_menu_service__["a" /* HideMenuService */]) === 'function' && _e) || Object, (typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_5__service_all_service_service__["a" /* AllServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_5__service_all_service_service__["a" /* AllServiceService */]) === 'function' && _f) || Object, (typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_6_ng2_toastr_ng2_toastr__["ToastsManager"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_6_ng2_toastr_ng2_toastr__["ToastsManager"]) === 'function' && _g) || Object, (typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"]) === 'function' && _h) || Object])
], CustomerComponent);
var _a, _b, _c, _d, _e, _f, _g, _h;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/customer.component.js.map

/***/ }),

/***/ 855:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MonthPicker; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

let MonthPicker = class MonthPicker {
    constructor() {
        this.months = [
            { val: '01', name: 'มกราคม' },
            { val: '02', name: 'กุมภาพันธ์' },
            { val: '03', name: 'มีนาคม' },
            { val: '04', name: 'เมษายน' },
            { val: '05', name: 'พฤษภาคม' },
            { val: '06', name: 'มิถุนายน' },
            { val: '07', name: 'กรกฎาคม' },
            { val: '08', name: 'สิงหาคม' },
            { val: '09', name: 'กันยายน' },
            { val: '10', name: 'ตุลาคม' },
            { val: '11', name: 'พฤษจิกายน' },
            { val: '12', name: 'ธันวาคม' }
        ];
    }
    ngOnInit() {
        this.getMonth();
    }
    getMonth() {
        var today = new Date();
        this.mm = today.getMonth() + 1;
        if (this.mm < 10) {
            this.mm = '0' + this.mm;
        }
    }
};
MonthPicker = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'month-picker',
        template: `
    <div>
      <select class="form-control"  required>
        <option  *ngFor="let p of months"  [selected]="mm === p.val " [value]="p.val">{{p.name}}</option>    
      </select>
    </div>`
    }), 
    __metadata('design:paramtypes', [])
], MonthPicker);
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/month-picker.component.js.map

/***/ }),

/***/ 856:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return YearPicker; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

let YearPicker = class YearPicker {
    constructor() {
        this.years = [];
    }
    ngOnInit() {
        this.getYear();
    }
    getYear() {
        var today = new Date();
        this.yy = today.getFullYear();
        for (var i = (this.yy - 100); i <= this.yy; i++) {
            this.years.push(i);
        }
    }
};
YearPicker = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'year-picker',
        template: `
    <div>
      <select class="form-control"  required>
              <option  *ngFor="let y of years"  [selected]="yy === y ">{{y}}</option>    
      </select>
    </div>`
    }), 
    __metadata('design:paramtypes', [])
], YearPicker);
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/year-picker.component.js.map

/***/ }),

/***/ 857:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_forms__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_validation__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_validation___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ng2_validation__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_moment__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExpenseAddComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};






let ExpenseAddComponent = class ExpenseAddComponent {
    constructor(fb, allService, toastr, vcr) {
        this.fb = fb;
        this.allService = allService;
        this.toastr = toastr;
        this.vcr = vcr;
        this.Heading = 'รายจ่ายจิปาถะ';
        this.isLoading = false;
        this.selDate = { year: 0, month: 0, day: 0 };
        this.myDatePickerOptions = {
            // other options...
            dateFormat: 'dd/mm/yyyy',
            firstDayOfWeek: 'su',
            selectionTxtFontSize: '16px',
            editableDateField: false,
            openSelectorOnInputClick: true
        };
        this.toastr.setRootViewContainerRef(vcr);
        //validate FormBuilder
        this.detail = fb.control('', __WEBPACK_IMPORTED_MODULE_0__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_0__angular_forms__["Validators"].required]));
        this.expense_date = fb.control('', __WEBPACK_IMPORTED_MODULE_0__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_0__angular_forms__["Validators"].required]));
        this.amount = fb.control('', [__WEBPACK_IMPORTED_MODULE_0__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_0__angular_forms__["Validators"].required]), __WEBPACK_IMPORTED_MODULE_3_ng2_validation__["CustomValidators"].number]);
        this.expenseForm = fb.group({
            'detail': this.detail,
            'expense_date': this.expense_date,
            'amount': this.amount
        });
    }
    ngOnInit() {
        this.setData();
        this.setCurrentDate();
    }
    setData() {
        this.data = {
            'detail': '',
            'expense_date': '',
            'amount': '',
            'user_id': ''
        };
    }
    setCurrentDate() {
        console.log('current date', __WEBPACK_IMPORTED_MODULE_5_moment__().format());
        this.date = {
            'year': parseInt(__WEBPACK_IMPORTED_MODULE_5_moment__().format('YYYY')),
            'month': parseInt(__WEBPACK_IMPORTED_MODULE_5_moment__().format('MM')),
            'day': parseInt(__WEBPACK_IMPORTED_MODULE_5_moment__().format('DD')),
        };
        this.selDate = this.date;
    }
    setFormatDate(data) {
        if (typeof data === 'object') {
            return data.year + '-' + data.month + '-' + data.day;
        }
        else if (typeof data === 'string') {
            return data;
        }
    }
    sendForm() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('this.expenseForm.value : ', this.expenseForm.value, this.date.date);
            this.data = this.expenseForm.value;
            this.data.expense_date = this.setFormatDate(this.date.date);
            console.log('this.data', this.data);
            this.isLoading = true;
            try {
                let res_add_expense = yield this.allService.post('generalExpense', this.data);
                this.expenseForm.reset();
                this.setCurrentDate();
                this.isLoading = false;
                this.showSuccess(`เพิ่ม ${res_add_expense.data.detail} สำเร็จแล้ว`);
            }
            catch (error) {
                console.log("Error create expense type", error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.sendForm();
                }
                else {
                    this.isLoading = false;
                    this.showError(`ไม่สามารถเพิ่มรายจ่ายได้ กรุณาลองใหม่อีกครั้ง`);
                }
            }
        });
    }
    // --------------------------------------- Toast ---------------------------------------//
    showSuccess(message) {
        this.toastr.success(message, 'สำเร็จ!');
    }
    showError(message) {
        this.toastr.error(message, 'เกิดข้อผิดพลาด');
    }
    // --------------------------------------- date ---------------------------------------//
    onDateChanged(event) {
        console.log('IMyDateModel', event);
        this.date = event.date;
        console.log('IMyDateModel', this.date);
    }
};
ExpenseAddComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
        selector: 'app-expense-add',
        template: __webpack_require__(1223),
        styles: [__webpack_require__(1182)]
    }), 
    __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_forms__["FormBuilder"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_forms__["FormBuilder"]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__["a" /* AllServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__["a" /* AllServiceService */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr__["ToastsManager"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr__["ToastsManager"]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewContainerRef"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewContainerRef"]) === 'function' && _d) || Object])
], ExpenseAddComponent);
var _a, _b, _c, _d;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/expense-add.component.js.map

/***/ }),

/***/ 858:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ng2_toastr_ng2_toastr__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_ng2_toastr_ng2_toastr__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__service_all_service_service__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_bootstrap__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_validation__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_validation___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_ng2_validation__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_moment__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExpenseComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};







let ExpenseComponent = class ExpenseComponent {
    constructor(allService, toastr, vcr, fb) {
        this.allService = allService;
        this.toastr = toastr;
        this.vcr = vcr;
        this.fb = fb;
        this.Heading = 'รายจ่ายจิปาถะ';
        this.isLoading = false;
        //data
        this.data = null;
        //table
        this.dtOptions = {};
        this.myDateRangePickerOptions = {
            dateFormat: 'dd/mm/yyyy',
            firstDayOfWeek: 'su'
        };
        this.dateRange = {
            beginDate: { year: '', month: '', day: '' },
            endDate: { year: '', month: '', day: '' }
        };
        this.toastr.setRootViewContainerRef(vcr);
        this.isLoading = false;
        this.isHideMenu = true;
        //validate FormBuilder
        this.detail = fb.control('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required]));
        this.expense_date = fb.control('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required]));
        this.amount = fb.control('', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required]), __WEBPACK_IMPORTED_MODULE_5_ng2_validation__["CustomValidators"].number]);
        this.expenseForm = fb.group({
            'detail': this.detail,
            'expense_date': this.expense_date,
            'amount': this.amount
        });
    }
    ngOnInit() {
        this.initTable();
        this.initData();
        this.getData();
    }
    initTable() {
        this.dtOptions = {
            displayLength: 10,
            paginationType: 'simple_numbers',
            language: {
                info: 'แสดง _START_ ถึง _END_ จาก _TOTAL_',
                paginate: {
                    previous: '<',
                    next: '>',
                },
                search: 'ค้นหา',
                emptyTable: 'ไม่มีข้อมูล',
                infoEmpty: 'แสดง 0 ถึง 0 จาก 0',
                zeroRecords: 'ไม่มีข้อมูล',
                lengthMenu: 'แสดง _MENU_ แถว'
            }
        };
    }
    initData() {
        this.dateRange = {
            beginDate: { year: __WEBPACK_IMPORTED_MODULE_6_moment__().year(), month: __WEBPACK_IMPORTED_MODULE_6_moment__().month() + 1, day: __WEBPACK_IMPORTED_MODULE_6_moment__().date() },
            endDate: { year: __WEBPACK_IMPORTED_MODULE_6_moment__().year(), month: __WEBPACK_IMPORTED_MODULE_6_moment__().month() + 1, day: __WEBPACK_IMPORTED_MODULE_6_moment__().date() }
        };
        this.dateSelect = {
            'date_from': this.setFormatDate(__WEBPACK_IMPORTED_MODULE_6_moment__().format('YYYY-MM-DD')),
            'date_to': this.setFormatDate(__WEBPACK_IMPORTED_MODULE_6_moment__().format('YYYY-MM-DD'))
        };
    }
    getData() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = !this.isLoading;
            console.log('loadData');
            try {
                let res_get_product_type = yield this.allService.get('generalExpense', this.dateSelect);
                this.data = res_get_product_type.data;
                console.log('data', this.data);
                this.isLoading = false;
            }
            catch (error) {
                console.log("Error get data prodict type!", error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.getData();
                }
                else {
                    this.isLoading = false;
                    this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
                }
            }
        });
    }
    // --------------------------------------- Open Modal ---------------------------------------//
    openEditModal(id) {
        this.selectedItem = this.data.filter(data => data.id === id)[0];
        let dateTemp = __WEBPACK_IMPORTED_MODULE_6_moment__(this.selectedItem.expense_date, 'YYYY-MM-DD');
        this.selectedItem.expense_date = {
            'year': parseInt(dateTemp.format('YYYY')),
            'month': parseInt(dateTemp.format('MM')),
            'day': parseInt(dateTemp.format('DD'))
        };
        this.ex_date = this.selectedItem.expense_date;
        this.editModal.show();
    }
    openDeleteModal(id) {
        this.selectedItem = this.data.filter(data => data.id === id)[0];
        this.deleteModal.show();
    }
    saveChange() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('save data', this.selectedItem, this.expenseForm.value);
            this.isLoading = !this.isLoading;
            this.selectedItem.expense_date = this.setFormatDate(this.expenseForm.value.expense_date);
            console.log('this.selectedItem', this.selectedItem);
            try {
                let res_edit_product_type = yield this.allService.putWithId('generalExpense', this.selectedItem.id, this.selectedItem);
                this.showSuccess('แก้ไขข้อมูลเรียบร้อย');
                this.getData();
            }
            catch (error) {
                console.log("Error update data prodict type!", error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.saveChange();
                }
                else {
                    this.showError('ไม่สามารถแก้ไขข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
                    this.isLoading = false;
                }
            }
            this.editModal.hide();
        });
    }
    deleteItem() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('delete data', this.selectedItem);
            this.isLoading = true;
            try {
                let res_delete_product_type = yield this.allService.deleteWithId('generalExpense', this.selectedItem.id);
                this.showSuccess('ลบข้อมูลเรียบร้อย');
                this.getData();
            }
            catch (error) {
                console.log("Error delete data prodict type!", error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.deleteItem();
                }
                else {
                    this.showError('ไม่สามารถลบข้อมูลได้');
                    this.isLoading = false;
                }
            }
            this.deleteModal.hide();
        });
    }
    // --------------------------------------- Toast ---------------------------------------//
    showSuccess(message) {
        this.toastr.success(message, 'สำเร็จ!');
    }
    showError(message) {
        this.toastr.error(message, 'เกิดข้อผิดพลาด');
    }
    // --------------------------------------- date ---------------------------------------//
    onDateRangeChanged(event) {
        this.dateSelect = {
            'date_from': this.setFormatDate(event.beginDate),
            'date_to': this.setFormatDate(event.endDate)
        };
        this.getData();
    }
    setFormatDate(data) {
        if (typeof data === 'object') {
            return data.year + '-' + data.month + '-' + data.day;
        }
        else if (typeof data === 'string') {
            return data;
        }
    }
};
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3__angular_core__["ViewChild"])('editModal'), 
    __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_4_ng2_bootstrap__["f" /* ModalDirective */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4_ng2_bootstrap__["f" /* ModalDirective */]) === 'function' && _a) || Object)
], ExpenseComponent.prototype, "editModal", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3__angular_core__["ViewChild"])('deleteModal'), 
    __metadata('design:type', (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4_ng2_bootstrap__["f" /* ModalDirective */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4_ng2_bootstrap__["f" /* ModalDirective */]) === 'function' && _b) || Object)
], ExpenseComponent.prototype, "deleteModal", void 0);
ExpenseComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3__angular_core__["Component"])({
        selector: 'app-expense',
        template: __webpack_require__(1224),
        styles: [__webpack_require__(1183)]
    }), 
    __metadata('design:paramtypes', [(typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__service_all_service_service__["a" /* AllServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__service_all_service_service__["a" /* AllServiceService */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0_ng2_toastr_ng2_toastr__["ToastsManager"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0_ng2_toastr_ng2_toastr__["ToastsManager"]) === 'function' && _d) || Object, (typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__angular_core__["ViewContainerRef"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__angular_core__["ViewContainerRef"]) === 'function' && _e) || Object, (typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"]) === 'function' && _f) || Object])
], ExpenseComponent);
var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/expense.component.js.map

/***/ }),

/***/ 859:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__shared_hide_sidebar_service__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_hide_menu_service__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__sidebar_sidebar_component__ = __webpack_require__(452);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeaderComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};






let HeaderComponent = class HeaderComponent {
    constructor(hideMenuService, hideSidebarService, sidebar, allService, router) {
        this.hideMenuService = hideMenuService;
        this.hideSidebarService = hideSidebarService;
        this.sidebar = sidebar;
        this.allService = allService;
        this.router = router;
        this.generatedNumber = 0;
        // this.getUser();
    }
    ngOnInit() {
        this.getUser();
        this.isHideMenu = false;
        this.hideMenuService.next(this.isHideMenu);
        console.log('this.isHideMenu init', this.isHideMenu);
    }
    //hide menu
    hideMenu() {
        this.isHideMenu = !this.isHideMenu;
        this.hideMenuService.next(this.isHideMenu);
        console.log('this.isHideMenu function', this.isHideMenu);
        return this.isHideMenu;
    }
    logout() {
        this.allService.setToken(null);
        localStorage.clear();
        this.hideSidebarService.next(true);
        this.router.navigate(['/login']);
    }
    getUser() {
        return __awaiter(this, void 0, void 0, function* () {
            let userToken = this.allService.getLocal('user');
            if (userToken === null) {
                try {
                    let response = yield this.allService.get('auth/getAuthenticatedUser');
                    this.user = response.data.user;
                    this.allService.setLocal('user', this.user);
                    console.log('this.user', this.user);
                }
                catch (error) {
                    console.log('error getUser', error);
                    if (error.status === 401) {
                        yield this.allService.refreshToken();
                        this.getUser();
                    }
                }
            }
            else {
                this.user = userToken;
            }
        });
    }
    goToReport() {
        this.user.shop_id === null ? this.router.navigate(['/report/all/list']) : this.router.navigate(['/report/list']);
    }
};
HeaderComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3__angular_core__["Component"])({
        selector: 'app-header',
        template: __webpack_require__(1225),
        styles: [__webpack_require__(1184)]
    }), 
    __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_4__shared_hide_menu_service__["a" /* HideMenuService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4__shared_hide_menu_service__["a" /* HideMenuService */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__shared_hide_sidebar_service__["a" /* HideSidebarService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__shared_hide_sidebar_service__["a" /* HideSidebarService */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_5__sidebar_sidebar_component__["a" /* SidebarComponent */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_5__sidebar_sidebar_component__["a" /* SidebarComponent */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__["a" /* AllServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__["a" /* AllServiceService */]) === 'function' && _d) || Object, (typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === 'function' && _e) || Object])
], HeaderComponent);
var _a, _b, _c, _d, _e;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/header.component.js.map

/***/ }),

/***/ 860:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

let HomeComponent = class HomeComponent {
    constructor() {
    }
    ngOnInit() {
    }
};
HomeComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-home',
        template: __webpack_require__(1226),
        styles: [__webpack_require__(1185)]
    }), 
    __metadata('design:paramtypes', [])
], HomeComponent);
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/home.component.js.map

/***/ }),

/***/ 861:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__app_component__ = __webpack_require__(451);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(843);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_1__app_module__["a"]; });


//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/index.js.map

/***/ }),

/***/ 862:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoadingComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

let LoadingComponent = class LoadingComponent {
    constructor() {
    }
    ngOnInit() {
    }
};
LoadingComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-loading',
        template: __webpack_require__(1227),
        styles: [__webpack_require__(1186)]
    }), 
    __metadata('design:paramtypes', [])
], LoadingComponent);
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/loading.component.js.map

/***/ }),

/***/ 863:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__service_all_service_service__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__(13);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};




let LoginComponent = class LoginComponent {
    constructor(router, fb, allService) {
        this.router = router;
        this.fb = fb;
        this.allService = allService;
        this.isLoading = false;
        allService.checkLogin();
        //Validate Form
        this.username = fb.control('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required]));
        this.password = fb.control('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required]));
        this.userForm = fb.group({
            'username': this.username,
            'password': this.password
        });
    }
    ngOnInit() {
    }
    sendForm() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('data', this.userForm.value);
            this.isLoading = !this.isLoading;
            try {
                let res_login = yield this.allService.post('auth/authenticate', this.userForm.value);
                yield this.allService.setToken(res_login.data.token);
                console.log('res_login', res_login.data);
                let res_user = yield this.allService.get('auth/getAuthenticatedUser');
                console.log('this.user', res_user.data.user);
                yield this.allService.setLocal('user', res_user.data.user);
                let user = yield this.allService.getLocal('user');
                if (user.shop_id === null) {
                    this.router.navigate(['/report/all/list']);
                }
                else {
                    this.router.navigate(['/report/list']);
                }
                this.isLoading = !this.isLoading;
            }
            catch (error) {
                console.log("Error login", error);
                this.message = 'ไม่สามารถเข้าสู่ระบบได้ กรุณาลองใหม่อีกครั้ง';
                this.isLoading = !this.isLoading;
            }
        });
    }
};
LoginComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
        selector: 'app-login',
        template: __webpack_require__(1228),
        styles: [__webpack_require__(1187)]
    }), 
    __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__service_all_service_service__["a" /* AllServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__service_all_service_service__["a" /* AllServiceService */]) === 'function' && _c) || Object])
], LoginComponent);
var _a, _b, _c;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/login.component.js.map

/***/ }),

/***/ 864:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaginationComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

let PaginationComponent = class PaginationComponent {
    constructor() {
        this.currentPage = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.pages = [];
    }
    ;
    ngOnInit() {
        this.selectedIdx = this.pageActive || 0;
        this.setPage();
    }
    setPage() {
        let page = Math.ceil(this.total / this.perPage);
        for (let i = 1; i <= page; i++) {
            this.pages.push(i);
        }
    }
    selectPage(index) {
        if (index >= 0 && index < this.pages.length) {
            this.selectedIdx = index;
            this.currentPage.emit(this.selectedIdx + 1);
        }
    }
};
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(), 
    __metadata('design:type', Number)
], PaginationComponent.prototype, "total", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(), 
    __metadata('design:type', Number)
], PaginationComponent.prototype, "perPage", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(), 
    __metadata('design:type', Number)
], PaginationComponent.prototype, "pageActive", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(), 
    __metadata('design:type', Object)
], PaginationComponent.prototype, "currentPage", void 0);
PaginationComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-pagination',
        template: __webpack_require__(1229),
        styles: [__webpack_require__(1188)]
    }), 
    __metadata('design:paramtypes', [])
], PaginationComponent);
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/pagination.component.js.map

/***/ }),

/***/ 865:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__shared_hide_menu_service__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service_all_service_service__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductTypeAddComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};





let ProductTypeAddComponent = class ProductTypeAddComponent {
    constructor(hideMenuService, fb, allService, toastr, vcr) {
        this.hideMenuService = hideMenuService;
        this.fb = fb;
        this.allService = allService;
        this.toastr = toastr;
        this.vcr = vcr;
        this.Heading = 'ประเภทสินค้า';
        this.isLoading = false;
        this.toastr.setRootViewContainerRef(vcr);
        this.isHideMenu = true;
        hideMenuService.subscribe({
            next: isHideMenu => {
                this.isHideMenu = isHideMenu;
                return this.isHideMenu;
            }
        });
        //validate FormBuilder
        this.name = fb.control('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required]));
        this.detail = fb.control('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required]));
        this.productTypeForm = fb.group({
            'name': this.name,
            'detail': this.detail
        });
    }
    ngOnInit() {
        this.setData();
    }
    setData() {
        this.data = {
            'name': '',
            'detail': ''
        };
    }
    sendForm() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('data : ', this.data, this.productTypeForm.value);
            let data = this.productTypeForm.value;
            this.isLoading = !this.isLoading;
            try {
                let res_add_product_type = yield this.allService.post('productType', data);
                this.productTypeForm.reset();
                this.isLoading = !this.isLoading;
                this.showSuccess(`เพิ่ม ${res_add_product_type.data.name} สำเร็จแล้ว`);
            }
            catch (error) {
                let response = error;
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.sendForm();
                }
                else {
                    console.log("Error create product type", response);
                    if (error.status === 401) {
                        yield this.allService.refreshToken();
                        this.sendForm();
                    }
                    else {
                        this.isLoading = false;
                        this.showError(`ไม่สามารถเพิ่มประเภทสินค้าได้ กรุณาลองใหม่อีกครั้ง`);
                    }
                }
            }
        });
    }
    // --------------------------------------- Toast ---------------------------------------//
    showSuccess(message) {
        this.toastr.success(message, 'สำเร็จ!');
    }
    showError(message) {
        this.toastr.error(message, 'เกิดข้อผิดพลาด');
    }
};
ProductTypeAddComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
        selector: 'app-product-type-add',
        template: __webpack_require__(1230),
        styles: [__webpack_require__(1189)]
    }), 
    __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__shared_hide_menu_service__["a" /* HideMenuService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__shared_hide_menu_service__["a" /* HideMenuService */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__service_all_service_service__["a" /* AllServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__service_all_service_service__["a" /* AllServiceService */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr__["ToastsManager"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr__["ToastsManager"]) === 'function' && _d) || Object, (typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2__angular_core__["ViewContainerRef"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__angular_core__["ViewContainerRef"]) === 'function' && _e) || Object])
], ProductTypeAddComponent);
var _a, _b, _c, _d, _e;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/product-type-add.component.js.map

/***/ }),

/***/ 866:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_hide_menu_service__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service_all_service_service__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(2);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductTypeComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};






let ProductTypeComponent = class ProductTypeComponent {
    constructor(hideMenuService, allService, toastr, vcr, fb) {
        this.hideMenuService = hideMenuService;
        this.allService = allService;
        this.toastr = toastr;
        this.vcr = vcr;
        this.fb = fb;
        this.Heading = 'ประเภทสินค้า';
        this.isLoading = false;
        //data
        this.data = null;
        //table
        this.dtOptions = {};
        this.toastr.setRootViewContainerRef(vcr);
        this.isLoading = false;
        this.getData();
        this.isHideMenu = true;
        hideMenuService.subscribe({
            next: isHideMenu => {
                this.isHideMenu = isHideMenu;
                return this.isHideMenu;
            }
        });
        //validate FormBuilder
        this.name = fb.control('', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].required]));
        this.detail = fb.control('', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].required]));
        this.productTypeForm = fb.group({
            'name': this.name,
            'detail': this.detail
        });
    }
    ngOnInit() {
        this.initTable();
    }
    initTable() {
        this.dtOptions = {
            displayLength: 10,
            paginationType: 'simple_numbers',
            language: {
                info: 'แสดง _START_ ถึง _END_ จาก _TOTAL_',
                paginate: {
                    previous: '<',
                    next: '>',
                },
                search: 'ค้นหา',
                emptyTable: 'ไม่มีข้อมูล',
                infoEmpty: 'แสดง 0 ถึง 0 จาก 0',
                zeroRecords: 'ไม่มีข้อมูล',
                lengthMenu: 'แสดง _MENU_ แถว'
            }
        };
    }
    getData() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = !this.isLoading;
            console.log('loadData');
            try {
                let res_get_product_type = yield this.allService.get('productType');
                this.data = res_get_product_type.data;
                this.isLoading = false;
            }
            catch (error) {
                console.log("Error get data prodict type!", error);
                if (error.status === 404) {
                    this.isLoading = false;
                }
                else if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.getData();
                }
                else {
                    this.isLoading = false;
                }
            }
        });
    }
    // --------------------------------------- Open Modal ---------------------------------------//
    openEditModal(id) {
        this.selectedItem = this.data.filter(data => data.id == id)[0];
        this.editModal.show();
    }
    openDeleteModal(id) {
        this.selectedItem = this.data.filter(data => data.id == id)[0];
        this.deleteModal.show();
    }
    saveChange() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('save data', this.selectedItem, this.productTypeForm.value);
            this.isLoading = true;
            console.log('loadData');
            try {
                let res_edit_product_type = yield this.allService.putWithId('productType', this.selectedItem.id, this.selectedItem);
                this.showSuccess('แก้ไขข้อมูลเรียบร้อย');
                this.getData();
            }
            catch (error) {
                console.log("Error update data prodict type!", error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.saveChange();
                }
                else {
                    this.isLoading = false;
                    this.showError(`ไม่สามารถแก้ไขได้ กรุณาลองใหม่อีกครั้ง`);
                }
            }
            this.editModal.hide();
        });
    }
    deleteItem() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('delete data', this.selectedItem);
            this.isLoading = true;
            try {
                let res_delete_product_type = yield this.allService.deleteWithId('productType', this.selectedItem.id);
                this.showSuccess('ลบข้อมูลเรียบร้อย');
                this.getData();
            }
            catch (error) {
                console.log("Error delete data prodict type!", error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.deleteItem();
                }
                else {
                    this.isLoading = false;
                    this.showError(`ไม่สามารถลบประเภทสินค้าได้ กรุณาลองใหม่อีกครั้ง`);
                }
            }
            this.deleteModal.hide();
        });
    }
    // --------------------------------------- Toast ---------------------------------------//
    showSuccess(message) {
        this.toastr.success(message, 'สำเร็จ!');
    }
    showError(message) {
        this.toastr.error(message, 'เกิดข้อผิดพลาด');
    }
};
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('editModal'), 
    __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["f" /* ModalDirective */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["f" /* ModalDirective */]) === 'function' && _a) || Object)
], ProductTypeComponent.prototype, "editModal", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('deleteModal'), 
    __metadata('design:type', (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["f" /* ModalDirective */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["f" /* ModalDirective */]) === 'function' && _b) || Object)
], ProductTypeComponent.prototype, "deleteModal", void 0);
ProductTypeComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-product-type',
        template: __webpack_require__(1231),
        styles: [__webpack_require__(1190)]
    }), 
    __metadata('design:paramtypes', [(typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__shared_hide_menu_service__["a" /* HideMenuService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__shared_hide_menu_service__["a" /* HideMenuService */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__service_all_service_service__["a" /* AllServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__service_all_service_service__["a" /* AllServiceService */]) === 'function' && _d) || Object, (typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr__["ToastsManager"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr__["ToastsManager"]) === 'function' && _e) || Object, (typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"]) === 'function' && _f) || Object, (typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_5__angular_forms__["FormBuilder"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_5__angular_forms__["FormBuilder"]) === 'function' && _g) || Object])
], ProductTypeComponent);
var _a, _b, _c, _d, _e, _f, _g;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/product-type.component.js.map

/***/ }),

/***/ 867:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_hide_menu_service__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service_all_service_service__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_moment__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AcceptProductComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};







let AcceptProductComponent = class AcceptProductComponent {
    constructor(hideMenuService, allService, toastr, vcr, fb) {
        this.hideMenuService = hideMenuService;
        this.allService = allService;
        this.toastr = toastr;
        this.vcr = vcr;
        this.fb = fb;
        this.Heading = 'การนำเข้าสินค้า';
        this.isLoading = false;
        //data
        this.data = null;
        //table
        this.dtOptions = {};
        this.typeaheadLoading = false;
        this.asyncSelected = '';
        this.shop_data = [];
        this.myDateRangePickerOptions = {
            dateFormat: 'dd/mm/yyyy',
            firstDayOfWeek: 'su'
        };
        this.dateRange = {
            beginDate: { year: '', month: '', day: '' },
            endDate: { year: '', month: '', day: '' }
        };
        this.toastr.setRootViewContainerRef(vcr);
        this.loadData();
        this.isHideMenu = true;
        hideMenuService.subscribe({
            next: isHideMenu => {
                this.isHideMenu = isHideMenu;
                return this.isHideMenu;
            }
        });
        //validate FormBuilder
        this.product_id = fb.control('', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].required]));
        this.amount = fb.control('', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].required]));
        this.acceptProductTypeForm = fb.group({
            'amount': this.amount,
            'remark': this.remark,
            'product_id': this.product_id
        });
    }
    ngOnInit() {
        this.initData();
        this.initTable();
    }
    initData() {
        this.productSelected = {
            code: '',
            name: ''
        };
        this.selectedItem = null;
        this.dateRange = {
            beginDate: { year: __WEBPACK_IMPORTED_MODULE_6_moment__().year(), month: __WEBPACK_IMPORTED_MODULE_6_moment__().month() + 1, day: __WEBPACK_IMPORTED_MODULE_6_moment__().date() },
            endDate: { year: __WEBPACK_IMPORTED_MODULE_6_moment__().year(), month: __WEBPACK_IMPORTED_MODULE_6_moment__().month() + 1, day: __WEBPACK_IMPORTED_MODULE_6_moment__().date() }
        };
        this.dateSelect = {
            'date_from': this.setFormatDate(__WEBPACK_IMPORTED_MODULE_6_moment__().format('YYYY-MM-DD')),
            'date_to': this.setFormatDate(__WEBPACK_IMPORTED_MODULE_6_moment__().format('YYYY-MM-DD'))
        };
    }
    loadData() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.getData();
        });
    }
    initTable() {
        this.dtOptions = {
            displayLength: 10,
            bSort: false,
            paginationType: 'simple_numbers',
            language: {
                info: 'แสดง _START_ ถึง _END_ จาก _TOTAL_',
                paginate: {
                    previous: '<',
                    next: '>',
                },
                search: 'ค้นหา',
                emptyTable: 'ไม่มีข้อมูล',
                infoEmpty: 'แสดง 0 ถึง 0 จาก 0',
                zeroRecords: 'ไม่มีข้อมูล',
                lengthMenu: 'แสดง _MENU_ แถว'
            }
        };
    }
    getData() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = true;
            console.log('loadData');
            try {
                let res_get_tran_in_product = yield this.allService.get('productTransections/getProductTransectionIn', this.dateSelect);
                this.data = res_get_tran_in_product.data;
                for (var i = 0; i < this.data.length; i++) {
                    this.data[i].date = __WEBPACK_IMPORTED_MODULE_6_moment__(this.data[i].created_at).format("YYYY-MM-DD");
                }
                console.log('res_get_tran_in_product', this.data);
                this.isLoading = false;
            }
            catch (error) {
                console.log("Error get data prodict type!", error);
                if (error.status === 404) {
                    this.isLoading = false;
                    this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
                }
                else if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.getData();
                }
                else {
                    this.isLoading = false;
                    this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
                }
            }
        });
    }
    // --------------------------------------- Open Modal ---------------------------------------//
    openEditModal(id) {
        this.selectedItem = Object.assign({}, this.data.filter(data => data.id == id)[0]);
        this.productSelected.name = this.selectedItem.product.name;
        this.editModal.show();
    }
    openDeleteModal(id) {
        this.selectedItem = Object.assign({}, this.data.filter(data => data.id == id)[0]);
        this.deleteModal.show();
    }
    saveChange() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = true;
            console.log('this.selectedItem', this.selectedItem);
            console.log('this.productSelected', this.productSelected);
            let data = Object.assign({}, this.selectedItem, this.acceptProductTypeForm.value);
            data.product_id = this.productSelected.id || this.selectedItem.product_id;
            console.log('data', data);
            try {
                let res_tran_in_product = yield this.allService.putWithId('productTransections', this.selectedItem.id, data);
                this.initData();
                this.getData();
                this.showSuccess(`แก้ไขข้อมูลเรียบร้อย`);
            }
            catch (error) {
                console.log("Error create product type", error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.saveChange();
                }
                else {
                    this.isLoading = false;
                    this.showError(`ไม่สามารถแก้ไขได้ กรุณาลองใหม่อีกครั้ง`);
                }
            }
            this.editModal.hide();
        });
    }
    deleteItem() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('delete data', this.selectedItem);
            this.isLoading = true;
            try {
                let res_delete_product_type = yield this.allService.deleteWithId('productTransections', this.selectedItem.id);
                this.showSuccess('ลบข้อมูลเรียบร้อย');
                this.getData();
            }
            catch (error) {
                console.log("Error delete data prodict type!", error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.deleteItem();
                }
                else {
                    this.isLoading = false;
                    this.showError('ไม่สามารถลบข้อมูลได้');
                }
            }
            this.deleteModal.hide();
        });
    }
    // --------------------------------------- Toast ---------------------------------------//
    showSuccess(message) {
        this.toastr.success(message, 'สำเร็จ!');
    }
    showError(message) {
        this.toastr.error(message, 'เกิดข้อผิดพลาด');
    }
    //--------------------------------------- Typeahead ---------------------------------------//
    changeTypeaheadLoading(e) {
        this.typeaheadLoading = e;
    }
    changeTypeaheadNoResults(status, e) {
        this.typeaheadNoResults = e;
    }
    typeaheadOnSelect(e) {
        this.productSelected = e.item;
        console.log('Selected value:', e);
    }
    // --------------------------------------- date ---------------------------------------//
    onDateRangeChanged(event) {
        this.dateSelect = {
            'date_from': this.setFormatDate(event.beginDate),
            'date_to': this.setFormatDate(event.endDate)
        };
        this.getData();
    }
    setFormatDate(data) {
        if (typeof data === 'object') {
            return data.year + '-' + data.month + '-' + data.day;
        }
        else if (typeof data === 'string') {
            return data;
        }
    }
};
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('editModal'), 
    __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["f" /* ModalDirective */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["f" /* ModalDirective */]) === 'function' && _a) || Object)
], AcceptProductComponent.prototype, "editModal", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('deleteModal'), 
    __metadata('design:type', (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["f" /* ModalDirective */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["f" /* ModalDirective */]) === 'function' && _b) || Object)
], AcceptProductComponent.prototype, "deleteModal", void 0);
AcceptProductComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-accept-product',
        template: __webpack_require__(1232),
        styles: [__webpack_require__(1191)]
    }), 
    __metadata('design:paramtypes', [(typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__shared_hide_menu_service__["a" /* HideMenuService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__shared_hide_menu_service__["a" /* HideMenuService */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__service_all_service_service__["a" /* AllServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__service_all_service_service__["a" /* AllServiceService */]) === 'function' && _d) || Object, (typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr__["ToastsManager"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr__["ToastsManager"]) === 'function' && _e) || Object, (typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"]) === 'function' && _f) || Object, (typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_5__angular_forms__["FormBuilder"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_5__angular_forms__["FormBuilder"]) === 'function' && _g) || Object])
], AcceptProductComponent);
var _a, _b, _c, _d, _e, _f, _g;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/accept-product.component.js.map

/***/ }),

/***/ 868:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_forms__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_validation__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_validation___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ng2_validation__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddAcceptProductComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};





let AddAcceptProductComponent = class AddAcceptProductComponent {
    constructor(fb, allService, toastr, vcr) {
        this.fb = fb;
        this.allService = allService;
        this.toastr = toastr;
        this.vcr = vcr;
        this.Heading = 'เพิ่มรายการนำเข้าสินค้า';
        this.isLoading = false;
        this.shop_data = [];
        this.typeaheadLoading = false;
        this.asyncSelected = '';
        this.toastr.setRootViewContainerRef(vcr);
        //validate FormBuilder
        this.product_id = fb.control('', __WEBPACK_IMPORTED_MODULE_0__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_0__angular_forms__["Validators"].required]));
        this.amount = fb.control('1', __WEBPACK_IMPORTED_MODULE_0__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_0__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_3_ng2_validation__["CustomValidators"].number, __WEBPACK_IMPORTED_MODULE_3_ng2_validation__["CustomValidators"].min(0)]));
        this.acceptProductTypeForm = fb.group({
            'amount': this.amount,
            'remark': this.remark,
            'product_id': this.product_id
        });
    }
    ngOnInit() {
        this.setData();
        this.loadData();
    }
    setData() {
        this.data = {
            'shop_source_id': '',
            'amount': '',
            'code': '',
            'remark': '',
            'shop_destination_id': '',
            'product_id': ''
        };
        this.productSelected = {
            code: '',
            name: ''
        };
    }
    loadData() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = true;
            try {
                let resp_product_data = yield this.allService.get('product');
                this.products = resp_product_data.data;
                this.isLoading = false;
            }
            catch (error) {
                console.log('error get shop', error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.loadData();
                }
                else {
                    this.isLoading = false;
                    this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
                }
            }
        });
    }
    sendForm() {
        return __awaiter(this, void 0, void 0, function* () {
            let data = this.acceptProductTypeForm.value;
            this.isLoading = true;
            try {
                data.shop_destination_id = this.allService.getLocal('user').shop_id;
                data.product_id = this.productSelected.id;
                console.log('data', data);
                let res_tran_in_product = yield this.allService.post('productTransections', data);
                this.acceptProductTypeForm.reset();
                this.isLoading = false;
                this.showSuccess(`นำเข้าสินค้าสำเร็จแล้ว`);
            }
            catch (error) {
                let response = error;
                console.log("Error create product type", response);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.sendForm();
                }
                else {
                    this.isLoading = false;
                    this.showError(`ไม่สามารถนำเข้าสินค้าได้ กรุณาลองใหม่อีกครั้ง`);
                }
            }
        });
    }
    // --------------------------------------- Toast ---------------------------------------//
    showSuccess(message) {
        this.toastr.success(message, 'สำเร็จ!');
    }
    showError(message) {
        this.toastr.error(message, 'เกิดข้อผิดพลาด');
    }
    //--------------------------------------- Typeahead ---------------------------------------//
    changeTypeaheadLoading(e) {
        this.typeaheadLoading = e;
    }
    changeTypeaheadNoResults(status, e) {
        this.typeaheadNoResults = e;
    }
    typeaheadOnSelect(e) {
        this.productSelected = e.item;
        console.log('Selected value:', e);
    }
};
AddAcceptProductComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
        selector: 'app-add-accept-product',
        template: __webpack_require__(1233),
        styles: [__webpack_require__(1192)]
    }), 
    __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_forms__["FormBuilder"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_forms__["FormBuilder"]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__["a" /* AllServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__["a" /* AllServiceService */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr__["ToastsManager"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr__["ToastsManager"]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewContainerRef"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewContainerRef"]) === 'function' && _d) || Object])
], AddAcceptProductComponent);
var _a, _b, _c, _d;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/add-accept-product.component.js.map

/***/ }),

/***/ 869:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ng2_toastr_ng2_toastr__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_ng2_toastr_ng2_toastr__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__(13);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InfoProductComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};






let InfoProductComponent = class InfoProductComponent {
    constructor(fb, router, route, allService, toastr, vcr) {
        this.fb = fb;
        this.router = router;
        this.route = route;
        this.allService = allService;
        this.toastr = toastr;
        this.vcr = vcr;
        this.Heading = 'ข้อมูลสินค้า';
        this.isProductDetailLoading = false;
        this.edit = false;
        this.product_type_data = [];
        this.toastr.setRootViewContainerRef(vcr);
        //validate FormBuilder
        this.name = fb.control('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required]));
        this.product_type_id = fb.control('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required]));
        this.cost = fb.control('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required]));
        this.sell_price = fb.control('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required]));
        this.productForm = fb.group({
            'code': this.code,
            'autoCode': this.autoCode,
            'name': this.name,
            'product_type_id': this.product_type_id,
            'cost': this.cost,
            'sell_price': this.sell_price,
            'detail': this.detail,
            'remark': this.remark,
        });
    }
    ngOnInit() {
        this.subGetParams = this.route.params.subscribe(params => {
            this.productId = params['id'];
        });
        console.log('this.productId', this.productId);
        this.getDataProductDetail();
    }
    getDataProductDetail() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isProductDetailLoading = true;
            try {
                let resp_productDetail = yield this.allService.getWithId('product', this.productId);
                let res_product_type_data = yield this.allService.get('productType');
                this.product_type_datas = res_product_type_data.data;
                this.productDetail = resp_productDetail.data;
                this.setDataSelect();
                console.log('resp_productDetail', this.productDetail);
                this.isProductDetailLoading = false;
            }
            catch (error) {
                console.log('error get product detail ', error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.getDataProductDetail();
                }
            }
        });
    }
    setDataSelect() {
        console.log('this.product_type_datas', this.product_type_datas);
        for (let i = 0; i < this.product_type_datas.length; i++) {
            this.product_type_data.push({ 'value': this.product_type_datas[i].id.toString(), 'label': this.product_type_datas[i].name });
        }
        if (this.productDetail.product_type_id) {
            this.productForm.controls['product_type_id'].setValue(this.productDetail.product_type_id.toString());
            // this.productForm.controls['product_type_id'].setValue('113');
            console.log('this.productForm.value', this.productForm.value);
        }
        console.log('product_type_data', this.product_type_data);
    }
    saveChange() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                console.log('save change', this.productForm.value);
                let data = Object.assign({}, this.productForm.value);
                if (!data.product_type_id) {
                    data.product_type_id = parseInt(this.productDetail.product_type_id);
                }
                console.log('data', data);
                let res_edit_product = yield this.allService.putWithId('product', this.productId, data);
                this.showSuccess('แก้ไขข้อมูลเรียบร้อย');
                this.edit = false;
                this.getDataProductDetail();
            }
            catch (error) {
                console.log("Error update data product type!", error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.saveChange();
                }
                else {
                    this.showError('กรุณาลองใหม่อีกครั้ง');
                }
            }
        });
    }
    //--------------------------------------- Print Barcode ---------------------------------------//
    printBarcode() {
        let navigationExtras;
        navigationExtras = {
            queryParams: {
                "dataSelect": JSON.stringify({ data: [this.productDetail] })
            }
        };
        window.open("/#/product/print?dataSelect=" + JSON.stringify({ data: [this.productDetail] }), "_blank");
        // this.router.navigate(['product/print'], navigationExtras);
    }
    // --------------------------------------- Toast ---------------------------------------//
    showSuccess(message) {
        this.toastr.success(message, 'สำเร็จ!');
    }
    showError(message) {
        this.toastr.error(message, 'เกิดข้อผิดพลาด');
    }
};
InfoProductComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3__angular_core__["Component"])({
        selector: 'app-info-product',
        template: __webpack_require__(1234),
        styles: [__webpack_require__(1193)]
    }), 
    __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__angular_router__["b" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4__angular_router__["b" /* Router */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4__angular_router__["c" /* ActivatedRoute */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4__angular_router__["c" /* ActivatedRoute */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__["a" /* AllServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__["a" /* AllServiceService */]) === 'function' && _d) || Object, (typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_0_ng2_toastr_ng2_toastr__["ToastsManager"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0_ng2_toastr_ng2_toastr__["ToastsManager"]) === 'function' && _e) || Object, (typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_3__angular_core__["ViewContainerRef"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__angular_core__["ViewContainerRef"]) === 'function' && _f) || Object])
], InfoProductComponent);
var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/info-product.component.js.map

/***/ }),

/***/ 870:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ng2_bootstrap__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service_all_service_service__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddProductComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};





let AddProductComponent = class AddProductComponent {
    constructor(fb, allService, toastr, vcr) {
        this.fb = fb;
        this.allService = allService;
        this.toastr = toastr;
        this.vcr = vcr;
        this.Heading = 'สินค้า';
        this.isLoading = false;
        this.codes = [{ 'code': '' }];
        this.product_type_data = [];
        this.autoCodeProduct = false;
        this.brands = ["I-Phone", "Samsung", "Vivo", "Oppo", "Huawei", "Ais Lava", "Dtac ZTE", "True", "Lenovo", "Acer", "Asus", "Inovo", "Aplus", "Nipda", "Timi", "TWZ", "SKG", "Telego", "Nova", "Nex", "WIko", "Infone", "Aston", "Sony", "HTC"];
        this.brands_select = '';
        this.autoTranIn = false;
        this.autoTranIn = this.allService.getLocal('user').shop_id !== null;
        console.log('autoTranIn', this.autoTranIn);
        this.toastr.setRootViewContainerRef(vcr);
        //validate FormBuilder
        this.name = fb.control('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required]));
        this.product_type_id = fb.control('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required]));
        this.cost = fb.control('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required]));
        this.sell_price = fb.control('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required]));
        this.productForm = fb.group({
            'code': this.code,
            'autoCode': this.autoCode,
            'name': this.name,
            'product_type_id': this.product_type_id,
            'cost': this.cost,
            'sell_price': this.sell_price,
            'detail': this.detail,
            'remark': this.remark,
            'isCreateTranIn': this.isCreateTranIn
        });
        //validate FormBuilder
        this.name_product_type = fb.control('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required]));
        this.detail_product_type = fb.control('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required]));
        this.productTypeForm = fb.group({
            'name_product_type': this.name_product_type,
            'detail_product_type': this.detail_product_type
        });
    }
    ngOnInit() {
        this.setData();
        this.loadData();
    }
    setData() {
        this.data = {
            'codes': '',
            'name': '',
            'product_type_id': '',
            'cost': '',
            'sell_price': '',
            'detail': '',
            'remark': '',
            'isCreateTranIn': ''
        };
    }
    clearData() {
        this.productForm.reset();
        this.codes = [{ 'code': '' }];
    }
    loadData() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let res_product_type_data = yield this.allService.get('productType');
                this.product_type_datas = res_product_type_data.data;
                console.log('product_type_datas', this.product_type_datas);
                this.setDataSelect();
                this.isLoading = false;
            }
            catch (error) {
                console.log('error load data product', error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.loadData();
                }
            }
        });
    }
    setDataSelect() {
        for (let i = 0; i < this.product_type_datas.length; i++) {
            this.product_type_data.push({ 'value': this.product_type_datas[i].id, 'label': this.product_type_datas[i].name });
        }
        console.log('product_type_data', this.product_type_data);
    }
    checkAutoCode() {
        console.log('checkAutoCode', this.productForm.value.autoCode);
        // let ctrl = this.productForm.get('code');
        // this.productForm.value.autoCode ? ctrl.disable() : ctrl.enable();
        this.codes = [{ 'code': '' }];
    }
    sendForm() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('data : ', this.productForm.value);
            let data = this.productForm.value;
            data.codes = this.setCodeFormat();
            data.name = this.brands_select !== '' ? this.brands_select + ' ' + data.name : data.name;
            this.isLoading = true;
            if (data.autoCode) {
                data.code = null;
            }
            try {
                console.log('data', data);
                let res_add_product = yield this.allService.post('product', data);
                this.clearData();
                this.isLoading = false;
                this.showSuccess(`เพิ่ม ${data.name} สำเร็จแล้ว`);
            }
            catch (error) {
                console.log("Error add product", error);
                this.isLoading = false;
                this.showError(`ไม่สามารถเพิ่มสินค้าได้ กรุณาลองใหม่อีกครั้ง`);
            }
        });
    }
    // --------------------------------------- Toast ---------------------------------------//
    showSuccess(message) {
        this.toastr.success(message, 'สำเร็จ!');
    }
    showError(message) {
        this.toastr.error(message, 'เกิดข้อผิดพลาด');
    }
    // --------------------------------------- Open Modal ---------------------------------------//
    openEditModal() {
        this.editModal.show();
    }
    saveProductType() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('data : ', this.data, this.productTypeForm.value);
            let data = {
                'name': this.productTypeForm.value.name_product_type,
                'detail': this.productTypeForm.value.detail_product_type
            };
            this.isLoading = true;
            try {
                let res_add_product_type = yield this.allService.post('productType', data);
                yield this.loadData();
                this.productTypeForm.reset();
                this.editModal.hide();
                this.showSuccess(`เพิ่ม ${res_add_product_type.data.name} สำเร็จแล้ว`);
            }
            catch (error) {
                let response = error;
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.saveProductType();
                }
                else {
                    console.log("Error create product type", response);
                    this.isLoading = false;
                    this.showError(`ไม่สามารถเพิ่มประเภทสินค้าได้ กรุณาลองใหม่อีกครั้ง`);
                }
            }
        });
    }
    // --------------------------------------- Codes ---------------------------------------//
    addCodes() {
        console.log('codes', this.codes);
        this.codes.push({ 'code': '' });
    }
    deleteCodes(index) {
        if (this.codes.length > 1) {
            this.codes.splice(index, 1);
        }
        else {
            this.codes = [{ 'code': '' }];
        }
    }
    setCodeFormat() {
        let data = [];
        for (var index = 0; index < this.codes.length; index++) {
            data.push(this.codes[index].code);
        }
        console.log('data', data);
        return JSON.stringify(data);
    }
};
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewChild"])('editModal'), 
    __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0_ng2_bootstrap__["f" /* ModalDirective */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0_ng2_bootstrap__["f" /* ModalDirective */]) === 'function' && _a) || Object)
], AddProductComponent.prototype, "editModal", void 0);
AddProductComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
        selector: 'app-add-product',
        template: __webpack_require__(1235),
        styles: [__webpack_require__(1194)]
    }), 
    __metadata('design:paramtypes', [(typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__service_all_service_service__["a" /* AllServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__service_all_service_service__["a" /* AllServiceService */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr__["ToastsManager"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr__["ToastsManager"]) === 'function' && _d) || Object, (typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewContainerRef"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewContainerRef"]) === 'function' && _e) || Object])
], AddProductComponent);
var _a, _b, _c, _d, _e;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/add-product.component.js.map

/***/ }),

/***/ 871:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_forms__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_bootstrap__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_router__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__shared_hide_menu_service__ = __webpack_require__(32);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ManageProductComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};







let ManageProductComponent = class ManageProductComponent {
    constructor(router, hideMenuService, allService, toastr, vcr, fb) {
        this.router = router;
        this.hideMenuService = hideMenuService;
        this.allService = allService;
        this.toastr = toastr;
        this.vcr = vcr;
        this.fb = fb;
        this.Heading = 'สินค้า';
        this.isLoading = false;
        //table
        this.dtOptions = {};
        //Checkbox
        this.selected = [];
        this.product_type_data = [];
        this.product_status_data = [];
        this.autoCodeProduct = false;
        this.toastr.setRootViewContainerRef(vcr);
        this.getCategory();
        this.isHideMenu = true;
        hideMenuService.subscribe({
            next: isHideMenu => {
                this.isHideMenu = isHideMenu;
                return this.isHideMenu;
            }
        });
        this.haveSelected = false;
        //validate FormBuilder
        this.name = fb.control('', __WEBPACK_IMPORTED_MODULE_0__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_0__angular_forms__["Validators"].required]));
        this.product_type_id = fb.control('', __WEBPACK_IMPORTED_MODULE_0__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_0__angular_forms__["Validators"].required]));
        this.sell_price = fb.control('', __WEBPACK_IMPORTED_MODULE_0__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_0__angular_forms__["Validators"].required]));
        this.productForm = fb.group({
            'code': this.code,
            'autoCode': this.autoCode,
            'name': this.name,
            'product_type_id': this.product_type_id,
            'cost': this.cost,
            'sell_price': this.sell_price,
            'detail': this.detail,
            'remark': this.remark,
        });
        this.search_text = fb.control('');
        this.search_type = fb.control('0');
        this.search_status = fb.control('0');
        this.productSearchForm = fb.group({
            'search_text': this.search_text,
            'search_type': this.search_type,
            'search_status': this.search_status
        });
    }
    ngOnInit() {
        this.initTable();
    }
    initTable() {
        this.dtOptions = {
            displayLength: 10,
            paginationType: 'simple_numbers',
            language: {
                info: 'แสดง _START_ ถึง _END_ จาก _TOTAL_',
                paginate: {
                    previous: '<',
                    next: '>',
                },
                search: 'ค้นหา',
                emptyTable: 'ไม่มีข้อมูล',
                infoEmpty: 'แสดง 0 ถึง 0 จาก 0',
                zeroRecords: 'ไม่มีข้อมูล',
                lengthMenu: 'แสดง _MENU_ แถว'
            },
            columnDefs: [{
                    orderable: false,
                    className: 'select-checkbox',
                    targets: 0
                }],
            select: {
                style: 'os',
                selector: 'td:first-child',
                blurable: true,
                items: 'column',
                className: 'row-selected'
            },
            rowCallback: (nRow, aData, iDisplayIndex, iDisplayIndexFull) => {
                let self = this;
                $('td', nRow).unbind('click');
                $('td', nRow).bind('click', () => {
                    self.clickHandler(aData);
                });
                return nRow;
            }
        };
    }
    clickHandler(dataSelect) {
        console.log('someClickHandler', dataSelect);
        this.selectedItem = this.product_data.find(data => data.code === dataSelect[1]);
        console.log('selectedItem', this.selectedItem);
    }
    checkProduct() {
        var index = this.selected.find(data => data.code === this.selectedItem.code);
        if (index) {
            this.selected.splice(index, 1);
        }
        else {
            this.selected.push(this.selectedItem);
        }
        this.selectedLength = this.selected.length;
        if (this.selected.length > 0) {
            this.haveSelected = true;
        }
        else {
            this.haveSelected = false;
        }
        console.log('this.selected', this.selected);
    }
    getCategory() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = !this.isLoading;
            try {
                let res_product_type_data = yield this.allService.get('productType');
                console.log('res_product_type_data', res_product_type_data);
                this.product_type_datas = res_product_type_data.data;
                this.setData();
                this.isLoading = false;
            }
            catch (error) {
                console.log('error manage product', error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.getCategory();
                }
            }
        });
    }
    getData() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = !this.isLoading;
            try {
                let params = {
                    'text': this.productSearchForm.value.search_text,
                    'type': this.productSearchForm.value.search_type,
                    'status': this.productSearchForm.value.search_status,
                };
                this.product_data = null;
                let res_product_data = yield this.allService.post('product/search', params);
                console.log('res_product_data', res_product_data);
                this.product_data = res_product_data.data;
                this.isLoading = false;
            }
            catch (error) {
                console.log('error manage product', error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.getData();
                }
            }
        });
    }
    setData() {
        for (let i = 0; i < this.product_type_datas.length; i++) {
            this.product_type_data.push({ 'value': this.product_type_datas[i].id, 'label': this.product_type_datas[i].name });
        }
        this.product_search_type = [{ 'value': 0, 'label': 'ทั้งหมด' }].concat(this.product_type_data);
        this.product_status_data = [
            { value: 0, label: 'ทั้งหมด' },
            { value: 1, label: 'ขายแล้ว' },
            { value: 2, label: 'อยู่ในสต็อค' }
        ];
    }
    sendSearchForm() {
        console.log('this.search', this.productSearchForm.value);
        this.getData();
    }
    // --------------------------------------- Open Modal ---------------------------------------//
    openEditModal(id) {
        this.selectedItem = this.product_data.filter(product => product.id == id)[0];
        this.selectedItem.productTypeSelect = [this.selectedItem.product_type_id];
        this.selectedItem.storeSelect = [this.selectedItem.store_id];
        this.editModal.show();
    }
    openDeleteModal(id) {
        console.log('openDeleteModal', id);
        this.selectedItem = this.product_data.filter(product => product.id === id)[0];
        console.log('selectedItem', this.selectedItem);
        this.deleteModal.show();
    }
    saveChange() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = true;
            console.log(' this.selectedItem', this.selectedItem);
            try {
                let res_edit_product = yield this.allService.putWithId('product', this.selectedItem.id, this.selectedItem);
                console.log('res_edit_product', res_edit_product);
                this.showSuccess('แก้ไขข้อมูลเรียบร้อย');
                this.getData();
            }
            catch (error) {
                console.log('Error update data product type!', error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.saveChange();
                }
                else {
                    this.showError('กรุณาลองใหม่อีกครั้ง');
                }
            }
            this.editModal.hide();
        });
    }
    deleteItem() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('delete data', this.selectedItem);
            this.isLoading = true;
            try {
                let res_delete_product = yield this.allService.deleteWithId('product', this.selectedItem.id);
                yield this.showSuccess('ลบข้อมูลเรียบร้อย');
                this.product_data = this.product_data.filter(product => product.id !== this.selectedItem.id);
                this.selectedItem = null;
                this.deleteModal.hide();
                this.isLoading = false;
            }
            catch (error) {
                console.log("Error delete data prodict type!", error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.deleteItem();
                }
                else {
                    this.showError('ไม่สามารถลบข้อมูลได้');
                    this.deleteModal.hide();
                }
            }
        });
    }
    // --------------------------------------- Toast ---------------------------------------//
    showSuccess(message) {
        this.toastr.success(message, 'สำเร็จ!');
    }
    showError(message) {
        this.toastr.error(message, 'เกิดข้อผิดพลาด');
    }
    //--------------------------------------- Show Detail ---------------------------------------//
    showDetail(id) {
        this.router.navigate(['product/manage/list/info', id]);
    }
    //--------------------------------------- Print Barcode ---------------------------------------//
    printBarcode(id) {
        console.log('this.selected', this.selected);
        let navigationExtras;
        let url;
        if (id) {
            url = JSON.stringify({ data: [this.product_data.filter(product => product.id == id)[0]] });
        }
        else {
            url = JSON.stringify({ data: this.selected });
        }
        window.open("/#/product/print?dataSelect=" + url, "_blank");
    }
};
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3__angular_core__["ViewChild"])('editModal'), 
    __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_4_ng2_bootstrap__["f" /* ModalDirective */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4_ng2_bootstrap__["f" /* ModalDirective */]) === 'function' && _a) || Object)
], ManageProductComponent.prototype, "editModal", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3__angular_core__["ViewChild"])('deleteModal'), 
    __metadata('design:type', (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4_ng2_bootstrap__["f" /* ModalDirective */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4_ng2_bootstrap__["f" /* ModalDirective */]) === 'function' && _b) || Object)
], ManageProductComponent.prototype, "deleteModal", void 0);
ManageProductComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3__angular_core__["Component"])({
        selector: 'app-manage-product',
        template: __webpack_require__(1236),
        styles: [__webpack_require__(1195)]
    }), 
    __metadata('design:paramtypes', [(typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_5__angular_router__["b" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_5__angular_router__["b" /* Router */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_6__shared_hide_menu_service__["a" /* HideMenuService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_6__shared_hide_menu_service__["a" /* HideMenuService */]) === 'function' && _d) || Object, (typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__["a" /* AllServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__["a" /* AllServiceService */]) === 'function' && _e) || Object, (typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr__["ToastsManager"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr__["ToastsManager"]) === 'function' && _f) || Object, (typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_3__angular_core__["ViewContainerRef"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__angular_core__["ViewContainerRef"]) === 'function' && _g) || Object, (typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_0__angular_forms__["FormBuilder"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_forms__["FormBuilder"]) === 'function' && _h) || Object])
], ManageProductComponent);
var _a, _b, _c, _d, _e, _f, _g, _h;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/manage-product.component.js.map

/***/ }),

/***/ 872:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(13);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PrintBarcodeComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


let PrintBarcodeComponent = class PrintBarcodeComponent {
    constructor(route) {
        this.Heading = 'พิมพ์ Barcode';
        this.barcodeSelect = [];
        console.log(route.snapshot.data);
        route.queryParams.subscribe(params => {
            this.dataSelect = JSON.parse(params["dataSelect"]).data;
            console.log(this.dataSelect);
            this.setFormat();
        });
    }
    ngOnInit() {
    }
    setFormat() {
        for (let i = 0; i < this.dataSelect.length; i++) {
            let temp = {
                // 'barcode': 'http://www.pion.co.th/images/Knowledges/Barcode/barcode01.jpg',
                'barcode': this.dataSelect[i].barcode,
                'code': this.dataSelect[i].code,
                'id': this.dataSelect[i].id,
                'name': this.dataSelect[i].name,
                'quantity': '1'
            };
            this.barcodeSelect.push(temp);
        }
        this.barcodeSelect.map(() => { });
        this.oldItem = this.barcodeSelect;
    }
    removeItem(index) {
        this.barcodeSelect.splice(index, 1);
        console.log('barcodeSelect', this.barcodeSelect);
    }
    print() {
        let printContents, popupWin;
        printContents = document.getElementById('print-section').innerHTML;
        // printContents = this.printSection;
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open('', '_blank');
        popupWin.document.write(`
      <html>
        <head>
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
          <link href="https://fonts.googleapis.com/css?familyTrirong" rel="stylesheet">
          <link href="https://fonts.googleapis.com/css?family=Nunito+Sans" rel="stylesheet">
          <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
          
          <style>
            // @page {
            //   size:  auto;   /* auto is the initial value */
            //   margin: 5mm;  /* this affects the margin in the printer settings */
            // }
            // html, body {
            //   font-family: 'Nunito Sans', 'Trirong', 'FontAwesome';
            //   width:98%;
            //   background: #FFF; 
            //   font-size: 9.5pt;
            // }
            .panel-body .row {
              margin: 0px;
            }

            .panel-body .row .eachBarcode {
              border: 1px solid;
              text-align: center;
              margin-left: -1px;
              margin-bottom: -1px;
              width: 20%;
              padding: 10px 15px 5px 15px;
              float: left;
            }

            .panel-body .row .eachBarcode img {
              padding: 0px;
              height: 25;
              margin-bottom: 5px;
            }

            .panel-body .row .eachBarcode p {
              font-size: x-small;
              color: black;
              margin: 0px;
              line-height: 10px;
            }
            
          </style>
        </head>
      <body onload="window.print();window.close()">${printContents}</body>
      </html>`);
        popupWin.document.close();
    }
    printFormat() {
        let printContents, popupWin;
        printContents = document.getElementById('print-section').innerHTML;
        // printContents = this.printSection;
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open('', '_blank');
        popupWin.document.write(`
      <html>
        <head>
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
          <link href="https://fonts.googleapis.com/css?familyTrirong" rel="stylesheet">
          <link href="https://fonts.googleapis.com/css?family=Nunito+Sans" rel="stylesheet">
          <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
          
          <style>
            // @page {
            //   size:  auto;   /* auto is the initial value */
            //   margin: 5mm;  /* this affects the margin in the printer settings */
            // }
            // html, body {
            //   font-family: 'Nunito Sans', 'Trirong', 'FontAwesome';
            //   width:98%;
            //   background: #FFF; 
            //   font-size: 9.5pt;
            // }
            .panel-body {
              padding-top: 1.65cm;
              padding-left: 3mm;
              padding-right: 4mm;
            }

            .panel-body .row {
              margin: 0px;
            }

            .panel-body .row .eachBarcode {
             
              text-align: center;
              width: 38mm;
              height: 22mm;
              float: left;
              padding-left: 10px;
              padding-right: 10px;
              display: flex;
              flex-direction: column;
              justify-content: center;
              margin-right: 3mm;
            }

            .panel-body .row .eachBarcode:nth-child(5n){
              margin: 0px;
            }

            .panel-body .row .eachBarcode img {
              padding: 0px;
              height: 25;
              margin-bottom: 5px;
              width: 100%;
            }

            .panel-body .row .eachBarcode p {
              font-size: x-small;
              color: black;
              margin: 0px;
              line-height: 10px;
            }
            
          </style>
        </head>
      <body onload="window.print();window.close()">${printContents}</body>
      </html>`);
        popupWin.document.close();
    }
};
PrintBarcodeComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-print-barcode',
        template: __webpack_require__(1237),
        styles: [__webpack_require__(1196)]
    }), 
    __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* ActivatedRoute */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* ActivatedRoute */]) === 'function' && _a) || Object])
], PrintBarcodeComponent);
var _a;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/print-barcode.component.js.map

/***/ }),

/***/ 873:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ng2_validation__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ng2_validation___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_ng2_validation__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_bootstrap__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_debounceTime__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_debounceTime___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_debounceTime__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_distinctUntilChanged__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_distinctUntilChanged___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_distinctUntilChanged__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_add_observable_of__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_add_observable_of___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_rxjs_add_observable_of__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddSellProductComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};










let AddSellProductComponent = class AddSellProductComponent {
    constructor(fb, allService, toastr, vcr) {
        this.fb = fb;
        this.allService = allService;
        this.toastr = toastr;
        this.vcr = vcr;
        this.Heading = 'เพิ่มการขายสินค้า';
        this.isLoading = false;
        this.asyncSelected = '';
        this.toastr.setRootViewContainerRef(vcr);
        //validate FormBuilder
        this.product_id = fb.control('', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].required]));
        this.sell_type = fb.control('1', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].required]));
        this.sell_price = fb.control('', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_0_ng2_validation__["CustomValidators"].number, __WEBPACK_IMPORTED_MODULE_0_ng2_validation__["CustomValidators"].min(0)]));
        this.amount = fb.control('1', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_0_ng2_validation__["CustomValidators"].number, __WEBPACK_IMPORTED_MODULE_0_ng2_validation__["CustomValidators"].min(0)]));
        this.sellProductForm = fb.group({
            'amount': this.amount,
            'remark': this.remark,
            'product_id': this.product_id,
            'shop_source_id': this.shop_source_id,
            'customer_id': this.customer_id,
            'sell_price': this.sell_price,
            'sell_type': this.sell_type
        });
        //validate FormBuilder Modal add customer
        this.name_customer = fb.control('', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].required]));
        this.tel_customer = fb.control('', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].required]));
        this.customerForm = fb.group({
            'name_customer': this.name_customer,
            'tel_customer': this.tel_customer
        });
    }
    ngOnInit() {
        this.setData();
        this.loadData();
    }
    setData() {
        this.data = {
            'amount': '',
            'remark': '',
            'product_id': '',
            'shop_source_id': '',
            'customer_id': '',
            'sell_price': '',
            'sell_type': ''
        };
        this.productSelected = {
            code: '',
            name: '',
            sell_price: 0
        };
        this.customerSelected = {
            name: ''
        };
        this.typeaheadLoading = {
            'product': false,
            'customer': false
        };
        this.typeaheadNoResults = {
            'product': false,
            'customer': false
        };
        this.amount_value = 1;
        this.price = 0;
    }
    loadData() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = true;
            try {
                let resp_product_data = yield this.allService.get('product');
                let resp_customer_data = yield this.allService.get('customer');
                console.log('resp_product_data', resp_product_data);
                console.log('resp_customer_data', resp_customer_data);
                this.products = resp_product_data.data;
                this.customers = resp_customer_data.data;
                this.isLoading = false;
            }
            catch (error) {
                console.log('error get data', error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.loadData();
                }
                else {
                    this.isLoading = false;
                    this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
                }
            }
        });
    }
    sendForm() {
        return __awaiter(this, void 0, void 0, function* () {
            let data = this.sellProductForm.value;
            console.log('data', this.sellProductForm.value);
            this.isLoading = !this.isLoading;
            try {
                data.shop_source_id = this.allService.getLocal('user').shop_id;
                data.product_id = this.productSelected.id;
                data.customer_id = this.customerSelected.id ? this.customerSelected.id : null;
                data.sell_type = parseInt(this.sellProductForm.value.sell_type);
                console.log('data', data);
                let res_sell_product = yield this.allService.post('productTransections', data);
                // this.sellProductForm.reset();
                this.setData();
                this.isLoading = !this.isLoading;
                this.showSuccess(`บันทึกการขายสินค้าสำเร็จแล้ว`);
            }
            catch (error) {
                let response = error;
                console.log("Error create product type", response);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.sendForm();
                }
                else {
                    this.isLoading = !this.isLoading;
                    this.showError(`ไม่สามารถบันทึการขายสินค้าได้ กรุณาลองใหม่อีกครั้ง`);
                }
            }
        });
    }
    // --------------------------------------- Toast ---------------------------------------//
    showSuccess(message) {
        this.toastr.success(message, 'สำเร็จ!');
    }
    showError(message) {
        this.toastr.error(message, 'เกิดข้อผิดพลาด');
    }
    //--------------------------------------- Typeahead ---------------------------------------//
    changeTypeaheadLoading(status, e) {
        this.typeaheadLoading[status] = e;
    }
    changeTypeaheadNoResults(status, e) {
        this.typeaheadNoResults[status] = e;
    }
    typeaheadOnSelect(status, e) {
        switch (status) {
            case 'product':
                this.productSelected = Object.assign({}, e.item);
                this.multipleValue();
                break;
            case 'customer':
                this.customerSelected = Object.assign({}, e.item);
                break;
        }
        console.log('Selected value:', e);
    }
    //--------------------------------------- Modal ---------------------------------------//
    openAddCustomerModal() {
        this.editModal.show();
    }
    saveCustomer() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('save data', this.customerForm.value);
            let customer_data = {
                'name': this.customerForm.value.name_customer,
                'tel': this.customerForm.value.tel_customer,
            };
            try {
                let res_edit_customer = yield this.allService.post('customer', customer_data);
                this.showSuccess('แก้ไขข้อมูลเรียบร้อย');
                this.customerForm.reset();
                this.loadData();
            }
            catch (error) {
                console.log('error add customer', error);
                if (error.status === 500) {
                    this.showError('ไม่สามารถเพิ่มลูกค้าได้ กรุณาลองใหม่อีกครั้ง');
                }
                else if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.saveCustomer();
                }
                else {
                    this.showError('กรุณาลองใหม่อีกครั้ง');
                }
            }
            this.editModal.hide();
        });
    }
    multipleValue() {
        console.log(this.productSelected.sell_price, this.amount_value);
        console.log(this.productSelected.sell_price * this.amount_value);
        this.price = this.productSelected.sell_price * this.amount_value;
    }
};
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3__angular_core__["ViewChild"])('editModal'), 
    __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_4_ng2_bootstrap__["f" /* ModalDirective */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4_ng2_bootstrap__["f" /* ModalDirective */]) === 'function' && _a) || Object)
], AddSellProductComponent.prototype, "editModal", void 0);
AddSellProductComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3__angular_core__["Component"])({
        selector: 'app-add-sell-product',
        template: __webpack_require__(1238),
        styles: [__webpack_require__(1197)]
    }), 
    __metadata('design:paramtypes', [(typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_5__angular_forms__["FormBuilder"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_5__angular_forms__["FormBuilder"]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__["a" /* AllServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__["a" /* AllServiceService */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr__["ToastsManager"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr__["ToastsManager"]) === 'function' && _d) || Object, (typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__angular_core__["ViewContainerRef"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__angular_core__["ViewContainerRef"]) === 'function' && _e) || Object])
], AddSellProductComponent);
var _a, _b, _c, _d, _e;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/add-sell-product.component.js.map

/***/ }),

/***/ 874:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_hide_menu_service__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service_all_service_service__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ng2_validation__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ng2_validation___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_ng2_validation__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_moment__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SellProductComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};








let SellProductComponent = class SellProductComponent {
    constructor(hideMenuService, allService, toastr, vcr, fb) {
        this.hideMenuService = hideMenuService;
        this.allService = allService;
        this.toastr = toastr;
        this.vcr = vcr;
        this.fb = fb;
        this.Heading = 'การขายสินค้า';
        this.isLoading = false;
        //data
        this.data = null;
        //table
        this.dtOptions = {};
        this.asyncSelected = '';
        this.shop_data = [];
        this.myDateRangePickerOptions = {
            dateFormat: 'dd/mm/yyyy',
            firstDayOfWeek: 'su'
        };
        this.dateRange = {
            beginDate: { year: '', month: '', day: '' },
            endDate: { year: '', month: '', day: '' }
        };
        this.toastr.setRootViewContainerRef(vcr);
        this.loadData();
        this.isHideMenu = true;
        hideMenuService.subscribe({
            next: isHideMenu => {
                this.isHideMenu = isHideMenu;
                return this.isHideMenu;
            }
        });
        //validate FormBuilder
        // this.product_id = fb.control('', Validators.compose([Validators.required]));
        this.sell_type = fb.control('1', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].required]));
        this.sell_price = fb.control('', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_6_ng2_validation__["CustomValidators"].number, __WEBPACK_IMPORTED_MODULE_6_ng2_validation__["CustomValidators"].min(0)]));
        this.amount = fb.control('1', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_6_ng2_validation__["CustomValidators"].number, __WEBPACK_IMPORTED_MODULE_6_ng2_validation__["CustomValidators"].min(0)]));
        this.sellProductForm = fb.group({
            'amount': this.amount,
            'remark': this.remark,
            'product_id': this.product_id,
            'shop_source_id': this.shop_source_id,
            'customer_id': this.customer_id,
            'sell_price': this.sell_price,
            'sell_type': this.sell_type
        });
        //validate FormBuilder Modal add customer
        this.name_customer = fb.control('', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].required]));
        this.tel_customer = fb.control('', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].required]));
        this.customerForm = fb.group({
            'name_customer': this.name_customer,
            'tel_customer': this.tel_customer
        });
    }
    ngOnInit() {
        this.initData();
        this.initTable();
    }
    initData() {
        this.productSelected = {
            code: '',
            name: ''
        };
        this.customerSelected = {
            name: ''
        };
        this.typeaheadNoResults = {
            'product': false,
            'customer': false
        };
        this.selectedItem = null;
        this.dateRange = {
            beginDate: { year: __WEBPACK_IMPORTED_MODULE_7_moment__().year(), month: __WEBPACK_IMPORTED_MODULE_7_moment__().month() + 1, day: __WEBPACK_IMPORTED_MODULE_7_moment__().date() },
            endDate: { year: __WEBPACK_IMPORTED_MODULE_7_moment__().year(), month: __WEBPACK_IMPORTED_MODULE_7_moment__().month() + 1, day: __WEBPACK_IMPORTED_MODULE_7_moment__().date() }
        };
        this.dateSelect = {
            'date_from': this.setFormatDate(__WEBPACK_IMPORTED_MODULE_7_moment__().format('YYYY-MM-DD')),
            'date_to': this.setFormatDate(__WEBPACK_IMPORTED_MODULE_7_moment__().format('YYYY-MM-DD'))
        };
    }
    loadData() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.getData();
        });
    }
    initTable() {
        this.dtOptions = {
            displayLength: 10,
            bSort: false,
            paginationType: 'simple_numbers',
            language: {
                info: 'แสดง _START_ ถึง _END_ จาก _TOTAL_',
                paginate: {
                    previous: '<',
                    next: '>',
                },
                search: 'ค้นหา',
                emptyTable: 'ไม่มีข้อมูล',
                infoEmpty: 'แสดง 0 ถึง 0 จาก 0',
                zeroRecords: 'ไม่มีข้อมูล',
                lengthMenu: 'แสดง _MENU_ แถว'
            }
        };
    }
    getData() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = true;
            this.data = null;
            try {
                let res_get_customer = yield this.allService.get('customer');
                let res_get_sell_product = yield this.allService.get('productTransections/getSellTransection', this.dateSelect);
                this.customers = res_get_customer.data;
                this.data = res_get_sell_product.data;
                for (var i = 0; i < this.data.length; i++) {
                    this.data[i].date = __WEBPACK_IMPORTED_MODULE_7_moment__(this.data[i].created_at).format("YYYY-MM-DD");
                }
                console.log('res_get_sell_product', this.data);
                this.isLoading = false;
            }
            catch (error) {
                console.log("Error get data prodict type!", error);
                if (error.status === 404) {
                    this.isLoading = false;
                    this.showError('กรุณาลองใหม่อีกครั้ง');
                }
                else if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.getData();
                }
                else {
                    this.isLoading = false;
                    this.showError('กรุณาลองใหม่อีกครั้ง');
                }
            }
        });
    }
    multipleValue() {
        this.price = this.selectedItem.sell_price * this.amount_value;
    }
    // --------------------------------------- Open Modal ---------------------------------------//
    openEditModal(id) {
        let data_select = Object.assign({}, this.data.filter(data => data.id == id)[0]);
        this.productSelected.name = data_select.product.name;
        this.customerSelected.name = data_select.customer ? data_select.customer.name : null;
        this.sellProductForm.value.sell_type = data_select.sell_type;
        this.price = data_select.sell_price;
        this.amount_value = data_select.amount;
        this.selectedItem = Object.assign({}, data_select);
        console.log('selectedItem', this.selectedItem);
        this.editModal.show();
    }
    openDeleteModal(id) {
        this.selectedItem = Object.assign({}, this.data.filter(data => data.id == id)[0]);
        this.deleteModal.show();
    }
    saveChange() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = true;
            console.log('this.selectedItem', this.selectedItem);
            console.log('this.productSelected', this.productSelected);
            try {
                let data = Object.assign({}, this.selectedItem, this.sellProductForm.value);
                console.log('this.sellProductForm.value', this.sellProductForm.value);
                data.product_id = this.productSelected.id || this.selectedItem.product_id;
                data.sell_type = parseInt(this.selectedItem.sell_type);
                data.shop_source_id = this.allService.getLocal('user').shop_id;
                data.customer_id = this.customerSelected.id ? this.customerSelected.id : null;
                console.log('data', data);
                let res_tran_in_product = yield this.allService.putWithId('productTransections', this.selectedItem.id, data);
                this.initData();
                this.getData();
                this.showSuccess(`แก้ไขข้อมูลเรียบร้อย`);
            }
            catch (error) {
                let response = error;
                console.log("Error create product type", response);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.saveChange();
                }
                else {
                    this.isLoading = false;
                    this.showError(`ไม่สามารถแก้ไขได้ กรุณาลองใหม่อีกครั้ง`);
                }
            }
            this.editModal.hide();
        });
    }
    deleteItem() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('delete data', this.selectedItem);
            this.isLoading = true;
            try {
                let res_delete_product_type = yield this.allService.deleteWithId('productTransections', this.selectedItem.id);
                this.initData();
                this.getData();
                this.showSuccess('ลบข้อมูลเรียบร้อย');
            }
            catch (error) {
                console.log("Error delete data prodict type!", error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.deleteItem();
                }
                else {
                    this.isLoading = false;
                    this.showError('ไม่สามารถลบข้อมูลได้');
                }
            }
            this.deleteModal.hide();
        });
    }
    // --------------------------------------- Toast ---------------------------------------//
    showSuccess(message) {
        this.toastr.success(message, 'สำเร็จ!');
    }
    showError(message) {
        this.toastr.error(message, 'เกิดข้อผิดพลาด');
    }
    //--------------------------------------- Typeahead ---------------------------------------//
    changeTypeaheadLoading(status, e) {
        this.typeaheadLoading[status] = e;
    }
    changeTypeaheadNoResults(status, e) {
        this.typeaheadNoResults[status] = e;
    }
    typeaheadOnSelect(status, e) {
        console.log('e', e);
        switch (status) {
            case 'product':
                this.productSelected = e.item;
                break;
            case 'customer':
                this.customerSelected = e.item;
                break;
        }
        console.log('Selected value:', e);
    }
    // --------------------------------------- date ---------------------------------------//
    onDateRangeChanged(event) {
        this.dateSelect = {
            'date_from': this.setFormatDate(event.beginDate),
            'date_to': this.setFormatDate(event.endDate)
        };
        this.getData();
    }
    setFormatDate(data) {
        if (typeof data === 'object') {
            return data.year + '-' + data.month + '-' + data.day;
        }
        else if (typeof data === 'string') {
            return data;
        }
    }
};
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('editModal'), 
    __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["f" /* ModalDirective */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["f" /* ModalDirective */]) === 'function' && _a) || Object)
], SellProductComponent.prototype, "editModal", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('deleteModal'), 
    __metadata('design:type', (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["f" /* ModalDirective */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["f" /* ModalDirective */]) === 'function' && _b) || Object)
], SellProductComponent.prototype, "deleteModal", void 0);
SellProductComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-sell-product',
        template: __webpack_require__(1239),
        styles: [__webpack_require__(1198)]
    }), 
    __metadata('design:paramtypes', [(typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__shared_hide_menu_service__["a" /* HideMenuService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__shared_hide_menu_service__["a" /* HideMenuService */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__service_all_service_service__["a" /* AllServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__service_all_service_service__["a" /* AllServiceService */]) === 'function' && _d) || Object, (typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr__["ToastsManager"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr__["ToastsManager"]) === 'function' && _e) || Object, (typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"]) === 'function' && _f) || Object, (typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_5__angular_forms__["FormBuilder"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_5__angular_forms__["FormBuilder"]) === 'function' && _g) || Object])
], SellProductComponent);
var _a, _b, _c, _d, _e, _f, _g;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/sell-product.component.js.map

/***/ }),

/***/ 875:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ng2_validation__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ng2_validation___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_ng2_validation__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_bootstrap__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_debounceTime__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_debounceTime___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_debounceTime__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_distinctUntilChanged__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_distinctUntilChanged___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_rxjs_add_operator_distinctUntilChanged__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_add_observable_of__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_add_observable_of___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_rxjs_add_observable_of__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddTransferProductComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};










let AddTransferProductComponent = class AddTransferProductComponent {
    constructor(fb, allService, toastr, vcr) {
        this.fb = fb;
        this.allService = allService;
        this.toastr = toastr;
        this.vcr = vcr;
        this.Heading = 'เพิ่มการโอนสินค้า';
        this.isLoading = false;
        this.asyncSelected = '';
        this.toastr.setRootViewContainerRef(vcr);
        //validate FormBuilder
        this.product_id = fb.control('', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].required]));
        this.product_detail = fb.control({ value: '', disabled: true });
        this.amount = fb.control('1', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_0_ng2_validation__["CustomValidators"].number, __WEBPACK_IMPORTED_MODULE_0_ng2_validation__["CustomValidators"].min(0)]));
        this.transferProductForm = fb.group({
            'amount': this.amount,
            'remark': this.remark,
            'product_id': this.product_id,
            'shop_source_id': this.shop_source_id,
            'shop_destination_id': this.shop_destination_id,
            'product_detail': this.product_detail
        });
    }
    ngOnInit() {
        this.setData();
        this.loadData();
    }
    setData() {
        this.data = {
            'amount': '',
            'remark': '',
            'product_id': '',
            'shop_source_id': '',
            'shop_destination_id': '',
            'sell_price': '',
            'sell_type': '',
            'product_detail': ''
        };
        this.productSelected = {
            code: '',
            name: '',
            sell_price: 0
        };
        this.shopDestinationSelected = {
            name: ''
        };
        this.typeaheadLoading = {
            'product': false,
            'shopDestination': false
        };
        this.typeaheadNoResults = {
            'product': false,
            'shopDestination': false
        };
    }
    loadData() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = true;
            try {
                let resp_product_data = yield this.allService.get('product');
                let resp_shopDestination_data = yield this.allService.get('shop');
                console.log('resp_product_data', resp_product_data);
                console.log('resp_shopDestination_data', resp_shopDestination_data);
                this.products = resp_product_data.data;
                this.shopDestinations = resp_shopDestination_data.data.filter(item => item.id !== this.allService.getLocal('user').shop_id);
                console.log('this.shopDestinations', this.shopDestinations);
                this.isLoading = false;
            }
            catch (error) {
                console.log('error get data', error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.loadData();
                }
                else {
                    this.isLoading = false;
                    this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
                }
            }
        });
    }
    sendForm() {
        return __awaiter(this, void 0, void 0, function* () {
            let data = this.transferProductForm.value;
            console.log('data', this.transferProductForm.value);
            this.isLoading = true;
            try {
                data.shop_source_id = this.allService.getLocal('user').shop_id;
                data.product_id = this.productSelected.id;
                console.log('data', data);
                let res_sell_product = yield this.allService.post('productTransections', data);
                // this.transferProductForm.reset();
                this.setData();
                this.isLoading = false;
                this.showSuccess(`บันทึกการโอนสินค้าสำเร็จแล้ว`);
            }
            catch (error) {
                let response = error;
                console.log("Error create product type", response);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.sendForm();
                }
                else {
                    this.isLoading = false;
                    this.showError(`ไม่สามารถบันทึการโอนสินค้าได้ กรุณาลองใหม่อีกครั้ง`);
                }
            }
        });
    }
    // --------------------------------------- Toast ---------------------------------------//
    showSuccess(message) {
        this.toastr.success(message, 'สำเร็จ!');
    }
    showError(message) {
        this.toastr.error(message, 'เกิดข้อผิดพลาด');
    }
    //--------------------------------------- Typeahead ---------------------------------------//
    changeTypeaheadLoading(status, e) {
        this.typeaheadLoading[status] = e;
    }
    changeTypeaheadNoResults(status, e) {
        this.typeaheadNoResults[status] = e;
    }
    typeaheadOnSelect(status, e) {
        switch (status) {
            case 'product':
                this.productSelected = Object.assign({}, e.item);
                break;
            case 'shopDestination':
                this.shopDestinationSelected = Object.assign({}, e.item);
                break;
        }
        console.log('Selected value:', e);
    }
};
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3__angular_core__["ViewChild"])('editModal'), 
    __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_4_ng2_bootstrap__["f" /* ModalDirective */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4_ng2_bootstrap__["f" /* ModalDirective */]) === 'function' && _a) || Object)
], AddTransferProductComponent.prototype, "editModal", void 0);
AddTransferProductComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3__angular_core__["Component"])({
        selector: 'app-add-transfer-product',
        template: __webpack_require__(1240),
        styles: [__webpack_require__(1199)]
    }), 
    __metadata('design:paramtypes', [(typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_5__angular_forms__["FormBuilder"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_5__angular_forms__["FormBuilder"]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__["a" /* AllServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__["a" /* AllServiceService */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr__["ToastsManager"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr__["ToastsManager"]) === 'function' && _d) || Object, (typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__angular_core__["ViewContainerRef"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__angular_core__["ViewContainerRef"]) === 'function' && _e) || Object])
], AddTransferProductComponent);
var _a, _b, _c, _d, _e;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/add-transfer-product.component.js.map

/***/ }),

/***/ 876:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_hide_menu_service__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service_all_service_service__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ng2_validation__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ng2_validation___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_ng2_validation__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_moment__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TransferProductComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};








let TransferProductComponent = class TransferProductComponent {
    constructor(hideMenuService, allService, toastr, vcr, fb) {
        this.hideMenuService = hideMenuService;
        this.allService = allService;
        this.toastr = toastr;
        this.vcr = vcr;
        this.fb = fb;
        this.Heading = 'การโอนสินค้า';
        this.isLoading = false;
        //data
        this.data = null;
        //table
        this.dtOptions = {};
        this.asyncSelected = '';
        this.myDateRangePickerOptions = {
            dateFormat: 'dd/mm/yyyy',
            firstDayOfWeek: 'su'
        };
        this.dateRange = {
            beginDate: { year: '', month: '', day: '' },
            endDate: { year: '', month: '', day: '' }
        };
        this.toastr.setRootViewContainerRef(vcr);
        this.loadData();
        //validate FormBuilder
        this.product_id = fb.control('', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].required]));
        this.amount = fb.control('1', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_5__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_6_ng2_validation__["CustomValidators"].number, __WEBPACK_IMPORTED_MODULE_6_ng2_validation__["CustomValidators"].min(0)]));
        this.transferProductForm = fb.group({
            'amount': this.amount,
            'remark': this.remark,
            'product_id': this.product_id,
            'shop_source_id': this.shop_source_id,
            'shop_destination_id': this.shop_destination_id,
        });
    }
    ngOnInit() {
        this.initData();
        this.initTable();
    }
    initData() {
        this.productSelected = {
            code: '',
            name: ''
        };
        this.shopDestinationSelected = {
            name: ''
        };
        this.typeaheadNoResults = {
            'product': false,
            'shopDestination': false
        };
        this.selectedItem = null;
        this.dateRange = {
            beginDate: { year: __WEBPACK_IMPORTED_MODULE_7_moment__().year(), month: __WEBPACK_IMPORTED_MODULE_7_moment__().month() + 1, day: __WEBPACK_IMPORTED_MODULE_7_moment__().date() },
            endDate: { year: __WEBPACK_IMPORTED_MODULE_7_moment__().year(), month: __WEBPACK_IMPORTED_MODULE_7_moment__().month() + 1, day: __WEBPACK_IMPORTED_MODULE_7_moment__().date() }
        };
        this.dateSelect = {
            'date_from': this.setFormatDate(__WEBPACK_IMPORTED_MODULE_7_moment__().format('YYYY-MM-DD')),
            'date_to': this.setFormatDate(__WEBPACK_IMPORTED_MODULE_7_moment__().format('YYYY-MM-DD'))
        };
    }
    loadData() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.getDataForEdit();
            yield this.getData();
        });
    }
    initTable() {
        this.dtOptions = {
            displayLength: 10,
            bSort: false,
            paginationType: 'simple_numbers',
            language: {
                info: 'แสดง _START_ ถึง _END_ จาก _TOTAL_',
                paginate: {
                    previous: '<',
                    next: '>',
                },
                search: 'ค้นหา',
                emptyTable: 'ไม่มีข้อมูล',
                infoEmpty: 'แสดง 0 ถึง 0 จาก 0',
                zeroRecords: 'ไม่มีข้อมูล',
                lengthMenu: 'แสดง _MENU_ แถว'
            }
        };
    }
    getDataForEdit() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = true;
            try {
                let resp_product_data = yield this.allService.get('product');
                let resp_shopDestination_data = yield this.allService.get('shop');
                console.log('resp_product_data', resp_product_data);
                console.log('resp_shopDestination_data', resp_shopDestination_data);
                this.products = resp_product_data.data;
                this.shopDestinations = resp_shopDestination_data.data.filter(item => item.id !== this.allService.getLocal('user').shop_id);
                this.isLoading = false;
            }
            catch (error) {
                console.log('error get data', error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.getDataForEdit();
                }
                else {
                    this.isLoading = false;
                    this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
                }
            }
        });
    }
    getData() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = true;
            console.log('loadData');
            try {
                let res_get_sell_product = yield this.allService.get('productTransections/getProductTransectionTransfer', this.dateSelect);
                this.data = res_get_sell_product.data;
                for (var i = 0; i < this.data.length; i++) {
                    this.data[i].date = __WEBPACK_IMPORTED_MODULE_7_moment__(this.data[i].created_at).format("YYYY-MM-DD");
                }
                console.log('res_get_sell_product', this.data);
                this.isLoading = false;
            }
            catch (error) {
                console.log("Error get data prodict type!", error);
                if (error.status === 404) {
                    this.isLoading = false;
                    this.showError('กรุณาลองใหม่อีกครั้ง');
                }
                else if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.getData();
                }
                else {
                    this.isLoading = false;
                    this.showError('กรุณาลองใหม่อีกครั้ง');
                }
            }
        });
    }
    // --------------------------------------- Open Modal ---------------------------------------//
    openEditModal(id) {
        let data_select = Object.assign({}, this.data.filter(data => data.id == id)[0]);
        this.productSelected.name = data_select.product.name;
        // this.shopDestinationSelected.name = data_select.shop_destination ? data_select.shop_destination.name : null;
        this.selectedItem = Object.assign({}, data_select);
        console.log('selectedItem', this.selectedItem);
        this.editModal.show();
    }
    openDeleteModal(id) {
        this.selectedItem = Object.assign({}, this.data.filter(data => data.id == id)[0]);
        this.deleteModal.show();
    }
    saveChange() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = true;
            console.log('this.selectedItem', this.selectedItem);
            console.log('this.productSelected', this.productSelected);
            try {
                let data = Object.assign({}, this.selectedItem, this.transferProductForm.value);
                console.log('this.transferProductForm.value', this.transferProductForm.value);
                data.product_id = this.productSelected.id || this.selectedItem.product_id;
                data.shop_source_id = this.allService.getLocal('user').shop_id;
                data.shop_destination_id = this.shopDestinationSelected.id || this.selectedItem.shop_destination_id;
                console.log('data', data);
                let res_tran_in_product = yield this.allService.putWithId('productTransections', this.selectedItem.id, data);
                this.initData();
                this.getData();
                this.showSuccess(`แก้ไขข้อมูลเรียบร้อย`);
            }
            catch (error) {
                let response = error;
                console.log("Error create product type", response);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.saveChange();
                }
                else {
                    this.isLoading = false;
                    this.showError(`ไม่สามารถแก้ไขได้ กรุณาลองใหม่อีกครั้ง`);
                }
            }
            this.editModal.hide();
        });
    }
    deleteItem() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('delete data', this.selectedItem);
            this.isLoading = true;
            try {
                let res_delete_product_type = yield this.allService.deleteWithId('productTransections', this.selectedItem.id);
                this.initData();
                this.getData();
                this.showSuccess('ลบข้อมูลเรียบร้อย');
            }
            catch (error) {
                console.log("Error delete data prodict type!", error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.deleteItem();
                }
                else {
                    this.isLoading = false;
                    this.showError('ไม่สามารถลบข้อมูลได้');
                }
            }
            this.deleteModal.hide();
        });
    }
    // --------------------------------------- Toast ---------------------------------------//
    showSuccess(message) {
        this.toastr.success(message, 'สำเร็จ!');
    }
    showError(message) {
        this.toastr.error(message, 'เกิดข้อผิดพลาด');
    }
    //--------------------------------------- Typeahead ---------------------------------------//
    changeTypeaheadLoading(status, e) {
        this.typeaheadLoading[status] = e;
    }
    changeTypeaheadNoResults(status, e) {
        this.typeaheadNoResults[status] = e;
    }
    typeaheadOnSelect(status, e) {
        switch (status) {
            case 'product':
                this.productSelected = Object.assign({}, e.item);
                break;
            case 'shopDestination':
                this.shopDestinationSelected = Object.assign({}, e.item);
                break;
        }
        console.log('Selected value:', e);
    }
    // --------------------------------------- date ---------------------------------------//
    onDateRangeChanged(event) {
        this.dateSelect = {
            'date_from': this.setFormatDate(event.beginDate),
            'date_to': this.setFormatDate(event.endDate)
        };
        this.getData();
    }
    setFormatDate(data) {
        if (typeof data === 'object') {
            return data.year + '-' + data.month + '-' + data.day;
        }
        else if (typeof data === 'string') {
            return data;
        }
    }
};
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('editModal'), 
    __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["f" /* ModalDirective */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["f" /* ModalDirective */]) === 'function' && _a) || Object)
], TransferProductComponent.prototype, "editModal", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('deleteModal'), 
    __metadata('design:type', (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["f" /* ModalDirective */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1_ng2_bootstrap__["f" /* ModalDirective */]) === 'function' && _b) || Object)
], TransferProductComponent.prototype, "deleteModal", void 0);
TransferProductComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-transfer-product',
        template: __webpack_require__(1241),
        styles: [__webpack_require__(1200)]
    }), 
    __metadata('design:paramtypes', [(typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__shared_hide_menu_service__["a" /* HideMenuService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__shared_hide_menu_service__["a" /* HideMenuService */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__service_all_service_service__["a" /* AllServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__service_all_service_service__["a" /* AllServiceService */]) === 'function' && _d) || Object, (typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr__["ToastsManager"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4_ng2_toastr_ng2_toastr__["ToastsManager"]) === 'function' && _e) || Object, (typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"]) === 'function' && _f) || Object, (typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_5__angular_forms__["FormBuilder"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_5__angular_forms__["FormBuilder"]) === 'function' && _g) || Object])
], TransferProductComponent);
var _a, _b, _c, _d, _e, _f, _g;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/transfer-product.component.js.map

/***/ }),

/***/ 877:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_toastr_ng2_toastr__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ng2_toastr_ng2_toastr__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_moment__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReportAllComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};





let ReportAllComponent = class ReportAllComponent {
    constructor(allService, toastr, vcr, router) {
        this.allService = allService;
        this.toastr = toastr;
        this.vcr = vcr;
        this.router = router;
        this.Heading = 'รายงานทั้งหมด';
        this.isLoading = false;
        //data
        this.data = null;
        //table
        this.dtOptions = {};
        this.toastr.setRootViewContainerRef(vcr);
        this.isLoading = false;
    }
    ngOnInit() {
        this.initTable();
        this.loadService();
    }
    loadService() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.setCurrentDate();
            yield this.setDataToGet('today');
        });
    }
    initTable() {
        this.dtOptions = {
            displayLength: 10,
            paginationType: 'simple_numbers',
            language: {
                info: 'แสดง _START_ ถึง _END_ จาก _TOTAL_',
                paginate: {
                    previous: '<',
                    next: '>',
                },
                search: 'ค้นหา',
                emptyTable: 'ไม่มีข้อมูล',
                infoEmpty: 'แสดง 0 ถึง 0 จาก 0',
                zeroRecords: 'ไม่มีข้อมูล',
                lengthMenu: 'แสดง _MENU_ แถว'
            },
            ordering: false,
            searching: false,
            lengthChange: false
        };
    }
    setCurrentDate() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('current date', __WEBPACK_IMPORTED_MODULE_4_moment__().format());
            let today = {
                'year': parseInt(__WEBPACK_IMPORTED_MODULE_4_moment__().format('YYYY')),
                'month': parseInt(__WEBPACK_IMPORTED_MODULE_4_moment__().format('MM')),
                'day': parseInt(__WEBPACK_IMPORTED_MODULE_4_moment__().format('DD')),
            };
            this.today = this.setFormatDate(today);
            this.date_start = Object.assign({}, today);
            this.date_end = Object.assign({}, today);
        });
    }
    setFormatDate(data) {
        if (typeof data === 'object') {
            return data.year + '-' + data.month + '-' + data.day;
        }
        else if (typeof data === 'string') {
            return data;
        }
    }
    setDataToGet(status) {
        return __awaiter(this, void 0, void 0, function* () {
            switch (status) {
                case 'thisMonth':
                    this.dateSelect = {
                        'date_from': __WEBPACK_IMPORTED_MODULE_4_moment__().startOf("month").format('YYYY-MM-DD'),
                        'date_to': __WEBPACK_IMPORTED_MODULE_4_moment__().endOf("month").format('YYYY-MM-DD')
                    };
                    break;
                case 'lastMonth':
                    this.dateSelect = {
                        'date_from': __WEBPACK_IMPORTED_MODULE_4_moment__().subtract(1, 'months').startOf("month").format('YYYY-MM-DD'),
                        'date_to': __WEBPACK_IMPORTED_MODULE_4_moment__().endOf("month").format('YYYY-MM-DD')
                    };
                    break;
                case 'twoMonthAgo':
                    this.dateSelect = {
                        'date_from': __WEBPACK_IMPORTED_MODULE_4_moment__().subtract(2, 'months').startOf("month").format('YYYY-MM-DD'),
                        'date_to': __WEBPACK_IMPORTED_MODULE_4_moment__().endOf("month").format('YYYY-MM-DD')
                    };
                    break;
                case 'today':
                    this.dateSelect = {
                        'date_from': this.today,
                        'date_to': this.today
                    };
                    break;
                case 'yesterday':
                    this.dateSelect = {
                        'date_from': __WEBPACK_IMPORTED_MODULE_4_moment__().subtract(1, 'days').format('YYYY-MM-DD'),
                        'date_to': this.today
                    };
                    break;
                case 'threeDayAgo':
                    this.dateSelect = {
                        'date_from': __WEBPACK_IMPORTED_MODULE_4_moment__().subtract(2, 'days').format('YYYY-MM-DD'),
                        'date_to': this.today
                    };
                    break;
                case 'fiveDayAgo':
                    this.dateSelect = {
                        'date_from': __WEBPACK_IMPORTED_MODULE_4_moment__().subtract(4, 'days').format('YYYY-MM-DD'),
                        'date_to': this.today
                    };
                    break;
                case 'sevenDayAgo':
                    this.dateSelect = {
                        'date_from': __WEBPACK_IMPORTED_MODULE_4_moment__().subtract(6, 'days').format('YYYY-MM-DD'),
                        'date_to': this.today
                    };
                    break;
            }
            console.log('dateSelect', this.dateSelect);
            this.date_start = this.setFormatDate(this.dateSelect.date_from);
            this.date_end = this.setFormatDate(this.dateSelect.date_to);
            this.getData();
        });
    }
    getData() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = !this.isLoading;
            console.log('loadData', this.dateSelect);
            try {
                let res_get_summary_all_type = yield this.allService.post('report/getSummary', this.dateSelect);
                this.data = res_get_summary_all_type.data;
                console.log('data', this.data);
                this.isLoading = false;
            }
            catch (error) {
                console.log("Error get data prodict type!", error);
                if (error.status === 404) {
                    this.isLoading = false;
                }
                else if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.getData();
                }
                else {
                    this.isLoading = false;
                }
            }
        });
    }
    goToDetail(item) {
        let navigationExtras = {
            queryParams: { 'shop_id': item.id, 'shop_name': item.name }
        };
        // Redirect the user
        this.router.navigate(['/report/all/list/info'], navigationExtras);
    }
    // --------------------------------------- date ---------------------------------------//
    onDateRangeChanged(event) {
        this.dateSelect = {
            'date_from': this.setFormatDate(event.beginDate),
            'date_to': this.setFormatDate(event.endDate)
        };
        this.getData();
    }
};
ReportAllComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
        selector: 'app-report-all',
        template: __webpack_require__(1242),
        styles: [__webpack_require__(1201)]
    }), 
    __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__["a" /* AllServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__["a" /* AllServiceService */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3_ng2_toastr_ng2_toastr__["ToastsManager"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3_ng2_toastr_ng2_toastr__["ToastsManager"]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewContainerRef"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewContainerRef"]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0__angular_router__["b" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_router__["b" /* Router */]) === 'function' && _d) || Object])
], ReportAllComponent);
var _a, _b, _c, _d;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/report-all.component.js.map

/***/ }),

/***/ 878:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_forms__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_moment__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReportDayComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};





let ReportDayComponent = class ReportDayComponent {
    constructor(allService, toastr, vcr, fb) {
        this.allService = allService;
        this.toastr = toastr;
        this.vcr = vcr;
        this.fb = fb;
        this.Heading = 'รายงาน';
        this.isLoading = false;
        //data
        this.data = null;
        //table
        this.dtOptions = {};
        this.dateSelect = {
            'date_from': '',
            'date_to': ''
        };
        this.toastr.setRootViewContainerRef(vcr);
        this.isLoading = false;
        this.reportDateForm = fb.group({
            'date_start_form': this.date_start_form,
            'date_end_form': this.date_end_form
        });
    }
    ngOnInit() {
        this.setCurrentDate();
        this.getReport('today');
    }
    //--------------------------------------- Date ---------------------------------------//
    setCurrentDate() {
        console.log('current date', __WEBPACK_IMPORTED_MODULE_4_moment__().format());
        let today = {
            'year': parseInt(__WEBPACK_IMPORTED_MODULE_4_moment__().format('YYYY')),
            'month': parseInt(__WEBPACK_IMPORTED_MODULE_4_moment__().format('MM')),
            'day': parseInt(__WEBPACK_IMPORTED_MODULE_4_moment__().format('DD')),
        };
        this.today = this.setFormatDate(today);
        this.date_start = Object.assign({}, today);
        this.date_end = Object.assign({}, today);
    }
    setFormatDate(data) {
        if (typeof data === 'object') {
            return data.year + '-' + data.month + '-' + data.day;
        }
        else if (typeof data === 'string') {
            return data;
        }
    }
    getReport(status) {
        // let dateSelect;
        switch (status) {
            case 'dateSelect':
                this.dateSelect = {
                    'date_from': this.reportDateForm.value.date_start,
                    'date_to': this.reportDateForm.value.date_end
                };
                break;
            case 'today':
                this.dateSelect = {
                    'date_from': this.today,
                    'date_to': this.today
                };
                break;
            case 'yesterday':
                this.dateSelect = {
                    'date_from': __WEBPACK_IMPORTED_MODULE_4_moment__().subtract(1, 'days').format('YYYY-MM-DD'),
                    'date_to': this.today
                };
                break;
            case 'threeDayAgo':
                this.dateSelect = {
                    'date_from': __WEBPACK_IMPORTED_MODULE_4_moment__().subtract(2, 'days').format('YYYY-MM-DD'),
                    'date_to': this.today
                };
                break;
            case 'fiveDayAgo':
                this.dateSelect = {
                    'date_from': __WEBPACK_IMPORTED_MODULE_4_moment__().subtract(4, 'days').format('YYYY-MM-DD'),
                    'date_to': this.today
                };
                break;
            case 'sevenDayAgo':
                this.dateSelect = {
                    'date_from': __WEBPACK_IMPORTED_MODULE_4_moment__().subtract(6, 'days').format('YYYY-MM-DD'),
                    'date_to': this.today
                };
                break;
        }
        console.log('this.dateSelect', this.dateSelect);
        this.date_start = this.setFormatDate(this.dateSelect.date_from);
        this.date_end = this.setFormatDate(this.dateSelect.date_to);
        this.getData(this.dateSelect);
    }
    getData(data) {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = !this.isLoading;
            console.log('loadData');
            this.data = data;
            let temp_data = this.data;
            try {
                this.data.shop_id = yield this.allService.getLocal('user').shop_id;
                let res_get_summary_type = yield this.allService.post('report/getSummary', this.data);
                this.data = res_get_summary_type.data;
                console.log('res_get_summary_type', this.data);
                this.isLoading = false;
            }
            catch (error) {
                console.log("Error get data prodict type!", error);
                if (error.status === 404) {
                    this.isLoading = false;
                }
                else if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.getData(temp_data);
                }
                else {
                    this.isLoading = false;
                }
            }
        });
    }
};
ReportDayComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3__angular_core__["Component"])({
        selector: 'app-report-day',
        template: __webpack_require__(1243),
        styles: [__webpack_require__(1202)]
    }), 
    __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__["a" /* AllServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__["a" /* AllServiceService */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr__["ToastsManager"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr__["ToastsManager"]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__angular_core__["ViewContainerRef"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__angular_core__["ViewContainerRef"]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0__angular_forms__["FormBuilder"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_forms__["FormBuilder"]) === 'function' && _d) || Object])
], ReportDayComponent);
var _a, _b, _c, _d;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/report-day.component.js.map

/***/ }),

/***/ 879:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_forms__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_moment__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReportMonthComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};





let ReportMonthComponent = class ReportMonthComponent {
    constructor(allService, toastr, vcr, fb) {
        this.allService = allService;
        this.toastr = toastr;
        this.vcr = vcr;
        this.fb = fb;
        this.Heading = 'รายงาน';
        this.isLoading = false;
        //data
        this.data = null;
        //table
        this.dtOptions = {};
        this.dateSelect = {
            'date_from': '',
            'date_to': ''
        };
        this.toastr.setRootViewContainerRef(vcr);
        this.isLoading = false;
        this.reportDateForm = fb.group({
            'date_start_form': this.date_start_form,
            'date_end_form': this.date_end_form
        });
    }
    ngOnInit() {
        this.loadService();
    }
    loadService() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.setCurrentDate();
            yield this.getReport('thisMonth');
        });
    }
    //--------------------------------------- Date ---------------------------------------//
    setCurrentDate() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('current date', __WEBPACK_IMPORTED_MODULE_4_moment__().format());
            let today = {
                'year': parseInt(__WEBPACK_IMPORTED_MODULE_4_moment__().format('YYYY')),
                'month': parseInt(__WEBPACK_IMPORTED_MODULE_4_moment__().format('MM')),
                'day': 1,
            };
            this.today = this.setFormatDate(today);
            this.date_start = Object.assign({}, today);
            this.date_end = Object.assign({}, today);
        });
    }
    setFormatDate(data) {
        if (typeof data === 'object') {
            return data.year + '-' + data.month + '-' + data.day;
        }
        else if (typeof data === 'string') {
            return data;
        }
    }
    getReport(status) {
        return __awaiter(this, void 0, void 0, function* () {
            // let dateSelect;
            switch (status) {
                case 'dateSelect':
                    this.dateSelect = {
                        'date_from': this.reportDateForm.value.date_start,
                        'date_to': this.reportDateForm.value.date_end
                    };
                    break;
                case 'thisMonth':
                    this.dateSelect = {
                        'date_from': __WEBPACK_IMPORTED_MODULE_4_moment__().startOf("month").format('YYYY-MM-DD'),
                        'date_to': __WEBPACK_IMPORTED_MODULE_4_moment__().endOf("month").format('YYYY-MM-DD')
                    };
                    break;
                case 'lastMonth':
                    this.dateSelect = {
                        'date_from': __WEBPACK_IMPORTED_MODULE_4_moment__().subtract(1, 'months').startOf("month").format('YYYY-MM-DD'),
                        'date_to': __WEBPACK_IMPORTED_MODULE_4_moment__().endOf("month").format('YYYY-MM-DD')
                    };
                    break;
                case 'twoMonthAgo':
                    this.dateSelect = {
                        'date_from': __WEBPACK_IMPORTED_MODULE_4_moment__().subtract(2, 'months').startOf("month").format('YYYY-MM-DD'),
                        'date_to': __WEBPACK_IMPORTED_MODULE_4_moment__().endOf("month").format('YYYY-MM-DD')
                    };
                    break;
            }
            console.log('dateSelect', this.dateSelect);
            this.date_start = this.setFormatDate(this.dateSelect.date_from);
            this.date_end = this.setFormatDate(this.dateSelect.date_to);
            this.getData(this.dateSelect);
        });
    }
    getData(data) {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = !this.isLoading;
            console.log('loadData');
            this.data = data;
            try {
                this.data.shop_id = yield this.allService.getLocal('user').shop_id;
                let res_get_summary_type = yield this.allService.post('report/getSummary', this.data);
                this.data = res_get_summary_type.data;
                console.log('res_get_summary_type', this.data);
                this.isLoading = false;
            }
            catch (error) {
                console.log("Error get data prodict type!", error);
                if (error.status === 404) {
                    this.isLoading = false;
                }
                else if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.getData(this.data);
                }
                else {
                    this.isLoading = false;
                }
            }
        });
    }
};
ReportMonthComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3__angular_core__["Component"])({
        selector: 'app-report-month',
        template: __webpack_require__(1244),
        styles: [__webpack_require__(1203)]
    }), 
    __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__["a" /* AllServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__["a" /* AllServiceService */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr__["ToastsManager"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr__["ToastsManager"]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__angular_core__["ViewContainerRef"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__angular_core__["ViewContainerRef"]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0__angular_forms__["FormBuilder"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_forms__["FormBuilder"]) === 'function' && _d) || Object])
], ReportMonthComponent);
var _a, _b, _c, _d;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/report-month.component.js.map

/***/ }),

/***/ 880:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_forms__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReportComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};







let ReportComponent = class ReportComponent {
    constructor(allService, toastr, vcr, fb, route) {
        this.allService = allService;
        this.toastr = toastr;
        this.vcr = vcr;
        this.fb = fb;
        this.route = route;
        this.Heading = 'รายงาน';
        this.isLoading = false;
        //data
        this.dataDay = null;
        this.dataMonth = null;
        //table
        this.dtOptions = {};
        this.month_form = {
            'start_m': null,
            'start_y': null,
            'end_m': null,
            'end_y': null
        };
        this.myDateRangePickerOptions = {
            dateFormat: 'dd/mm/yyyy',
            firstDayOfWeek: 'su',
            selectionTxtFontSize: '16px',
            inputValueRequired: true,
            clearBtnTxt: 'ล้าง',
            beginDateBtnTxt: 'วันเริ่มต้น',
            endDateBtnTxt: 'วันสิ้นสุด',
            acceptBtnTxt: 'ตกลง',
            selectBeginDateTxt: 'เลือกวันที่เริ่มต้น',
            selectEndDateTxt: 'เลือกวันที่สิ้นสุด',
            editableDateRangeField: false
        };
        this.toastr.setRootViewContainerRef(vcr);
        this.isLoading = false;
        route.queryParams.subscribe(params => {
            this.shop_id = params["shop_id"];
            this.shop_name = params["shop_name"];
            this.loadService();
            console.log('this.shop_id', this.shop_id);
        });
    }
    ngOnInit() {
        this.initTable();
        this.setInitData();
    }
    initTable() {
        this.dtOptions = {
            displayLength: 10,
            paginationType: 'simple_numbers',
            language: {
                info: 'แสดง _START_ ถึง _END_ จาก _TOTAL_',
                paginate: {
                    previous: '<',
                    next: '>',
                },
                search: 'ค้นหา',
                emptyTable: 'ไม่มีข้อมูล',
                infoEmpty: 'แสดง 0 ถึง 0 จาก 0',
                zeroRecords: 'ไม่มีข้อมูล',
                lengthMenu: 'แสดง _MENU_ แถว'
            },
            ordering: false,
            searching: false,
            lengthChange: false
        };
    }
    setInitData() {
        this.dateSelect = {
            'date_from': '',
            'date_to': '',
            'shop_id': ''
        };
        this.monthSelect = {
            'date_from': '',
            'date_to': '',
            'shop_id': ''
        };
    }
    loadService() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = true;
            yield this.setCurrentDate();
            yield this.getReportDay('today');
            // await this.getReportMonth('thisMonth');
            this.isLoading = false;
        });
    }
    //--------------------------------------- Date ---------------------------------------//
    setCurrentDate() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('current date', __WEBPACK_IMPORTED_MODULE_5_moment__().format());
            let today = {
                'year': parseInt(__WEBPACK_IMPORTED_MODULE_5_moment__().format('YYYY')),
                'month': parseInt(__WEBPACK_IMPORTED_MODULE_5_moment__().format('MM')),
                'day': parseInt(__WEBPACK_IMPORTED_MODULE_5_moment__().format('DD')),
            };
            this.today = this.setFormatDate(today);
            this.month_form = {
                'start_m': parseInt(__WEBPACK_IMPORTED_MODULE_5_moment__().format('MM')),
                'start_y': parseInt(__WEBPACK_IMPORTED_MODULE_5_moment__().format('YYYY')),
                'end_m': parseInt(__WEBPACK_IMPORTED_MODULE_5_moment__().format('MM')),
                'end_y': parseInt(__WEBPACK_IMPORTED_MODULE_5_moment__().format('YYYY'))
            };
        });
    }
    setFormatDate(data) {
        if (typeof data === 'object') {
            return data.year + '-' + data.month + '-' + data.day;
        }
        else if (typeof data === 'string') {
            return data;
        }
    }
    getReportDay(status, days) {
        return __awaiter(this, void 0, void 0, function* () {
            // let dateSelect;
            switch (status) {
                case 'today':
                    this.dateSelect = {
                        'date_from': this.today,
                        'date_to': this.today
                    };
                    break;
                case 'yesterday':
                    this.dateSelect = {
                        'date_from': __WEBPACK_IMPORTED_MODULE_5_moment__().subtract(1, 'days').format('YYYY-MM-DD'),
                        'date_to': this.today
                    };
                    break;
                case 'ago':
                    this.dateSelect = {
                        'date_from': __WEBPACK_IMPORTED_MODULE_5_moment__().subtract((days - 1), 'days').format('YYYY-MM-DD'),
                        'date_to': this.today
                    };
                    break;
            }
            console.log('this.dateSelect', this.dateSelect);
            this.date_start = this.setFormatDate(this.dateSelect.date_from);
            this.date_end = this.setFormatDate(this.dateSelect.date_to);
            this.getDayData();
        });
    }
    getReportMonth(status) {
        return __awaiter(this, void 0, void 0, function* () {
            // let dateSelect;
            switch (status) {
                case 'thisMonth':
                    this.monthSelect = {
                        'date_from': __WEBPACK_IMPORTED_MODULE_5_moment__().startOf("month").format('YYYY-MM-DD'),
                        'date_to': __WEBPACK_IMPORTED_MODULE_5_moment__().endOf("month").format('YYYY-MM-DD')
                    };
                    break;
                case 'lastMonth':
                    this.monthSelect = {
                        'date_from': __WEBPACK_IMPORTED_MODULE_5_moment__().subtract(1, 'months').startOf("month").format('YYYY-MM-DD'),
                        'date_to': __WEBPACK_IMPORTED_MODULE_5_moment__().endOf("month").format('YYYY-MM-DD')
                    };
                    break;
                case 'twoMonthAgo':
                    this.monthSelect = {
                        'date_from': __WEBPACK_IMPORTED_MODULE_5_moment__().subtract(2, 'months').startOf("month").format('YYYY-MM-DD'),
                        'date_to': __WEBPACK_IMPORTED_MODULE_5_moment__().endOf("month").format('YYYY-MM-DD')
                    };
                    break;
            }
            console.log('monthSelect', this.monthSelect);
            this.date_start = this.setFormatDate(this.monthSelect.date_from);
            this.date_end = this.setFormatDate(this.monthSelect.date_to);
            this.getMonthData();
        });
    }
    getDayData() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = true;
            console.log('loadData');
            try {
                if (this.shop_id) {
                    this.dateSelect.shop_id = this.shop_id;
                }
                else {
                    this.dateSelect.shop_id = yield this.allService.getLocal('user').shop_id;
                }
                let res_get_summary_day_type = yield this.allService.post('report/getSummary', this.dateSelect);
                this.dataDay = res_get_summary_day_type.data;
                console.log('dataDay', this.dataDay);
                this.isLoading = false;
            }
            catch (error) {
                console.log("Error get data prodict type!", error);
                if (error.status === 404) {
                    this.isLoading = false;
                }
                else if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.getDayData();
                }
                else {
                    this.isLoading = false;
                }
            }
        });
    }
    getMonthData() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = true;
            console.log('loadData');
            try {
                if (this.shop_id) {
                    this.monthSelect.shop_id = this.shop_id;
                }
                else {
                    this.monthSelect.shop_id = yield this.allService.getLocal('user').shop_id;
                }
                let res_get_summary_month_type = yield this.allService.post('report/getSummary', this.monthSelect);
                this.dataMonth = res_get_summary_month_type.data;
                console.log('dataMonth', this.dataMonth);
                this.isLoading = false;
            }
            catch (error) {
                console.log("Error get data prodict type!", error);
                if (error.status === 404) {
                    this.isLoading = false;
                }
                else if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.getMonthData();
                }
                else {
                    this.isLoading = false;
                }
            }
        });
    }
    // --------------------------------------- date ---------------------------------------//
    onDateRangeChanged(event) {
        console.log('onDateRangeChanged(): event: ', event);
        console.log('onDateRangeChanged(): Begin date: ', event.beginDate, ' End date: ', event.endDate);
        console.log('onDateRangeChanged(): Formatted: ', event.formatted);
        console.log('onDateRangeChanged(): BeginEpoc timestamp: ', event.beginEpoc, ' - endEpoc timestamp: ', event.endEpoc);
        this.dateSelect = {
            'date_from': this.setFormatDate(event.beginDate),
            'date_to': this.setFormatDate(event.endDate)
        };
        this.getDayData();
    }
    onMonthChange(event, status) {
        console.log('onMonthChange', event);
        console.log('onMonthChange status', status);
        switch (status) {
            case 'start':
                this.month_form.start_m = event;
                break;
            case 'end':
                this.month_form.end_m = event;
                break;
        }
        console.log('this.month_form', this.month_form);
    }
    onYearChange(event, status) {
        console.log('onYearChange', event);
        console.log('onYearChange status', status);
        switch (status) {
            case 'start':
                this.month_form.start_y = event;
                break;
            case 'end':
                this.month_form.end_y = event;
                break;
        }
        console.log('this.month_form', this.month_form);
    }
    setMonthSelect() {
        let start = this.month_form.start_m + '/' + this.month_form.start_y;
        let end = this.month_form.end_m + '/' + this.month_form.end_y;
        if (this.month_form.start_y === this.month_form.end_y && this.month_form.start_m <= this.month_form.end_m) {
            this.monthSelect = {
                'date_from': __WEBPACK_IMPORTED_MODULE_5_moment__(start, 'MM/YYYY').startOf("month").format('YYYY-MM-DD'),
                'date_to': __WEBPACK_IMPORTED_MODULE_5_moment__(end, 'MM/YYYY').endOf("month").format('YYYY-MM-DD')
            };
            this.getMonthData();
        }
        else if (this.month_form.start_y < this.month_form.end_y) {
            this.monthSelect = {
                'date_from': __WEBPACK_IMPORTED_MODULE_5_moment__(start, 'MM/YYYY').startOf("month").format('YYYY-MM-DD'),
                'date_to': __WEBPACK_IMPORTED_MODULE_5_moment__(end, 'MM/YYYY').endOf("month").format('YYYY-MM-DD')
            };
            this.getMonthData();
        }
        else {
            console.log('errror set date');
            this.showError('กรุณาเลือกเดือนเริ่มต้น น้อยกว่า เดือนสิ้นสุด');
        }
        console.log('this.monthSelect', this.monthSelect);
    }
    // --------------------------------------- Toast ---------------------------------------//
    showSuccess(message) {
        this.toastr.success(message, 'สำเร็จ!');
    }
    showError(message) {
        this.toastr.error(message, 'เกิดข้อผิดพลาด');
    }
};
ReportComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3__angular_core__["Component"])({
        selector: 'app-report',
        template: __webpack_require__(1245),
        styles: [__webpack_require__(1204)]
    }), 
    __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__["a" /* AllServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__["a" /* AllServiceService */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr__["ToastsManager"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr__["ToastsManager"]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__angular_core__["ViewContainerRef"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__angular_core__["ViewContainerRef"]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0__angular_forms__["FormBuilder"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_forms__["FormBuilder"]) === 'function' && _d) || Object, (typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__angular_router__["c" /* ActivatedRoute */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4__angular_router__["c" /* ActivatedRoute */]) === 'function' && _e) || Object])
], ReportComponent);
var _a, _b, _c, _d, _e;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/report.component.js.map

/***/ }),

/***/ 881:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ToastService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


let ToastService = class ToastService {
    constructor(toastr, vRef) {
        this.toastr = toastr;
        this.vRef = vRef;
    }
    showSuccess(toastr, message) {
        toastr.success(message, 'สำเร็จ!');
    }
    showError(toastr, message) {
        toastr.error(message, 'เกิดข้อผิดพลาด');
    }
};
ToastService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(), 
    __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr__["ToastsManager"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1_ng2_toastr_ng2_toastr__["ToastsManager"]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"]) === 'function' && _b) || Object])
], ToastService);
var _a, _b;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/toast.service.js.map

/***/ }),

/***/ 882:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ArrayNumberPipe; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

let ArrayNumberPipe = class ArrayNumberPipe {
    transform(value, args) {
        let res = [];
        for (let i = 0; i < value; i++) {
            res.push(i);
        }
        return res;
    }
};
ArrayNumberPipe = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
        name: 'arrayNumber'
    }), 
    __metadata('design:paramtypes', [])
], ArrayNumberPipe);
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/array-number.pipe.js.map

/***/ }),

/***/ 883:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_toastr_ng2_toastr__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ng2_toastr_ng2_toastr__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddStoreComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};




let AddStoreComponent = class AddStoreComponent {
    constructor(fb, allService, toastr, vcr) {
        this.fb = fb;
        this.allService = allService;
        this.toastr = toastr;
        this.vcr = vcr;
        this.Heading = 'เพิ่มร้านค้า';
        this.isLoading = false;
        this.toastr.setRootViewContainerRef(vcr);
        //validate FormBuilder
        this.name = fb.control('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required]));
        this.tel = fb.control('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required]));
        this.address = fb.control('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required]));
        this.storeForm = fb.group({
            'name': this.name,
            'tel': this.tel,
            'address': this.address
        });
    }
    ngOnInit() {
        this.setData();
    }
    setData() {
        this.data = {
            'name': '',
            'tel': '',
            'address': ''
        };
    }
    sendForm() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('data : ', this.data);
            this.isLoading = true;
            try {
                let res_add_store = yield this.allService.post('shop', this.storeForm.value);
                console.log('response create store : ', res_add_store);
                this.storeForm.reset();
                this.isLoading = false;
                this.showSuccess(`ทำการเพิ่มร้าน ${res_add_store.data.name} สำเร็จแล้ว`);
            }
            catch (error) {
                console.log("Error create store", error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.sendForm();
                }
                else {
                    this.isLoading = false;
                    this.showError(`ไม่สามารถเพิ่มร้านค้าได้ กรุณาลองใหม่อีกครั้ง`);
                }
            }
        });
    }
    // --------------------------------------- Toast ---------------------------------------//
    showSuccess(message) {
        this.toastr.success(message, 'สำเร็จ!');
    }
    showError(message) {
        this.toastr.error(message, 'เกิดข้อผิดพลาด');
    }
};
AddStoreComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-add-store',
        template: __webpack_require__(1247),
        styles: [__webpack_require__(1206)]
    }), 
    __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__["a" /* AllServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__service_all_service_service__["a" /* AllServiceService */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3_ng2_toastr_ng2_toastr__["ToastsManager"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3_ng2_toastr_ng2_toastr__["ToastsManager"]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"]) === 'function' && _d) || Object])
], AddStoreComponent);
var _a, _b, _c, _d;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/add-store.component.js.map

/***/ }),

/***/ 884:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng2_bootstrap__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_hide_menu_service__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_all_service_service__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_toastr_ng2_toastr__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_ng2_toastr_ng2_toastr__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StoreComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};






let StoreComponent = class StoreComponent {
    constructor(fb, hideMenuService, allService, toastr, vcr) {
        this.fb = fb;
        this.hideMenuService = hideMenuService;
        this.allService = allService;
        this.toastr = toastr;
        this.vcr = vcr;
        this.Heading = 'ร้านค้า';
        this.isLoading = false;
        //data
        this.data = null;
        //table
        this.dtOptions = {};
        this.toastr.setRootViewContainerRef(vcr);
        this.getData();
        this.isHideMenu = true;
        hideMenuService.subscribe({
            next: isHideMenu => {
                this.isHideMenu = isHideMenu;
                return this.isHideMenu;
            }
        });
        //validate FormBuilder
        this.name = fb.control('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required]));
        this.tel = fb.control('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required]));
        this.address = fb.control('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required]));
        this.storeForm = fb.group({
            'name': this.name,
            'tel': this.tel,
            'address': this.address
        });
    }
    ngOnInit() {
        this.initTable();
    }
    initTable() {
        this.dtOptions = {
            displayLength: 10,
            bSort: false,
            paginationType: 'simple_numbers',
            language: {
                info: 'แสดง _START_ ถึง _END_ จาก _TOTAL_',
                paginate: {
                    previous: '<',
                    next: '>',
                },
                search: 'ค้นหา',
                emptyTable: 'ไม่มีข้อมูล',
                infoEmpty: 'แสดง 0 ถึง 0 จาก 0',
                zeroRecords: 'ไม่มีข้อมูล',
                lengthMenu: 'แสดง _MENU_ แถว'
            }
        };
    }
    getData() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = !this.isLoading;
            try {
                let res_get_store = yield this.allService.get('shop');
                console.log('Get data shop : ', res_get_store.data);
                this.data = res_get_store.data;
                this.isLoading = false;
            }
            catch (error) {
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.getData();
                }
                else {
                    this.isLoading = false;
                    this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
                }
            }
        });
    }
    // --------------------------------------- Open Modal ---------------------------------------//
    openEditModal(id) {
        this.selectedItem = this.data.filter(data => data.id == id)[0];
        this.editModal.show();
    }
    openDeleteModal(id) {
        this.selectedItem = this.data.filter(data => data.id == id)[0];
        this.deleteModal.show();
    }
    saveChange() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('save data', this.storeForm.value);
            this.isLoading = true;
            console.log('loadData');
            try {
                let res_edit_store = yield this.allService.putWithId('shop', this.selectedItem.id, this.storeForm.value);
                this.showSuccess('แก้ไขข้อมูลเรียบร้อย');
                this.getData();
            }
            catch (error) {
                console.log('error edit store');
                if (error.status === 500) {
                    this.showError('ไม่พบร้านค้า กรุณาลองใหม่อีกครั้ง');
                    this.isLoading = false;
                }
                else if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.saveChange();
                }
                else {
                    this.showError('กรุณาลองใหม่อีกครั้ง');
                    this.isLoading = false;
                }
            }
            this.editModal.hide();
        });
    }
    deleteItem() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('delete data', this.selectedItem);
            this.isLoading = true;
            try {
                let res_delete_store = yield this.allService.deleteWithId('shop', this.selectedItem.id);
                this.showSuccess('ลบข้อมูลเรียบร้อย');
                this.getData();
            }
            catch (error) {
                console.log("Error delete data shop!", error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.deleteItem();
                }
                else {
                    this.showError('ไม่สามารถลบข้อมูลได้');
                    this.isLoading = false;
                }
            }
            this.deleteModal.hide();
        });
    }
    // --------------------------------------- Toast ---------------------------------------//
    showSuccess(message) {
        this.toastr.success(message, 'สำเร็จ!');
    }
    showError(message) {
        this.toastr.error(message, 'เกิดข้อผิดพลาด');
    }
};
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('editModal'), 
    __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2_ng2_bootstrap__["f" /* ModalDirective */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2_ng2_bootstrap__["f" /* ModalDirective */]) === 'function' && _a) || Object)
], StoreComponent.prototype, "editModal", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('deleteModal'), 
    __metadata('design:type', (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2_ng2_bootstrap__["f" /* ModalDirective */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2_ng2_bootstrap__["f" /* ModalDirective */]) === 'function' && _b) || Object)
], StoreComponent.prototype, "deleteModal", void 0);
StoreComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-store',
        template: __webpack_require__(1248),
        styles: [__webpack_require__(1207)]
    }), 
    __metadata('design:paramtypes', [(typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__shared_hide_menu_service__["a" /* HideMenuService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__shared_hide_menu_service__["a" /* HideMenuService */]) === 'function' && _d) || Object, (typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__service_all_service_service__["a" /* AllServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4__service_all_service_service__["a" /* AllServiceService */]) === 'function' && _e) || Object, (typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_5_ng2_toastr_ng2_toastr__["ToastsManager"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_5_ng2_toastr_ng2_toastr__["ToastsManager"]) === 'function' && _f) || Object, (typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"]) === 'function' && _g) || Object])
], StoreComponent);
var _a, _b, _c, _d, _e, _f, _g;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/store.component.js.map

/***/ }),

/***/ 885:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__shared_hide_sidebar_service__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service_all_service_service__ = __webpack_require__(4);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserGuardService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




let UserGuardService = class UserGuardService {
    constructor(router, allService, hideSidebarService) {
        this.router = router;
        this.allService = allService;
        this.hideSidebarService = hideSidebarService;
    }
    canActivate() {
        if (this.allService.getLocal('user')) {
            if (this.allService.getLocal('user').shop_id) {
                this.isHideSidebar = false;
                this.router.navigate(['/report/all/list']);
                return true;
            }
            else {
                this.router.navigate(['/report/list']);
                return false;
            }
        }
        else {
            this.isHideSidebar = true;
            this.router.navigate(['/login']);
        }
        this.hideSidebarService.next(this.isHideSidebar);
    }
};
UserGuardService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(), 
    __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__service_all_service_service__["a" /* AllServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__service_all_service_service__["a" /* AllServiceService */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__shared_hide_sidebar_service__["a" /* HideSidebarService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__shared_hide_sidebar_service__["a" /* HideSidebarService */]) === 'function' && _c) || Object])
], UserGuardService);
var _a, _b, _c;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/user-guard.service.js.map

/***/ }),

/***/ 886:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng2_validation__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng2_validation___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ng2_validation__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_toastr_ng2_toastr__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ng2_toastr_ng2_toastr__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_all_service_service__ = __webpack_require__(4);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserAddComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};





let UserAddComponent = class UserAddComponent {
    constructor(fb, allService, toastr, vcr) {
        this.fb = fb;
        this.allService = allService;
        this.toastr = toastr;
        this.vcr = vcr;
        this.Heading = 'ผู้ใช้งาน';
        this.isLoading = false;
        this.shop_data = [];
        this.toastr.setRootViewContainerRef(vcr);
        this.loadData();
        //Validate Form
        this.username = fb.control('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required]));
        this.password = fb.control('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_2_ng2_validation__["CustomValidators"].rangeLength([6, 20])]));
        this.repassword = fb.control('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_2_ng2_validation__["CustomValidators"].rangeLength([6, 20]), __WEBPACK_IMPORTED_MODULE_2_ng2_validation__["CustomValidators"].equalTo(this.password)]));
        this.name = fb.control('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required]));
        // this.shop_id = fb.control('', Validators.compose([Validators.required]));
        // this.detail = fb.control('', Validators.compose([Validators.required]));
        // this.permission = fb.control('', Validators.compose([Validators.required]));
        this.userPermissionForm = fb.group({
            'username': this.username,
            'password': this.password,
            'repassword': this.repassword,
            'shop_id': this.shop_id,
            'name': this.name,
            'detail': this.detail,
        });
    }
    ngOnInit() {
        this.setData();
    }
    setData() {
        this.data = {
            'username': '',
            'password': '',
            'repassword': '',
            'shop_id': '',
            'permission': ''
        };
    }
    loadData() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = true;
            try {
                let resp_shop_data = yield this.allService.get('shop');
                console.log('resp_shop_data', resp_shop_data);
                this.shops = resp_shop_data.data;
                if (this.shops.length > 0) {
                    for (let i = 0; i < this.shops.length; i++) {
                        this.shop_data.push({ 'value': this.shops[i].id, 'label': this.shops[i].name });
                    }
                }
                this.isLoading = false;
            }
            catch (error) {
                console.log('error get shop', error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.loadData();
                }
                else {
                    this.showSuccess('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
                }
            }
        });
    }
    sendForm() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('data', this.userPermissionForm.value);
            this.isLoading = true;
            try {
                let resp_add_user = yield this.allService.post('user', this.userPermissionForm.value);
                console.log('resp_add_user', resp_add_user);
                this.userPermissionForm.reset();
                this.isLoading = false;
                this.showSuccess(`เพิ่มผู้ใช้ ${resp_add_user.data.name} สำเร็จแล้ว`);
            }
            catch (error) {
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.sendForm();
                }
                else {
                    console.log("Error create user", error);
                    this.isLoading = false;
                    this.showError(`ไม่สามารถเพิ่มผู้ใช้ได้ กรุณาลองใหม่อีกครั้ง`);
                }
            }
        });
    }
    // --------------------------------------- Toast ---------------------------------------//
    showSuccess(message) {
        this.toastr.success(message, 'สำเร็จ!');
    }
    showError(message) {
        this.toastr.error(message, 'เกิดข้อผิดพลาด');
    }
};
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(), 
    __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormControl"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormControl"]) === 'function' && _a) || Object)
], UserAddComponent.prototype, "formControl", void 0);
UserAddComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-user-add',
        template: __webpack_require__(1249),
        styles: [__webpack_require__(1208)]
    }), 
    __metadata('design:paramtypes', [(typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4__service_all_service_service__["a" /* AllServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4__service_all_service_service__["a" /* AllServiceService */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3_ng2_toastr_ng2_toastr__["ToastsManager"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3_ng2_toastr_ng2_toastr__["ToastsManager"]) === 'function' && _d) || Object, (typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewContainerRef"]) === 'function' && _e) || Object])
], UserAddComponent);
var _a, _b, _c, _d, _e;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/user-add.component.js.map

/***/ }),

/***/ 887:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ng2_validation__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ng2_validation___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_ng2_validation__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_bootstrap__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_hide_menu_service__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_all_service_service__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ng2_toastr_ng2_toastr__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ng2_toastr_ng2_toastr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_ng2_toastr_ng2_toastr__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserPermissionComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator.throw(value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments)).next());
    });
};







let UserPermissionComponent = class UserPermissionComponent {
    constructor(fb, hideMenuService, allService, toastr, vcr) {
        this.fb = fb;
        this.hideMenuService = hideMenuService;
        this.allService = allService;
        this.toastr = toastr;
        this.vcr = vcr;
        this.Heading = 'ผู้ใช้งาน';
        this.isLoading = false;
        this.shop_data = [];
        //data
        this.data = null;
        //table
        this.dtOptions = {};
        this.changePassword = false;
        this.toastr.setRootViewContainerRef(vcr);
        this.loadData();
        this.isHideMenu = true;
        hideMenuService.subscribe({
            next: isHideMenu => {
                this.isHideMenu = isHideMenu;
                return this.isHideMenu;
            }
        });
        //Validate Form
        this.username = fb.control('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required]));
        this.password = fb.control('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_0_ng2_validation__["CustomValidators"].rangeLength([6, 20])]));
        this.repassword = fb.control('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_0_ng2_validation__["CustomValidators"].rangeLength([6, 20]), __WEBPACK_IMPORTED_MODULE_0_ng2_validation__["CustomValidators"].equalTo(this.password)]));
        // this.shop_id = fb.control('', Validators.compose([Validators.required]));
        this.name = fb.control('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required]));
        this.detail = fb.control('', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["Validators"].required]));
        // this.permission = fb.control('', Validators.compose([Validators.required]));
        this.userPermissionForm = fb.group({
            'username': this.username,
            'password': this.password,
            'repassword': this.repassword,
            'shop_id': this.shop_id,
            'name': this.name,
            'detail': this.detail,
            'changePass': this.changePass,
        });
    }
    ngOnInit() {
        this.dtOptions = {
            displayLength: 10,
            paginationType: 'simple_numbers',
            language: {
                info: 'แสดง _START_ ถึง _END_ จาก _TOTAL_',
                paginate: {
                    previous: '<',
                    next: '>',
                },
                search: 'ค้นหา',
                emptyTable: 'ไม่มีข้อมูล',
                infoEmpty: 'แสดง 0 ถึง 0 จาก 0',
                zeroRecords: 'ไม่มีข้อมูล',
                lengthMenu: 'แสดง _MENU_ แถว'
            },
        };
    }
    setDefault() {
        this.selectedItem = {
            'id': '',
            'username': '',
            'name': '',
            'shop': '',
            'shop_id': '',
            'detail': '',
            'password': '',
            'repassword': ''
        };
        this.shop_data = [];
    }
    loadData() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isLoading = true;
            try {
                let resp_user_data = yield this.allService.get('user');
                let resp_store_data = yield this.allService.get('shop');
                console.log('resp_user_data', resp_user_data);
                console.log('resp_store_data', resp_store_data);
                this.data = resp_user_data.data;
                this.shops = resp_store_data.data;
                for (let i = 0; i < this.shops.length; i++) {
                    this.shop_data.push({ 'value': this.shops[i].id, 'label': this.shops[i].name });
                }
                console.log('shop_data', this.shop_data);
                this.isLoading = false;
            }
            catch (error) {
                console.log('error get userPermission', error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.loadData();
                }
                else {
                    this.isLoading = false;
                    this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
                }
            }
        });
    }
    checkChangePassword() {
        this.changePassword = !this.changePassword;
    }
    //--------------------------------------- Open Modal ---------------------------------------//
    openEditModal(item) {
        console.log('item', item);
        console.log('data', this.data);
        this.selectedItem = {
            'id': item.id,
            'username': item.username,
            'name': item.name,
            'shop': item.shop.name,
            'shop_id': item.shop_id.toString(),
            'detail': item.detail,
            'password': '',
            'repassword': ''
        };
        console.log('this.selectedItem', this.selectedItem);
        this.editModal.show();
    }
    openDeleteModal(item) {
        this.selectedItem = {
            'id': item.id,
            'username': item.username,
            'name': item.name,
            'shop': item.shop.name,
            'shop_id': item.shop_id.toString(),
            'detail': item.detail,
            'password': '',
            'repassword': ''
        };
        console.log('this.selectedItem', this.selectedItem);
        this.deleteModal.show();
    }
    saveChange() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('save data', this.selectedItem);
            this.isLoading = true;
            let data = null;
            if (this.changePassword) {
                data = this.selectedItem;
            }
            else {
                data = {
                    'detail': this.selectedItem.detail,
                    'id': this.selectedItem.id,
                    'name': this.selectedItem.name,
                    'shop_id': this.selectedItem.shop_id,
                    'username': this.selectedItem.username
                };
            }
            try {
                console.log('data', data);
                let resp_edit_user = yield this.allService.putWithId('user', this.selectedItem.id, data);
                console.log('Update data user : ', resp_edit_user.data);
                this.showSuccess('แก้ไขข้อมูลเรียบร้อย');
                this.setDefault();
                this.loadData();
            }
            catch (error) {
                console.log('error edit', error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.saveChange();
                }
                else {
                    this.isLoading = false;
                    this.showError('ไม่สามารถแก้ไขข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
                }
            }
            this.editModal.hide();
        });
    }
    deleteItem() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('delete data', this.selectedItem);
            this.isLoading = !this.isLoading;
            try {
                let resp_delete_user = yield this.allService.deleteWithId('user', this.selectedItem.id);
                console.log('Delete data user : ', resp_delete_user.data);
                this.showSuccess('ลบข้อมูลเรียบร้อย');
                this.setDefault();
                this.loadData();
            }
            catch (error) {
                console.log("Error delete data user!", error);
                if (error.status === 401) {
                    yield this.allService.refreshToken();
                    this.deleteItem();
                }
                else {
                    this.isLoading = false;
                    this.showError('ไม่สามารถลบข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
                }
            }
            this.deleteModal.hide();
        });
    }
    // --------------------------------------- Toast ---------------------------------------//
    showSuccess(message) {
        this.toastr.success(message, 'สำเร็จ!');
    }
    showError(message) {
        this.toastr.error(message, 'เกิดข้อผิดพลาด');
    }
};
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["ViewChild"])('editModal'), 
    __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3_ng2_bootstrap__["f" /* ModalDirective */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3_ng2_bootstrap__["f" /* ModalDirective */]) === 'function' && _a) || Object)
], UserPermissionComponent.prototype, "editModal", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["ViewChild"])('deleteModal'), 
    __metadata('design:type', (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3_ng2_bootstrap__["f" /* ModalDirective */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3_ng2_bootstrap__["f" /* ModalDirective */]) === 'function' && _b) || Object)
], UserPermissionComponent.prototype, "deleteModal", void 0);
UserPermissionComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
        selector: 'app-user-permission',
        template: __webpack_require__(1250),
        styles: [__webpack_require__(1209)]
    }), 
    __metadata('design:paramtypes', [(typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormBuilder"]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__shared_hide_menu_service__["a" /* HideMenuService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4__shared_hide_menu_service__["a" /* HideMenuService */]) === 'function' && _d) || Object, (typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_5__service_all_service_service__["a" /* AllServiceService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_5__service_all_service_service__["a" /* AllServiceService */]) === 'function' && _e) || Object, (typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_6_ng2_toastr_ng2_toastr__["ToastsManager"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_6_ng2_toastr_ng2_toastr__["ToastsManager"]) === 'function' && _f) || Object, (typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_2__angular_core__["ViewContainerRef"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__angular_core__["ViewContainerRef"]) === 'function' && _g) || Object])
], UserPermissionComponent);
var _a, _b, _c, _d, _e, _f, _g;
//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/user-permission.component.js.map

/***/ }),

/***/ 888:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.
const environment = {
    production: false
};
/* harmony export (immutable) */ __webpack_exports__["a"] = environment;

//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/environment.js.map

/***/ }),

/***/ 889:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_core_js_es6_symbol__ = __webpack_require__(916);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_core_js_es6_symbol___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_core_js_es6_symbol__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_core_js_es6_object__ = __webpack_require__(909);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_core_js_es6_object___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_core_js_es6_object__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_core_js_es6_function__ = __webpack_require__(905);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_core_js_es6_function___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_core_js_es6_function__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_core_js_es6_parse_int__ = __webpack_require__(911);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_core_js_es6_parse_int___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_core_js_es6_parse_int__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_core_js_es6_parse_float__ = __webpack_require__(910);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_core_js_es6_parse_float___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_core_js_es6_parse_float__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_core_js_es6_number__ = __webpack_require__(908);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_core_js_es6_number___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_core_js_es6_number__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_core_js_es6_math__ = __webpack_require__(907);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_core_js_es6_math___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_core_js_es6_math__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_core_js_es6_string__ = __webpack_require__(915);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_core_js_es6_string___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_core_js_es6_string__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_core_js_es6_date__ = __webpack_require__(904);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_core_js_es6_date___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_core_js_es6_date__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_core_js_es6_array__ = __webpack_require__(903);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_core_js_es6_array___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_core_js_es6_array__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_core_js_es6_regexp__ = __webpack_require__(913);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_core_js_es6_regexp___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_core_js_es6_regexp__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_core_js_es6_map__ = __webpack_require__(906);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_core_js_es6_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_core_js_es6_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_core_js_es6_set__ = __webpack_require__(914);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_core_js_es6_set___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12_core_js_es6_set__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_core_js_es6_reflect__ = __webpack_require__(912);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_core_js_es6_reflect___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_13_core_js_es6_reflect__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_core_js_es7_reflect__ = __webpack_require__(917);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_core_js_es7_reflect___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_14_core_js_es7_reflect__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_zone_js_dist_zone__ = __webpack_require__(1315);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_zone_js_dist_zone___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_15_zone_js_dist_zone__);
















//# sourceMappingURL=/Users/Hohapo/Documents/Work/admin_Store/adminStore-project/src/polyfills.js.map

/***/ })

},[1317]);
//# sourceMappingURL=main.bundle.map