import { AdminStoreProjectPage } from './app.po';

describe('admin-store-project App', function() {
  let page: AdminStoreProjectPage;

  beforeEach(() => {
    page = new AdminStoreProjectPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
