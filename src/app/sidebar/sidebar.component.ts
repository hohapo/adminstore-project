import { AllServiceService } from './../service/all-service.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { HideMenuService } from '../shared/hide-menu.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  public isCollapsed = {
    'store': false,
    'product': false,
    'accept': false,
    'transfer': false,
    'sell': false,
    'change': false,
    'fix': false,
    'type': false,
    'customer': false,
    'expense': false,
    'report': false,
    'user': false
  };
  public isSelected = false;
  public router;

  public isHideMenu;

  public permission;

  constructor(private _router: Router, public hideMenuService: HideMenuService, public allService: AllServiceService) {
    this.router = _router;

    hideMenuService.subscribe({
      next: isHideMenu => {
        this.isHideMenu = isHideMenu;
        return this.isHideMenu;
      }
    });

  }

  ngOnInit() {
    this.init();
  }

  async init() {
    await this.checkPermission();

  }

  async checkPermission() {
    let user = await this.allService.getLocal('user');
    this.permission = user.shop_id !== null ? true : false;
  }

  checkCollapsed(tab) {
    for (var key in this.isCollapsed) {
      if (this.isCollapsed.hasOwnProperty(tab) && key === tab) {
        this.isCollapsed[key] = !this.isCollapsed[key];
      } else {
        this.isCollapsed[key] = false;
      }
    }
    return this.isCollapsed;
  }

}
