import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AllServiceService } from '../service/all-service.service';
import { CustomValidators } from 'ng2-validation';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'app-customer-add',
  templateUrl: './customer-add.component.html',
  styleUrls: ['./customer-add.component.scss']
})
export class CustomerAddComponent implements OnInit {

  public Heading = 'ลูกค้า';

  public isLoading: Boolean = false;

  public customerForm: FormGroup;
  public name: FormControl;
  public tel: FormControl;

  public data;

  constructor(private fb: FormBuilder, private allService: AllServiceService, public toastr: ToastsManager,
    public vcr: ViewContainerRef) {

    this.toastr.setRootViewContainerRef(vcr);

    //validate FormBuilder
    this.name = fb.control('', Validators.compose([Validators.required]));
    this.tel = fb.control('', Validators.compose([Validators.required]));

    this.customerForm = fb.group({
      'name': this.name,
      'tel': this.tel
    });

  }

  ngOnInit() {
    this.setData();
  }

  setData() {
    this.data = {
      'name': '',
      'tel': ''
    };
  }

  async sendForm() {
    console.log('data : ', this.data);
    this.isLoading = true

    try {
      let res_add_customer = await this.allService.post('customer', this.customerForm.value);
      this.customerForm.reset();
      this.isLoading = false;
      this.showSuccess(`เพิ่ม ${res_add_customer.data.name} สำเร็จแล้ว`);
    } catch (error) {
      console.log("Error create store", error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.sendForm();
      } else {
        this.showError('ไม่สามารถเพิ่มข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
        this.isLoading = false;
      }
    }

  }


  // --------------------------------------- Toast ---------------------------------------//
  showSuccess(message) {
    this.toastr.success(message, 'สำเร็จ!');
  }

  showError(message) {
    this.toastr.error(message, 'เกิดข้อผิดพลาด');
  }

}
