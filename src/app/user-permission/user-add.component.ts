
import { Component, OnInit, ViewContainerRef, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { AllServiceService } from './../service/all-service.service';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.scss']
})
export class UserAddComponent implements OnInit {

  public Heading = 'ผู้ใช้งาน';

  @Input() formControl: FormControl;

  public isLoading: Boolean = false;

  public userPermissionForm: FormGroup;
  public username: FormControl;
  public password: FormControl;
  public repassword: FormControl;
  public shop_id: FormControl;
  public name: FormControl;
  public detail: FormControl;
  public permission: FormControl;

  public data;
  public shops;
  public shop_data = [];

  constructor(private fb: FormBuilder, public allService: AllServiceService, public toastr: ToastsManager, public vcr: ViewContainerRef) {

    this.toastr.setRootViewContainerRef(vcr);

    this.loadData();

    //Validate Form
    this.username = fb.control('', Validators.compose([Validators.required]));
    this.password = fb.control('', Validators.compose([Validators.required, CustomValidators.rangeLength([6, 20])]));
    this.repassword = fb.control('', Validators.compose([Validators.required, CustomValidators.rangeLength([6, 20]), CustomValidators.equalTo(this.password)]));
    this.name = fb.control('', Validators.compose([Validators.required]));
    // this.shop_id = fb.control('', Validators.compose([Validators.required]));
    // this.detail = fb.control('', Validators.compose([Validators.required]));
    // this.permission = fb.control('', Validators.compose([Validators.required]));

    this.userPermissionForm = fb.group(
      {
        'username': this.username,
        'password': this.password,
        'repassword': this.repassword,
        'shop_id': this.shop_id,
        'name': this.name,
        'detail': this.detail,
      }
    );

  }

  ngOnInit() {
    this.setData();
  }

  setData() {
    this.data = {
      'username': '',
      'password': '',
      'repassword': '',
      'shop_id': '',
      'permission': ''
    };
  }

  async loadData() {
    this.isLoading = true;
    try {
      let resp_shop_data = await this.allService.get('shop');

      console.log('resp_shop_data', resp_shop_data);

      this.shops = resp_shop_data.data;
      if (this.shops.length > 0) {
        for (let i = 0; i < this.shops.length; i++) {
          this.shop_data.push({ 'value': this.shops[i].id, 'label': this.shops[i].name });
        }
      }

      this.isLoading = false;

    } catch (error) {
      console.log('error get shop', error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.loadData();
      } else {
        this.showSuccess('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
      }
    }
  }


  async sendForm() {
    console.log('data', this.userPermissionForm.value);
    this.isLoading = true;
    try {
      let resp_add_user = await this.allService.post('user', this.userPermissionForm.value);

      console.log('resp_add_user', resp_add_user);

      this.userPermissionForm.reset();
      this.isLoading = false;
      this.showSuccess(`เพิ่มผู้ใช้ ${resp_add_user.data.name} สำเร็จแล้ว`);

    } catch (error) {
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.sendForm();
      } else {
        console.log("Error create user", error);
        this.isLoading = false;
        this.showError(`ไม่สามารถเพิ่มผู้ใช้ได้ กรุณาลองใหม่อีกครั้ง`);
      }
    }

  }

  // --------------------------------------- Toast ---------------------------------------//
  showSuccess(message) {
    this.toastr.success(message, 'สำเร็จ!');
  }

  showError(message) {
    this.toastr.error(message, 'เกิดข้อผิดพลาด');
  }

}
