import { CustomValidators } from 'ng2-validation';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { ModalDirective } from 'ng2-bootstrap';
import { HideMenuService } from '../shared/hide-menu.service';
import { AllServiceService } from '../service/all-service.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'app-user-permission',
  templateUrl: './user-permission.component.html',
  styleUrls: ['./user-permission.component.scss']
})
export class UserPermissionComponent implements OnInit {

  @ViewChild('editModal') public editModal: ModalDirective;
  @ViewChild('deleteModal') public deleteModal: ModalDirective;

  public Heading = 'ผู้ใช้งาน';

  public isLoading: Boolean = false;

  //FormControl
  public userPermissionForm: FormGroup;
  public username: FormControl;
  public password: FormControl;
  public repassword: FormControl;
  public shop_id: FormControl;
  public name: FormControl;
  public detail: FormControl;
  public permission: FormControl;
  public changePass: FormControl;

  //select
  public shops;
  public shop_data = [];
  public shop_selected;

  //data
  public data = null;

  //table
  public dtOptions: any = {};

  //Menu
  public isHideMenu;

  //Modal  
  public selectedItem;

  public changePassword = false;

  public constructor(
    private fb: FormBuilder,
    public hideMenuService: HideMenuService,
    public allService: AllServiceService,
    public toastr: ToastsManager,
    public vcr: ViewContainerRef) {

    this.toastr.setRootViewContainerRef(vcr);

    this.loadData();
    this.isHideMenu = true;

    hideMenuService.subscribe({
      next: isHideMenu => {
        this.isHideMenu = isHideMenu;
        return this.isHideMenu;
      }
    });

    //Validate Form
    this.username = fb.control('', Validators.compose([Validators.required]));
    this.password = fb.control('', Validators.compose([CustomValidators.rangeLength([6, 20])]));
    this.repassword = fb.control('', Validators.compose([CustomValidators.rangeLength([6, 20]), CustomValidators.equalTo(this.password)]));
    // this.shop_id = fb.control('', Validators.compose([Validators.required]));
    this.name = fb.control('', Validators.compose([Validators.required]));
    this.detail = fb.control('', Validators.compose([Validators.required]));
    // this.permission = fb.control('', Validators.compose([Validators.required]));

    this.userPermissionForm = fb.group(
      {
        'username': this.username,
        'password': this.password,
        'repassword': this.repassword,
        'shop_id': this.shop_id,
        'name': this.name,
        'detail': this.detail,
        'changePass': this.changePass,
        // 'permission': this.permission
      }
    );

  }

  ngOnInit() {
    this.dtOptions = {
      displayLength: 10,
      paginationType: 'simple_numbers',
      language: {
        info: 'แสดง _START_ ถึง _END_ จาก _TOTAL_',
        paginate: {
          previous: '<',
          next: '>',
        },
        search: 'ค้นหา',
        emptyTable: 'ไม่มีข้อมูล',
        infoEmpty: 'แสดง 0 ถึง 0 จาก 0',
        zeroRecords: 'ไม่มีข้อมูล',
        lengthMenu: 'แสดง _MENU_ แถว'
      },
    };
  }

  setDefault() {
    this.selectedItem = {
      'id': '',
      'username': '',
      'name': '',
      'shop': '',
      'shop_id': '',
      'detail': '',
      'password': '',
      'repassword': ''
    };
    this.shop_data = [];
  }


  async loadData() {
    this.isLoading = true;

    try {
      let resp_user_data = await this.allService.get('user');
      let resp_store_data = await this.allService.get('shop');

      console.log('resp_user_data', resp_user_data);
      console.log('resp_store_data', resp_store_data);

      this.data = resp_user_data.data;
      this.shops = resp_store_data.data;
      for (let i = 0; i < this.shops.length; i++) {
        this.shop_data.push({ 'value': this.shops[i].id, 'label': this.shops[i].name });
      }

      console.log('shop_data', this.shop_data);

      this.isLoading = false;

    } catch (error) {
      console.log('error get userPermission', error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.loadData();
      } else {
        this.isLoading = false;
        this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
      }
    }
  }

  checkChangePassword() {
    this.changePassword = !this.changePassword;
  }

  //--------------------------------------- Open Modal ---------------------------------------//
  openEditModal(item): void {
    console.log('item', item);
    console.log('data', this.data);

    this.selectedItem = {
      'id': item.id,
      'username': item.username,
      'name': item.name,
      'shop': item.shop.name,
      'shop_id': item.shop_id.toString(),
      'detail': item.detail,
      'password': '',
      'repassword': ''
    };

    console.log('this.selectedItem', this.selectedItem);
    this.editModal.show();
  }

  openDeleteModal(item): void {
    this.selectedItem = {
      'id': item.id,
      'username': item.username,
      'name': item.name,
      'shop': item.shop.name,
      'shop_id': item.shop_id.toString(),
      'detail': item.detail,
      'password': '',
      'repassword': ''
    };
    console.log('this.selectedItem', this.selectedItem);
    this.deleteModal.show();
  }

  async saveChange() {
    console.log('save data', this.selectedItem);
    this.isLoading = true;
    let data = null;

    if (this.changePassword) {
      data = this.selectedItem;
    } else {
      data = {
        'detail': this.selectedItem.detail,
        'id': this.selectedItem.id,
        'name': this.selectedItem.name,
        'shop_id': this.selectedItem.shop_id,
        'username': this.selectedItem.username
      };
    }

    try {
      console.log('data', data);
      let resp_edit_user = await this.allService.putWithId('user', this.selectedItem.id, data);
      console.log('Update data user : ', resp_edit_user.data);
      this.showSuccess('แก้ไขข้อมูลเรียบร้อย');
      this.setDefault();
      this.loadData();
    } catch (error) {
      console.log('error edit', error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.saveChange();
      } else {
        this.isLoading = false;
        this.showError('ไม่สามารถแก้ไขข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
      }
    }

    this.editModal.hide();
  }

  async deleteItem() {
    console.log('delete data', this.selectedItem);
    this.isLoading = !this.isLoading;

    try {
      let resp_delete_user = await this.allService.deleteWithId('user', this.selectedItem.id);

      console.log('Delete data user : ', resp_delete_user.data);
      this.showSuccess('ลบข้อมูลเรียบร้อย');
      this.setDefault();
      this.loadData();
    } catch (error) {
      console.log("Error delete data user!", error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.deleteItem();
      } else {
        this.isLoading = false;
        this.showError('ไม่สามารถลบข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
      }
    }
    this.deleteModal.hide();
  }

  // --------------------------------------- Toast ---------------------------------------//
  showSuccess(message) {
    this.toastr.success(message, 'สำเร็จ!');
  }

  showError(message) {
    this.toastr.error(message, 'เกิดข้อผิดพลาด');
  }

}
