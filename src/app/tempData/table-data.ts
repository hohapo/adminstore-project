export const TableData: Array<any> = [
  {
    "id": 1,
    "name": "Rath-Sporer",
    "phone": "387-(356)735-5274",
    "address": "76853 Northfield Place"
  }, {
    "id": 2,
    "name": "Hodkiewicz, Keeling and Hane",
    "phone": "86-(693)423-2424",
    "address": "3 Hermina Crossing"
  }, {
    "id": 3,
    "name": "Emard LLC",
    "phone": "264-(349)107-0217",
    "address": "6397 Moose Avenue"
  }, {
    "id": 4,
    "name": "McCullough, Kassulke and Gutmann",
    "phone": "86-(565)930-6624",
    "address": "442 Caliangt Avenue"
  }, {
    "id": 5,
    "name": "Mayer, Kuhic and Stracke",
    "phone": "598-(532)891-8916",
    "address": "698 Swallow Street"
  }, {
    "id": 6,
    "name": "Renner, Greenholt and Schaden",
    "phone": "963-(216)524-3315",
    "address": "81 Farwell Trail"
  }, {
    "id": 7,
    "name": "Simonis Inc",
    "phone": "62-(788)734-5528",
    "address": "6922 Graceland Place"
  }, {
    "id": 8,
    "name": "Zboncak, Ankunding and Kutch",
    "phone": "86-(987)687-2665",
    "address": "97 Carpenter Pass"
  }, {
    "id": 9,
    "name": "Goodwin LLC",
    "phone": "86-(550)212-5751",
    "address": "03 Mifflin Court"
  }, {
    "id": 10,
    "name": "Gerlach Group",
    "phone": "227-(679)239-3506",
    "address": "1283 Sutteridge Way"
  }, {
    "id": 11,
    "name": "Halvorson Inc",
    "phone": "7-(462)470-9972",
    "address": "93973 Logan Place"
  }, {
    "id": 12,
    "name": "Parker-Donnelly",
    "phone": "218-(987)822-7517",
    "address": "268 Morrow Lane"
  }, {
    "id": 13,
    "name": "Olson-Kerluke",
    "phone": "976-(692)421-5596",
    "address": "2 Blackbird Park"
  }, {
    "id": 14,
    "name": "Cassin, Doyle and Cruickshank",
    "phone": "55-(627)801-0072",
    "address": "84586 Swallow Street"
  }, {
    "id": 15,
    "name": "Bradtke Group",
    "phone": "66-(116)440-2149",
    "address": "9777 Redwing Junction"
  }, {
    "id": 16,
    "name": "Denesik Group",
    "phone": "7-(457)493-5576",
    "address": "40 Hazelcrest Junction"
  }, {
    "id": 17,
    "name": "Hudson Inc",
    "phone": "98-(287)590-4563",
    "address": "7298 Pine View Court"
  }, {
    "id": 18,
    "name": "Yost Inc",
    "phone": "62-(516)766-5406",
    "address": "5996 Heffernan Drive"
  }, {
    "id": 19,
    "name": "Stiedemann-Kerluke",
    "phone": "261-(878)250-6417",
    "address": "38091 Gateway Hill"
  }, {
    "id": 20,
    "name": "Ryan LLC",
    "phone": "86-(386)704-5141",
    "address": "751 Jana Drive"
  }
];
