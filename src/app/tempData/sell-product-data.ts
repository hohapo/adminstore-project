export const SellProductData: Array<any> = [{
    "id": 1,
    "name": "Octreotide Acetate",
    "date": "16/04/2016",
    "price": 21954,
    "department": "Baby",
    "customer": "Donna Davis",
    "extra": "Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst."
}, {
    "id": 2,
    "name": "Promethazine Hydrochloride",
    "date": "20/07/2016",
    "price": 57980,
    "department": "Automotive",
    "customer": "Nicholas Peters",
    "extra": "Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum."
}, {
    "id": 3,
    "name": "Bisacodyl",
    "date": "13/02/2016",
    "price": 90753,
    "department": "Outdoors",
    "customer": "Willie Gonzales",
    "extra": "Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi."
}, {
    "id": 4,
    "name": "loperamide hydrochloride",
    "date": "24/06/2016",
    "price": 29950,
    "department": "Garden",
    "customer": "Rebecca Hayes",
    "extra": "Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem."
}, {
    "id": 5,
    "name": "VIPERA BERUS VENOM",
    "date": "03/04/2016",
    "price": 66193,
    "department": "Tools",
    "customer": "Robin Washington",
    "extra": "Fusce consequat. Nulla nisl. Nunc nisl."
}, {
    "id": 6,
    "name": "Temazepam",
    "date": "01/03/2016",
    "price": 23454,
    "department": "Shoes",
    "customer": "Laura Reynolds",
    "extra": "Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem."
}, {
    "id": 7,
    "name": "TELMISARTAN",
    "date": "26/01/2016",
    "price": 62089,
    "department": "Home",
    "customer": "Diane Hunt",
    "extra": "In congue. Etiam justo. Etiam pretium iaculis justo."
}, {
    "id": 8,
    "name": "Fosinopril sodium",
    "date": "11/06/2016",
    "price": 69157,
    "department": "Baby",
    "customer": "Wanda Mills",
    "extra": "Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus."
}, {
    "id": 9,
    "name": "PRAMIPEXOLE DIHYDROCHLORIDE",
    "date": "02/11/2016",
    "price": 30367,
    "department": "Grocery",
    "customer": "Rebecca Howell",
    "extra": "Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam."
}, {
    "id": 10,
    "name": "Ramipril",
    "date": "16/12/2016",
    "price": 83226,
    "department": "Music",
    "customer": "Julia Fernandez",
    "extra": "Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis."
}, {
    "id": 11,
    "name": "Phenobarbital, Hyoscyamine Sulfate, Atropine Sulfate, Scopolamine Hydrobromide",
    "date": "03/09/2016",
    "price": 52294,
    "department": "Health",
    "customer": "Philip Henderson",
    "extra": "Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis."
}, {
    "id": 12,
    "name": "Meclizine HCl",
    "date": "10/01/2016",
    "price": 40264,
    "department": "Games",
    "customer": "Ernest Taylor",
    "extra": "Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi."
}, {
    "id": 13,
    "name": "Cocklebur",
    "date": "04/09/2016",
    "price": 8394,
    "department": "Movies",
    "customer": "Howard Ward",
    "extra": "Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh."
}];