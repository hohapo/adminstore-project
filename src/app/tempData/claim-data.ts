export const ClaimData: Array<any> = [{
  "id": "55566-9500",
  "date": "24/06/2016",
  "product_name": "Stehr LLC",
  "product_type": "dedicated",
  "customer_name": "Julie",
  "status": 2,
  "extra": "Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus."
}, {
  "id": "62802-146",
  "date": "23/10/2016",
  "product_name": "Legros-Bernier",
  "product_type": "Seamless",
  "customer_name": "Annie",
  "status": 3,
  "extra": "Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat."
}, {
  "id": "55312-376",
  "date": "28/07/2016",
  "product_name": "MacGyver-Lindgren",
  "product_type": "productivity",
  "customer_name": "Ronald",
  "status": 3,
  "extra": "Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat."
}, {
  "id": "50405-008",
  "date": "24/04/2016",
  "product_name": "Stoltenberg-Hills",
  "product_type": "intranet",
  "customer_name": "Jose",
  "status": 1,
  "extra": "Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit."
}, {
  "id": "13734-036",
  "date": "08/05/2016",
  "product_name": "Stokes Group",
  "product_type": "Ameliorated",
  "customer_name": "Walter",
  "status": 2,
  "extra": "Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum."
}, {
  "id": "11673-281",
  "date": "17/07/2016",
  "product_name": "Jast-Lang",
  "product_type": "web-enabled",
  "customer_name": "Paul",
  "status": 1,
  "extra": "Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem."
}, {
  "id": "42213-365",
  "date": "03/08/2016",
  "product_name": "Mante and Sons",
  "product_type": "6th generation",
  "customer_name": "Nicole",
  "status": 1,
  "extra": "Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius."
}, {
  "id": "10631-110",
  "date": "25/01/2016",
  "product_name": "Halvorson and Sons",
  "product_type": "Optional",
  "customer_name": "Jennifer",
  "status": 2,
  "extra": "Fusce consequat. Nulla nisl. Nunc nisl."
}, {
  "id": "62169-203",
  "date": "07/09/2016",
  "product_name": "Pollich, Frami and Casper",
  "product_type": "Sharable",
  "customer_name": "Jason",
  "status": 2,
  "extra": "Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat."
}, {
  "id": "64942-1207",
  "date": "06/11/2016",
  "product_name": "Kirlin and Sons",
  "product_type": "bifurcated",
  "customer_name": "Roger",
  "status": 1,
  "extra": "Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis."
}, {
  "id": "64725-0550",
  "date": "01/07/2016",
  "product_name": "Ondricka Inc",
  "product_type": "zero administration",
  "customer_name": "Adam",
  "status": 1,
  "extra": "Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis."
}, {
  "id": "55154-4776",
  "date": "06/07/2016",
  "product_name": "Wisozk and Sons",
  "product_type": "grid-enabled",
  "customer_name": "Christina",
  "status": 2,
  "extra": "Fusce consequat. Nulla nisl. Nunc nisl."
}, {
  "id": "57627-153",
  "date": "27/03/2016",
  "product_name": "Zieme LLC",
  "product_type": "optimizing",
  "customer_name": "Thomas",
  "status": 3,
  "extra": "Sed ante. Vivamus tortor. Duis mattis egestas metus."
}, {
  "id": "37808-110",
  "date": "07/09/2016",
  "product_name": "Stokes, Koch and Kuphal",
  "product_type": "background",
  "customer_name": "Jean",
  "status": 1,
  "extra": "Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh."
}, {
  "id": "65044-5053",
  "date": "01/06/2016",
  "product_name": "Toy, Stroman and Rodriguez",
  "product_type": "zero tolerance",
  "customer_name": "Jerry",
  "status": 2,
  "extra": "Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum."
}, {
  "id": "35356-677",
  "date": "12/02/2016",
  "product_name": "Schinner-Franecki",
  "product_type": "benchmark",
  "customer_name": "Margaret",
  "status": 1,
  "extra": "Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum."
}, {
  "id": "0615-0564",
  "date": "27/04/2016",
  "product_name": "Nolan LLC",
  "product_type": "foreground",
  "customer_name": "Karen",
  "status": 2,
  "extra": "Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit."
}, {
  "id": "0603-5093",
  "date": "12/03/2016",
  "product_name": "Bergnaum LLC",
  "product_type": "Enhanced",
  "customer_name": "Denise",
  "status": 1,
  "extra": null
}, {
  "id": "58118-0214",
  "date": "15/02/2016",
  "product_name": "Durgan, Becker and Reichert",
  "product_type": "throughput",
  "customer_name": "Jessica",
  "status": 1,
  "extra": "Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque."
}];