export const AcceptProductData: Array<any> = [{
    "id": 1,
    "name": "DOCUSATE SODIUM",
    "date": "09/25/2016",
    "piece": 18,
    "price": 9538,
    "total": 171684,
    "extra": "Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat."
}, {
    "id": 2,
    "name": "diphenhydramine hydrochloride",
    "date": "08/05/2016",
    "piece": 2,
    "price": 2651,
    "total": 5302,
    "extra": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus."
}, {
    "id": 3,
    "name": "clozapine",
    "date": "08/05/2016",
    "piece": 18,
    "price": 5083,
    "total": 91494,
    "extra": "Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis."
}, {
    "id": 4,
    "name": "sertraline hydrochloride",
    "date": "08/05/2016",
    "piece": 18,
    "price": 5735,
    "total": 103230,
    "extra": "Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus."
}, {
    "id": 5,
    "name": "lisinopril",
    "date": "02/05/2016",
    "piece": 27,
    "price": 2186,
    "total": 59022,
    "extra": "Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi."
}, {
    "id": 6,
    "name": "nifedipine",
    "date": "03/31/2016",
    "piece": 98,
    "price": 5718,
    "total": 560364,
    "extra": "Phasellus in felis. Donec semper sapien a libero. Nam dui."
}, {
    "id": 7,
    "name": "Ranitidine",
    "date": "07/17/2016",
    "piece": 43,
    "price": 8912,
    "total": 383216,
    "extra": "Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus."
}, {
    "id": 8,
    "name": "Salicylic Acid",
    "date": "10/14/2016",
    "piece": 94,
    "price": 6253,
    "total": 587782,
    "extra": "Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus."
}, {
    "id": 9,
    "name": "Hydroxychloroquine Sulfate",
    "date": "06/15/2016",
    "piece": 81,
    "price": 9230,
    "total": 747630,
    "extra": "Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem."
}, {
    "id": 10,
    "name": "Etodolac",
    "date": "11/19/2016",
    "piece": 62,
    "price": 3372,
    "total": 209064,
    "extra": "Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis."
}, {
    "id": 11,
    "name": "Titanium Dioxide, Octinoxate",
    "date": "06/12/2016",
    "piece": 18,
    "price": 9487,
    "total": 170766,
    "extra": "Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam."
}, {
    "id": 12,
    "name": "Quetiapine fumarate",
    "date": "07/14/2016",
    "piece": 54,
    "price": 4775,
    "total": 257850,
    "extra": "Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius."
}, {
    "id": 13,
    "name": "Alcohol",
    "date": "12/22/2016",
    "piece": 64,
    "price": 9987,
    "total": 639168,
    "extra": "Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est."
}, {
    "id": 14,
    "name": "doxazosin",
    "date": "06/17/2016",
    "piece": 71,
    "price": 6081,
    "total": 431751,
    "extra": "In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo."
}, {
    "id": 15,
    "name": "Sodium Sulfacetamide, Sulfur",
    "date": "05/14/2016",
    "piece": 66,
    "price": 4686,
    "total": 309276,
    "extra": "In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet."
}];