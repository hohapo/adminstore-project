import { HideMenuService } from './../shared/hide-menu.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { AllServiceService } from '../service/all-service.service';
import { CustomValidators } from 'ng2-validation';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'app-product-type-add',
  templateUrl: './product-type-add.component.html',
  styleUrls: ['./product-type-add.component.scss']
})
export class ProductTypeAddComponent implements OnInit {

  public Heading = 'ประเภทสินค้า';

  //Menu
  public isHideMenu;

  public isLoading: Boolean = false;

  public productTypeForm: FormGroup;
  public name: FormControl;
  public detail: FormControl;

  public data;

  constructor(
    public hideMenuService: HideMenuService,
    private fb: FormBuilder,
    private allService: AllServiceService,
    public toastr: ToastsManager,
    public vcr: ViewContainerRef) {

    this.toastr.setRootViewContainerRef(vcr);

    this.isHideMenu = true;

    hideMenuService.subscribe({
      next: isHideMenu => {
        this.isHideMenu = isHideMenu;
        return this.isHideMenu;
      }
    });

    //validate FormBuilder
    this.name = fb.control('', Validators.compose([Validators.required]));
    this.detail = fb.control('', Validators.compose([Validators.required]));

    this.productTypeForm = fb.group({
      'name': this.name,
      'detail': this.detail
    });
  }

  ngOnInit() {
    this.setData();
  }

  setData() {
    this.data = {
      'name': '',
      'detail': ''
    };
  }

  async sendForm() {
    console.log('data : ', this.data, this.productTypeForm.value);
    let data = this.productTypeForm.value;
    this.isLoading = !this.isLoading;
    try {
      let res_add_product_type = await this.allService.post('productType', data);
      this.productTypeForm.reset();
      this.isLoading = !this.isLoading;
      this.showSuccess(`เพิ่ม ${res_add_product_type.data.name} สำเร็จแล้ว`);
    } catch (error) {
      let response = error;
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.sendForm();
      } else {
        console.log("Error create product type", response);
        if (error.status === 401) {
          await this.allService.refreshToken();
          this.sendForm();
        } else {
          this.isLoading = false;
          this.showError(`ไม่สามารถเพิ่มประเภทสินค้าได้ กรุณาลองใหม่อีกครั้ง`);
        }
      }
    }
  }

  // --------------------------------------- Toast ---------------------------------------//
  showSuccess(message) {
    this.toastr.success(message, 'สำเร็จ!');
  }

  showError(message) {
    this.toastr.error(message, 'เกิดข้อผิดพลาด');
  }

}
