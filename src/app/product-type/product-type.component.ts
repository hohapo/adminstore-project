import { async } from '@angular/core/testing';
import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { ModalDirective } from 'ng2-bootstrap';
import { HideMenuService } from '../shared/hide-menu.service';
import { AllServiceService } from '../service/all-service.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';

@Component({
  selector: 'app-product-type',
  templateUrl: './product-type.component.html',
  styleUrls: ['./product-type.component.scss']
})
export class ProductTypeComponent implements OnInit {

  @ViewChild('editModal') public editModal: ModalDirective;
  @ViewChild('deleteModal') public deleteModal: ModalDirective;

  public Heading = 'ประเภทสินค้า';

  public isLoading: Boolean = false;

  //data
  public data = null;

  //table
  public dtOptions: any = {};

  //Pagination
  public totalLength;

  //Menu
  public isHideMenu;

  //Modal  
  public selectedItem;

  //form
  public productTypeForm: FormGroup;
  public name: FormControl;
  public detail: FormControl;

  public constructor(
    public hideMenuService: HideMenuService,
    public allService: AllServiceService,
    public toastr: ToastsManager,
    public vcr: ViewContainerRef,
    private fb: FormBuilder) {

    this.toastr.setRootViewContainerRef(vcr);

    this.isLoading = false;

    this.getData();
    this.isHideMenu = true;

    hideMenuService.subscribe({
      next: isHideMenu => {
        this.isHideMenu = isHideMenu;
        return this.isHideMenu;
      }
    });

    //validate FormBuilder
    this.name = fb.control('', Validators.compose([Validators.required]));
    this.detail = fb.control('', Validators.compose([Validators.required]));

    this.productTypeForm = fb.group({
      'name': this.name,
      'detail': this.detail
    });

  }

  ngOnInit() {
    this.initTable();
  }

  initTable() {
    this.dtOptions = {
      displayLength: 10,
      paginationType: 'simple_numbers',
      language: {
        info: 'แสดง _START_ ถึง _END_ จาก _TOTAL_',
        paginate: {
          previous: '<',
          next: '>',
        },
        search: 'ค้นหา',
        emptyTable: 'ไม่มีข้อมูล',
        infoEmpty: 'แสดง 0 ถึง 0 จาก 0',
        zeroRecords: 'ไม่มีข้อมูล',
        lengthMenu: 'แสดง _MENU_ แถว'
      }
    };
  }


  async getData() {
    this.isLoading = !this.isLoading;
    console.log('loadData');
    try {
      let res_get_product_type = await this.allService.get('productType');
      this.data = res_get_product_type.data;
      this.isLoading = false;
    } catch (error) {
      console.log("Error get data prodict type!", error);
      if (error.status === 404) {
        this.isLoading = false;
      } else if (error.status === 401) {
        await this.allService.refreshToken();
        this.getData();
      } else {
        this.isLoading = false;
      }
    }
  }


  // --------------------------------------- Open Modal ---------------------------------------//
  openEditModal(id): void {
    this.selectedItem = this.data.filter(data => data.id == id)[0];
    this.editModal.show();
  }

  openDeleteModal(id): void {
    this.selectedItem = this.data.filter(data => data.id == id)[0];
    this.deleteModal.show();
  }

  async saveChange() {
    console.log('save data', this.selectedItem, this.productTypeForm.value);
    this.isLoading = true;
    console.log('loadData');
    try {
      let res_edit_product_type = await this.allService.putWithId('productType', this.selectedItem.id, this.selectedItem);
      this.showSuccess('แก้ไขข้อมูลเรียบร้อย');
      this.getData();
    } catch (error) {
      console.log("Error update data prodict type!", error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.saveChange();
      } else {
        this.isLoading = false;
        this.showError(`ไม่สามารถแก้ไขได้ กรุณาลองใหม่อีกครั้ง`);
      }
    }
    this.editModal.hide();
  }

  async deleteItem() {
    console.log('delete data', this.selectedItem);
    this.isLoading = true;
    try {
      let res_delete_product_type = await this.allService.deleteWithId('productType', this.selectedItem.id);
      this.showSuccess('ลบข้อมูลเรียบร้อย');
      this.getData();
    } catch (error) {
      console.log("Error delete data prodict type!", error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.deleteItem();
      } else {
        this.isLoading = false;
        this.showError(`ไม่สามารถลบประเภทสินค้าได้ กรุณาลองใหม่อีกครั้ง`);
      }
    }

    this.deleteModal.hide();
  }

  // --------------------------------------- Toast ---------------------------------------//
  showSuccess(message) {
    this.toastr.success(message, 'สำเร็จ!');
  }

  showError(message) {
    this.toastr.error(message, 'เกิดข้อผิดพลาด');
  }

}
