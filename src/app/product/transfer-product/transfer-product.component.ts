import { Observable } from 'rxjs/Observable';
import { async } from '@angular/core/testing';
import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { ModalDirective, TypeaheadMatch } from 'ng2-bootstrap';
import { HideMenuService } from '../../shared/hide-menu.service';
import { AllServiceService } from '../../service/all-service.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import * as moment from 'moment';
import { IMyOptions, IMyDateRangeModel } from 'mydaterangepicker';

@Component({
  selector: 'app-transfer-product',
  templateUrl: './transfer-product.component.html',
  styleUrls: ['./transfer-product.component.scss']
})
export class TransferProductComponent implements OnInit {

  @ViewChild('editModal') public editModal: ModalDirective;
  @ViewChild('deleteModal') public deleteModal: ModalDirective;


  Heading = 'การโอนสินค้า';

  public isLoading: Boolean = false;

  //date
  public dateSelect;

  //data
  public data = null;

  //table
  public dtOptions: any = {};

  //Typeahead
  public products;
  public productSelected: any;
  public shopDestinations;
  public shopDestinationSelected: any;
  public typeaheadLoading: Object;
  public dataSource: Observable<any>;
  public asyncSelected: string = '';
  public typeaheadNoResults;


  //Menu
  public isHideMenu;

  //Modal  
  public selectedItem;

  public transferProductForm: FormGroup;

  public product_id: FormControl;
  public amount: FormControl;
  public shop_source_id: FormControl;
  public shop_destination_id: FormControl;
  public remark: FormControl;

  private myDateRangePickerOptions: IMyOptions = {
    dateFormat: 'dd/mm/yyyy',
    firstDayOfWeek: 'su'
  };

  private dateRange: Object = {
    beginDate: { year: '', month: '', day: '' },
    endDate: { year: '', month: '', day: '' }
  };

  public constructor(public hideMenuService: HideMenuService,
    public allService: AllServiceService,
    public toastr: ToastsManager,
    public vcr: ViewContainerRef,
    private fb: FormBuilder) {
    this.toastr.setRootViewContainerRef(vcr);

    this.loadData();

    //validate FormBuilder
    this.product_id = fb.control('', Validators.compose([Validators.required]));
    this.amount = fb.control('1', Validators.compose([Validators.required, CustomValidators.number, CustomValidators.min(0)]));

    this.transferProductForm = fb.group({
      'amount': this.amount,
      'remark': this.remark,
      'product_id': this.product_id,
      'shop_source_id': this.shop_source_id,
      'shop_destination_id': this.shop_destination_id,
    });

  }

  ngOnInit() {
    this.initData();
    this.initTable();
  }


  initData() {
    this.productSelected = {
      code: '',
      name: ''
    };
    this.shopDestinationSelected = {
      name: ''
    };
    this.typeaheadNoResults = {
      'product': false,
      'shopDestination': false
    };
    this.selectedItem = null;
    this.dateRange = {
      beginDate: { year: moment().year(), month: moment().month() + 1, day: moment().date() },
      endDate: { year: moment().year(), month: moment().month() + 1, day: moment().date() }
    };
    this.dateSelect = {
      'date_from': this.setFormatDate(moment().format('YYYY-MM-DD')),
      'date_to': this.setFormatDate(moment().format('YYYY-MM-DD'))
    }
  }

  async loadData() {
    await this.getDataForEdit();
    await this.getData();
  }

  initTable() {
    this.dtOptions = {
      displayLength: 10,
      bSort: false,
      paginationType: 'simple_numbers',
      language: {
        info: 'แสดง _START_ ถึง _END_ จาก _TOTAL_',
        paginate: {
          previous: '<',
          next: '>',
        },
        search: 'ค้นหา',
        emptyTable: 'ไม่มีข้อมูล',
        infoEmpty: 'แสดง 0 ถึง 0 จาก 0',
        zeroRecords: 'ไม่มีข้อมูล',
        lengthMenu: 'แสดง _MENU_ แถว'
      }
    };
  }

  async getDataForEdit() {
    this.isLoading = true;
    try {
      let resp_product_data = await this.allService.get('product');
      let resp_shopDestination_data = await this.allService.get('shop');

      console.log('resp_product_data', resp_product_data);
      console.log('resp_shopDestination_data', resp_shopDestination_data);

      this.products = resp_product_data.data;
      this.shopDestinations = resp_shopDestination_data.data.filter(item => item.id !== this.allService.getLocal('user').shop_id);

      this.isLoading = false;

    } catch (error) {
      console.log('error get data', error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.getDataForEdit();
      } else {
        this.isLoading = false;
        this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
      }
    }
  }

  async getData() {
    this.isLoading = true;
    console.log('loadData');
    try {
      let res_get_sell_product = await this.allService.get('productTransections/getProductTransectionTransfer', this.dateSelect);

      this.data = res_get_sell_product.data;

      for (var i = 0; i < this.data.length; i++) {
        this.data[i].date = moment(this.data[i].created_at).format("YYYY-MM-DD");
      }

      console.log('res_get_sell_product', this.data);
      this.isLoading = false;
    } catch (error) {
      console.log("Error get data prodict type!", error);
      if (error.status === 404) {
        this.isLoading = false;
        this.showError('กรุณาลองใหม่อีกครั้ง');
      } else if (error.status === 401) {
        await this.allService.refreshToken();
        this.getData();
      } else {
        this.isLoading = false;
        this.showError('กรุณาลองใหม่อีกครั้ง');
      }
    }
  }


  // --------------------------------------- Open Modal ---------------------------------------//
  openEditModal(id): void {
    let data_select = Object.assign({}, this.data.filter(data => data.id == id)[0]);
    this.productSelected.name = data_select.product.name;
    // this.shopDestinationSelected.name = data_select.shop_destination ? data_select.shop_destination.name : null;

    this.selectedItem = Object.assign({}, data_select);
    console.log('selectedItem', this.selectedItem);
    this.editModal.show();
  }

  openDeleteModal(id): void {
    this.selectedItem = Object.assign({}, this.data.filter(data => data.id == id)[0]);
    this.deleteModal.show();
  }

  async saveChange() {
    this.isLoading = true;
    console.log('this.selectedItem', this.selectedItem);
    console.log('this.productSelected', this.productSelected);
    try {
      let data = Object.assign({}, this.selectedItem, this.transferProductForm.value);
      console.log('this.transferProductForm.value', this.transferProductForm.value);

      data.product_id = this.productSelected.id || this.selectedItem.product_id;
      data.shop_source_id = this.allService.getLocal('user').shop_id;
      data.shop_destination_id = this.shopDestinationSelected.id || this.selectedItem.shop_destination_id;

      console.log('data', data)
      let res_tran_in_product = await this.allService.putWithId('productTransections', this.selectedItem.id, data);
      this.initData();
      this.getData();
      this.showSuccess(`แก้ไขข้อมูลเรียบร้อย`);
    } catch (error) {
      let response = error;
      console.log("Error create product type", response);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.saveChange();
      } else {
        this.isLoading = false;
        this.showError(`ไม่สามารถแก้ไขได้ กรุณาลองใหม่อีกครั้ง`);
      }
    }

    this.editModal.hide();
  }

  async deleteItem() {
    console.log('delete data', this.selectedItem);
    this.isLoading = true;
    try {
      let res_delete_product_type = await this.allService.deleteWithId('productTransections', this.selectedItem.id);
      this.initData();
      this.getData();
      this.showSuccess('ลบข้อมูลเรียบร้อย');
    } catch (error) {
      console.log("Error delete data prodict type!", error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.deleteItem();
      } else {
        this.isLoading = false;
        this.showError('ไม่สามารถลบข้อมูลได้');
      }
    }

    this.deleteModal.hide();
  }
  // --------------------------------------- Toast ---------------------------------------//
  showSuccess(message) {
    this.toastr.success(message, 'สำเร็จ!');
  }

  showError(message) {
    this.toastr.error(message, 'เกิดข้อผิดพลาด');
  }


  //--------------------------------------- Typeahead ---------------------------------------//
  public changeTypeaheadLoading(status, e: boolean): void {
    this.typeaheadLoading[status] = e;
  }

  public changeTypeaheadNoResults(status, e: boolean): void {
    this.typeaheadNoResults[status] = e;
  }

  public typeaheadOnSelect(status, e: TypeaheadMatch): void {
    switch (status) {
      case 'product':
        this.productSelected = Object.assign({}, e.item);
        break;
      case 'shopDestination':
        this.shopDestinationSelected = Object.assign({}, e.item);
        break;
    }

    console.log('Selected value:', e);
  }

  // --------------------------------------- date ---------------------------------------//
  onDateRangeChanged(event: IMyDateRangeModel) {
    this.dateSelect = {
      'date_from': this.setFormatDate(event.beginDate),
      'date_to': this.setFormatDate(event.endDate)
    }
    this.getData();
  }

  setFormatDate(data?) {
    if (typeof data === 'object') {
      return data.year + '-' + data.month + '-' + data.day
    } else if (typeof data === 'string') {
      return data
    }
  }

}
