import { CustomValidators } from 'ng2-validation';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { AllServiceService } from './../../service/all-service.service';
import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { TypeaheadMatch, ModalDirective } from 'ng2-bootstrap';
import { CustomerData } from '../../tempData/customer-data';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';


import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/of';

@Component({
  selector: 'app-add-transfer-product',
  templateUrl: './add-transfer-product.component.html',
  styleUrls: ['./add-transfer-product.component.scss']
})
export class AddTransferProductComponent implements OnInit {

  @ViewChild('editModal') public editModal: ModalDirective;

  Heading = 'เพิ่มการโอนสินค้า';

  public isLoading: Boolean = false;

  public transferProductForm: FormGroup;

  public product_id: FormControl;
  public amount: FormControl;
  public shop_source_id: FormControl;
  public shop_destination_id: FormControl;
  public remark: FormControl;
  public product_detail: FormControl;

  public data;

  //Typeahead
  public products;
  public productSelected: any;
  public shopDestinations;
  public shopDestinationSelected: any;
  public typeaheadLoading: Object;
  public dataSource: Observable<any>;
  public asyncSelected: string = '';
  public typeaheadNoResults;

  constructor(
    private fb: FormBuilder,
    private allService: AllServiceService,
    public toastr: ToastsManager,
    public vcr: ViewContainerRef) {

    this.toastr.setRootViewContainerRef(vcr);

    //validate FormBuilder
    this.product_id = fb.control('', Validators.compose([Validators.required]));
    this.product_detail = fb.control({ value: '', disabled: true });
    this.amount = fb.control('1', Validators.compose([Validators.required, CustomValidators.number, CustomValidators.min(0)]));

    this.transferProductForm = fb.group({
      'amount': this.amount,
      'remark': this.remark,
      'product_id': this.product_id,
      'shop_source_id': this.shop_source_id,
      'shop_destination_id': this.shop_destination_id,
      'product_detail': this.product_detail
    });

  }

  ngOnInit() {
    this.setData();
    this.loadData();
  }

  setData() {
    this.data = {
      'amount': '',
      'remark': '',
      'product_id': '',
      'shop_source_id': '',
      'shop_destination_id': '',
      'sell_price': '',
      'sell_type': '',
      'product_detail': ''
    };
    this.productSelected = {
      code: '',
      name: '',
      sell_price: 0
    }
    this.shopDestinationSelected = {
      name: ''
    }
    this.typeaheadLoading = {
      'product': false,
      'shopDestination': false
    }
    this.typeaheadNoResults = {
      'product': false,
      'shopDestination': false
    }
  }

  async loadData() {
    this.isLoading = true;
    try {
      let resp_product_data = await this.allService.get('product');
      let resp_shopDestination_data = await this.allService.get('shop');

      console.log('resp_product_data', resp_product_data);
      console.log('resp_shopDestination_data', resp_shopDestination_data);

      this.products = resp_product_data.data;
      this.shopDestinations = resp_shopDestination_data.data.filter(item => item.id !== this.allService.getLocal('user').shop_id);
      console.log('this.shopDestinations', this.shopDestinations)
      this.isLoading = false;

    } catch (error) {
      console.log('error get data', error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.loadData();
      } else {
        this.isLoading = false;
        this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
      }
    }
  }

  async sendForm() {
    let data = this.transferProductForm.value;
    console.log('data', this.transferProductForm.value);
    this.isLoading = true;
    try {

      data.shop_source_id = this.allService.getLocal('user').shop_id;
      data.product_id = this.productSelected.id;

      console.log('data', data);

      let res_sell_product = await this.allService.post('productTransections', data);

      // this.transferProductForm.reset();
      this.setData();
      this.isLoading = false;
      this.showSuccess(`บันทึกการโอนสินค้าสำเร็จแล้ว`);
    } catch (error) {
      let response = error;
      console.log("Error create product type", response);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.sendForm();
      } else {
        this.isLoading = false;
        this.showError(`ไม่สามารถบันทึการโอนสินค้าได้ กรุณาลองใหม่อีกครั้ง`);
      }
    }
  }

  // --------------------------------------- Toast ---------------------------------------//
  showSuccess(message) {
    this.toastr.success(message, 'สำเร็จ!');
  }

  showError(message) {
    this.toastr.error(message, 'เกิดข้อผิดพลาด');
  }

  //--------------------------------------- Typeahead ---------------------------------------//
  public changeTypeaheadLoading(status, e: boolean): void {
    this.typeaheadLoading[status] = e;
  }

  public changeTypeaheadNoResults(status, e: boolean): void {
    this.typeaheadNoResults[status] = e;
  }

  public typeaheadOnSelect(status, e: TypeaheadMatch): void {
    switch (status) {
      case 'product':
        this.productSelected = Object.assign({}, e.item);
        break;
      case 'shopDestination':
        this.shopDestinationSelected = Object.assign({}, e.item);
        break;
    }

    console.log('Selected value:', e);
  }




}
