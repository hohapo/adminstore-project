import { Observable } from 'rxjs/Observable';
import { async } from '@angular/core/testing';
import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { ModalDirective, TypeaheadMatch } from 'ng2-bootstrap';
import { HideMenuService } from '../../shared/hide-menu.service';
import { AllServiceService } from '../../service/all-service.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import * as moment from 'moment';
import { IMyOptions, IMyDateRangeModel } from 'mydaterangepicker';

@Component({
  selector: 'app-sell-product',
  templateUrl: './sell-product.component.html',
  styleUrls: ['./sell-product.component.scss']
})
export class SellProductComponent implements OnInit {

  @ViewChild('editModal') public editModal: ModalDirective;
  @ViewChild('deleteModal') public deleteModal: ModalDirective;


  Heading = 'การขายสินค้า';

  public isLoading: Boolean = false;

  //date
  public dateSelect;

  //data
  public data = null;

  //table
  public dtOptions: any = {};

  //Typeahead
  public products;
  public productSelected: any;
  public customers;
  public customerSelected: any;
  public typeaheadLoading: Object;
  public dataSource: Observable<any>;
  public asyncSelected: string = '';
  public typeaheadNoResults;

  //Menu
  public isHideMenu;

  //Modal  
  public selectedItem;
  public price: number;
  public amount_value: number;

  public sellProductForm: FormGroup;

  public product_id: FormControl;
  public amount: FormControl;
  public shop_source_id: FormControl;
  public customer_id: FormControl;
  public sell_price: FormControl;
  public sell_type: FormControl;
  public remark: FormControl;

  public customerForm: FormGroup;

  public name_customer: FormControl;
  public tel_customer: FormControl;

  public shops;
  public shop_data = [];

  private myDateRangePickerOptions: IMyOptions = {
    dateFormat: 'dd/mm/yyyy',
    firstDayOfWeek: 'su'
  };

  private dateRange: Object = {
    beginDate: { year: '', month: '', day: '' },
    endDate: { year: '', month: '', day: '' }
  };

  public constructor(public hideMenuService: HideMenuService,
    public allService: AllServiceService,
    public toastr: ToastsManager,
    public vcr: ViewContainerRef,
    private fb: FormBuilder) {
    this.toastr.setRootViewContainerRef(vcr);

    this.loadData();
    this.isHideMenu = true;

    hideMenuService.subscribe({
      next: isHideMenu => {
        this.isHideMenu = isHideMenu;
        return this.isHideMenu;
      }
    });

    //validate FormBuilder
    // this.product_id = fb.control('', Validators.compose([Validators.required]));
    this.sell_type = fb.control('1', Validators.compose([Validators.required]));
    this.sell_price = fb.control('', Validators.compose([Validators.required, CustomValidators.number, CustomValidators.min(0)]));
    this.amount = fb.control('1', Validators.compose([Validators.required, CustomValidators.number, CustomValidators.min(0)]));

    this.sellProductForm = fb.group({
      'amount': this.amount,
      'remark': this.remark,
      'product_id': this.product_id,
      'shop_source_id': this.shop_source_id,
      'customer_id': this.customer_id,
      'sell_price': this.sell_price,
      'sell_type': this.sell_type
    });

    //validate FormBuilder Modal add customer
    this.name_customer = fb.control('', Validators.compose([Validators.required]));
    this.tel_customer = fb.control('', Validators.compose([Validators.required]));


    this.customerForm = fb.group({
      'name_customer': this.name_customer,
      'tel_customer': this.tel_customer
    });

  }

  ngOnInit() {
    this.initData();
    this.initTable();
  }


  initData() {
    this.productSelected = {
      code: '',
      name: ''
    };

    this.customerSelected = {

      name: ''
    };

    this.typeaheadNoResults = {
      'product': false,
      'customer': false
    };

    this.selectedItem = null;

    this.dateRange = {
      beginDate: { year: moment().year(), month: moment().month() + 1, day: moment().date() },
      endDate: { year: moment().year(), month: moment().month() + 1, day: moment().date() }
    };

    this.dateSelect = {
      'date_from': this.setFormatDate(moment().format('YYYY-MM-DD')),
      'date_to': this.setFormatDate(moment().format('YYYY-MM-DD'))
    }
  }

  async loadData() {
    await this.getData();
  }

  initTable() {
    this.dtOptions = {
      displayLength: 10,
      bSort: false,
      paginationType: 'simple_numbers',
      language: {
        info: 'แสดง _START_ ถึง _END_ จาก _TOTAL_',
        paginate: {
          previous: '<',
          next: '>',
        },
        search: 'ค้นหา',
        emptyTable: 'ไม่มีข้อมูล',
        infoEmpty: 'แสดง 0 ถึง 0 จาก 0',
        zeroRecords: 'ไม่มีข้อมูล',
        lengthMenu: 'แสดง _MENU_ แถว'
      }
    };
  }

  async getData() {
    this.isLoading = true;
    this.data = null;
    try {
      let res_get_customer = await this.allService.get('customer');
      let res_get_sell_product = await this.allService.get('productTransections/getSellTransection', this.dateSelect);
      this.customers = res_get_customer.data;
      this.data = res_get_sell_product.data;
      for (var i = 0; i < this.data.length; i++) {
        this.data[i].date = moment(this.data[i].created_at).format("YYYY-MM-DD");
      }

      console.log('res_get_sell_product', this.data);
      this.isLoading = false;
    } catch (error) {
      console.log("Error get data prodict type!", error);
      if (error.status === 404) {
        this.isLoading = false;
        this.showError('กรุณาลองใหม่อีกครั้ง');
      } else if (error.status === 401) {
        await this.allService.refreshToken();
        this.getData();
      } else {
        this.isLoading = false;
        this.showError('กรุณาลองใหม่อีกครั้ง');
      }
    }
  }

  multipleValue() {
    this.price = this.selectedItem.sell_price * this.amount_value;
  }


  // --------------------------------------- Open Modal ---------------------------------------//
  openEditModal(id): void {
    let data_select = Object.assign({}, this.data.filter(data => data.id == id)[0]);
    this.productSelected.name = data_select.product.name;
    this.customerSelected.name = data_select.customer ? data_select.customer.name : null;

    this.sellProductForm.value.sell_type = data_select.sell_type;
    this.price = data_select.sell_price;
    this.amount_value = data_select.amount;

    this.selectedItem = Object.assign({}, data_select);
    console.log('selectedItem', this.selectedItem);
    this.editModal.show();
  }

  openDeleteModal(id): void {
    this.selectedItem = Object.assign({}, this.data.filter(data => data.id == id)[0]);
    this.deleteModal.show();
  }

  async saveChange() {
    this.isLoading = true;
    console.log('this.selectedItem', this.selectedItem);
    console.log('this.productSelected', this.productSelected);
    try {
      let data = Object.assign({}, this.selectedItem, this.sellProductForm.value);
      console.log('this.sellProductForm.value', this.sellProductForm.value);

      data.product_id = this.productSelected.id || this.selectedItem.product_id;
      data.sell_type = parseInt(this.selectedItem.sell_type);
      data.shop_source_id = this.allService.getLocal('user').shop_id;
      data.customer_id = this.customerSelected.id ? this.customerSelected.id : null;

      console.log('data', data)
      let res_tran_in_product = await this.allService.putWithId('productTransections', this.selectedItem.id, data);
      this.initData();
      this.getData();
      this.showSuccess(`แก้ไขข้อมูลเรียบร้อย`);
    } catch (error) {
      let response = error;
      console.log("Error create product type", response);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.saveChange();
      } else {
        this.isLoading = false;
        this.showError(`ไม่สามารถแก้ไขได้ กรุณาลองใหม่อีกครั้ง`);
      }
    }

    this.editModal.hide();
  }

  async deleteItem() {
    console.log('delete data', this.selectedItem);
    this.isLoading = true;
    try {
      let res_delete_product_type = await this.allService.deleteWithId('productTransections', this.selectedItem.id);
      this.initData();
      this.getData();
      this.showSuccess('ลบข้อมูลเรียบร้อย');
    } catch (error) {
      console.log("Error delete data prodict type!", error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.deleteItem();
      } else {
        this.isLoading = false;
        this.showError('ไม่สามารถลบข้อมูลได้');
      }
    }

    this.deleteModal.hide();
  }
  // --------------------------------------- Toast ---------------------------------------//
  showSuccess(message) {
    this.toastr.success(message, 'สำเร็จ!');
  }

  showError(message) {
    this.toastr.error(message, 'เกิดข้อผิดพลาด');
  }

  //--------------------------------------- Typeahead ---------------------------------------//
  public changeTypeaheadLoading(status, e: boolean): void {
    this.typeaheadLoading[status] = e;
  }

  public changeTypeaheadNoResults(status, e: boolean): void {
    this.typeaheadNoResults[status] = e;
  }

  public typeaheadOnSelect(status, e: TypeaheadMatch): void {
    console.log('e', e);

    switch (status) {
      case 'product':
        this.productSelected = e.item;
        break;
      case 'customer':
        this.customerSelected = e.item;
        break;
    }

    console.log('Selected value:', e);
  }

  // --------------------------------------- date ---------------------------------------//
  onDateRangeChanged(event: IMyDateRangeModel) {
    this.dateSelect = {
      'date_from': this.setFormatDate(event.beginDate),
      'date_to': this.setFormatDate(event.endDate)
    }
    this.getData();
  }

  setFormatDate(data?) {
    if (typeof data === 'object') {
      return data.year + '-' + data.month + '-' + data.day
    } else if (typeof data === 'string') {
      return data
    }
  }
}
