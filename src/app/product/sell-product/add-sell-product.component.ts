import { CustomValidators } from 'ng2-validation';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { AllServiceService } from './../../service/all-service.service';
import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { TypeaheadMatch, ModalDirective } from 'ng2-bootstrap';
import { CustomerData } from '../../tempData/customer-data';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';


import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/of';

@Component({
  selector: 'app-add-sell-product',
  templateUrl: './add-sell-product.component.html',
  styleUrls: ['./add-sell-product.component.scss']
})
export class AddSellProductComponent implements OnInit {

  //Modal
  @ViewChild('editModal') public editModal: ModalDirective;

  Heading = 'เพิ่มการขายสินค้า';

  public isLoading: Boolean = false;

  public sellProductForm: FormGroup;

  public product_id: FormControl;
  public amount: FormControl;
  public shop_source_id: FormControl;
  public customer_id: FormControl;
  public sell_price: FormControl;
  public sell_type: FormControl;
  public remark: FormControl;

  public customerForm: FormGroup;

  public name_customer: FormControl;
  public tel_customer: FormControl;

  public data;

  //Typeahead
  public products;
  public productSelected: any;
  public price: number;
  public amount_value: number;
  public customers;
  public customerSelected: any;
  public typeaheadLoading: Object;
  public dataSource: Observable<any>;
  public asyncSelected: string = '';
  public typeaheadNoResults;

  constructor(
    private fb: FormBuilder,
    private allService: AllServiceService,
    public toastr: ToastsManager,
    public vcr: ViewContainerRef) {

    this.toastr.setRootViewContainerRef(vcr);

    //validate FormBuilder
    this.product_id = fb.control('', Validators.compose([Validators.required]));
    this.sell_type = fb.control('1', Validators.compose([Validators.required]));
    this.sell_price = fb.control('', Validators.compose([Validators.required, CustomValidators.number, CustomValidators.min(0)]));
    this.amount = fb.control('1', Validators.compose([Validators.required, CustomValidators.number, CustomValidators.min(0)]));

    this.sellProductForm = fb.group({
      'amount': this.amount,
      'remark': this.remark,
      'product_id': this.product_id,
      'shop_source_id': this.shop_source_id,
      'customer_id': this.customer_id,
      'sell_price': this.sell_price,
      'sell_type': this.sell_type
    });

    //validate FormBuilder Modal add customer
    this.name_customer = fb.control('', Validators.compose([Validators.required]));
    this.tel_customer = fb.control('', Validators.compose([Validators.required]));


    this.customerForm = fb.group({
      'name_customer': this.name_customer,
      'tel_customer': this.tel_customer
    });
  }

  ngOnInit() {
    this.setData();
    this.loadData();
  }

  setData() {
    this.data = {
      'amount': '',
      'remark': '',
      'product_id': '',
      'shop_source_id': '',
      'customer_id': '',
      'sell_price': '',
      'sell_type': ''
    };
    this.productSelected = {
      code: '',
      name: '',
      sell_price: 0
    }
    this.customerSelected = {
      name: ''
    }
    this.typeaheadLoading = {
      'product': false,
      'customer': false
    }
    this.typeaheadNoResults = {
      'product': false,
      'customer': false
    }
    this.amount_value = 1;
    this.price = 0;
  }

  async loadData() {
    this.isLoading = true;
    try {
      let resp_product_data = await this.allService.get('product');
      let resp_customer_data = await this.allService.get('customer');

      console.log('resp_product_data', resp_product_data);
      console.log('resp_customer_data', resp_customer_data);

      this.products = resp_product_data.data;
      this.customers = resp_customer_data.data;

      this.isLoading = false;

    } catch (error) {
      console.log('error get data', error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.loadData();
      } else {
        this.isLoading = false;
        this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
      }
    }
  }

  async sendForm() {
    let data = this.sellProductForm.value;
    console.log('data', this.sellProductForm.value);
    this.isLoading = !this.isLoading;
    try {

      data.shop_source_id = this.allService.getLocal('user').shop_id;
      data.product_id = this.productSelected.id;
      data.customer_id = this.customerSelected.id ? this.customerSelected.id : null;
      data.sell_type = parseInt(this.sellProductForm.value.sell_type);

      console.log('data', data);

      let res_sell_product = await this.allService.post('productTransections', data);

      // this.sellProductForm.reset();
      this.setData();
      this.isLoading = !this.isLoading;
      this.showSuccess(`บันทึกการขายสินค้าสำเร็จแล้ว`);
    } catch (error) {
      let response = error;
      console.log("Error create product type", response);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.sendForm();
      } else {
        this.isLoading = !this.isLoading;
        this.showError(`ไม่สามารถบันทึการขายสินค้าได้ กรุณาลองใหม่อีกครั้ง`);
      }
    }
  }

  // --------------------------------------- Toast ---------------------------------------//
  showSuccess(message) {
    this.toastr.success(message, 'สำเร็จ!');
  }

  showError(message) {
    this.toastr.error(message, 'เกิดข้อผิดพลาด');
  }

  //--------------------------------------- Typeahead ---------------------------------------//
  public changeTypeaheadLoading(status, e: boolean): void {
    this.typeaheadLoading[status] = e;
  }

  public changeTypeaheadNoResults(status, e: boolean): void {
    this.typeaheadNoResults[status] = e;
  }

  public typeaheadOnSelect(status, e: TypeaheadMatch): void {
    switch (status) {
      case 'product':
        this.productSelected = Object.assign({}, e.item);
        this.multipleValue();
        break;
      case 'customer':
        this.customerSelected = Object.assign({}, e.item);
        break;
    }

    console.log('Selected value:', e);
  }

  //--------------------------------------- Modal ---------------------------------------//
  openAddCustomerModal(): void {
    this.editModal.show();
  }

  async saveCustomer() {
    console.log('save data', this.customerForm.value);
    let customer_data = {
      'name': this.customerForm.value.name_customer,
      'tel': this.customerForm.value.tel_customer,
    }
    try {
      let res_edit_customer = await this.allService.post('customer', customer_data);
      this.showSuccess('แก้ไขข้อมูลเรียบร้อย');
      this.customerForm.reset();
      this.loadData();

    } catch (error) {
      console.log('error add customer', error);
      if (error.status === 500) {
        this.showError('ไม่สามารถเพิ่มลูกค้าได้ กรุณาลองใหม่อีกครั้ง');
      } else if (error.status === 401) {
        await this.allService.refreshToken();
        this.saveCustomer();
      } else {
        this.showError('กรุณาลองใหม่อีกครั้ง');
      }
    }
    this.editModal.hide();
  }

  multipleValue() {
    console.log(this.productSelected.sell_price, this.amount_value);
    console.log(this.productSelected.sell_price * this.amount_value);
    this.price = this.productSelected.sell_price * this.amount_value;
  }


}
