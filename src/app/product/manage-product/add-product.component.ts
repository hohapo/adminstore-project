import { ModalDirective } from 'ng2-bootstrap';
import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AllServiceService } from '../../service/all-service.service';
import { CustomValidators } from 'ng2-validation';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
	selector: 'app-add-product',
	templateUrl: './add-product.component.html',
	styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {

	@ViewChild('editModal') public editModal: ModalDirective;

	public Heading = 'สินค้า';

	public isLoading: Boolean = false;

	public codes = [{ 'code': '' }];

	public productForm: FormGroup;
	public code: FormControl;
	public autoCode: FormControl;
	public name: FormControl;
	public product_type_id: FormControl;
	public cost: FormControl;
	public sell_price: FormControl;
	public detail: FormControl;
	public remark: FormControl;
	public isCreateTranIn: FormControl;

	public product_type_data = [];
	public product_type_datas;

	public autoCodeProduct = false;

	public data;

	public brands = ["I-Phone", "Samsung", "Vivo", "Oppo", "Huawei", "Ais Lava", "Dtac ZTE", "True", "Lenovo", "Acer", "Asus", "Inovo", "Aplus", "Nipda", "Timi", "TWZ", "SKG", "Telego", "Nova", "Nex", "WIko", "Infone", "Aston", "Sony", "HTC"];
	public brands_select = '';

	public autoTranIn: Boolean = false;

	//form
	public productTypeForm: FormGroup;
	public name_product_type: FormControl;
	public detail_product_type: FormControl;

	constructor(
		private fb: FormBuilder,
		private allService: AllServiceService, public toastr: ToastsManager,
		public vcr: ViewContainerRef) {

		this.autoTranIn = this.allService.getLocal('user').shop_id !== null;
		console.log('autoTranIn', this.autoTranIn)

		this.toastr.setRootViewContainerRef(vcr);

		//validate FormBuilder
		this.name = fb.control('', Validators.compose([Validators.required]));
		this.product_type_id = fb.control('', Validators.compose([Validators.required]));
		this.cost = fb.control('', Validators.compose([Validators.required]));
		this.sell_price = fb.control('', Validators.compose([Validators.required]));

		this.productForm = fb.group({
			'code': this.code,
			'autoCode': this.autoCode,
			'name': this.name,
			'product_type_id': this.product_type_id,
			'cost': this.cost,
			'sell_price': this.sell_price,
			'detail': this.detail,
			'remark': this.remark,
			'isCreateTranIn': this.isCreateTranIn
		});

		//validate FormBuilder
		this.name_product_type = fb.control('', Validators.compose([Validators.required]));
		this.detail_product_type = fb.control('', Validators.compose([Validators.required]));

		this.productTypeForm = fb.group({
			'name_product_type': this.name_product_type,
			'detail_product_type': this.detail_product_type
		});

	}

	ngOnInit() {
		this.setData();
		this.loadData();
	}

	setData() {
		this.data = {
			'codes': '',
			'name': '',
			'product_type_id': '',
			'cost': '',
			'sell_price': '',
			'detail': '',
			'remark': '',
			'isCreateTranIn': ''
		};
	}

	clearData() {
		this.productForm.reset();
		this.codes = [{ 'code': '' }];
	}

	async loadData() {
		try {
			let res_product_type_data = await this.allService.get('productType');

			this.product_type_datas = res_product_type_data.data;

			console.log('product_type_datas', this.product_type_datas);
			this.setDataSelect();
			this.isLoading = false;
		} catch (error) {
			console.log('error load data product', error);
			if (error.status === 401) {
				await this.allService.refreshToken()
				this.loadData()
			}

		}
	}

	setDataSelect() {
		for (let i = 0; i < this.product_type_datas.length; i++) {
			this.product_type_data.push({ 'value': this.product_type_datas[i].id, 'label': this.product_type_datas[i].name });
		}
		console.log('product_type_data', this.product_type_data);
	}


	checkAutoCode() {
		console.log('checkAutoCode', this.productForm.value.autoCode);
		// let ctrl = this.productForm.get('code');
		// this.productForm.value.autoCode ? ctrl.disable() : ctrl.enable();
		this.codes = [{ 'code': '' }]
	}

	async sendForm() {
		console.log('data : ', this.productForm.value);
		let data = this.productForm.value;
		data.codes = this.setCodeFormat();
		data.name = this.brands_select !== '' ? this.brands_select + ' ' + data.name : data.name;
		this.isLoading = true;
		if (data.autoCode) {
			data.code = null;
		}
		try {
			console.log('data', data);
			let res_add_product = await this.allService.post('product', data); 
			this.clearData();
			this.isLoading = false;
			this.showSuccess(`เพิ่ม ${data.name} สำเร็จแล้ว`);
		} catch (error) {
			console.log("Error add product", error);
			this.isLoading = false;
			this.showError(`ไม่สามารถเพิ่มสินค้าได้ กรุณาลองใหม่อีกครั้ง`);
		}
	}

	// --------------------------------------- Toast ---------------------------------------//
	showSuccess(message) {
		this.toastr.success(message, 'สำเร็จ!');
	}

	showError(message) {
		this.toastr.error(message, 'เกิดข้อผิดพลาด');
	}

	// --------------------------------------- Open Modal ---------------------------------------//
	openEditModal(): void {
		this.editModal.show();
	}

	async saveProductType() {
		console.log('data : ', this.data, this.productTypeForm.value);
		let data = {
			'name': this.productTypeForm.value.name_product_type,
			'detail': this.productTypeForm.value.detail_product_type
		};

		this.isLoading = true;
		try {
			let res_add_product_type = await this.allService.post('productType', data);
			await this.loadData();
			this.productTypeForm.reset();
			this.editModal.hide();
			this.showSuccess(`เพิ่ม ${res_add_product_type.data.name} สำเร็จแล้ว`);
		} catch (error) {
			let response = error;
			if (error.status === 401) {
				await this.allService.refreshToken();
				this.saveProductType();
			} else {
				console.log("Error create product type", response);
				this.isLoading = false;
				this.showError(`ไม่สามารถเพิ่มประเภทสินค้าได้ กรุณาลองใหม่อีกครั้ง`);
			}
		}
	}


	// --------------------------------------- Codes ---------------------------------------//
	addCodes() {
		console.log('codes', this.codes)
		this.codes.push({ 'code': '' });
	}

	deleteCodes(index) {
		if (this.codes.length > 1) {
			this.codes.splice(index, 1);
		} else {
			this.codes = [{ 'code': '' }]
		}
	}

	setCodeFormat() {
		let data = [];
		for (var index = 0; index < this.codes.length; index++) {
			data.push(this.codes[index].code)
		}
		console.log('data', data)
		return JSON.stringify(data)
	}

}
