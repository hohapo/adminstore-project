import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { AllServiceService } from './../../service/all-service.service';
import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { ModalDirective } from 'ng2-bootstrap';
import { Router, NavigationExtras } from '@angular/router';
import { HideMenuService } from '../../shared/hide-menu.service';

@Component({
  selector: 'app-manage-product',
  templateUrl: './manage-product.component.html',
  styleUrls: ['./manage-product.component.scss']
})
export class ManageProductComponent implements OnInit {

  @ViewChild('editModal') public editModal: ModalDirective;
  @ViewChild('deleteModal') public deleteModal: ModalDirective;

  public Heading = 'สินค้า';

  public isLoading: Boolean = false;

  //data
  public product_data;

  //table
  public dtOptions: any = {};

  //Menu
  public isHideMenu;

  //Modal
  public selectedItem;
  public closeResult: string;

  //Checkbox
  public selected = [];
  public selectedLength;
  public haveSelected;

  public clickData;

  //Validate
  public productForm: FormGroup;
  public code: FormControl;
  public autoCode: FormControl;
  public name: FormControl;
  public product_type_id: FormControl;
  public cost: FormControl;
  public sell_price: FormControl;
  public detail: FormControl;
  public remark: FormControl;

  //SearchForm
  public productSearchForm: FormGroup;
  public search_text: FormControl;
  public search_type: FormControl;
  public search_status: FormControl;

  public product_type_data = [];
  public product_type_datas;

  public product_search_type;

  public product_status_data = [];
  public product_status_datas;


  public autoCodeProduct = false;



  public constructor(
    private router: Router,
    public hideMenuService: HideMenuService,
    public allService: AllServiceService,
    public toastr: ToastsManager,
    public vcr: ViewContainerRef,
    private fb: FormBuilder) {

    this.toastr.setRootViewContainerRef(vcr);


    this.getCategory();
    this.isHideMenu = true;

    hideMenuService.subscribe({
      next: isHideMenu => {
        this.isHideMenu = isHideMenu;
        return this.isHideMenu;
      }
    });

    this.haveSelected = false;

    //validate FormBuilder
    this.name = fb.control('', Validators.compose([Validators.required]));
    this.product_type_id = fb.control('', Validators.compose([Validators.required]));
    this.sell_price = fb.control('', Validators.compose([Validators.required]));

    this.productForm = fb.group({
      'code': this.code,
      'autoCode': this.autoCode,
      'name': this.name,
      'product_type_id': this.product_type_id,
      'cost': this.cost,
      'sell_price': this.sell_price,
      'detail': this.detail,
      'remark': this.remark,
    });

    this.search_text = fb.control('');
    this.search_type = fb.control('0');
    this.search_status = fb.control('0');

    this.productSearchForm = fb.group({
      'search_text': this.search_text,
      'search_type': this.search_type,
      'search_status': this.search_status
    });

  }

  ngOnInit() {
    this.initTable();
  }

  initTable() {
    this.dtOptions = {
      displayLength: 10,
      paginationType: 'simple_numbers',
      language: {
        info: 'แสดง _START_ ถึง _END_ จาก _TOTAL_',
        paginate: {
          previous: '<',
          next: '>',
        },
        search: 'ค้นหา',
        emptyTable: 'ไม่มีข้อมูล',
        infoEmpty: 'แสดง 0 ถึง 0 จาก 0',
        zeroRecords: 'ไม่มีข้อมูล',
        lengthMenu: 'แสดง _MENU_ แถว'
      },
      columnDefs: [{
        orderable: false,
        className: 'select-checkbox',
        targets: 0
      }],
      select: {
        style: 'os',
        selector: 'td:first-child',
        blurable: true,
        items: 'column',
        className: 'row-selected'
      },
      rowCallback: (nRow: number, aData: any, iDisplayIndex: number, iDisplayIndexFull: number) => {
        let self = this;
        $('td', nRow).unbind('click');
        $('td', nRow).bind('click', () => {
          self.clickHandler(aData);
        });
        return nRow;
      }
    };
  }

  clickHandler(dataSelect: any): void {
    console.log('someClickHandler', dataSelect);
    this.selectedItem = this.product_data.find(data => data.code === dataSelect[1]);
    console.log('selectedItem', this.selectedItem)
  }


  checkProduct() {
    var index = this.selected.find(data => data.code === this.selectedItem.code);

    if (index) {
      this.selected.splice(index, 1);
    } else {
      this.selected.push(this.selectedItem);
    }

    this.selectedLength = this.selected.length;

    if (this.selected.length > 0) {
      this.haveSelected = true;
    } else {
      this.haveSelected = false;
    }

    console.log('this.selected', this.selected);

  }

  async getCategory() {
    this.isLoading = !this.isLoading;
    try {
      let res_product_type_data = await this.allService.get('productType');

      console.log('res_product_type_data', res_product_type_data);

      this.product_type_datas = res_product_type_data.data;

      this.setData();

      this.isLoading = false;

    } catch (error) {
      console.log('error manage product', error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.getCategory();
      }
    }
  }

  async getData() {
    this.isLoading = !this.isLoading;
    try {
      let params = {
        'text': this.productSearchForm.value.search_text,
        'type': this.productSearchForm.value.search_type,
        'status': this.productSearchForm.value.search_status,
      }
      this.product_data = null;
      let res_product_data = await this.allService.post('product/search', params);

      console.log('res_product_data', res_product_data);

      this.product_data = res_product_data.data;

      this.isLoading = false;

    } catch (error) {
      console.log('error manage product', error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.getData();
      }
    }
  }

  setData() {
    for (let i = 0; i < this.product_type_datas.length; i++) {
      this.product_type_data.push({ 'value': this.product_type_datas[i].id, 'label': this.product_type_datas[i].name });
    }
    this.product_search_type = [{ 'value': 0, 'label': 'ทั้งหมด' }].concat(this.product_type_data)
    this.product_status_data = [
      { value: 0, label: 'ทั้งหมด' },
      { value: 1, label: 'ขายแล้ว' },
      { value: 2, label: 'อยู่ในสต็อค' }
    ];
  }

  sendSearchForm() {
    console.log('this.search', this.productSearchForm.value);
    this.getData();
  }

  // --------------------------------------- Open Modal ---------------------------------------//
  openEditModal(id): void {
    this.selectedItem = this.product_data.filter(product => product.id == id)[0];
    this.selectedItem.productTypeSelect = [this.selectedItem.product_type_id];
    this.selectedItem.storeSelect = [this.selectedItem.store_id];
    this.editModal.show();
  }

  openDeleteModal(id): void {
    console.log('openDeleteModal', id)
    this.selectedItem = this.product_data.filter(product => product.id === id)[0];
    console.log('selectedItem', this.selectedItem)
    this.deleteModal.show();
  }

  async saveChange() {
    this.isLoading = true;
    console.log(' this.selectedItem', this.selectedItem);
    try {
      let res_edit_product = await this.allService.putWithId('product', this.selectedItem.id, this.selectedItem);
      console.log('res_edit_product', res_edit_product);
      this.showSuccess('แก้ไขข้อมูลเรียบร้อย');
      this.getData();
    } catch (error) {
      console.log('Error update data product type!', error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.saveChange();
      } else {
        this.showError('กรุณาลองใหม่อีกครั้ง');
      }
    }
    this.editModal.hide();
  }

  async deleteItem() {
    console.log('delete data', this.selectedItem);
    this.isLoading = true;

    try {
      let res_delete_product = await this.allService.deleteWithId('product', this.selectedItem.id);
      await this.showSuccess('ลบข้อมูลเรียบร้อย');
      this.product_data = this.product_data.filter(product => product.id !== this.selectedItem.id);
      this.selectedItem = null;
      this.deleteModal.hide();
      this.isLoading = false;
      // this.getData();
    } catch (error) {
      console.log("Error delete data prodict type!", error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.deleteItem();
      } else {
        this.showError('ไม่สามารถลบข้อมูลได้');
        this.deleteModal.hide();
      }
    }


  }

  // --------------------------------------- Toast ---------------------------------------//
  showSuccess(message) {
    this.toastr.success(message, 'สำเร็จ!');
  }

  showError(message) {
    this.toastr.error(message, 'เกิดข้อผิดพลาด');
  }

  //--------------------------------------- Show Detail ---------------------------------------//

  showDetail(id) {
    this.router.navigate(['product/manage/list/info', id]);
  }

  //--------------------------------------- Print Barcode ---------------------------------------//

  printBarcode(id?) {
    console.log('this.selected', this.selected);
    let navigationExtras: NavigationExtras;
    let url
    if (id) {
      url = JSON.stringify({ data: [this.product_data.filter(product => product.id == id)[0]] })
    } else {
      url = JSON.stringify({ data: this.selected })
    }
    window.open("/#/product/print?dataSelect=" + url, "_blank")
  }

}
