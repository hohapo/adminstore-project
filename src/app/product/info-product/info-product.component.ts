import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { AllServiceService } from './../../service/all-service.service';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ProductData } from '../../tempData/product-data';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router, NavigationExtras } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-info-product',
  templateUrl: './info-product.component.html',
  styleUrls: ['./info-product.component.scss']
})
export class InfoProductComponent implements OnInit {

  public Heading = 'ข้อมูลสินค้า';

  public isProductDetailLoading: Boolean = false;

  public subGetParams: Subscription;
  public productId;
  public productDetail;

  public edit: Boolean = false;

  public selectedItem;

  public productForm: FormGroup;

  public code: FormControl;
  public autoCode: FormControl;
  public name: FormControl;
  public product_type_id: FormControl;
  public cost: FormControl;
  public sell_price: FormControl;
  public detail: FormControl;
  public remark: FormControl;

  public product_type_data = [];
  public product_type_datas;

  public constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private allService: AllServiceService,
    public toastr: ToastsManager,
    public vcr: ViewContainerRef) {

    this.toastr.setRootViewContainerRef(vcr);

    //validate FormBuilder
    this.name = fb.control('', Validators.compose([Validators.required]));
    this.product_type_id = fb.control('', Validators.compose([Validators.required]));
    this.cost = fb.control('', Validators.compose([Validators.required]));
    this.sell_price = fb.control('', Validators.compose([Validators.required]));

    this.productForm = fb.group({
      'code': this.code,
      'autoCode': this.autoCode,
      'name': this.name,
      'product_type_id': this.product_type_id,
      'cost': this.cost,
      'sell_price': this.sell_price,
      'detail': this.detail,
      'remark': this.remark,
    });
  }

  ngOnInit() {
    this.subGetParams = this.route.params.subscribe(
      params => {
        this.productId = params['id'];
      }
    );
    console.log('this.productId', this.productId);

    this.getDataProductDetail();
  }

  async getDataProductDetail() {
    this.isProductDetailLoading = true;
    try {
      let resp_productDetail = await this.allService.getWithId('product', this.productId)
      let res_product_type_data = await this.allService.get('productType');
      this.product_type_datas = res_product_type_data.data;
      this.productDetail = resp_productDetail.data;

      this.setDataSelect();
      console.log('resp_productDetail', this.productDetail);
      this.isProductDetailLoading = false;
    } catch (error) {
      console.log('error get product detail ', error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.getDataProductDetail()
      }
    }
  }
  setDataSelect() {
    console.log('this.product_type_datas', this.product_type_datas)
    for (let i = 0; i < this.product_type_datas.length; i++) {
      this.product_type_data.push({ 'value': this.product_type_datas[i].id.toString(), 'label': this.product_type_datas[i].name });
    }
    if (this.productDetail.product_type_id) {
      this.productForm.controls['product_type_id'].setValue(this.productDetail.product_type_id.toString());
      // this.productForm.controls['product_type_id'].setValue('113');
      console.log('this.productForm.value', this.productForm.value)
    }
    console.log('product_type_data', this.product_type_data);
  }

  async saveChange() {
    try {
      console.log('save change', this.productForm.value)
      let data = Object.assign({}, this.productForm.value);
      if (!data.product_type_id) {
        data.product_type_id = parseInt(this.productDetail.product_type_id);
      }
      console.log('data', data);
      let res_edit_product = await this.allService.putWithId('product', this.productId, data);
      this.showSuccess('แก้ไขข้อมูลเรียบร้อย');
      this.edit = false;
      this.getDataProductDetail();
    } catch (error) {
      console.log("Error update data product type!", error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.saveChange();
      } else {
        this.showError('กรุณาลองใหม่อีกครั้ง');
      }
    }
  }

  //--------------------------------------- Print Barcode ---------------------------------------//

  printBarcode() {
    let navigationExtras: NavigationExtras;
    navigationExtras = {
      queryParams: {
        "dataSelect": JSON.stringify({ data: [this.productDetail] })
      }
    };

    window.open("/#/product/print?dataSelect=" + JSON.stringify({ data: [this.productDetail] }), "_blank")

    // this.router.navigate(['product/print'], navigationExtras);
  }



  // --------------------------------------- Toast ---------------------------------------//
  showSuccess(message) {
    this.toastr.success(message, 'สำเร็จ!');
  }

  showError(message) {
    this.toastr.error(message, 'เกิดข้อผิดพลาด');
  }

}
