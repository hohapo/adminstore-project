import { Observable } from 'rxjs/Observable';
import { async } from '@angular/core/testing';
import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { ModalDirective, TypeaheadMatch } from 'ng2-bootstrap';
import { HideMenuService } from '../../shared/hide-menu.service';
import { AllServiceService } from '../../service/all-service.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import * as moment from 'moment';
import { IMyOptions, IMyDateRangeModel } from 'mydaterangepicker';


@Component({
  selector: 'app-accept-product',
  templateUrl: './accept-product.component.html',
  styleUrls: ['./accept-product.component.scss']
})
export class AcceptProductComponent implements OnInit {

  @ViewChild('editModal') public editModal: ModalDirective;
  @ViewChild('deleteModal') public deleteModal: ModalDirective;

  Heading = 'การนำเข้าสินค้า';

  public isLoading: Boolean = false;

  //date
  public dateSelect;

  //data
  public data = null;

  //table
  public dtOptions: any = {};

  //Typeahead
  public products;
  public productSelected: any;
  public typeaheadLoading: boolean = false;
  public dataSource: Observable<any>;
  public asyncSelected: string = '';
  public typeaheadNoResults;

  //Menu
  public isHideMenu;

  //Modal  
  public selectedItem;

  public acceptProductTypeForm: FormGroup;
  public shop_source_id: FormControl;
  public amount: FormControl;
  public code: FormControl;
  public remark: FormControl;
  public shop_destination_id: FormControl;
  public product_id: FormControl;
  public product_barcode: FormControl;

  public shops;
  public shop_data = [];

  private myDateRangePickerOptions: IMyOptions = {
    dateFormat: 'dd/mm/yyyy',
    firstDayOfWeek: 'su'
  };

  private dateRange: Object = {
    beginDate: { year: '', month: '', day: '' },
    endDate: { year: '', month: '', day: '' }
  };

  public constructor(public hideMenuService: HideMenuService,
    public allService: AllServiceService,
    public toastr: ToastsManager,
    public vcr: ViewContainerRef,
    private fb: FormBuilder) {
    this.toastr.setRootViewContainerRef(vcr);

    this.loadData();
    this.isHideMenu = true;

    hideMenuService.subscribe({
      next: isHideMenu => {
        this.isHideMenu = isHideMenu;
        return this.isHideMenu;
      }
    });

    //validate FormBuilder
    this.product_id = fb.control('', Validators.compose([Validators.required]));
    this.amount = fb.control('', Validators.compose([Validators.required]));

    this.acceptProductTypeForm = fb.group({
      'amount': this.amount,
      'remark': this.remark,
      'product_id': this.product_id
    });

  }

  ngOnInit() {
    this.initData();
    this.initTable();
  }

  initData() {
    this.productSelected = {
      code: '',
      name: ''
    }
    this.selectedItem = null;
    this.dateRange = {
      beginDate: { year: moment().year(), month: moment().month() + 1, day: moment().date() },
      endDate: { year: moment().year(), month: moment().month() + 1, day: moment().date() }
    };

    this.dateSelect = {
      'date_from': this.setFormatDate(moment().format('YYYY-MM-DD')),
      'date_to': this.setFormatDate(moment().format('YYYY-MM-DD'))
    }
  }

  async loadData() {
    await this.getData();
  }

  initTable() {
    this.dtOptions = {
      displayLength: 10,
      bSort: false,
      paginationType: 'simple_numbers',
      language: {
        info: 'แสดง _START_ ถึง _END_ จาก _TOTAL_',
        paginate: {
          previous: '<',
          next: '>',
        },
        search: 'ค้นหา',
        emptyTable: 'ไม่มีข้อมูล',
        infoEmpty: 'แสดง 0 ถึง 0 จาก 0',
        zeroRecords: 'ไม่มีข้อมูล',
        lengthMenu: 'แสดง _MENU_ แถว'
      }
    };
  }


  async getData() {
    this.isLoading = true;
    console.log('loadData');
    try {
      let res_get_tran_in_product = await this.allService.get('productTransections/getProductTransectionIn', this.dateSelect);

      this.data = res_get_tran_in_product.data;

      for (var i = 0; i < this.data.length; i++) {
        this.data[i].date = moment(this.data[i].created_at).format("YYYY-MM-DD");
      }

      console.log('res_get_tran_in_product', this.data);
      this.isLoading = false;
    } catch (error) {
      console.log("Error get data prodict type!", error);
      if (error.status === 404) {
        this.isLoading = false;
        this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
      } else if (error.status === 401) {
        await this.allService.refreshToken();
        this.getData();
      } else {
        this.isLoading = false;
        this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
      }
    }
  }


  // --------------------------------------- Open Modal ---------------------------------------//
  openEditModal(id): void {
    this.selectedItem = Object.assign({}, this.data.filter(data => data.id == id)[0]);
    this.productSelected.name = this.selectedItem.product.name;
    this.editModal.show();
  }

  openDeleteModal(id): void {
    this.selectedItem = Object.assign({}, this.data.filter(data => data.id == id)[0]);
    this.deleteModal.show();
  }

  async saveChange() {
    this.isLoading = true;
    console.log('this.selectedItem', this.selectedItem);
    console.log('this.productSelected', this.productSelected);
    let data = Object.assign({}, this.selectedItem, this.acceptProductTypeForm.value);
    data.product_id = this.productSelected.id || this.selectedItem.product_id;
    console.log('data', data)
    try {
      let res_tran_in_product = await this.allService.putWithId('productTransections', this.selectedItem.id, data);
      this.initData();
      this.getData();
      this.showSuccess(`แก้ไขข้อมูลเรียบร้อย`);
    } catch (error) {
      console.log("Error create product type", error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.saveChange();
      } else {
        this.isLoading = false;
        this.showError(`ไม่สามารถแก้ไขได้ กรุณาลองใหม่อีกครั้ง`);
      }
    }

    this.editModal.hide();
  }

  async deleteItem() {
    console.log('delete data', this.selectedItem);
    this.isLoading = true;
    try {
      let res_delete_product_type = await this.allService.deleteWithId('productTransections', this.selectedItem.id);
      this.showSuccess('ลบข้อมูลเรียบร้อย');
      this.getData();
    } catch (error) {
      console.log("Error delete data prodict type!", error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.deleteItem();
      } else {
        this.isLoading = false;
        this.showError('ไม่สามารถลบข้อมูลได้');
      }
    }
    this.deleteModal.hide();
  }
  // --------------------------------------- Toast ---------------------------------------//
  showSuccess(message) {
    this.toastr.success(message, 'สำเร็จ!');
  }

  showError(message) {
    this.toastr.error(message, 'เกิดข้อผิดพลาด');
  }

  //--------------------------------------- Typeahead ---------------------------------------//
  public changeTypeaheadLoading(e: boolean): void {
    this.typeaheadLoading = e;
  }

  public changeTypeaheadNoResults(status, e: boolean): void {
    this.typeaheadNoResults = e;
  }

  public typeaheadOnSelect(e: TypeaheadMatch): void {
    this.productSelected = e.item;
    console.log('Selected value:', e);
  }

  // --------------------------------------- date ---------------------------------------//
  onDateRangeChanged(event: IMyDateRangeModel) {
    this.dateSelect = {
      'date_from': this.setFormatDate(event.beginDate),
      'date_to': this.setFormatDate(event.endDate)
    }
    this.getData();
  }

  setFormatDate(data?) {
    if (typeof data === 'object') {
      return data.year + '-' + data.month + '-' + data.day
    } else if (typeof data === 'string') {
      return data
    }
  }
}
