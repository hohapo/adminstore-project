import { TypeaheadMatch } from 'ng2-bootstrap';
import { Observable } from 'rxjs/Observable';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { AllServiceService } from '../../service/all-service.service';
import { CustomValidators } from 'ng2-validation';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-add-accept-product',
  templateUrl: './add-accept-product.component.html',
  styleUrls: ['./add-accept-product.component.scss']
})
export class AddAcceptProductComponent implements OnInit {

  public Heading = 'เพิ่มรายการนำเข้าสินค้า';

  public isLoading: Boolean = false;

  public acceptProductTypeForm: FormGroup;
  public shop_source_id: FormControl;
  public amount: FormControl;
  public code: FormControl;
  public remark: FormControl;
  public shop_destination_id: FormControl;
  public product_id: FormControl;
  public product_barcode: FormControl;

  public data;

  public shops;
  public shop_data = [];

  //Typeahead
  public products;
  public productSelected: any;
  public typeaheadLoading: boolean = false;
  public dataSource: Observable<any>;
  public asyncSelected: string = '';
  public typeaheadNoResults;


  constructor(
    private fb: FormBuilder,
    private allService: AllServiceService,
    public toastr: ToastsManager,
    public vcr: ViewContainerRef) {

    this.toastr.setRootViewContainerRef(vcr);

    //validate FormBuilder
    this.product_id = fb.control('', Validators.compose([Validators.required]));
    this.amount = fb.control('1', Validators.compose([Validators.required, CustomValidators.number, CustomValidators.min(0)]));

    this.acceptProductTypeForm = fb.group({
      'amount': this.amount,
      'remark': this.remark,
      'product_id': this.product_id
    });
  }

  ngOnInit() {
    this.setData();
    this.loadData();
  }

  setData() {
    this.data = {
      'shop_source_id': '',
      'amount': '',
      'code': '',
      'remark': '',
      'shop_destination_id': '',
      'product_id': ''
    };
    this.productSelected = {
      code: '',
      name: ''
    }
  }


  async loadData() {
    this.isLoading = true;
    try {
      let resp_product_data = await this.allService.get('product');
      this.products = resp_product_data.data;

      this.isLoading = false;

    } catch (error) {
      console.log('error get shop', error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.loadData();
      } else {
        this.isLoading = false;
        this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
      }
    }
  }

  async sendForm() {
    let data = this.acceptProductTypeForm.value;
    this.isLoading = true;
    try {

      data.shop_destination_id = this.allService.getLocal('user').shop_id;
      data.product_id = this.productSelected.id;

      console.log('data', data);

      let res_tran_in_product = await this.allService.post('productTransections', data);

      this.acceptProductTypeForm.reset();
      this.isLoading = false;
      this.showSuccess(`นำเข้าสินค้าสำเร็จแล้ว`);
    } catch (error) {
      let response = error;
      console.log("Error create product type", response);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.sendForm();
      } else {
        this.isLoading = false;
        this.showError(`ไม่สามารถนำเข้าสินค้าได้ กรุณาลองใหม่อีกครั้ง`);
      }
    }
  }

  // --------------------------------------- Toast ---------------------------------------//
  showSuccess(message) {
    this.toastr.success(message, 'สำเร็จ!');
  }

  showError(message) {
    this.toastr.error(message, 'เกิดข้อผิดพลาด');
  }

  //--------------------------------------- Typeahead ---------------------------------------//
  public changeTypeaheadLoading(e: boolean): void {
    this.typeaheadLoading = e;
  }

  public changeTypeaheadNoResults(status, e: boolean): void {
    this.typeaheadNoResults = e;
  }

  public typeaheadOnSelect(e: TypeaheadMatch): void {
    this.productSelected = e.item;
    console.log('Selected value:', e);
  }

}
