import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-print-barcode',
  templateUrl: './print-barcode.component.html',
  styleUrls: ['./print-barcode.component.scss']
})
export class PrintBarcodeComponent implements OnInit {

  Heading = 'พิมพ์ Barcode';

  public oldItem;
  public dataSelect;
  public barcodeSelect = [];
  public arr: Array<any>;

  constructor(route: ActivatedRoute) {
    console.log(route.snapshot.data);
    route.queryParams.subscribe(params => {
      this.dataSelect = JSON.parse(params["dataSelect"]).data;
      console.log(this.dataSelect);
      this.setFormat();
    });

  }

  ngOnInit() {
  }

  setFormat() {
    for (let i = 0; i < this.dataSelect.length; i++) {
      let temp = {
        // 'barcode': 'http://www.pion.co.th/images/Knowledges/Barcode/barcode01.jpg',
        'barcode': this.dataSelect[i].barcode,
        'code': this.dataSelect[i].code,
        'id': this.dataSelect[i].id,
        'name': this.dataSelect[i].name,
        'quantity': '1'
      };
      this.barcodeSelect.push(temp);
    }
    this.barcodeSelect.map(() => { })
    this.oldItem = this.barcodeSelect;
  }

  removeItem(index) {
    this.barcodeSelect.splice(index, 1);
    console.log('barcodeSelect', this.barcodeSelect);
  }

  print(): void {
    let printContents, popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    // printContents = this.printSection;

    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open('', '_blank');
    popupWin.document.write(`
      <html>
        <head>
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
          <link href="https://fonts.googleapis.com/css?familyTrirong" rel="stylesheet">
          <link href="https://fonts.googleapis.com/css?family=Nunito+Sans" rel="stylesheet">
          <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
          
          <style>
            // @page {
            //   size:  auto;   /* auto is the initial value */
            //   margin: 5mm;  /* this affects the margin in the printer settings */
            // }
            // html, body {
            //   font-family: 'Nunito Sans', 'Trirong', 'FontAwesome';
            //   width:98%;
            //   background: #FFF; 
            //   font-size: 9.5pt;
            // }
            .panel-body .row {
              margin: 0px;
            }

            .panel-body .row .eachBarcode {
              border: 1px solid;
              text-align: center;
              margin-left: -1px;
              margin-bottom: -1px;
              width: 20%;
              padding: 10px 15px 5px 15px;
              float: left;
            }

            .panel-body .row .eachBarcode img {
              padding: 0px;
              height: 25;
              margin-bottom: 5px;
            }

            .panel-body .row .eachBarcode p {
              font-size: x-small;
              color: black;
              margin: 0px;
              line-height: 10px;
            }
            
          </style>
        </head>
      <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }


  printFormat(): void {
    let printContents, popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    // printContents = this.printSection;

    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open('', '_blank');
    popupWin.document.write(`
      <html>
        <head>
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
          <link href="https://fonts.googleapis.com/css?familyTrirong" rel="stylesheet">
          <link href="https://fonts.googleapis.com/css?family=Nunito+Sans" rel="stylesheet">
          <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
          
          <style>
            // @page {
            //   size:  auto;   /* auto is the initial value */
            //   margin: 5mm;  /* this affects the margin in the printer settings */
            // }
            // html, body {
            //   font-family: 'Nunito Sans', 'Trirong', 'FontAwesome';
            //   width:98%;
            //   background: #FFF; 
            //   font-size: 9.5pt;
            // }
            .panel-body {
              padding-top: 1.65cm;
              padding-left: 3mm;
              padding-right: 4mm;
            }

            .panel-body .row {
              margin: 0px;
            }

            .panel-body .row .eachBarcode {
             
              text-align: center;
              width: 38mm;
              height: 22mm;
              float: left;
              padding-left: 10px;
              padding-right: 10px;
              display: flex;
              flex-direction: column;
              justify-content: center;
              margin-right: 3mm;
            }

            .panel-body .row .eachBarcode:nth-child(5n){
              margin: 0px;
            }

            .panel-body .row .eachBarcode img {
              padding: 0px;
              height: 25;
              margin-bottom: 5px;
              width: 100%;
            }

            .panel-body .row .eachBarcode p {
              font-size: x-small;
              color: black;
              margin: 0px;
              line-height: 10px;
            }
            
          </style>
        </head>
      <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

}
