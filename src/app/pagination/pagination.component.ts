import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

  @Input() total:number;
  @Input() perPage:number;
  @Input() pageActive:number;
  @Output() currentPage = new EventEmitter<number>();;

  public pages = [];
  public selectedIdx;

  constructor() { 
   
  }

  ngOnInit() {
    this.selectedIdx = this.pageActive || 0;
    this.setPage();
  }

  setPage(){
    let page = Math.ceil(this.total / this.perPage);
    for(let i = 1; i <= page; i++){
      this.pages.push(i);
    }
  }

  selectPage(index):void {
    if(index >= 0 && index < this.pages.length){
      this.selectedIdx = index;
      this.currentPage.emit(this.selectedIdx+1);
    }
  }

}
