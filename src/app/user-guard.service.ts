import { HideSidebarService } from './shared/hide-sidebar.service';
import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AllServiceService } from './service/all-service.service';

@Injectable()
export class UserGuardService implements CanActivate {

  public isHideSidebar: boolean;

  constructor(private router: Router, private allService: AllServiceService, public hideSidebarService: HideSidebarService) { }

  canActivate() {
    if (this.allService.getLocal('user')) {
      if (this.allService.getLocal('user').shop_id) {
        this.isHideSidebar = false;
        this.router.navigate(['/report/all/list']);
        return true;
      } else {
        this.router.navigate(['/report/list']);
        return false;
      }
    } else {
      this.isHideSidebar = true;
      this.router.navigate(['/login']);
    }
    this.hideSidebarService.next(this.isHideSidebar);
  }


}
