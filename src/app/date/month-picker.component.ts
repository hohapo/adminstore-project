import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'month-picker',

  template: `
    <div>
      <select class="form-control"  required>
        <option  *ngFor="let p of months"  [selected]="mm === p.val " [value]="p.val">{{p.name}}</option>    
      </select>
    </div>`
})

export class MonthPicker implements OnInit {

  public mm;

  months = [
    { val: '01', name: 'มกราคม' },
    { val: '02', name: 'กุมภาพันธ์' },
    { val: '03', name: 'มีนาคม' },
    { val: '04', name: 'เมษายน' },
    { val: '05', name: 'พฤษภาคม' },
    { val: '06', name: 'มิถุนายน' },
    { val: '07', name: 'กรกฎาคม' },
    { val: '08', name: 'สิงหาคม' },
    { val: '09', name: 'กันยายน' },
    { val: '10', name: 'ตุลาคม' },
    { val: '11', name: 'พฤษจิกายน' },
    { val: '12', name: 'ธันวาคม' }
  ];

  ngOnInit() {
    this.getMonth();
  }

  getMonth() {
    var today = new Date();
    this.mm = today.getMonth() + 1;
    if (this.mm < 10) {
      this.mm = '0' + this.mm
    }
  }
}


