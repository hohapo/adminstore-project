import { Injectable, EventEmitter } from '@angular/core';
@Injectable()
export class LoginStyleService extends EventEmitter<any> {

  public isLoginPage: boolean;

  constructor(){
    super();
  }

  initLoginStyle() {
    this.isLoginPage = true;
    console.log('initLoginStyle :: ', this.isLoginPage);
    return this.isLoginPage;
  }

  setLoginStyle() {
    this.isLoginPage = !this.isLoginPage;
    console.log('set LoginStyle :: ', this.isLoginPage);
    return this.isLoginPage;
  }

  getLoginStyle() {
    return this.isLoginPage;
  }


}
