/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { HideMenuService } from './hide-menu.service';

describe('Service: HideMenu', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HideMenuService]
    });
  });

  it('should ...', inject([HideMenuService], (service: HideMenuService) => {
    expect(service).toBeTruthy();
  }));
});
