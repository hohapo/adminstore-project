import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class HideMenuService extends EventEmitter<any> {

  public isShowMenu: boolean;

  constructor() {
    super();
  }

  initHideMenu() {
    this.isShowMenu = true;
    console.log('init hide menu :: ', this.isShowMenu);
    return this.isShowMenu;
  }

  setHideMenu() {
    this.isShowMenu = !this.isShowMenu;
    console.log('set hide menu :: ', this.isShowMenu);
    return this.isShowMenu;
  }

  getHideMenu() {
    return this.isShowMenu;
  }


}
