/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { HideSidebarService } from './hide-sidebar.service';

describe('Service: HideSidebar', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HideSidebarService]
    });
  });

  it('should ...', inject([HideSidebarService], (service: HideSidebarService) => {
    expect(service).toBeTruthy();
  }));
});
