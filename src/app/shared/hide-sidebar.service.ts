import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class HideSidebarService extends EventEmitter<any> {

  public isHideSidebar: boolean;

  constructor() {
    super();
  }

  initHideSidebar() {
    this.isHideSidebar = true;
    console.log('init hide Sidebar :: ', this.isHideSidebar);
    return this.isHideSidebar;
  }

  setHideSidebar() {
    this.isHideSidebar = !this.isHideSidebar;
    console.log('set hide Sidebar :: ', this.isHideSidebar);
    return this.isHideSidebar;
  }

  getHideSidebar() {
    return this.isHideSidebar;
  }

}
