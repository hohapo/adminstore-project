import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { AllServiceService } from '../service/all-service.service';
import { CustomValidators } from 'ng2-validation';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import * as moment from "moment";
import { IMyOptions, IMyDateModel, IMyDate } from 'mydatepicker';


@Component({
  selector: 'app-expense-add',
  templateUrl: './expense-add.component.html',
  styleUrls: ['./expense-add.component.scss']
})
export class ExpenseAddComponent implements OnInit {

  public Heading = 'รายจ่ายจิปาถะ';

  public isLoading: Boolean = false;

  public expenseForm: FormGroup;
  public detail: FormControl;
  public expense_date: FormControl;
  public amount: FormControl;

  public data;
  public date;

  private selDate: IMyDate = { year: 0, month: 0, day: 0 };
  private myDatePickerOptions: IMyOptions = {
    // other options...
    dateFormat: 'dd/mm/yyyy',
    firstDayOfWeek: 'su',
    selectionTxtFontSize: '16px',
    editableDateField: false,
    openSelectorOnInputClick: true
  };

  constructor(
    private fb: FormBuilder,
    private allService: AllServiceService,
    public toastr: ToastsManager,
    public vcr: ViewContainerRef) {

    this.toastr.setRootViewContainerRef(vcr);


    //validate FormBuilder
    this.detail = fb.control('', Validators.compose([Validators.required]));
    this.expense_date = fb.control('', Validators.compose([Validators.required]));
    this.amount = fb.control('', [Validators.compose([Validators.required]), CustomValidators.number]);

    this.expenseForm = fb.group({
      'detail': this.detail,
      'expense_date': this.expense_date,
      'amount': this.amount
    });

  }

  ngOnInit() {
    this.setData();
    this.setCurrentDate();
  }

  setData() {
    this.data = {
      'detail': '',
      'expense_date': '',
      'amount': '',
      'user_id': ''
    };
  }

  setCurrentDate() {
    console.log('current date', moment().format())
    this.date = {
      'year': parseInt(moment().format('YYYY')),
      'month': parseInt(moment().format('MM')),
      'day': parseInt(moment().format('DD')),
    }
    this.selDate = this.date;
  }

  setFormatDate(data?) {
    if (typeof data === 'object') {
      return data.year + '-' + data.month + '-' + data.day
    } else if (typeof data === 'string') {
      return data
    }
  }

  async sendForm() {
    console.log('this.expenseForm.value : ', this.expenseForm.value, this.date.date);

    this.data = this.expenseForm.value;
    this.data.expense_date = this.setFormatDate(this.date.date);
    console.log('this.data', this.data);

    this.isLoading = true;
    try {
      let res_add_expense = await this.allService.post('generalExpense', this.data);
      this.expenseForm.reset();
      this.setCurrentDate();
      this.isLoading = false;
      this.showSuccess(`เพิ่ม ${res_add_expense.data.detail} สำเร็จแล้ว`);
    } catch (error) {
      console.log("Error create expense type", error);
      if (error.status === 401) {
        await this.allService.refreshToken()
        this.sendForm()
      } else {
        this.isLoading = false;
        this.showError(`ไม่สามารถเพิ่มรายจ่ายได้ กรุณาลองใหม่อีกครั้ง`);
      }
    }
  }

  // --------------------------------------- Toast ---------------------------------------//
  showSuccess(message) {
    this.toastr.success(message, 'สำเร็จ!');
  }

  showError(message) {
    this.toastr.error(message, 'เกิดข้อผิดพลาด');
  }

  // --------------------------------------- date ---------------------------------------//
  onDateChanged(event: IMyDateModel) {
    console.log('IMyDateModel', event);
    this.date = event.date;
    console.log('IMyDateModel', this.date);
  }

}
