import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { AllServiceService } from './../service/all-service.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { ModalDirective } from 'ng2-bootstrap';
import { TableData } from '../tempData/table-data';
import { NgbPaginationConfig, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { CustomValidators } from 'ng2-validation';
import * as moment from 'moment';
import { IMyOptions, IMyDateRangeModel } from 'mydaterangepicker';

@Component({
  selector: 'app-expense',
  templateUrl: './expense.component.html',
  styleUrls: ['./expense.component.scss']
})
export class ExpenseComponent implements OnInit {

  @ViewChild('editModal') public editModal: ModalDirective;
  @ViewChild('deleteModal') public deleteModal: ModalDirective;

  public Heading = 'รายจ่ายจิปาถะ';

  public isLoading: Boolean = false;

  //date
  public dateSelect;

  //data
  public data = null;

  //table
  public dtOptions: any = {};

  //Pagination
  public totalLength;

  //Menu
  public isHideMenu;

  //Modal  
  public selectedItem;

  //form
  public expenseForm: FormGroup;
  public detail: FormControl;
  public expense_date: FormControl;
  public amount: FormControl;
  public ex_date: NgbDateStruct;
  public date: { year: number, month: number };

  private myDateRangePickerOptions: IMyOptions = {
    dateFormat: 'dd/mm/yyyy',
    firstDayOfWeek: 'su'
  };

  private dateRange: Object = {
    beginDate: { year: '', month: '', day: '' },
    endDate: { year: '', month: '', day: '' }
  };

  public constructor(
    public allService: AllServiceService,
    public toastr: ToastsManager,
    public vcr: ViewContainerRef,
    private fb: FormBuilder) {

    this.toastr.setRootViewContainerRef(vcr);

    this.isLoading = false;

    this.isHideMenu = true;

    //validate FormBuilder
    this.detail = fb.control('', Validators.compose([Validators.required]));
    this.expense_date = fb.control('', Validators.compose([Validators.required]));
    this.amount = fb.control('', [Validators.compose([Validators.required]), CustomValidators.number]);

    this.expenseForm = fb.group({
      'detail': this.detail,
      'expense_date': this.expense_date,
      'amount': this.amount
    });

  }

  ngOnInit() {
    this.initTable();
    this.initData();
    this.getData();
  }

  initTable() {
    this.dtOptions = {
      displayLength: 10,
      paginationType: 'simple_numbers',
      language: {
        info: 'แสดง _START_ ถึง _END_ จาก _TOTAL_',
        paginate: {
          previous: '<',
          next: '>',
        },
        search: 'ค้นหา',
        emptyTable: 'ไม่มีข้อมูล',
        infoEmpty: 'แสดง 0 ถึง 0 จาก 0',
        zeroRecords: 'ไม่มีข้อมูล',
        lengthMenu: 'แสดง _MENU_ แถว'
      }
    };
  }

  initData() {
    this.dateRange = {
      beginDate: { year: moment().year(), month: moment().month() + 1, day: moment().date() },
      endDate: { year: moment().year(), month: moment().month() + 1, day: moment().date() }
    };

    this.dateSelect = {
      'date_from': this.setFormatDate(moment().format('YYYY-MM-DD')),
      'date_to': this.setFormatDate(moment().format('YYYY-MM-DD'))
    }
  }

  async getData() {
    this.isLoading = !this.isLoading;
    console.log('loadData');
    try {
      let res_get_product_type = await this.allService.get('generalExpense', this.dateSelect);
      this.data = res_get_product_type.data;
      console.log('data', this.data);
      this.isLoading = false;
    } catch (error) {
      console.log("Error get data prodict type!", error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.getData();
      } else {
        this.isLoading = false;
        this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
      }
    }
  }



  // --------------------------------------- Open Modal ---------------------------------------//
  openEditModal(id): void {
    this.selectedItem = this.data.filter(data => data.id === id)[0];
    let dateTemp = moment(this.selectedItem.expense_date, 'YYYY-MM-DD');
    this.selectedItem.expense_date = {
      'year': parseInt(dateTemp.format('YYYY')),
      'month': parseInt(dateTemp.format('MM')),
      'day': parseInt(dateTemp.format('DD'))
    };
    this.ex_date = this.selectedItem.expense_date;
    this.editModal.show();
  }

  openDeleteModal(id): void {
    this.selectedItem = this.data.filter(data => data.id === id)[0];
    this.deleteModal.show();
  }

  async saveChange() {
    console.log('save data', this.selectedItem, this.expenseForm.value);
    this.isLoading = !this.isLoading;
    this.selectedItem.expense_date = this.setFormatDate(this.expenseForm.value.expense_date);
    console.log('this.selectedItem', this.selectedItem);
    try {
      let res_edit_product_type = await this.allService.putWithId('generalExpense', this.selectedItem.id, this.selectedItem);
      this.showSuccess('แก้ไขข้อมูลเรียบร้อย');
      this.getData();
    } catch (error) {
      console.log("Error update data prodict type!", error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.saveChange();
      } else {
        this.showError('ไม่สามารถแก้ไขข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
        this.isLoading = false
      }
    }
    this.editModal.hide();
  }

  async deleteItem() {
    console.log('delete data', this.selectedItem);
    this.isLoading = true;
    try {
      let res_delete_product_type = await this.allService.deleteWithId('generalExpense', this.selectedItem.id);
      this.showSuccess('ลบข้อมูลเรียบร้อย');
      this.getData();
    } catch (error) {
      console.log("Error delete data prodict type!", error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.deleteItem();
      } else {
        this.showError('ไม่สามารถลบข้อมูลได้');
        this.isLoading = false
      }
    }
    this.deleteModal.hide();
  }

  // --------------------------------------- Toast ---------------------------------------//
  showSuccess(message) {
    this.toastr.success(message, 'สำเร็จ!');
  }

  showError(message) {
    this.toastr.error(message, 'เกิดข้อผิดพลาด');
  }

  // --------------------------------------- date ---------------------------------------//
  onDateRangeChanged(event: IMyDateRangeModel) {
    this.dateSelect = {
      'date_from': this.setFormatDate(event.beginDate),
      'date_to': this.setFormatDate(event.endDate)
    }
    this.getData();
  }

  setFormatDate(data?) {
    if (typeof data === 'object') {
      return data.year + '-' + data.month + '-' + data.day;
    } else if (typeof data === 'string') {
      return data;
    }
  }

}
