import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PaginationModule, ModalModule, TypeaheadModule, ProgressbarModule, CollapseModule } from 'ng2-bootstrap';
import { ComponentLoaderFactory } from 'ng2-bootstrap/component-loader';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { CustomFormsModule } from 'ng2-validation';
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { SelectModule } from 'angular2-select';
import { DataTablesModule } from 'angular-datatables';
import { MyDatePickerModule } from 'mydatepicker';
import { MyDateRangePickerModule } from 'mydaterangepicker';

import { HideMenuService } from './shared/hide-menu.service';
import { PositioningService } from 'ng2-bootstrap/positioning';
import { ProgressbarConfig } from 'ng2-bootstrap/progressbar';

import { AuthGuardService } from './auth-guard.service';
import { UserGuardService } from './user-guard.service';
import { AllServiceService } from './service/all-service.service';
import { ToastService } from './service/toast.service';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { StoreComponent } from './store/store.component';
import { ManageProductComponent } from './product/manage-product/manage-product.component';
import { AcceptProductComponent } from './product/accept-product/accept-product.component';
import { SellProductComponent } from './product/sell-product/sell-product.component';
import { TransferProductComponent } from './product/transfer-product/transfer-product.component';
import { CustomerComponent } from './customer/customer.component';
import { ProductTypeComponent } from './product-type/product-type.component';
import { ReportComponent } from './report/report.component';
import { InfoProductComponent } from './product/info-product/info-product.component';
import { AddProductComponent } from './product/manage-product/add-product.component';
import { AddAcceptProductComponent } from './product/accept-product/add-accept-product.component';
import { AddSellProductComponent } from './product/sell-product/add-sell-product.component';
import { AddTransferProductComponent } from './product/transfer-product/add-transfer-product.component';
import { AddStoreComponent } from './store/add-store.component';
import { ProductTypeAddComponent } from './product-type/product-type-add.component';
import { CustomerAddComponent } from './customer/customer-add.component';
import { ExpenseComponent } from './expense/expense.component';
import { ExpenseAddComponent } from './expense/expense-add.component';
import { ReportAllComponent } from './report/report-all.component';
import { ReportDayComponent } from './report/report-day.component';
import { ReportMonthComponent } from './report/report-month.component';
import { LoginComponent } from './login/login.component';
import { UserPermissionComponent } from './user-permission/user-permission.component';
import { HomeComponent } from './home/home.component';
import { UserAddComponent } from './user-permission/user-add.component';
import { FixManageComponent } from './claim-fix/fix-manage.component';
import { FixInfoComponent } from './claim-fix/fix-info.component';
import { FixAddComponent } from './claim-fix/fix-add.component';
import { FixPrintComponent } from './claim-fix/fix-print.component';
import { ChangeManageComponent } from './claim-change/change-manage.component';
import { ChangeInfoComponent } from './claim-change/change-info.component';
import { ChangeAddComponent } from './claim-change/change-add.component';
import { ChangePrintComponent } from './claim-change/change-print.component';
import { PrintBarcodeComponent } from './product/print-barcode/print-barcode.component';
import { ArrayNumberPipe } from './shared/array-number.pipe';
import { LoadingComponent } from './loading/loading.component';
import { PaginationComponent } from './pagination/pagination.component';
import { HideSidebarService } from './shared/hide-sidebar.service';
import { MonthPicker } from './date/month-picker.component';
import { YearPicker } from './date/year-picker.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidebarComponent,
    StoreComponent,
    ManageProductComponent,
    AcceptProductComponent,
    SellProductComponent,
    TransferProductComponent,
    CustomerComponent,
    ProductTypeComponent,
    ReportComponent,
    InfoProductComponent,
    AddProductComponent,
    AddAcceptProductComponent,
    AddSellProductComponent,
    AddTransferProductComponent,
    AddStoreComponent,
    ProductTypeAddComponent,
    CustomerAddComponent,
    ExpenseComponent,
    ExpenseAddComponent,
    ReportAllComponent,
    ReportDayComponent,
    ReportMonthComponent,
    LoginComponent,
    UserPermissionComponent,
    HomeComponent,
    UserAddComponent,
    FixManageComponent,
    FixInfoComponent,
    FixAddComponent,
    FixPrintComponent,
    ChangeManageComponent,
    ChangeInfoComponent,
    ChangeAddComponent,
    ChangePrintComponent,
    PrintBarcodeComponent,
    ArrayNumberPipe,
    LoadingComponent,
    PaginationComponent,
    MonthPicker,
    YearPicker
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    NgbModule.forRoot(),
    PaginationModule,
    TypeaheadModule,
    ProgressbarModule,
    CollapseModule,
    ModalModule,
    ChartsModule,
    CustomFormsModule,
    ToastModule.forRoot(),
    SelectModule,
    DataTablesModule,
    MyDatePickerModule,
    MyDateRangePickerModule,
    RouterModule.forRoot([
      { path: 'login', component: LoginComponent },
      {
        path: '',
        canActivate: [AuthGuardService],
        children: [
          {
            path: 'user',
            children: [
              { path: 'list', component: UserPermissionComponent },
              { path: 'add', component: UserAddComponent }
            ]
          },
          {
            path: 'store',
            children: [
              { path: 'list', component: StoreComponent },
              { path: 'add', component: AddStoreComponent }
            ]
          },
          {
            path: 'product',
            children: [
              { path: 'manage/list', component: ManageProductComponent },
              { path: 'manage/list/info/:id', component: InfoProductComponent },
              { path: 'manage/add', component: AddProductComponent },
              { path: 'type/list', component: ProductTypeComponent },
              { path: 'type/add', component: ProductTypeAddComponent },
              { path: 'accept/list', component: AcceptProductComponent },
              { path: 'accept/add', component: AddAcceptProductComponent },
              { path: 'transfer/list', component: TransferProductComponent },
              { path: 'transfer/add', component: AddTransferProductComponent },
              { path: 'sell/list', component: SellProductComponent },
              { path: 'sell/add', component: AddSellProductComponent },
              { path: 'print', component: PrintBarcodeComponent, data: [{ 'dataSelect': 'lass' }] }
            ]
          },
          {
            path: 'customer',
            children: [
              { path: 'list', component: CustomerComponent },
              { path: 'add', component: CustomerAddComponent }
            ]
          },
          {
            path: 'report',
            children: [
              { path: 'list', component: ReportComponent },
              { path: 'all/list', component: ReportAllComponent },
              { path: 'all/list/info', component: ReportComponent },

            ]
          },
          {
            path: 'claim',
            children: [
              {
                path: 'fix',
                children: [
                  {
                    path: 'list', children: [
                      { path: '', component: FixManageComponent },
                      {
                        path: 'info/:id',
                        children: [
                          { path: '', component: FixInfoComponent },
                          { path: 'print', component: FixPrintComponent }
                        ]
                      }
                    ]
                  },
                  { path: 'add', component: FixAddComponent }
                ]
              },
              {
                path: 'change',
                children: [
                  {
                    path: 'list', children: [
                      { 'path': '', component: ChangeManageComponent },
                      {
                        path: 'info/:id',
                        children: [
                          { path: '', component: ChangeInfoComponent },
                          { path: 'print', component: ChangePrintComponent }
                        ]
                      }
                    ]
                  },
                  { path: 'add', component: ChangeAddComponent }
                ]
              }
            ]
          },
          {
            path: 'expense',
            children: [
              { path: 'list', component: ExpenseComponent },
              { path: 'add', component: ExpenseAddComponent }
            ]
          },
        ]
      },
      // { path: '404', component: NotFoundComponent },
      { path: '', redirectTo: '/report', pathMatch: 'full' },
      // { path: '**', redirectTo: '/', pathMatch: 'full' }
    ], { useHash: true })
  ],
  providers: [
    AuthGuardService,
    UserGuardService,
    AllServiceService,
    HideMenuService,
    HideSidebarService,
    SidebarComponent,
    ComponentLoaderFactory,
    PositioningService,
    ProgressbarConfig,
    ToastService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
