import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { ModalDirective } from 'ng2-bootstrap';
import { AllServiceService } from '../service/all-service.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import * as moment from 'moment';
import { IMyOptions, IMyDateRangeModel } from 'mydaterangepicker';

@Component({
  selector: 'app-change-manage',
  templateUrl: './change-manage.component.html',
  styleUrls: ['./change-manage.component.scss']
})
export class ChangeManageComponent implements OnInit {

  @ViewChild('deleteModal') public deleteModal: ModalDirective;

  public Heading = 'การเปลี่ยนสินค้า';

  //date
  public dateSelect;

  public isLoading: Boolean = false;

  //data
  public data = null;

  //table
  public dtOptions: any = {};

  //Menu
  public isHideMenu;

  //Modal  
  public selectedItem;

  private myDateRangePickerOptions: IMyOptions = {
    dateFormat: 'dd/mm/yyyy',
    firstDayOfWeek: 'su'
  };

  private dateRange: Object = {
    beginDate: { year: '', month: '', day: '' },
    endDate: { year: '', month: '', day: '' }
  };

  public constructor(
    private fb: FormBuilder,
    public allService: AllServiceService,
    public toastr: ToastsManager,
    public vcr: ViewContainerRef,
    public router: Router) {

    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.dtOptions = {
      displayLength: 10,
      paginationType: 'simple_numbers',
      language: {
        info: 'แสดง _START_ ถึง _END_ จาก _TOTAL_',
        paginate: {
          previous: '<',
          next: '>',
        },
        search: 'ค้นหา',
        emptyTable: 'ไม่มีข้อมูล',
        infoEmpty: 'แสดง 0 ถึง 0 จาก 0',
        zeroRecords: 'ไม่มีข้อมูล',
        lengthMenu: 'แสดง _MENU_ แถว'
      }
    };
    this.initData();
    this.loadData();
  }

  initData() {
    this.dateRange = {
      beginDate: { year: moment().year(), month: moment().month() + 1, day: moment().date() },
      endDate: { year: moment().year(), month: moment().month() + 1, day: moment().date() }
    };

    this.dateSelect = {
      'date_from': this.setFormatDate(moment().format('YYYY-MM-DD')),
      'date_to': this.setFormatDate(moment().format('YYYY-MM-DD'))
    }
  }

  async loadData() {
    this.isLoading = true;

    try {
      let resp_exchange = await this.allService.get('exchangeTransection', this.dateSelect);

      console.log('exchangeTransection', resp_exchange);

      this.data = resp_exchange.data;

      this.isLoading = false;

    } catch (error) {
      console.log('error get exchange', error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.loadData();
      } else {
        this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
        this.isLoading = false;
      }
    }
  }

  //--------------------------------------- Open Modal ---------------------------------------//

  openDeleteModal(item): void {
    this.selectedItem = item;
    this.deleteModal.show();
  }

  async deleteItem() {
    console.log('delete data', this.selectedItem);
    this.isLoading = !this.isLoading;

    try {
      let resp_delete_exchange = await this.allService.deleteWithId('exchangeTransection', this.selectedItem.id);

      console.log('Delete data user : ', resp_delete_exchange.data);
      this.showSuccess('ลบข้อมูลเรียบร้อย');
      this.loadData();
    } catch (error) {
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.deleteItem();
      } else {
        this.showError('ไม่สามารถลบข้อมูลได้');
        this.isLoading = false;
      }
    }
    this.deleteModal.hide();
  }




  //--------------------------------------- Detail ---------------------------------------//
  showDetail(item) {
    console.log('item', item);
    this.router.navigate(['claim/change/list/info', item.id]);
  }

  // --------------------------------------- Toast ---------------------------------------//
  showSuccess(message) {
    this.toastr.success(message, 'สำเร็จ!');
  }

  showError(message) {
    this.toastr.error(message, 'เกิดข้อผิดพลาด');
  }

  // --------------------------------------- date ---------------------------------------//
  onDateRangeChanged(event: IMyDateRangeModel) {
    this.dateSelect = {
      'date_from': this.setFormatDate(event.beginDate),
      'date_to': this.setFormatDate(event.endDate)
    }
    this.loadData();
  }

  setFormatDate(data?) {
    if (typeof data === 'object') {
      return data.year + '-' + data.month + '-' + data.day
    } else if (typeof data === 'string') {
      return data
    }
  }
}
