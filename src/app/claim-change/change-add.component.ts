import { Router } from '@angular/router';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { AllServiceService } from './../service/all-service.service';
import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { TypeaheadMatch, ModalDirective } from 'ng2-bootstrap';
import { HideMenuService } from '../shared/hide-menu.service';

import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/of';
import * as moment from 'moment';
import { IMyOptions, IMyDateModel, IMyDate } from 'mydatepicker';

@Component({
  selector: 'app-change-add',
  templateUrl: './change-add.component.html',
  styleUrls: ['./change-add.component.scss']
})
export class ChangeAddComponent implements OnInit {

  Heading = 'เพิ่มรายการเปลี่ยนสินค้า';

  public isLoading: Boolean = false;

  //data
  public customers;
  public products;


  //form wizard
  public formName;
  public isFirstTab;
  public isLastTab;
  public selectedTab;

  //progress
  public progress;

  //Dynamic Form
  public brokens = [{ 'message': '' }];


  //Typeahead
  public customerSelected: any;
  public product_source: any;
  public product_target: any;
  public dataSource: Observable<any>;
  public asyncSelected: string = '';
  public typeaheadLoading: boolean = false;
  public typeaheadNoResults;

  public date;

  //Form
  public tel: FormControl;
  public customer_id: FormControl;
  public exchange_date: FormControl;
  public source_product_id: FormControl;
  public target_product_id: FormControl;
  public shop_id: FormControl;
  public cause_detail: FormControl;
  public remark: FormControl;
  public staff_name: FormControl;

  public infoForm: FormGroup;
  public productForm: FormGroup;
  public causeForm: FormGroup;
  public employeeForm: FormGroup;

  //Modal  
  @ViewChild('editModal') public editModal: ModalDirective;

  //Form
  public customerForm: FormGroup;
  public name_customer: FormControl;
  public tel_customer: FormControl;

  //date
  private selDate: IMyDate = { year: 0, month: 0, day: 0 };
  private myDatePickerOptions: IMyOptions = {
    // other options...
    dateFormat: 'dd/mm/yyyy',
    firstDayOfWeek: 'su',
    selectionTxtFontSize: '16px',
    editableDateField: false,
    openSelectorOnInputClick: true
  };

  public constructor(
    private fb: FormBuilder,
    private allService: AllServiceService,
    public toastr: ToastsManager,
    public vcr: ViewContainerRef,
    public router: Router) {

    this.toastr.setRootViewContainerRef(vcr);

    //form wizard
    this.formName = [
      {
        'name': 'ข้อมูลทั่วไป',
        'path': '#tab-1-info',
        'icon': 'fa-info',
        'desc': ''
      },
      {
        'name': 'สินค้า',
        'path': '#tab-2-product-broken',
        'icon': 'fa-cube',
        'desc': ''
      },
      {
        'name': 'อาการ',
        'path': '#tab-3-detail',
        'icon': 'fa-exclamation-triangle',
        'desc': ''
      },
      {
        'name': 'ข้อมูลพนักงาน',
        'path': '#tab-4-employee',
        'icon': 'fa-user',
        'desc': ''
      }
    ];
    this.isFirstTab = true;
    this.isLastTab = false;
    this.selectedTab = 1;

    //Progressbar
    this.progress = 20;

    //data
    this.customerSelected = {
      item: {
        'name': '',
        'tel': ''
      }
    };
    this.product_source = {
      code: '',
      name: ''
    }
    this.product_target = {
      code: '',
      name: ''
    }

    this.typeaheadNoResults = {
      'customer': false,
      'product_source': false,
      'product_target': false
    }

    //FormControl
    this.tel = fb.control('', Validators.compose([Validators.required]));
    this.exchange_date = fb.control('', Validators.compose([Validators.required]));
    this.source_product_id = fb.control('', Validators.compose([Validators.required]));
    this.target_product_id = fb.control('', Validators.compose([Validators.required]));

    this.infoForm = fb.group({
      'tel': this.tel,
      'customer_id': this.customer_id,
      'exchange_date': this.exchange_date,
    });

    this.productForm = fb.group({
      'source_product_id': this.source_product_id,
      'target_product_id': this.target_product_id,
      'shop_id': this.shop_id,
    });

    this.causeForm = fb.group({
      'cause_detail': this.cause_detail,
      'remark': this.remark,
    });

    this.employeeForm = fb.group({
      'staff_name': this.staff_name,
    });

    //validate FormBuilder Modal add cistomer
    this.name_customer = fb.control('', Validators.compose([Validators.required]));
    this.tel_customer = fb.control('', Validators.compose([Validators.required]));

    this.customerForm = fb.group({
      'name_customer': this.name_customer,
      'tel_customer': this.tel_customer
    });

  }

  ngOnInit() {
    this.setInit();
  }

  async setInit() {
    this.isLoading = true;
    await this.getCustomerData();
    await this.getProductData();
    await this.setCurrentDate();
    this.isLoading = false;
  }

  //--------------------------------------- get Customer Data ---------------------------------------//
  async getCustomerData() {
    try {
      let res_customer = await this.allService.get('customer');
      console.log('res_customer', res_customer);
      this.customers = res_customer.data;
    } catch (error) {
      console.log('error get customer', error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.getCustomerData();
      } else {
        this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
      }
    }
  }

  //--------------------------------------- get Product Data ---------------------------------------//
  async getProductData() {
    try {
      let res_product = await this.allService.get('product');
      console.log('res_product', res_product);
      this.products = res_product.data;
    } catch (error) {
      console.log('error get product', error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.getProductData();
      } else {
        this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
      }
    }
  }


  //--------------------------------------- Date ---------------------------------------//
  setCurrentDate() {
    console.log('current date', moment().format())
    this.date = {
      'year': parseInt(moment().format('YYYY')),
      'month': parseInt(moment().format('MM')),
      'day': parseInt(moment().format('DD')),
    }
    this.selDate = this.date;
  }

  setFormatDate(data?) {
    if (typeof data === 'object') {
      return data.year + '-' + data.month + '-' + data.day
    } else if (typeof data === 'string') {
      return data
    }
  }

  onDateChanged(event: IMyDateModel) {
    console.log('IMyDateModel', event);
    this.date = event.date;
    console.log('IMyDateModel', this.date);
    console.log('FormControl', this.infoForm.value.exchange_date);
  }


  //--------------------------------------- Progressbar ---------------------------------------//
  setProgress(status) {
    if (status === 1) {
      this.isFirstTab = true;
      this.isLastTab = false;
    } else if (status === 4) {
      this.isFirstTab = false;
      this.isLastTab = true;
    } else {
      this.isFirstTab = false;
      this.isLastTab = false;
    }
    return this.progress = status * 25;
  }


  //--------------------------------------- TAB ---------------------------------------//
  setTab(tab, action) {
    if (action === 'previous') {
      console.log('previous');
      this.selectedTab = this.selectedTab - 1;
      this.setProgress(this.selectedTab);
    } else if (action === 'next') {
      console.log('next');
      this.selectedTab = this.selectedTab + 1;
      this.setProgress(this.selectedTab);
    } else {
      this.selectedTab = tab;
      this.setProgress(this.selectedTab);
    }
  }

  //--------------------------------------- Typeahead ---------------------------------------//

  public changeTypeaheadLoading(e: boolean): void {
    this.typeaheadLoading = e;
  }

  public changeTypeaheadNoResults(status, e: boolean): void {
    switch (status) {
      case 0:
        this.typeaheadNoResults.customer = e;
        break;
      case 1:
        this.typeaheadNoResults.product_source = e;
        break;
      case 2:
        this.typeaheadNoResults.product_target = e;
        break;
    }

  }

  public typeaheadOnSelect(status, e: TypeaheadMatch): void {
    switch (status) {
      case 0:
        this.customerSelected = e.item;
        console.log('Selected value: ', e);
        break;
      case 1:
        this.product_source = e.item;
        console.log('Selected value:', e);
        break;
      case 2:
        this.product_target = e.item;
        console.log('Selected value:', e);
        break;
    }
  }


  //--------------------------------------- Modal ---------------------------------------//
  openAddCustomerModal(): void {
    this.editModal.show();
  }

  async saveCustomer() {
    console.log('save data', this.customerForm.value);
    this.isLoading = true;
    let customer_data = {
      'name': this.customerForm.value.name_customer,
      'tel': this.customerForm.value.tel_customer,
    }
    try {
      let res_edit_customer = await this.allService.post('customer', customer_data);
      this.showSuccess('แก้ไขข้อมูลเรียบร้อย');
      this.customerForm.reset();
      await this.getCustomerData();
      this.isLoading = false;
    } catch (error) {
      console.log('error add customer', error);
      if (error.status === 500) {
        this.showError('ไม่สามารถเพิ่มลูกค้าได้ กรุณาลองใหม่อีกครั้ง');
      } else if (error.status === 401) {
        await this.allService.refreshToken();
        this.saveCustomer();
      } else {
        this.showError('กรุณาลองใหม่อีกครั้ง');
        this.isLoading = false;
      }
    }
    this.editModal.hide();
  }

  // --------------------------------------- Save Data ---------------------------------------//
  async saveData() {
    this.isLoading = true;
    console.log('save data infoForm', this.infoForm.value);
    console.log('save data productForm', this.productForm.value);
    console.log('save data causeForm', this.causeForm.value);
    console.log('save data brokens', this.brokens);
    console.log('save data employeeForm', this.employeeForm.value);
    console.log('localstorage', this.allService.getLocal('user'));
    let user = this.allService.getLocal('user');
    let changeData = {
      tel: this.infoForm.value.tel,
      customer_id: this.customerSelected.id,
      exchange_date: this.setFormatDate(this.date.date),
      source_product_id: this.product_source.id,
      target_product_id: this.product_target.id,
      shop_id: user.shop_id,
      user_id: user.id,
      cause_detail: JSON.stringify(this.brokens),
      remark: this.causeForm.value.remark,
      staff_name: this.employeeForm.value.staff_name,
    }
    console.log('changeData', changeData);

    try {
      let res_change_product = await this.allService.post('exchangeTransection', changeData);
      this.showSuccess('เพิ่มข้อมูลเรียบร้อย');
      this.infoForm.reset;
      this.productForm.reset;
      this.causeForm.reset;
      this.employeeForm.reset;
      console.log('res_change_product', res_change_product);
      this.showDetail(res_change_product.data.id);
      this.isLoading = false;
    } catch (error) {
      console.log('error add customer', error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.saveData();
      } else if (error.status === 500) {
        this.showError('ไม่สามารถเพิ่มได้ กรุณาลองใหม่อีกครั้ง');
        this.isLoading = false;
      } else {
        this.showError('กรุณาลองใหม่อีกครั้ง');
        this.isLoading = false;
      }
    }
  }


  // --------------------------------------- Toast ---------------------------------------//
  showSuccess(message) {
    this.toastr.success(message, 'สำเร็จ!');
  }

  showError(message) {
    this.toastr.error(message, 'เกิดข้อผิดพลาด');
  }


  //--------------------------------------- Dynamic Form ---------------------------------------//
  addBrokens() {
    this.brokens.push({ 'message': '' });
  }

  deleteBrokens(index) {
		if (this.brokens.length > 1) {
			this.brokens.splice(index, 1);
		} else {
			this.brokens = [{ 'message': '' }];
		}
	}


  //--------------------------------------- Detail ---------------------------------------//
  showDetail(id) {
    console.log('id', id);
    this.router.navigate(['claim/fix/list/info', id]);
  }

}
