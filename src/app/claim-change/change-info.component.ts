import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Subscription } from 'rxjs/Subscription';
import { AllServiceService } from './../service/all-service.service';
import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { TypeaheadMatch, ModalDirective } from 'ng2-bootstrap';
import { CustomerData } from '../tempData/customer-data';
import { SellProductData } from '../tempData/sell-product-data';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { HideMenuService } from '../shared/hide-menu.service';

import * as moment from 'moment';

import { FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/of';

@Component({
  selector: 'app-change-info',
  templateUrl: './change-info.component.html',
  styleUrls: ['./change-info.component.scss']
})
export class ChangeInfoComponent implements OnInit {

  @ViewChild('deleteModal') public deleteModal: ModalDirective;

  Heading = 'ข้อมูลการเปลี่ยนสินค้า';

  public isLoading: Boolean;

  public change_id;

  public rootUrl;
  public subGetParams: Subscription;

  public statusText = [
    {
      'id': '1',
      'message': 'รับเรื่อง',
      'class': 'text-blue'
    },
    {
      'id': '2',
      'message': 'กำลังเปลี่ยน',
      'class': 'text-yellow'
    },
    {
      'id': '3',
      'message': 'สำเร็จ',
      'class': 'text-green'
    },
    {
      'id': '4',
      'message': 'ข้อผิดพลาด',
      'class': 'text-red'
    }
  ];


  public edit;

  public data;
  public data_original;

  //Typeahead
  public customers;
  public customerSelected;
  public productSelected: string = '';
  public dataSource: Observable<any>;
  public asyncSelected: string = '';
  public typeaheadLoading: boolean = false;
  public typeaheadNoResults: boolean = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private allService: AllServiceService,
    public vcr: ViewContainerRef,
    public toastr: ToastsManager) {

    this.toastr.setRootViewContainerRef(vcr);

    router.events.subscribe((url: any) => {
      this.rootUrl = url;
    });

  }

  ngOnInit() {
    this.subGetParams = this.route.params.subscribe(
      params => {
        this.change_id = params['id'];
      }
    );
    console.log('this.change_id', this.change_id);
    this.setInitEdit();
    this.setInitData();
    this.getAllData();

  }

  setInitData() {
    this.data = {
      'code': '',
      'exchange_date': '',
      'cause_detail': '',
      'broken': [],
      'customer_id': '',
      'HideMenuServiceremark': '',
      'shop_id': '',
      'source_product_id': '',
      'source_product': {
      },
      'staff_name': '',
      'target_product_id': '',
      'target_product': {},
      'tel': '',
      'user_id': '',
      'status': ''
    };
  }

  setInitEdit() {
    this.edit = {
      'info': false,
      'status': false,
      'source_product': false,
      'target_product': false,
      'broken': false,
      'remark': false
    };
    this.customerSelected = {
      'name': '',
      'id': '',
      'tel': ''
    }
  }

  cancleEdit() {
    console.log('data_original', this.data_original);
    this.data = Object.assign({}, this.data_original);
    this.setInitEdit();
  }

  //--------------------------------------- get data ---------------------------------------//
  async getAllData() {
    this.isLoading = true;
    await this.loadDataToEdit();
    await this.loadClaimData();
    this.isLoading = false;
  }

  async loadClaimData() {
    try {
      let resp_exchange = await this.allService.getWithId('exchangeTransection', this.change_id);
      console.log('exchangeTransection', resp_exchange);

      this.data_original = resp_exchange.data;
      this.data_original.broken = JSON.parse(resp_exchange.data.cause_detail);
      this.data_original.exchange_date = this.setFormatDate(resp_exchange.data.exchange_date);
      this.customerSelected = this.data.customer;
      this.data_original.dateFormat = this.setObjectDate(this.data_original.exchange_date);

      this.data = Object.assign({}, this.data_original);
      console.log('this.data', this.data);

    } catch (error) {
      console.log('error get exchange', error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.loadClaimData();
      } else {
        this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
      }
    }
  }

  async loadDataToEdit() {
    try {
      let resp_customer = await this.allService.get('customer');
      let resp_product = await this.allService.get('product');

      this.customers = resp_customer.data;

    } catch (error) {
      console.log('error get loadDataToEdit', error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.loadDataToEdit();
      } else {
        this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
      }
    }
  }

  //--------------------------------------- Date Format ---------------------------------------//
  setFormatDate(data) {
    if (typeof data === 'object') {
      return data.year + '-' + data.month + '-' + data.day
    } else if (typeof data === 'string') {
      return data
    }
  }

  setObjectDate(data) {
    let date = {
      'year': parseInt(moment(data).format('YYYY')),
      'month': parseInt(moment(data).format('MM')),
      'day': parseInt(moment(data).format('DD')),
    };
    console.log('date', date);
    return date;
  }



  //--------------------------------------- Dynamic Form ---------------------------------------//
  openPagePrint() {
    let gotourl = this.rootUrl.url + '/print';
    console.log('openPagePrint', this.data);
    // this.router.navigate([gotourl], { relativeTo: this.route });

    let navigationExtras: NavigationExtras;
    navigationExtras = {
      queryParams: {
        "id": this.change_id
      }
    };
    window.open('/#' + gotourl + '?id=' + this.change_id, "_blank");

    // this.router.navigate([gotourl], navigationExtras);

  }

  //--------------------------------------- Dynamic Form ---------------------------------------//
  addBrokens() {
    this.data.broken.push({ 'message': '' });
  }

  //--------------------------------------- Typeahead ---------------------------------------//
  public getStatesAsObservable(token: string): Observable<any> {
    let query = new RegExp(token, 'ig');

    return Observable.of(
      this.customers.filter((state: any) => {
        return query.test(state.name);
      })
    );
  }

  public changeTypeaheadLoading(e: boolean): void {
    this.typeaheadLoading = e;
  }

  public changeTypeaheadNoResults(e: boolean): void {
    this.typeaheadNoResults = e;
  }

  public typeaheadOnSelect(status, e: TypeaheadMatch): void {
    switch (status) {
      case 0:
        this.customerSelected = e.item;
        console.log('Selected value: ', e);
        break;
      case 1:
        this.productSelected = e.item;
        console.log('Selected value: ', e);
        break;
    }
  }

  //--------------------------------------- Edit Data ---------------------------------------//
  async editData() {
    this.isLoading = true;
    try {
      if (this.customerSelected) {
        this.data.customer_id = this.customerSelected.id;
      }
      console.log('this.data edit', this.data);
      this.data.cause_detail = JSON.stringify(this.data.broken);
      let resp_edit_data = await this.allService.putWithId('exchangeTransection', this.data.id, this.data)

      console.log('resp_edit_data', resp_edit_data);
      this.setInitEdit();
      this.getAllData();

    } catch (error) {
      console.log('error edit data', error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.editData();
      } else {
        this.showError('ไม่สามารถแก้ไขข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
      }
    }
  }

  //--------------------------------------- Open Modal ---------------------------------------//

  openDeleteModal(item): void {
    this.deleteModal.show();
  }

  async deleteItem() {
    this.isLoading = true;

    try {
      let resp_delete_exchange = await this.allService.deleteWithId('exchangeTransection', this.data.id);
      console.log('Delete data user : ', resp_delete_exchange.data);
      this.showSuccess('ลบข้อมูลเรียบร้อย');
      this.isLoading = false;
      this.router.navigate(['/claim/change/list']);
    } catch (error) {
      console.log("Error delete data user!", error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.deleteItem();
      } else {
        this.showError('ไม่สามารถลบข้อมูลได้');
        this.isLoading = false;
      }
    }

    this.deleteModal.hide();
  }

  // --------------------------------------- Toast ---------------------------------------//
  showSuccess(message) {
    this.toastr.success(message, 'สำเร็จ!');
  }

  showError(message) {
    this.toastr.error(message, 'เกิดข้อผิดพลาด');
  }


}
