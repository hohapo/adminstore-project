import { async } from '@angular/core/testing';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class AllServiceService {

  private headers;
  private options;
  private req_params: URLSearchParams = new URLSearchParams();

  // public baseUrl: string = 'http://128.199.118.178/stock/public/index.php/';
  public baseUrl: string = 'http://jipataphone.com/service/public/index.php/';

  private tempData;
  private token;
  private errorMessage;

  constructor(private http: Http, private router: Router) { }

  async post(path: string, data): Promise<any> {
    this.setHeader(null);
    let url = this.baseUrl + path;
    // data.user_id = await this.getLocal('user').shop_id;
    console.log('this.options', this.options)
    const response = await this.http.post(url, data, this.options).toPromise();
    return response.json();
  }

  async postWithId(path: string, id, data) {
    this.setHeader(null);
    let url = this.baseUrl + path + '/' + id;
    // data.user_id = await this.getLocal('user').shop_id;
    const response = await this.http.post(url, data, this.options).toPromise();
    return response.json();

  }

  async get(path: string, params?): Promise<any> {
    this.setHeader(params);
    let url = this.baseUrl + path;
    const response = await this.http.get(url, this.options).toPromise();
    return response.json();
  }

  async getWithId(path: string, id, params?) {
    this.setHeader(params);
    let url = this.baseUrl + path + '/' + id;
    const response = await this.http.get(url, this.options).toPromise();
    return response.json();
  }

  async put(path: string, data) {
    this.setHeader(null);
    let url = this.baseUrl + path;
    // const response = await this.http.put(url, data, this.options).toPromise();

    this.headers.append('X-HTTP-Method-Override', 'PUT');
    this.options = new RequestOptions({ headers: this.headers, search: this.req_params });
    const response = await this.http.post(url, data, this.options).toPromise();
    return response.json();
  }

  async putWithId(path: string, id, data) {
    this.setHeader(null);
    let url = this.baseUrl + path + '/' + id;
    // data.user_id = await this.getLocal('user').shop_id;
    this.headers.append('X-HTTP-Method-Override', 'PUT');
    this.options = new RequestOptions({ headers: this.headers, search: this.req_params });
    const response = await this.http.post(url, data, this.options).toPromise();
    return response.json();
  }

  async delete(path: string, params?) {
    this.setHeader(null);
    let url = this.baseUrl + path;
    this.headers.append('X-HTTP-Method-Override', 'DELETE');
    this.options = new RequestOptions({ headers: this.headers, search: this.req_params });
    const response = await this.http.post(url, null, this.options).toPromise();
    return response.json();
  }

  async deleteWithId(path: string, id, params?) {
    this.setHeader(null);
    let url = this.baseUrl + path + '/' + id;
    this.headers.append('X-HTTP-Method-Override', 'DELETE');
    this.options = new RequestOptions({ headers: this.headers, search: this.req_params });
    const response = await this.http.post(url, null, this.options).toPromise();
    return response.json();
  }

  setToken(value) {
    localStorage.setItem('token', value);
  }

  getToken() {
    let token = localStorage.getItem('token');
    return token;
  }

  setLocal(name: string, value: Object) {
    localStorage.setItem(name, JSON.stringify(value))
  }

  getLocal(name: string) {
    let result = localStorage.getItem(name);
    return result ? JSON.parse(result) : null;
  }

  async refreshToken() {
    let token = await this.get('auth/refreshToken');
    console.log('token', token.data.newToken);
    this.setToken(token.data.newToken);
  }

  checkLogin() {
    if (this.getToken === null) {
      this.router.navigate(['/login']);
    } else {
      return true;
    }
  }

  setHeader(params) {
    let authToken = 'Bearer ' + this.getToken();
    this.headers = new Headers({
      'Accept': 'application/json'
    });
    this.headers.append('Authorization', authToken);

    // let req_params: URLSearchParams = new URLSearchParams();
    if (params) {
      let keys = Object.keys(params)
      for (let i = 0; i < keys.length; i++) {
        this.req_params.set(keys[i], params[keys[i]])
      }
    }

    this.options = new RequestOptions({ headers: this.headers, search: this.req_params });
  }


}
