/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { AllServiceService } from './all-service.service';

describe('Service: AllService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AllServiceService]
    });
  });

  it('should ...', inject([AllServiceService], (service: AllServiceService) => {
    expect(service).toBeTruthy();
  }));
});
