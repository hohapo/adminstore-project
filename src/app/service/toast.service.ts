import {Injectable, ViewContainerRef} from '@angular/core';
import {ToastsManager} from 'ng2-toastr/ng2-toastr';

@Injectable()
export class ToastService {

  constructor(public toastr : ToastsManager, public vRef : ViewContainerRef) {
     
  }

  showSuccess(toastr,message) {
    toastr.success(message, 'สำเร็จ!');
  }

  showError(toastr,message) {
    toastr.error(message, 'เกิดข้อผิดพลาด');
  }

}
