import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AllServiceService } from '../service/all-service.service';
import { CustomValidators } from 'ng2-validation';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'app-add-store',
  templateUrl: './add-store.component.html',
  styleUrls: ['./add-store.component.scss']
})
export class AddStoreComponent implements OnInit {

  public Heading = 'เพิ่มร้านค้า';

  public isLoading: Boolean = false;

  public storeForm: FormGroup;
  public name: FormControl;
  public tel: FormControl;
  public address: FormControl;

  public data;

  constructor(private fb: FormBuilder, private allService: AllServiceService, public toastr: ToastsManager,
    public vcr: ViewContainerRef) {

    this.toastr.setRootViewContainerRef(vcr);

    //validate FormBuilder
    this.name = fb.control('', Validators.compose([Validators.required]));
    this.tel = fb.control('', Validators.compose([Validators.required]));
    this.address = fb.control('', Validators.compose([Validators.required]));

    this.storeForm = fb.group({
      'name': this.name,
      'tel': this.tel,
      'address': this.address
    });

  }

  ngOnInit() {
    this.setData();
  }

  setData() {
    this.data = {
      'name': '',
      'tel': '',
      'address': ''
    };
  }

  async sendForm() {
    console.log('data : ', this.data);
    this.isLoading = true;
    try {
      let res_add_store = await this.allService.post('shop', this.storeForm.value)
      console.log('response create store : ', res_add_store);
      this.storeForm.reset();
      this.isLoading = false;
      this.showSuccess(`ทำการเพิ่มร้าน ${res_add_store.data.name} สำเร็จแล้ว`);
    } catch (error) {
      console.log("Error create store", error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.sendForm();
      } else {
        this.isLoading = false;
        this.showError(`ไม่สามารถเพิ่มร้านค้าได้ กรุณาลองใหม่อีกครั้ง`);
      }
    }
  }

  // --------------------------------------- Toast ---------------------------------------//
  showSuccess(message) {
    this.toastr.success(message, 'สำเร็จ!');
  }

  showError(message) {
    this.toastr.error(message, 'เกิดข้อผิดพลาด');
  }

}
