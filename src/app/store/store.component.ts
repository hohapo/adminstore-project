import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ModalDirective } from 'ng2-bootstrap';
import { HideMenuService } from '../shared/hide-menu.service';
import { AllServiceService } from '../service/all-service.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['store.component.scss']
})
export class StoreComponent implements OnInit {

  @ViewChild('editModal') public editModal: ModalDirective;
  @ViewChild('deleteModal') public deleteModal: ModalDirective;

  public Heading = 'ร้านค้า';

  public isLoading: Boolean = false;

  //data
  public data = null;

  //table
  public dtOptions: any = {};

  //Menu
  public isHideMenu;

  //Modal  
  public selectedItem;

  //Form
  public storeForm: FormGroup;
  public name: FormControl;
  public tel: FormControl;
  public address: FormControl;

  public constructor(
    private fb: FormBuilder,
    public hideMenuService: HideMenuService,
    public allService: AllServiceService,
    public toastr: ToastsManager,
    public vcr: ViewContainerRef) {

    this.toastr.setRootViewContainerRef(vcr);

    this.getData();
    this.isHideMenu = true;

    hideMenuService.subscribe({
      next: isHideMenu => {
        this.isHideMenu = isHideMenu;
        return this.isHideMenu;
      }
    });

    //validate FormBuilder
    this.name = fb.control('', Validators.compose([Validators.required]));
    this.tel = fb.control('', Validators.compose([Validators.required]));
    this.address = fb.control('', Validators.compose([Validators.required]));

    this.storeForm = fb.group({
      'name': this.name,
      'tel': this.tel,
      'address': this.address
    });

  }

  ngOnInit() {
    this.initTable();
  }

  initTable() {
    this.dtOptions = {
      displayLength: 10,
      bSort: false,
      paginationType: 'simple_numbers',
      language: {
        info: 'แสดง _START_ ถึง _END_ จาก _TOTAL_',
        paginate: {
          previous: '<',
          next: '>',
        },
        search: 'ค้นหา',
        emptyTable: 'ไม่มีข้อมูล',
        infoEmpty: 'แสดง 0 ถึง 0 จาก 0',
        zeroRecords: 'ไม่มีข้อมูล',
        lengthMenu: 'แสดง _MENU_ แถว'
      }
    };
  }


  async getData() {
    this.isLoading = !this.isLoading;
    try {
      let res_get_store = await this.allService.get('shop');
      console.log('Get data shop : ', res_get_store.data);
      this.data = res_get_store.data;
      this.isLoading = false;
    } catch (error) {
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.getData();
      } else {
        this.isLoading = false;
        this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
      }
    }
  }

  // --------------------------------------- Open Modal ---------------------------------------//
  openEditModal(id): void {
    this.selectedItem = this.data.filter(data => data.id == id)[0];
    this.editModal.show();
  }

  openDeleteModal(id): void {
    this.selectedItem = this.data.filter(data => data.id == id)[0];
    this.deleteModal.show();
  }

  async saveChange() {
    console.log('save data', this.storeForm.value);
    this.isLoading = true;
    console.log('loadData');
    try {
      let res_edit_store = await this.allService.putWithId('shop', this.selectedItem.id, this.storeForm.value)
      this.showSuccess('แก้ไขข้อมูลเรียบร้อย');
      this.getData();
    } catch (error) {
      console.log('error edit store');

      if (error.status === 500) {
        this.showError('ไม่พบร้านค้า กรุณาลองใหม่อีกครั้ง');
        this.isLoading = false;
      } else if (error.status === 401) {
        await this.allService.refreshToken();
        this.saveChange();
      } else {
        this.showError('กรุณาลองใหม่อีกครั้ง');
        this.isLoading = false;
      }
    }
    this.editModal.hide();
  }

  async deleteItem() {
    console.log('delete data', this.selectedItem);
    this.isLoading = true;
    try {
      let res_delete_store = await this.allService.deleteWithId('shop', this.selectedItem.id);
      this.showSuccess('ลบข้อมูลเรียบร้อย');
      this.getData();
    } catch (error) {
      console.log("Error delete data shop!", error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.deleteItem();
      } else {
        this.showError('ไม่สามารถลบข้อมูลได้');
        this.isLoading = false
      }
    }

    this.deleteModal.hide();
  }

  // --------------------------------------- Toast ---------------------------------------//
  showSuccess(message) {
    this.toastr.success(message, 'สำเร็จ!');
  }

  showError(message) {
    this.toastr.error(message, 'เกิดข้อผิดพลาด');
  }

}
