import { HideSidebarService } from './shared/hide-sidebar.service';
import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AllServiceService } from './service/all-service.service';

@Injectable()
export class AuthGuardService implements CanActivate {

  public isHideSidebar: boolean;

  constructor(private router: Router, private allService: AllServiceService, public hideSidebarService: HideSidebarService) { }

  canActivate() {
    if (this.allService.getToken()) {
      this.isHideSidebar = false;
      return true;
    } else {
      this.isHideSidebar = true;
      this.router.navigate(['/login']);
    }
    this.hideSidebarService.next(this.isHideSidebar);
  }


}
