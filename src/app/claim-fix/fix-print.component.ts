import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { AllServiceService } from './../service/all-service.service';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'app-fix-print',
  templateUrl: './fix-print.component.html',
  styleUrls: ['./fix-print.component.scss']
})
export class FixPrintComponent implements OnInit {

  Heading = "พิมพ์ข้อมูลใบซ่อม";

  @ViewChild('print-section') public printSection: any;

  public rootUrl;
  public subGetParams: Observable<Object>;
  public store;
  public data;
  public change_id;
  public isLoading: Boolean;


  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private allService: AllServiceService,
    public toastr: ToastsManager,
    public vcr: ViewContainerRef) {

    this.toastr.setRootViewContainerRef(vcr);

    route.queryParams.subscribe(params => {
      this.change_id = params["id"];
      console.log(this.change_id);
      this.getData();
    });

  }

  ngOnInit() {
  }

  async getData() {
    this.isLoading = true;
    try {
      let user = await this.allService.getLocal('user');
      console.log('user', user);
      let resp_store = await this.allService.getWithId('shop', user.shop_id);
      let resp_repair = await this.allService.getWithId('repairTransection', this.change_id);
      console.log('repairTransection', resp_repair);

      this.store = resp_store.data;
      this.data = resp_repair.data;
      this.data.cause = resp_repair.data.cause_detail ? JSON.parse(resp_repair.data.cause_detail) : '';
      this.data.agreement = resp_repair.data.agreement_detail ? JSON.parse(resp_repair.data.agreement_detail) : '';
      this.data.repair = resp_repair.data.repair_detail ? JSON.parse(resp_repair.data.repair_detail) : '';
      this.data.repair_date = this.setFormatDate(resp_repair.data.repair_date);
      // this.customerSelected = this.data.customer_id;
      this.data.dateFormat = this.setObjectDate(this.data.repair_date);
      console.log('this.data', this.data);
      console.log('this.store', this.store);
      this.data.product_block = this.makeImei(this.data.item_imei);
      this.isLoading = false;
    } catch (error) {
      console.log('error get repair', error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.getData();
      } else {
        this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
        this.isLoading = false;
      }
    }
  }

  makeImei(data) {
    let result = [];
    for (var i = 0; i < data.length; i++) {
      result.push(data.slice(i, i + 1));
    }
    return result;
  }

  //--------------------------------------- Date Format ---------------------------------------//
  setFormatDate(data) {
    if (typeof data === 'object') {
      return data.year + '-' + data.month + '-' + data.day
    } else if (typeof data === 'string') {
      return data
    }
  }

  setObjectDate(data) {
    let date = moment(data);
    return {
      'day': date.format('DD'),
      'month': date.format('MM'),
      'year': date.format('YYYY')
    }
  }


  print(): void {
    let printContents, popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    // printContents = this.printSection;

    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <link href="https://fonts.googleapis.com/css?familyTrirong" rel="stylesheet">
          <link href="https://fonts.googleapis.com/css?family=Nunito+Sans" rel="stylesheet">
          <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

          <style>
            @page {
              size:  auto;   /* auto is the initial value */
              margin: 10mm;  /* this affects the margin in the printer settings */
            }
            html, body {
              font-family: 'Nunito Sans', 'Trirong', 'FontAwesome';
              height:98%;
              width:1000px;
              background: #FFF; 
              font-size: 16pt;
            }

            ol{
              margin-bottom:0px;
            }

            .panel-body {
              padding: 30px;
            }

            .panel-body .claimPrint-title {
              text-align: center;
            }

            .panel-body .claimPrint-title h2 {
              margin-top: 0;
              margin-bottom: 0;
              font-size: 25px;
            }

            .panel-body .claimPrint-title p {
              margin: 0;
            }

            .panel-body .logo {
              font-family: 'Oswald';
              font-weight: bold;
              margin-top: 0;
            }

            .panel-body hr {
              margin-bottom: 10px;
              margin-top: 0px;
              margin-left: -20px;
              margin-right: -20px;
            }

            .panel-body .table {
              table-layout: fixed;
              width: 100%;
              max-width: 100%;
              margin: 0px;
            }

            .panel-body .table td {
              border-top: none;
              padding: 8px;
              line-height: 1.42857143;
              vertical-align: top;
              font-size: 16px;
            }

            .panel-body .table td .header {
              margin: 0px;
              font-weight: 600;
            }

            .panel-body .table.table-claim-info {
              margin: 0px;
              margin-top: 18px;
            }

            .panel-body .table.table-claim-info tr td address {
              margin-bottom: 0px;
              font-style: normal;
              line-height: 1.42857143;
            }

            .panel-body .table.table-product tr td {
              padding: 8px 0px;
            }

            .panel-body .table.table-product tr td .imei {
              padding: 3px 6px;
              border: 1px solid #ddd;
              margin-right: 5px;
            }

            .panel-body .table.table-product tr td .nopadding {
              padding: 0px;
            }

            .panel-body .table.table-product tr .table-list p {
              margin: 10px 0px;
            }

            .panel-body .table.table-product tr .cost {
              padding-top: 30px;
              width: 20%;
            }

            .panel-body .table.table-product tr .cost p {
              margin: 10px 0px;
            }

            .panel-body .table.table-product tr .cost .textextra {
              word-wrap: break-word;
              overflow: hidden;
              text-overflow: ellipsis;
            }

            .panel-body .table.table-product tr .table-option thead tr td {
              text-align: center;
            }

            .panel-body .table.table-product tr .table-option tbody tr td {
              width: 33%;
              line-height: 25px;
            }

            .panel-body .table.table-product tr .table-option tbody tr .hasIcon {
              text-align: center;
            }

            .panel-body .table.table-product tr .table-option tbody tr .hasIcon .icon {
              font-size: 25px;
            }

            .panel-body .table.table-signature .signatureHeader {
              text-align: center;
              font-weight: bolder;
            }

            .panel-body .table.table-signature .signature {
              text-align: center;
              height: 15px;
            }

            .panel-body .table.table-signature .signature td {
              padding: 0px;
            }

            .panel-body .table.table-signature .signature .borderShow {
              border-bottom: 1px solid #ddd;
            }

            .panel-body .table.table-signature .signatureDate {
              text-align: center;
              height: 25px;
            }

            .panel-body .table.table-signature .signatureDate td {
              padding: 0px;
              padding-top: 10px;
              position: relative;
            }

            .panel-body .table.table-signature .signatureDate td p {
              margin: 0px;
              display: flex;
              align-items: center;
              justify-content: flex-end;
              vertical-align: text-bottom;
              position: absolute;
              right: 5px;
              bottom: -5px;
            }

            .panel-body .table.table-signature .signatureDate .borderShow {
              border-bottom: 1px solid #ddd;
            }

            .panel-body .table.table-signature .signatureSpace {
              height: 10px;
            }

            .panel-body .table.table-product .borderDiv {
                border-bottom: 1px solid #eee;
            }
          </style>
        </head>
      <body onload="setTimeout(function () { window.print(); window.close(); }, 1000)">${printContents}</body>
      </html>`
    );
    setTimeout(function () {
      popupWin.document.close();
    }, 500);
  }

  // --------------------------------------- Toast ---------------------------------------//
  showSuccess(message) {
    this.toastr.success(message, 'สำเร็จ!');
  }

  showError(message) {
    this.toastr.error(message, 'เกิดข้อผิดพลาด');
  }

}
