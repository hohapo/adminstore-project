import { Router } from '@angular/router';
import { CustomValidators } from 'ng2-validation';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { AllServiceService } from './../service/all-service.service';
import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { TypeaheadMatch, ModalDirective } from 'ng2-bootstrap';
import { CustomerData } from '../tempData/customer-data';
import { SellProductData } from '../tempData/sell-product-data';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { HideMenuService } from '../shared/hide-menu.service';

import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/of';
import * as moment from 'moment';
import { IMyOptions, IMyDateModel, IMyDate } from 'mydatepicker';

@Component({
  selector: 'app-fix-add',
  templateUrl: './fix-add.component.html',
  styleUrls: ['./fix-add.component.scss']
})
export class FixAddComponent implements OnInit {

  Heading = 'เพิ่มรายการซ่อมสินค้า';

  public isLoading: Boolean;

  //Checkbox
  public selected = [];
  public selectedLength;
  public haveSelected;

  public clickData;

  //form wizard
  public formName;
  public isFirstTab;
  public isLastTab;
  public selectedTab;

  //progress
  public progress;

  //Dynamic Form
  public brokens = [{ 'message': '' }];
  public fixs = [{ 'message': '' }];
  public agreements = [
    { 'message': 'ทางร้านขอสงวนสิทธิ์ในความรับผิดชอบต่อความสูญหายของข้อมูลที่จัดเก็บไว้ในเครื่องรวมทั้งอุปกรณ์แต่งของเครื่องดังกล่าวด้วย' },
    { 'message': 'กรณีที่ไม่สามารถติดต่อเสนอราคาได้ภายใน 15 วัน นับตั้งแต่วันที่รับเครื่อง ทางร้านขอสงวนสิทธิ์ในการให้บริการและส่งเครื่องคืนให้ลูกค้า โดยสามารถรับคืนได้ที่สาขาที่เข้ารับบริการ' },
    { 'message': 'กรุณามารับเครื่องคืนภายใน 30 วัน นับจากวันที่ได้รับแจ้งให้มารับคืน มิฉะนั้นทางร้านถือว่าท่านสละสิทธิ์การรับเครื่องคืนและอุปกรณ์ และท่านยินดีให้กรรมสิทธิ์ทั้งหมดตกเป็นของทางร้าน โดยจะไม่ติดใจเรียกร้องใดๆ ทั้งทางแพ่งและทางอาญากับทางร้าน' },
    { 'message': 'ท่านได้ทำการตรวจสอบเครื่องซ่อมและได้ทดลองใช้โทรศัพท์ดังกล่าวแล้วเห็นว่าใช้งานได้ตามปกติและได้ตรวจสอบตำหนิในบริเวณต่างๆของตัวเครื่องไม่พบสิ่งผิดปกติใดๆ จึงยินดีรับเครื่องไปและท่านได้ตรวจสอบรายละเอียดสินค้าที่ส่งซ่อมพบว่าได้รับครบถ้วนตามรายการที่ระบุข้างต้นแล้ว' }
  ];


  //Typeahead
  public customers;
  public products;
  public customerSelected: any;
  public productSelected: any;
  public dataSource: Observable<any>;
  public asyncSelected: string = '';
  public typeaheadLoading: boolean = false;
  public typeaheadNoResults;

  public date;

  //date
  private selDate: IMyDate = { year: 0, month: 0, day: 0 };
  private myDatePickerOptions: IMyOptions = {
    // other options...
    dateFormat: 'dd/mm/yyyy',
    firstDayOfWeek: 'su',
    selectionTxtFontSize: '16px',
    editableDateField: false,
    openSelectorOnInputClick: true
  };

  //Modal  
  @ViewChild('editModal') public editModal: ModalDirective;

  //Form Add customer
  public customerForm: FormGroup;
  public name_customer: FormControl;
  public tel_customer: FormControl;

  //Form
  // public code: FormControl;
  public repair_date: FormControl;
  public customer_id: FormControl;
  public tel: FormControl;

  public product_id: FormControl;
  public item_brand: FormControl;
  public item_color: FormControl;
  public item_blame: FormControl;
  public item_generation: FormControl;
  public item_imei: FormControl;
  public item_has_sim: FormControl;
  public item_has_mem: FormControl;
  public item_has_battery: FormControl;

  public cause_detail: FormControl;
  public estimate_price: FormControl;
  public cause_remark: FormControl;

  public repair_detail: FormControl;
  public repair_price: FormControl;
  public repair_remark: FormControl;

  public agreement_detail: FormControl;
  public agreement_remark: FormControl;
  public staff_name: FormControl;

  public infoForm: FormGroup;
  public productForm: FormGroup;
  public causeForm: FormGroup;
  public employeeForm: FormGroup;

  public constructor(
    private fb: FormBuilder,
    private allService: AllServiceService,
    public toastr: ToastsManager,
    public vcr: ViewContainerRef,
    public router: Router) {

    this.toastr.setRootViewContainerRef(vcr);

    //form wizard
    this.formName = [
      {
        'name': 'ข้อมูลทั่วไป',
        'path': '#tab-1-info',
        'icon': 'fa-info',
        'desc': ''
      },
      {
        'name': 'สินค้า',
        'path': '#tab-2-product',
        'icon': 'fa-cube',
        'desc': ''
      },
      {
        'name': 'อาการ',
        'path': '#tab-3-detail',
        'icon': 'fa-exclamation-triangle',
        'desc': ''
      },
      {
        'name': 'ข้อมูลพนักงาน',
        'path': '#tab-4-employee',
        'icon': 'fa-user',
        'desc': ''
      }
    ];
    this.isFirstTab = true;
    this.isLastTab = false;
    this.selectedTab = 1;

    //data
    this.customerSelected = {
      item: {
        'name': '',
        'tel': ''
      }
    };
    this.productSelected = {
      code: '',
      name: ''
    }

    this.typeaheadNoResults = {
      'customer': false,
      'product': false
    }

    //Progressbar
    this.progress = 25;

    this.repair_date = fb.control('', Validators.compose([Validators.required]));
    this.customer_id = fb.control('', Validators.compose([Validators.required]));
    this.tel = fb.control('', Validators.compose([Validators.required]));

    this.item_brand = fb.control('', Validators.compose([Validators.required]));
    this.item_color = fb.control('', Validators.compose([Validators.required]));
    this.item_blame = fb.control('', Validators.compose([Validators.required]));
    this.item_generation = fb.control('', Validators.compose([Validators.required]));
    this.item_imei = fb.control('', Validators.compose([Validators.required]));
    this.item_has_sim = fb.control('1', Validators.compose([Validators.required]));
    this.item_has_mem = fb.control('1', Validators.compose([Validators.required]));
    this.item_has_battery = fb.control('1', Validators.compose([Validators.required]));

    this.estimate_price = fb.control('0', Validators.compose([Validators.required, CustomValidators.number, CustomValidators.min(0)]));
    this.repair_price = fb.control('0', Validators.compose([CustomValidators.number, CustomValidators.min(0)]));

    this.infoForm = fb.group({
      'repair_date': this.repair_date,
      'customer_id': this.customer_id,
      'tel': this.tel
    });

    this.productForm = fb.group({
      'product_id': this.product_id,
      'item_brand': this.item_brand,
      'item_color': this.item_color,
      'item_blame': this.item_blame,
      'item_generation': this.item_generation,
      'item_imei': this.item_imei,
      'item_has_sim': this.item_has_sim,
      'item_has_mem': this.item_has_mem,
      'item_has_battery': this.item_has_battery
    });

    this.causeForm = fb.group({
      'cause_detail': this.cause_detail,
      'estimate_price': this.estimate_price,
      'cause_remark': this.cause_remark,
      'repair_detail': this.repair_detail,
      'repair_price': this.repair_price,
      'repair_remark': this.repair_remark,
      'agreement_detail': this.agreement_detail,
      'agreement_remark': this.agreement_remark,

    });

    this.employeeForm = fb.group({
      'staff_name': this.staff_name
    });

    //validate FormBuilder Modal add cistomer
    this.name_customer = fb.control('', Validators.compose([Validators.required]));
    this.tel_customer = fb.control('', Validators.compose([Validators.required]));

    this.customerForm = fb.group({
      'name_customer': this.name_customer,
      'tel_customer': this.tel_customer
    });

  }

  ngOnInit() {
    this.setInit();
  }

  async setInit() {
    await this.getCustomerData();
    await this.getProductData();
    await this.setCurrentDate();
  }

  //--------------------------------------- get Customer Data ---------------------------------------//
  async getCustomerData() {
    this.isLoading = true;

    try {
      let res_customer = await this.allService.get('customer');
      console.log('res_customer', res_customer);
      this.customers = res_customer.data;

      this.isLoading = false;

    } catch (error) {
      console.log('error get customer', error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.getCustomerData();
      } else {
        this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
        this.isLoading = false;
      }
    }
  }

  //--------------------------------------- get Product Data ---------------------------------------//
  async getProductData() {
    this.isLoading = true;

    try {
      let res_product = await this.allService.get('product');
      console.log('res_product', res_product);
      this.products = res_product.data;

      this.isLoading = false;

    } catch (error) {
      console.log('error get product', error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.getProductData();
      } else {
        this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
        this.isLoading = false;
      }
    }
  }


  //--------------------------------------- Progressbar ---------------------------------------//
  setProgress(status) {
    if (status === 1) {
      this.isFirstTab = true;
      this.isLastTab = false;
    } else if (status === 4) {
      this.isFirstTab = false;
      this.isLastTab = true;
    } else {
      this.isFirstTab = false;
      this.isLastTab = false;
    }
    return this.progress = status * 25;
  }

  //--------------------------------------- TAB ---------------------------------------//
  setTab(tab, action) {
    console.group('form');
    console.log('info form', this.infoForm.value);
    console.log('product form', this.productForm.value);
    console.log('cause form', this.causeForm.value);
    console.log('employee form', this.employeeForm.value);
    console.groupEnd();
    if (action === 'previous') {
      console.log('previous');
      this.selectedTab = this.selectedTab - 1;
      this.setProgress(this.selectedTab);
    } else if (action === 'next') {
      console.log('next');
      this.selectedTab = this.selectedTab + 1;
      this.setProgress(this.selectedTab);
    } else {
      this.selectedTab = tab;
      this.setProgress(this.selectedTab);
    }
  }

  //--------------------------------------- Date ---------------------------------------//
  setCurrentDate() {
    console.log('current date', moment().format())
    this.date = {
      'year': parseInt(moment().format('YYYY')),
      'month': parseInt(moment().format('MM')),
      'day': parseInt(moment().format('DD')),
    }
    this.selDate = this.date;
  }

  setFormatDate(data?) {
    if (typeof data === 'object') {
      return data.year + '-' + data.month + '-' + data.day
    } else if (typeof data === 'string') {
      return data
    }
  }

  onDateChanged(event: IMyDateModel) {
    console.log('IMyDateModel', event);
    this.date = event.date;
    console.log('IMyDateModel', this.date);
    console.log('FormControl', this.infoForm.value.exchange_date);
  }


  //--------------------------------------- Typeahead ---------------------------------------//
  public changeTypeaheadLoading(e: boolean): void {
    this.typeaheadLoading = e;
  }

  public changeTypeaheadNoResults(status, e: boolean): void {
    switch (status) {
      case 0:
        this.typeaheadNoResults.customer = e;
        break;
      case 1:
        this.typeaheadNoResults.product = e;
        break;
    }

  }

  public typeaheadOnSelect(status, e: TypeaheadMatch): void {
    switch (status) {
      case 0:
        this.customerSelected = e.item;
        console.log('Selected value: ', e);
        break;
      case 1:
        this.productSelected = e.item;
        console.log('Selected value:', e);
        break;
    }
  }

  //--------------------------------------- Modal ---------------------------------------//
  openAddCustomerModal(): void {
    this.editModal.show();
  }

  async saveCustomer() {
    console.log('save data', this.customerForm.value);
    let customer_data = {
      'name': this.customerForm.value.name_customer,
      'tel': this.customerForm.value.tel_customer,
    }
    try {
      let res_edit_customer = await this.allService.post('customer', customer_data);
      this.showSuccess('แก้ไขข้อมูลเรียบร้อย');
      console.log('res_edit_customer', res_edit_customer)
      this.customerForm.reset();
      this.getCustomerData();
      this.customerSelected = res_edit_customer.data;
      this.customerSelected.value = res_edit_customer.data.name;
      console.log('customerSelected', this.customerSelected)
    } catch (error) {
      console.log('error add customer', error);
      if (error.status === 500) {
        this.showError('ไม่สามารถเพิ่มลูกค้าได้ กรุณาลองใหม่อีกครั้ง');
      } else if (error.status === 401) {
        await this.allService.refreshToken();
        this.saveCustomer();
      } else {
        this.showError('กรุณาลองใหม่อีกครั้ง');
      }
    }
    this.editModal.hide();
  }


  //--------------------------------------- Dynamic Form ---------------------------------------//
  addBrokens() {
    this.brokens.push({ 'message': '' });
  }
  addFixs() {
    this.fixs.push({ 'message': '' });
  }
  addAgreements() {
    this.agreements.push({ 'message': '' });
  }

  deleteBrokens(index) {
    if (this.brokens.length > 1) {
      this.brokens.splice(index, 1);
    } else {
      this.brokens = [{ 'message': '' }];
    }
  }

  deleteFixs(index) {
    if (this.fixs.length > 1) {
      this.fixs.splice(index, 1);
    } else {
      this.fixs = [{ 'message': '' }];
    }
  }

  deleteAgreements(index) {
    if (this.agreements.length > 1) {
      this.agreements.splice(index, 1);
    } else {
      this.agreements = [{ 'message': '' }];
    }
  }

  // --------------------------------------- Toast ---------------------------------------//
  showSuccess(message) {
    this.toastr.success(message, 'สำเร็จ!');
  }

  showError(message) {
    this.toastr.error(message, 'เกิดข้อผิดพลาด');
  }

  // --------------------------------------- Save Data ---------------------------------------//
  async saveData() {
    this.isLoading = true;
    console.log('save data infoForm', this.infoForm.value);
    console.log('save data productForm', this.productForm.value);
    console.log('save data causeForm', this.causeForm.value);
    console.log('save data brokens', this.brokens);
    console.log('save data fixs', this.fixs);
    console.log('save data agreements', this.agreements);
    console.log('save data employeeForm', this.employeeForm.value);

    let changeData = {
      product_id: this.productSelected.id,
      // code: this.infoForm.value.code,
      tel: this.infoForm.value.tel,
      customer_id: this.customerSelected.id,
      repair_date: this.setFormatDate(this.date.date),
      item_brand: this.productForm.value.item_brand,
      item_color: this.productForm.value.item_color,
      item_blame: this.productForm.value.item_blame,
      item_generation: this.productForm.value.item_generation,
      item_imei: this.productForm.value.item_imei,
      item_has_sim: this.productForm.value.item_has_sim.toString(),
      item_has_mem: this.productForm.value.item_has_mem.toString(),
      item_has_battery: this.productForm.value.item_has_battery.toString(),
      cause_detail: JSON.stringify(this.brokens),
      estimate_price: this.causeForm.value.estimate_price,
      cause_remark: this.causeForm.value.cause_remark,
      repair_detail: JSON.stringify(this.fixs),
      repair_price: this.causeForm.value.repair_price,
      repair_remark: this.causeForm.value.repair_remark,
      agreement_detail: JSON.stringify(this.agreements),
      agreement_remark: this.causeForm.value.agreement_remark,
      staff_name: this.employeeForm.value.staff_name
    }
    console.log('changeData', changeData);

    try {
      let res_change_product = await this.allService.post('repairTransection', changeData);
      this.showSuccess('เพิ่มข้อมูลเรียบร้อย');
      this.infoForm.reset;
      this.productForm.reset;
      this.causeForm.reset;
      this.employeeForm.reset;
      console.log('res_change_product', res_change_product);
      this.showDetail(res_change_product.data.id);
      this.isLoading = false;
    } catch (error) {
      console.log('error add customer', error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.saveData();
      } else if (error.status === 500) {
        this.showError(`ไม่สามารถเพิ่มได้ กรุณาลองใหม่อีกครั้ง ${JSON.parse(error._body).message}`);
        this.isLoading = false;
      } else {
        this.showError(`กรุณาลองใหม่อีกครั้ง ${JSON.parse(error._body).message}`);
        this.isLoading = false;
      }
    }
  }

  //--------------------------------------- Detail ---------------------------------------//
  showDetail(id) {
    console.log('id', id);
    this.router.navigate(['claim/fix/list/info', id]);
  }

}
