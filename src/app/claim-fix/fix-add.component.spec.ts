/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { FixAddComponent } from './fix-add.component';

describe('FixAddComponent', () => {
  let component: FixAddComponent;
  let fixture: ComponentFixture<FixAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FixAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FixAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
