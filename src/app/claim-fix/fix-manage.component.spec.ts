/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { FixManageComponent } from './fix-manage.component';

describe('FixManageComponent', () => {
  let component: FixManageComponent;
  let fixture: ComponentFixture<FixManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FixManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FixManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
