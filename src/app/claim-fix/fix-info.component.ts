import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Subscription } from 'rxjs/Subscription';
import { AllServiceService } from './../service/all-service.service';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { TypeaheadMatch } from 'ng2-bootstrap';
import { CustomerData } from '../tempData/customer-data';
import { SellProductData } from '../tempData/sell-product-data';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { HideMenuService } from '../shared/hide-menu.service';

import * as moment from 'moment';

import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/of';

@Component({
  selector: 'app-fix-info',
  templateUrl: './fix-info.component.html',
  styleUrls: ['./fix-info.component.scss']
})
export class FixInfoComponent implements OnInit {

  Heading = 'ข้อมูลการซ่อมสินค้า';

  public isLoading: Boolean;

  public repair_id;

  public rootUrl;
  public subGetParams: Subscription;

  public statusText = [
    {
      'id': '1',
      'message': 'รับเรื่อง',
      'class': 'text-blue'
    },
    {
      'id': '2',
      'message': 'กำลังซ่อม',
      'class': 'text-yellow'
    },
    {
      'id': '3',
      'message': 'สำเร็จ',
      'class': 'text-green'
    },
    {
      'id': '4',
      'message': 'ข้อผิดพลาด',
      'class': 'text-red'
    }
  ];

  public edit;

  public data;
  public data_original;

  //Typeahead
  public customers;
  public customerSelected;
  public productSelected: string = '';
  public dataSource: Observable<any>;
  public asyncSelected: string = '';
  public typeaheadLoading: boolean = false;
  public typeaheadNoResults: boolean = false;

  //Form
  public item_has_sim: FormControl;
  public item_has_mem: FormControl;
  public item_has_battery: FormControl;

  public productHasItem: FormGroup;


  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private allService: AllServiceService,
    public toastr: ToastsManager,
    public vcr: ViewContainerRef) {

    this.toastr.setRootViewContainerRef(vcr);

    router.events.subscribe((url: any) => {
      this.rootUrl = url;
    });

    this.setInitEdit();

    this.productHasItem = this.fb.group({
      'item_has_sim': this.item_has_sim,
      'item_has_mem': this.item_has_mem,
      'item_has_battery': this.item_has_battery,
    });

  }

  ngOnInit() {
    this.subGetParams = this.route.params.subscribe(
      params => {
        this.repair_id = params['id'];
      }
    );
    console.log('this.repair_id', this.repair_id);

    this.setInitData();
    this.getAllData();
  }

  setInitData() {
    this.data = {
      'code': '',
      'repair_date': '',
      'cause_detail': '',
      'broken': [],
      'customer_id': '',
      'remark': '',
      'shop_id': '',
      'source_product_id': '',
      'source_product': {},
      'staff_name': '',
      'target_product_id': '',
      'target_product': {},
      'tel': '',
      'user_id': '',
      'status': ''
    };
  }

  setInitEdit() {
    this.edit = {
      'info': false,
      'broken': false,
      'fix': false,
      'status': false,
      'product': false,
      'agreement': false,
    };
    this.customerSelected = {
      'name': '',
      'id': '',
      'tel': ''
    }
  }

  setEdit(type?: string) {
    this.edit[type] = !this.edit[type];
  }

  cancleEdit() {
    console.log('data_original', this.data_original);
    this.data = Object.assign({}, this.data_original);
    this.setInitEdit();
  }

  //--------------------------------------- get data ---------------------------------------//
  async getAllData() {
    this.isLoading = true;
    await this.loadDataToEdit();
    await this.loadClaimData();
    this.isLoading = false;
  }

  async loadClaimData() {
    try {
      let resp_repair = await this.allService.getWithId('repairTransection', this.repair_id);
      console.log('repairTransection', resp_repair);

      this.data_original = resp_repair.data;
      this.data_original.broken = JSON.parse(this.data_original.cause_detail);
      this.data_original.fix = JSON.parse(this.data_original.repair_detail);
      this.data_original.agreement = JSON.parse(this.data_original.agreement_detail);
      this.data_original.repair_date = this.setFormatDate(this.data_original.repair_date);
      this.data_original.dateFormat = this.setObjectDate(this.data_original.repair_date);

      this.customerSelected = this.data_original.customer;
      console.log('this.data_original', this.data_original.item_has_sim);

      this.data = Object.assign({}, this.data_original);
      console.log('this.data', this.data);

    } catch (error) {
      console.log('error get repair', error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.loadClaimData();
      } else {
        this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
        this.isLoading = false;
      }
    }
  }

  async loadDataToEdit() {
    try {
      let resp_customer = await this.allService.get('customer');
      let resp_product = await this.allService.get('product');

      this.customers = resp_customer.data;

    } catch (error) {
      console.log('error get loadDataToEdit', error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.loadDataToEdit();
      } else {
        this.showError('ไม่สามารถโหลดข้อมูลได้ กรุณาลองใหม่อีกครั้ง');
        this.isLoading = false;
      }
    }
  }

  //--------------------------------------- Date Format ---------------------------------------//
  setFormatDate(data) {
    if (typeof data === 'object') {
      return data.year + '-' + data.month + '-' + data.day
    } else if (typeof data === 'string') {
      return data
    }
  }

  setObjectDate(data) {
    let date = {
      'year': parseInt(moment(data).format('YYYY')),
      'month': parseInt(moment(data).format('MM')),
      'day': parseInt(moment(data).format('DD')),
    };
    console.log('date', date);
    return date;
  }

  //--------------------------------------- Dynamic Form ---------------------------------------//
  openPagePrint() {
    let gotourl = this.rootUrl.url + '/print';
    console.log('openPagePrint', this.data);
    // this.router.navigate([gotourl], { relativeTo: this.route });

    let navigationExtras: NavigationExtras;
    navigationExtras = {
      queryParams: {
        "id": this.repair_id
      }
    };

    window.open('/#' + gotourl + '?id=' + this.repair_id, "_blank");
    // this.router.navigate([gotourl], navigationExtras);

  }

  //--------------------------------------- Typeahead ---------------------------------------//
  public getStatesAsObservable(token: string): Observable<any> {
    let query = new RegExp(token, 'ig');

    return Observable.of(
      this.customers.filter((state: any) => {
        return query.test(state.name);
      })
    );
  }

  public changeTypeaheadLoading(e: boolean): void {
    this.typeaheadLoading = e;
  }

  public changeTypeaheadNoResults(e: boolean): void {
    this.typeaheadNoResults = e;
  }

  public typeaheadOnSelect(status, e: TypeaheadMatch): void {
    switch (status) {
      case 0:
        this.customerSelected = e.item;
        console.log('Selected value: ', e);
        break;
      case 1:
        this.productSelected = e.item;
        console.log('Selected value: ', e);
        break;
    }
  }

  //--------------------------------------- Edit Data ---------------------------------------//


  async editData() {
    this.isLoading = true;
    try {
      if (this.customerSelected) {
        this.data.customer_id = this.customerSelected.id;
      }

      this.data.cause_detail = JSON.stringify(this.data.broken);
      this.data.repair_detail = JSON.stringify(this.data.fix);
      this.data.agreement_detail = JSON.stringify(this.data.agreement);

      console.log('this.data edit', this.data);

      let resp_edit_data = await this.allService.putWithId('repairTransection', this.data.id, this.data);

      console.log('resp_edit_data', resp_edit_data);
      this.setInitEdit();
      this.getAllData();

    } catch (error) {
      console.log('error edit data', error);
      if (error.status === 401) {
        await this.allService.refreshToken();
        this.editData();
      }
    }
  }


  //--------------------------------------- Dynamic Form ---------------------------------------//
  addBrokens() {
    this.data.broken.push({ 'message': '' });
  }
  addFixs() {
    this.data.fix.push({ 'message': '' });
  }
  addAgreements() {
    this.data.agreement.push({ 'message': '' });
  }

  // --------------------------------------- Toast ---------------------------------------//
  showSuccess(message) {
    this.toastr.success(message, 'สำเร็จ!');
  }

  showError(message) {
    this.toastr.error(message, 'เกิดข้อผิดพลาด');
  }


}
