import { HideSidebarService } from './../shared/hide-sidebar.service';
import { Router } from '@angular/router';
import { AllServiceService } from './../service/all-service.service';
import { Component, OnInit } from '@angular/core';

import { HideMenuService } from '../shared/hide-menu.service';
import { SidebarComponent } from '../sidebar/sidebar.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public isHideMenu;

  public user;

  generatedNumber: number = 0;

  constructor(
    public hideMenuService: HideMenuService,
    public hideSidebarService: HideSidebarService,
    public sidebar: SidebarComponent,
    public allService: AllServiceService,
    private router: Router) {
    // this.getUser();
  }

  ngOnInit() {
    this.getUser();
    this.isHideMenu = false;
    this.hideMenuService.next(this.isHideMenu);
    console.log('this.isHideMenu init', this.isHideMenu);
  }

  //hide menu
  public hideMenu() {
    this.isHideMenu = !this.isHideMenu;
    this.hideMenuService.next(this.isHideMenu);
    console.log('this.isHideMenu function', this.isHideMenu);
    return this.isHideMenu;
  }

  logout() {
    this.allService.setToken(null);
    localStorage.clear();
    this.hideSidebarService.next(true);
    this.router.navigate(['/login']);
  }

  async getUser() {
    let userToken = this.allService.getLocal('user');
    if (userToken === null) {
      try {
        let response = await this.allService.get('auth/getAuthenticatedUser');
        this.user = response.data.user;
        this.allService.setLocal('user', this.user);
        console.log('this.user', this.user);
      } catch (error) {
        console.log('error getUser', error);
        if (error.status === 401) {
          await this.allService.refreshToken();
          this.getUser();
        }
      }
    } else {
      this.user = userToken
    }
  }

  goToReport(){
    this.user.shop_id === null ?  this.router.navigate(['/report/all/list']) :  this.router.navigate(['/report/list']);
  }

}
