import { HideSidebarService } from './shared/hide-sidebar.service';
import { AllServiceService } from './service/all-service.service';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { HideMenuService } from './shared/hide-menu.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app works!';
  public isHideMenu;
  public isHideSidebar: boolean;
  public rootUrl;

  constructor(public hideMenuService: HideMenuService, public hideSidebarService: HideSidebarService, private router: Router, public allService: AllServiceService) {

    if (this.allService.getToken()) {
      console.log('have token', allService.getToken());
      this.isHideSidebar = true;
      // router.navigate(['/']);
    } else {
      console.log('not have token', allService.getToken());
      this.isHideSidebar = false;
      router.navigate(['/login']);
    }

    router.events.subscribe((url: any) => {
      this.rootUrl = url.url;
      if (this.rootUrl === '/login') {
        this.isHideSidebar = false;
      } else {
        this.isHideSidebar = true;
      }
    });

    hideSidebarService.subscribe({
      next: isHideSidebar => {
        this.isHideSidebar = isHideSidebar;
        console.log('isHideMenu', this.isHideMenu);
        return this.isHideMenu;
      }
    });

    hideMenuService.subscribe({
      next: isHideMenu => {
        this.isHideMenu = isHideMenu;
        console.log('isHideMenu', this.isHideMenu);
        return this.isHideMenu;
      }
    });
  }


}
