import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { AllServiceService } from './../service/all-service.service';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import * as moment from 'moment';


@Component({
  selector: 'app-report-month',
  templateUrl: './report-month.component.html',
  styleUrls: ['./report-month.component.scss']
})
export class ReportMonthComponent implements OnInit {

  public Heading = 'รายงาน';

  public isLoading: Boolean = false;

  //data
  public data = null;

  //table
  public dtOptions: any = {};

  public reportDateForm: FormGroup;
  public date_start_form: FormControl;
  public date_end_form: FormControl;

  public today;
  public dateSelect = {
    'date_from': '',
    'date_to': ''
  }
  public date_start;
  public date_end;

  public constructor(
    public allService: AllServiceService,
    public toastr: ToastsManager,
    public vcr: ViewContainerRef,
    private fb: FormBuilder) {

    this.toastr.setRootViewContainerRef(vcr);

    this.isLoading = false;

    this.reportDateForm = fb.group({
      'date_start_form': this.date_start_form,
      'date_end_form': this.date_end_form
    });

  }

  ngOnInit() {
    this.loadService();
  }

  async loadService(){
    await this.setCurrentDate();
    await this.getReport('thisMonth');
  }

  //--------------------------------------- Date ---------------------------------------//
  async setCurrentDate() {
    console.log('current date', moment().format())
    let today = {
      'year': parseInt(moment().format('YYYY')),
      'month': parseInt(moment().format('MM')),
      'day': 1,
    }
    this.today = this.setFormatDate(today);
    this.date_start = Object.assign({}, today);
    this.date_end = Object.assign({}, today);
  }

  setFormatDate(data?) {
    if (typeof data === 'object') {
      return data.year + '-' + data.month + '-' + data.day
    } else if (typeof data === 'string') {
      return data
    }
  }

  async getReport(status) {
    // let dateSelect;
    switch (status) {
      case 'dateSelect':
        this.dateSelect = {
          'date_from': this.reportDateForm.value.date_start,
          'date_to': this.reportDateForm.value.date_end
        }
        break;
      case 'thisMonth':
        this.dateSelect = {
          'date_from': moment().startOf("month").format('YYYY-MM-DD'),
          'date_to': moment().endOf("month").format('YYYY-MM-DD')
        }
        break;
      case 'lastMonth':
        this.dateSelect = {
          'date_from': moment().subtract(1, 'months').startOf("month").format('YYYY-MM-DD'),
          'date_to': moment().endOf("month").format('YYYY-MM-DD')
        }
        break;
      case 'twoMonthAgo':
        this.dateSelect = {
          'date_from': moment().subtract(2, 'months').startOf("month").format('YYYY-MM-DD'),
          'date_to': moment().endOf("month").format('YYYY-MM-DD')
        }
        break;
    }
    console.log('dateSelect', this.dateSelect);
    this.date_start = this.setFormatDate(this.dateSelect.date_from);
    this.date_end = this.setFormatDate(this.dateSelect.date_to);
    this.getData(this.dateSelect)
  }

  async getData(data) {
    this.isLoading = !this.isLoading;
    console.log('loadData');
    this.data = data;

    try {
      this.data.shop_id = await this.allService.getLocal('user').shop_id;
      let res_get_summary_type = await this.allService.post('report/getSummary', this.data);
      this.data = res_get_summary_type.data;
      console.log('res_get_summary_type', this.data);
      this.isLoading = false;
    } catch (error) {
      console.log("Error get data prodict type!", error);
      if (error.status === 404) {
        this.isLoading = false;
      } else if (error.status === 401) {
        await this.allService.refreshToken();
        this.getData(this.data);
      } else {
        this.isLoading = false;
      }
    }
  }

}
