import { NavigationExtras, Router } from '@angular/router';
import { async } from '@angular/core/testing';
import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { AllServiceService } from '../service/all-service.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import * as moment from 'moment';
import { IMyOptions, IMyDateRangeModel } from 'mydaterangepicker';

@Component({
  selector: 'app-report-all',
  templateUrl: './report-all.component.html',
  styleUrls: ['./report-all.component.scss']
})
export class ReportAllComponent implements OnInit {

  public Heading = 'รายงานทั้งหมด';

  public isLoading: Boolean = false;

  //data
  public data = null;
  public dateSelect;
  public today;
  public date_start;
  public date_end;

  //table
  public dtOptions: any = {};


  public constructor(
    public allService: AllServiceService,
    public toastr: ToastsManager,
    public vcr: ViewContainerRef,
    public router: Router) {

    this.toastr.setRootViewContainerRef(vcr);

    this.isLoading = false;

  }

  ngOnInit() {
    this.initTable();
    this.loadService();
  }

  async loadService() {
    await this.setCurrentDate();
    await this.setDataToGet('today');
  }

  initTable() {
    this.dtOptions = {
      displayLength: 10,
      paginationType: 'simple_numbers',
      language: {
        info: 'แสดง _START_ ถึง _END_ จาก _TOTAL_',
        paginate: {
          previous: '<',
          next: '>',
        },
        search: 'ค้นหา',
        emptyTable: 'ไม่มีข้อมูล',
        infoEmpty: 'แสดง 0 ถึง 0 จาก 0',
        zeroRecords: 'ไม่มีข้อมูล',
        lengthMenu: 'แสดง _MENU_ แถว'
      },
      ordering: false,
      searching: false,
      lengthChange: false
    };
  }

  async setCurrentDate() {
    console.log('current date', moment().format())
    let today = {
      'year': parseInt(moment().format('YYYY')),
      'month': parseInt(moment().format('MM')),
      'day': parseInt(moment().format('DD')),
    }
    this.today = this.setFormatDate(today);
    this.date_start = Object.assign({}, today);
    this.date_end = Object.assign({}, today);
  }

  setFormatDate(data?) {
    if (typeof data === 'object') {
      return data.year + '-' + data.month + '-' + data.day
    } else if (typeof data === 'string') {
      return data
    }
  }

  async setDataToGet(status) {
    switch (status) {
      case 'thisMonth':
        this.dateSelect = {
          'date_from': moment().startOf("month").format('YYYY-MM-DD'),
          'date_to': moment().endOf("month").format('YYYY-MM-DD')
        }
        break;
      case 'lastMonth':
        this.dateSelect = {
          'date_from': moment().subtract(1, 'months').startOf("month").format('YYYY-MM-DD'),
          'date_to': moment().endOf("month").format('YYYY-MM-DD')
        }
        break;
      case 'twoMonthAgo':
        this.dateSelect = {
          'date_from': moment().subtract(2, 'months').startOf("month").format('YYYY-MM-DD'),
          'date_to': moment().endOf("month").format('YYYY-MM-DD')
        }
        break;
      case 'today':
        this.dateSelect = {
          'date_from': this.today,
          'date_to': this.today
        }
        break;
      case 'yesterday':
        this.dateSelect = {
          'date_from': moment().subtract(1, 'days').format('YYYY-MM-DD'),
          'date_to': this.today
        }
        break;
      case 'threeDayAgo':
        this.dateSelect = {
          'date_from': moment().subtract(2, 'days').format('YYYY-MM-DD'),
          'date_to': this.today
        }
        break;
      case 'fiveDayAgo':
        this.dateSelect = {
          'date_from': moment().subtract(4, 'days').format('YYYY-MM-DD'),
          'date_to': this.today
        }
        break;
      case 'sevenDayAgo':
        this.dateSelect = {
          'date_from': moment().subtract(6, 'days').format('YYYY-MM-DD'),
          'date_to': this.today
        }
        break;
    }
    console.log('dateSelect', this.dateSelect);
    this.date_start = this.setFormatDate(this.dateSelect.date_from);
    this.date_end = this.setFormatDate(this.dateSelect.date_to);
    this.getData();
  }


  async getData() {
    this.isLoading = !this.isLoading;
    console.log('loadData', this.dateSelect);
    try {
      let res_get_summary_all_type = await this.allService.post('report/getSummary', this.dateSelect);

      this.data = res_get_summary_all_type.data;

      console.log('data', this.data);

      this.isLoading = false;
    } catch (error) {
      console.log("Error get data prodict type!", error);
      if (error.status === 404) {
        this.isLoading = false;
      } else if (error.status === 401) {
        await this.allService.refreshToken();
        this.getData();
      } else {
        this.isLoading = false;
      }
    }
  }

  goToDetail(item) {
    let navigationExtras: NavigationExtras = {
      queryParams: { 'shop_id': item.id, 'shop_name': item.name }
    };

    // Redirect the user
    this.router.navigate(['/report/all/list/info'], navigationExtras);
  }

  // --------------------------------------- date ---------------------------------------//
  onDateRangeChanged(event: IMyDateRangeModel) {
    this.dateSelect = {
      'date_from': this.setFormatDate(event.beginDate),
      'date_to': this.setFormatDate(event.endDate)
    }
    this.getData();
  }

}
