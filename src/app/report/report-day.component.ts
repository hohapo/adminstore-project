import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { AllServiceService } from './../service/all-service.service';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-report-day',
  templateUrl: './report-day.component.html',
  styleUrls: ['./report-day.component.scss']
})
export class ReportDayComponent implements OnInit {

  public Heading = 'รายงาน';

  public isLoading: Boolean = false;

  //data
  public data = null;

  //table
  public dtOptions: any = {};

  public reportDateForm: FormGroup;
  public date_start_form: FormControl;
  public date_end_form: FormControl;

  public today;
  public dateSelect = {
    'date_from': '',
    'date_to': ''
  }
  public date_start;
  public date_end;

  public constructor(
    public allService: AllServiceService,
    public toastr: ToastsManager,
    public vcr: ViewContainerRef,
    private fb: FormBuilder) {

    this.toastr.setRootViewContainerRef(vcr);

    this.isLoading = false;

    this.reportDateForm = fb.group({
      'date_start_form': this.date_start_form,
      'date_end_form': this.date_end_form
    });

  }

  ngOnInit() {
    this.setCurrentDate();
    this.getReport('today');
  }

  //--------------------------------------- Date ---------------------------------------//
  setCurrentDate() {
    console.log('current date', moment().format())
    let today = {
      'year': parseInt(moment().format('YYYY')),
      'month': parseInt(moment().format('MM')),
      'day': parseInt(moment().format('DD')),
    }
    this.today = this.setFormatDate(today);
    this.date_start = Object.assign({}, today);
    this.date_end = Object.assign({}, today);
  }

  setFormatDate(data?) {
    if (typeof data === 'object') {
      return data.year + '-' + data.month + '-' + data.day
    } else if (typeof data === 'string') {
      return data
    }
  }

  getReport(status) {
    // let dateSelect;
    switch (status) {
      case 'dateSelect':
        this.dateSelect = {
          'date_from': this.reportDateForm.value.date_start,
          'date_to': this.reportDateForm.value.date_end
        }
        break;
      case 'today':
        this.dateSelect = {
          'date_from': this.today,
          'date_to': this.today
        }
        break;
      case 'yesterday':
        this.dateSelect = {
          'date_from': moment().subtract(1, 'days').format('YYYY-MM-DD'),
          'date_to': this.today
        }
        break;
      case 'threeDayAgo':
        this.dateSelect = {
          'date_from': moment().subtract(2, 'days').format('YYYY-MM-DD'),
          'date_to': this.today
        }
        break;
      case 'fiveDayAgo':
        this.dateSelect = {
          'date_from': moment().subtract(4, 'days').format('YYYY-MM-DD'),
          'date_to': this.today
        }
        break;
      case 'sevenDayAgo':
        this.dateSelect = {
          'date_from': moment().subtract(6, 'days').format('YYYY-MM-DD'),
          'date_to': this.today
        }
        break;
    }
    console.log('this.dateSelect', this.dateSelect);
    this.date_start = this.setFormatDate(this.dateSelect.date_from);
    this.date_end = this.setFormatDate(this.dateSelect.date_to);
    this.getData(this.dateSelect)
  }

  async getData(data) {
    this.isLoading = !this.isLoading;
    console.log('loadData');
    this.data = data;
    let temp_data = this.data;
    try {
      this.data.shop_id = await this.allService.getLocal('user').shop_id;
      let res_get_summary_type = await this.allService.post('report/getSummary', this.data);
      this.data = res_get_summary_type.data;
      console.log('res_get_summary_type', this.data);
      this.isLoading = false;
    } catch (error) {
      console.log("Error get data prodict type!", error);
      if (error.status === 404) {
        this.isLoading = false;
      } else if (error.status === 401) {
        await this.allService.refreshToken();
        this.getData(temp_data);
      } else {
        this.isLoading = false;
      }
    }
  }

}
