import { Observable } from 'rxjs/Observable';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { AllServiceService } from './../service/all-service.service';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { IMyOptions, IMyDateRangeModel } from 'mydaterangepicker';

import 'rxjs/add/operator/map';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {

  public Heading = 'รายงาน';

  public isLoading: Boolean = false;

  //data
  public dataDay = null;
  public dataMonth = null;

  //table
  public dtOptions: any = {};

  public today;
  public date_form: Object;
  public month_form = {
    'start_m': null,
    'start_y': null,
    'end_m': null,
    'end_y': null
  };

  public dateSelect;
  public monthSelect;
  public date_start;
  public date_end;

  public shop_id;
  public shop_name;

  private myDateRangePickerOptions: IMyOptions = {
    dateFormat: 'dd/mm/yyyy',
    firstDayOfWeek: 'su',
    selectionTxtFontSize: '16px',
    inputValueRequired: true,
    clearBtnTxt: 'ล้าง',
    beginDateBtnTxt: 'วันเริ่มต้น',
    endDateBtnTxt: 'วันสิ้นสุด',
    acceptBtnTxt: 'ตกลง',
    selectBeginDateTxt: 'เลือกวันที่เริ่มต้น',
    selectEndDateTxt: 'เลือกวันที่สิ้นสุด',
    editableDateRangeField: false
  };

  public constructor(
    public allService: AllServiceService,
    public toastr: ToastsManager,
    public vcr: ViewContainerRef,
    private fb: FormBuilder,
    private route: ActivatedRoute) {

    this.toastr.setRootViewContainerRef(vcr);

    this.isLoading = false;

    route.queryParams.subscribe(params => {
      this.shop_id = params["shop_id"];
      this.shop_name = params["shop_name"];
      this.loadService();
      console.log('this.shop_id', this.shop_id);
    });

  }

  ngOnInit() {
    this.initTable();
    this.setInitData();
  }

  initTable() {
    this.dtOptions = {
      displayLength: 10,
      paginationType: 'simple_numbers',
      language: {
        info: 'แสดง _START_ ถึง _END_ จาก _TOTAL_',
        paginate: {
          previous: '<',
          next: '>',
        },
        search: 'ค้นหา',
        emptyTable: 'ไม่มีข้อมูล',
        infoEmpty: 'แสดง 0 ถึง 0 จาก 0',
        zeroRecords: 'ไม่มีข้อมูล',
        lengthMenu: 'แสดง _MENU_ แถว'
      },
      ordering: false,
      searching: false,
      lengthChange: false
    };
  }

  setInitData() {
    this.dateSelect = {
      'date_from': '',
      'date_to': '',
      'shop_id': ''
    };
    this.monthSelect = {
      'date_from': '',
      'date_to': '',
      'shop_id': ''
    };
  }

  async loadService() {
    this.isLoading = true;
    await this.setCurrentDate();
    await this.getReportDay('today');
    // await this.getReportMonth('thisMonth');
    this.isLoading = false;
  }


  //--------------------------------------- Date ---------------------------------------//
  async setCurrentDate() {
    console.log('current date', moment().format())
    let today = {
      'year': parseInt(moment().format('YYYY')),
      'month': parseInt(moment().format('MM')),
      'day': parseInt(moment().format('DD')),
    }
    this.today = this.setFormatDate(today);

    this.month_form = {
      'start_m': parseInt(moment().format('MM')),
      'start_y': parseInt(moment().format('YYYY')),
      'end_m': parseInt(moment().format('MM')),
      'end_y': parseInt(moment().format('YYYY'))
    };

  }

  setFormatDate(data?) {
    if (typeof data === 'object') {
      return data.year + '-' + data.month + '-' + data.day
    } else if (typeof data === 'string') {
      return data
    }
  }

  async getReportDay(status, days?: number) {
    // let dateSelect;
    switch (status) {
      case 'today':
        this.dateSelect = {
          'date_from': this.today,
          'date_to': this.today
        }
        break;
      case 'yesterday':
        this.dateSelect = {
          'date_from': moment().subtract(1, 'days').format('YYYY-MM-DD'),
          'date_to': this.today
        }
        break;
      case 'ago':
        this.dateSelect = {
          'date_from': moment().subtract((days - 1), 'days').format('YYYY-MM-DD'),
          'date_to': this.today
        }
        break;
    }
    console.log('this.dateSelect', this.dateSelect);
    this.date_start = this.setFormatDate(this.dateSelect.date_from);
    this.date_end = this.setFormatDate(this.dateSelect.date_to);
    this.getDayData()
  }

  async getReportMonth(status) {
    // let dateSelect;
    switch (status) {
      case 'thisMonth':
        this.monthSelect = {
          'date_from': moment().startOf("month").format('YYYY-MM-DD'),
          'date_to': moment().endOf("month").format('YYYY-MM-DD')
        }
        break;
      case 'lastMonth':
        this.monthSelect = {
          'date_from': moment().subtract(1, 'months').startOf("month").format('YYYY-MM-DD'),
          'date_to': moment().endOf("month").format('YYYY-MM-DD')
        }
        break;
      case 'twoMonthAgo':
        this.monthSelect = {
          'date_from': moment().subtract(2, 'months').startOf("month").format('YYYY-MM-DD'),
          'date_to': moment().endOf("month").format('YYYY-MM-DD')
        }
        break;
    }
    console.log('monthSelect', this.monthSelect);
    this.date_start = this.setFormatDate(this.monthSelect.date_from);
    this.date_end = this.setFormatDate(this.monthSelect.date_to);
    this.getMonthData();
  }


  async getDayData() {
    this.isLoading = true;
    console.log('loadData');
    try {
      if (this.shop_id) {
        this.dateSelect.shop_id = this.shop_id;
      } else {
        this.dateSelect.shop_id = await this.allService.getLocal('user').shop_id;
      }

      let res_get_summary_day_type = await this.allService.post('report/getSummary', this.dateSelect);

      this.dataDay = res_get_summary_day_type.data;

      console.log('dataDay', this.dataDay);

      this.isLoading = false;
    } catch (error) {
      console.log("Error get data prodict type!", error);
      if (error.status === 404) {
        this.isLoading = false;
      } else if (error.status === 401) {
        await this.allService.refreshToken();
        this.getDayData();
      } else {
        this.isLoading = false;
      }
    }
  }

  async getMonthData() {
    this.isLoading = true;
    console.log('loadData');
    try {
      if (this.shop_id) {
        this.monthSelect.shop_id = this.shop_id;
      } else {
        this.monthSelect.shop_id = await this.allService.getLocal('user').shop_id;
      }

      let res_get_summary_month_type = await this.allService.post('report/getSummary', this.monthSelect);

      this.dataMonth = res_get_summary_month_type.data;

      console.log('dataMonth', this.dataMonth);

      this.isLoading = false;
    } catch (error) {
      console.log("Error get data prodict type!", error);
      if (error.status === 404) {
        this.isLoading = false;
      } else if (error.status === 401) {
        await this.allService.refreshToken();
        this.getMonthData();
      } else {
        this.isLoading = false;
      }
    }
  }

  // --------------------------------------- date ---------------------------------------//
  onDateRangeChanged(event: IMyDateRangeModel) {
    console.log('onDateRangeChanged(): event: ', event);
    console.log('onDateRangeChanged(): Begin date: ', event.beginDate, ' End date: ', event.endDate);
    console.log('onDateRangeChanged(): Formatted: ', event.formatted);
    console.log('onDateRangeChanged(): BeginEpoc timestamp: ', event.beginEpoc, ' - endEpoc timestamp: ', event.endEpoc);
    this.dateSelect = {
      'date_from': this.setFormatDate(event.beginDate),
      'date_to': this.setFormatDate(event.endDate)
    }
    this.getDayData();
  }

  onMonthChange(event, status) {
    console.log('onMonthChange', event);
    console.log('onMonthChange status', status);
    switch (status) {
      case 'start':
        this.month_form.start_m = event;
        break;
      case 'end':
        this.month_form.end_m = event;
        break;
    }
    console.log('this.month_form', this.month_form);
  }

  onYearChange(event, status) {
    console.log('onYearChange', event);
    console.log('onYearChange status', status);
    switch (status) {
      case 'start':
        this.month_form.start_y = event;
        break;
      case 'end':
        this.month_form.end_y = event;
        break;
    }

    console.log('this.month_form', this.month_form);
  }

  setMonthSelect() {

    let start = this.month_form.start_m + '/' + this.month_form.start_y;
    let end = this.month_form.end_m + '/' + this.month_form.end_y;

    if (this.month_form.start_y === this.month_form.end_y && this.month_form.start_m <= this.month_form.end_m) {
      this.monthSelect = {
        'date_from': moment(start, 'MM/YYYY').startOf("month").format('YYYY-MM-DD'),
        'date_to': moment(end, 'MM/YYYY').endOf("month").format('YYYY-MM-DD')
      }
      this.getMonthData();
    } else if (this.month_form.start_y < this.month_form.end_y) {
      this.monthSelect = {
        'date_from': moment(start, 'MM/YYYY').startOf("month").format('YYYY-MM-DD'),
        'date_to': moment(end, 'MM/YYYY').endOf("month").format('YYYY-MM-DD')
      }
      this.getMonthData();
    } else {
      console.log('errror set date');
      this.showError('กรุณาเลือกเดือนเริ่มต้น น้อยกว่า เดือนสิ้นสุด');
    }
    console.log('this.monthSelect', this.monthSelect);
  }

  // --------------------------------------- Toast ---------------------------------------//
  showSuccess(message) {
    this.toastr.success(message, 'สำเร็จ!');
  }

  showError(message) {
    this.toastr.error(message, 'เกิดข้อผิดพลาด');
  }


}
