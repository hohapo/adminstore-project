import { AllServiceService } from './../service/all-service.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { URLSearchParams } from '@angular/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public isLoading: Boolean = false;

  public userForm: FormGroup;
  public username: FormControl;
  public password: FormControl;

  public message: String;

  constructor(private router: Router, private fb: FormBuilder, public allService: AllServiceService) {

    allService.checkLogin();

    //Validate Form
    this.username = fb.control('', Validators.compose([Validators.required]));
    this.password = fb.control('', Validators.compose([Validators.required]));
    this.userForm = fb.group(
      {
        'username': this.username,
        'password': this.password
      }
    );

  }

  ngOnInit() {
  }

  async sendForm() {
    console.log('data', this.userForm.value);
    this.isLoading = !this.isLoading;

    try {
      let res_login = await this.allService.post('auth/authenticate', this.userForm.value);
      await this.allService.setToken(res_login.data.token);
      console.log('res_login', res_login.data);
      
      let res_user = await this.allService.get('auth/getAuthenticatedUser');
      console.log('this.user', res_user.data.user);
      await this.allService.setLocal('user', res_user.data.user);

      let user = await this.allService.getLocal('user');

      if (user.shop_id === null) {
        this.router.navigate(['/report/all/list']);
      } else {
        this.router.navigate(['/report/list']);
      }

      this.isLoading = !this.isLoading;

    } catch (error) {
      console.log("Error login", error);
      this.message = 'ไม่สามารถเข้าสู่ระบบได้ กรุณาลองใหม่อีกครั้ง';
      this.isLoading = !this.isLoading;
    }
  }

}
